//
//  main.swift
//  SampleSDKSwift
//
//  Created by Arianne Peiso on 12/06/2019.
//  Copyright © 2019 Arianne Peiso. All rights reserved.
//

import Foundation
import ARGenieSDK

ARGenieExternalInitializer.initialize(CommandLine.argc, argv: CommandLine.unsafeArgv)

UIApplicationMain(CommandLine.argc, CommandLine.unsafeArgv, nil, NSStringFromClass(AppDelegate.self))


