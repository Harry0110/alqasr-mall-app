//
//  ApiMetadata.swift
//  MallAppSwift
//
//  Created by Freelancer on 18/06/19.
//  Copyright © 2019 Arianne Peiso. All rights reserved.
//

import Foundation
import UIKit

class APIMetadata{
    
    
    static let URL_BASE = "dev.magic-xperience.com"
    static let URL_GET_APP_TOKEN = "http://\(URL_BASE)/\(UserDefaults.standard.value(forKey: "DEFAULT_LANG")!)/wserv/wstoken/getapptoken"
    static let URL_AUTHENTICATE_USER = "http://\(URL_BASE)/\(UserDefaults.standard.value(forKey: "DEFAULT_LANG")!)/oauth2/token"
    
    static let URL_GET_USER_INFO = "http://\(URL_BASE)/\(UserDefaults.standard.value(forKey: "DEFAULT_LANG")!)/wservauth/wstoken/getuserinfo"
    static let URL_GET_ACCESS_TOKEN = "http://\(URL_BASE)/\(UserDefaults.standard.value(forKey: "DEFAULT_LANG")!)/oauth2/token"
    static let URL_UPDATE_USER_INFO = "http://\(URL_BASE)/\(UserDefaults.standard.value(forKey: "DEFAULT_LANG")!)/wservauth/wstoken/updateuser"
    static let URL_UPDATE_PASSWORD = "http://\(URL_BASE)/\(UserDefaults.standard.value(forKey: "DEFAULT_LANG")!)/wservauth/wstoken/updatepassword"
    static let  URL_CRETAE_USER = "http://\(URL_BASE)/\(UserDefaults.standard.value(forKey: "DEFAULT_LANG")!)/wserv/wstoken/createuser"
    static let URL_RESET_PASSWORD = "http://\(URL_BASE)/\(UserDefaults.standard.value(forKey: "DEFAULT_LANG")!)/wserv/wstoken/resetpassword"
    
}


class Constants{
    
    
    static let Email = "contact@magic-xperience.com"
    static let AppID = ""
    
    
}


enum networkAlert {
    static let networkmsg = "Please check your network connection"
}
