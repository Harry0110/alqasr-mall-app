//
//  UserService.swift
//  MallAppSwift
//
//  Created by Freelancer on 18/06/19.
//  Copyright © 2019 Arianne Peiso. All rights reserved.
//


import Foundation
import UIKit
import SystemConfiguration
import Alamofire
import Reachability

public class UserService:NSObject {
    
    func getaccesstoken(target:BaseViewController? = nil, complition:@escaping (NSDictionary?) ->Void){
        
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]
        
        let param = ["app_id":"alqasrid", "app_secret": "@lq@sr"] as [String : Any]
        
        let url = APIMetadata.URL_GET_APP_TOKEN
        
        print("this is the url:",url)
        
        
       Alamofire.request(APIMetadata.URL_GET_APP_TOKEN, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                print("success",data)
                complition(data as? NSDictionary)
                
            case.failure(let error):
                print("Not Success",error)
                complition(nil)
            }
            
        }
        
        
    }
    
    func authenticateUser(with grant_type:String,client_id:String, username:String,password:String,client_secret:String,target:BaseViewController? = nil , complition:@escaping (NSDictionary?) ->Void){
        
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]
        
        let param = ["grant_type":grant_type, "client_id":client_id , "username":username ,"password":password , "client_secret":client_secret ] as [String : Any]
        
        let url = APIMetadata.URL_AUTHENTICATE_USER
        
        print("this is the url:",url)
        
        Alamofire.request(url, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                print("success",data)
                complition(data as? NSDictionary)
                
            case.failure(let error):
                print("Not Success",error)
                complition(nil)
            }
            
        }
        
    }
    
    func createUser(with first_name:String,last_name:String, email:String,password:String,optin_newsletters:String,app_token:String,target:BaseViewController? = nil , complition:@escaping (NSDictionary?) ->Void){
        
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]


        let param = ["first_name":first_name, "last_name":last_name , "email":email ,"password":password , "optin_newsletters":optin_newsletters,"app_token":app_token] as [String : Any]
        
        let url = APIMetadata.URL_CRETAE_USER
        
        print("this is the url:",url)
        
        Alamofire.request(url, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                print("success",data)
                complition(data as? NSDictionary)
                
            case.failure(let error):
                print("Not Success",error)
                complition(nil)
            }
            
        }
        
    }
    
    
    func getuserinfo(with app_token:String,target:BaseViewController? = nil, complition:@escaping (NSDictionary?) ->Void){
        
        let headers = ["Content-Type": "application/x-www-form-urlencoded" ,  "Authorization": "Bearer \(UserDefaults.standard.value(forKey: "DeviceId")!)"]
        
        print(UserDefaults.standard.value(forKey: "App_Token")!)
        
        let param = ["app_token":app_token] as [String : Any]
        
        let url = APIMetadata.URL_GET_USER_INFO
        
        print("this is the url:",url)
        
        Alamofire.request(url, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                print("success",data)
                complition(data as? NSDictionary)
                
            case.failure(let error):
                print("Not Success",error)
                complition(nil)
            }
            
        }
        
    }
    
    
    
    func getUpdateuserinfo(with first_name:String,last_name:String,email:String,optin_newsletters:String,app_token:String,target:ProfileVC? = nil, complition:@escaping (NSDictionary?) ->Void){
        
        let headers = ["Content-Type": "application/x-www-form-urlencoded" ,  "Authorization": "Bearer \(UserDefaults.standard.value(forKey: "DeviceId")!)"]
        
        print(UserDefaults.standard.value(forKey: "App_Token")!)
        
        let param = ["first_name":first_name, "last_name": last_name, "email":email , "optin_newsletters": optin_newsletters, "app_token":app_token] as [String : Any]
        
        let url = APIMetadata.URL_UPDATE_USER_INFO
        
        print("this is the url:",url)
        
        Alamofire.request(url, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                print("success",data)
                complition(data as? NSDictionary)
                
            case.failure(let error):
                print("Not Success",error)
                complition(nil)
            }
            
        }
        
    }
    
    
    func UpdateuserPassword(with old_password:String,new_password:String,app_token:String,target:ProfileVC? = nil, complition:@escaping (NSDictionary?) ->Void){
        
        let headers = ["Content-Type": "application/x-www-form-urlencoded" ,  "Authorization": "Bearer \(UserDefaults.standard.value(forKey: "DeviceId")!)"]
        
        print(UserDefaults.standard.value(forKey: "App_Token")!)
        
        let param = ["old_password":old_password, "new_password": new_password, "app_token":app_token] as [String : Any]
        
        let url = APIMetadata.URL_UPDATE_PASSWORD
        
        print("this is the url:",url)
        
        Alamofire.request(url, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                print("success",data)
                complition(data as? NSDictionary)
                
            case.failure(let error):
                print("Not Success",error)
                complition(nil)
            }
            
        }
        
    }
    
    
  
    
    func getaccessTokenRevised(with grant_type:String,client_id:String,client_secret:String,refresh_token:String,target:BaseViewController? = nil , complition:@escaping (NSDictionary?) ->Void){
        
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]
        
        let param = ["grant_type":grant_type, "client_id":client_id, "client_secret":client_secret,"refresh_token":refresh_token] as [String : Any]
        
        let url = APIMetadata.URL_GET_ACCESS_TOKEN
        
        print("this is the url:",url)
        
        
        
        Alamofire.request(url, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                print("success",data)
                complition(data as? NSDictionary)
                
            case.failure(let error):
                print("Not Success",error)
                complition(nil)
            }
            
        }
        
    }
    
    func resetPassword(with email: String ,app_token:String, target:BaseViewController? = nil, complition:@escaping (NSDictionary?) ->Void){
        
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]
        
        let param = ["email":email, "app_token":app_token] as [String : Any]
        
        let url = APIMetadata.URL_RESET_PASSWORD
        
        print("this is the url:",url)
        
        
        Alamofire.request(url, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                print("success",data)
                complition(data as? NSDictionary)
                
            case.failure(let error):
                print("Not Success",error)
                complition(nil)
            }
            
        }
        
        
    }
    
    
    
//        func doUpDateProfile(with user_id:Int,name:String,email:String,mobile_num:String,user_img:String,os_type:String,target:BaseViewController? = nil, complition:@escaping (NSDictionary?) ->Void){
//
//            let param = ["user_id":String(user_id),"name":name,"email": email,"mobile_num":mobile_num,"user_img":user_img,"os_type":os_type] as [String : Any]
//            print(param)
//
//            let headers = ["Content-Type": "application/x-www-form-urlencoded"]
//            target?.showLoader()
//
//            Alamofire.request(APIMetadata.URL_UPDATE_PROFILE, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
//
//                switch(response.result) {
//                case.success(let data):
//                    print("success",data)
//                    complition(data as? NSDictionary)
//                case.failure(let error):
//                    print("Not Success",error)
//                    complition(nil)
//                }
//                target?.hideLoader()
//
//            }
//
//        }
        
    
    func ShowAlert(msg:String){
        
       
    }
    
    

    
}


public class NetworkConnection {
    
    class func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
        
    }
}


