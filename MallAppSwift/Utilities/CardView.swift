//
//  CardView.swift
//  MallAppSwift
//
//  Created by Freelancer on 21/06/19.
//  Copyright © 2019 Arianne Peiso. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class CardView: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 2
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 10
    @IBInspectable var shadowColor: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity: Float = 0
    
    override func layoutSubviews() {
        layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        
        layer.masksToBounds = false
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
    }
    
}

@IBDesignable
class CurvedButton: UIButton {
    
    let theme = ThemeClass()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let Object:NSDictionary = theme.getButtonView()
        
         let color = hexStringToUIColor(hex: Object.object(forKey: "BackGroundColor")! as! String)
        let color1 = hexStringToUIColor(hex: Object.object(forKey: "TextColor")! as! String)
        
        self.backgroundColor = color
        self.setTitleColor(color1, for: .normal)
        
//        self.backgroundColor = ColorConverter.hexStringToUIColor(hex: ColorCode.BackgroundViewColor)
//        self.setTitleColor(ColorConverter.hexStringToUIColor(hex: ColorCode.textLightwhiteColor), for: .normal)
        
        self.layer.cornerRadius = 0
         self.layer.borderWidth = 0
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
