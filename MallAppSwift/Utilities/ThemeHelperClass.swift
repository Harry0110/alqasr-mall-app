//
//  ThemeHelperClass.swift
//  MallAppSwift
//
//  Created by apple on 8/1/19.
//  Copyright © 2019 Arianne Peiso. All rights reserved.
//

import Foundation


class ThemeClass {
    

    func getPathNameTheme() -> String {
        
        let path: String = Bundle.main.path(forResource: "Theme", ofType: "plist")!
        return path
    }
    
    
    func getConfigFile() -> NSDictionary {
        
        let ConfigFile: NSDictionary = NSDictionary(contentsOfFile: getPathNameTheme())!
     
        return ConfigFile
    }
    
    func getThemeObject() -> NSDictionary {
        
        let File = (getConfigFile().object(forKey: "Theme") as! NSDictionary)
        
        return File
    }
    
    func getCurrentObject() -> NSDictionary {
        
        let file = (getThemeObject().object(forKey: "Current") as! NSDictionary)
        
        return file
    }
    
    func getMainView() -> NSDictionary{
        
        let file = (getCurrentObject().object(forKey: "MainView") as! NSDictionary)
        
        return file
        
    }
    
    func getpalleteObject() -> NSDictionary{
        
        let File = (getConfigFile().object(forKey: "Palette") as! NSDictionary)

        return File
    }
    
    func getCustomObject() -> NSDictionary {
        
        let file = (getpalleteObject().object(forKey: "Custom") as! NSDictionary)
        
        return file
    }
    
    func getButtonView() -> NSDictionary {
        
        let file = (getCurrentObject().object(forKey: "ButtonView") as! NSDictionary)
        
        return file
    }
    
    func getEventsView() -> NSDictionary {
        
        let file = (getCurrentObject().object(forKey: "EventsView") as! NSDictionary)
        
        return file
    }
    func getNewsView() -> NSDictionary {
        
        let file = (getCurrentObject().object(forKey: "NewsView") as! NSDictionary)
        
        return file
    }
    func getBottomBar() -> NSDictionary {
        
        let file = (getCurrentObject().object(forKey: "BottomBar") as! NSDictionary)
        
        return file
    }
    
    func getLoginView() -> NSDictionary {
        let file = (getCurrentObject().object(forKey: "LoginView") as! NSDictionary)
        
        return file
    }
    
    func getRegisterView() -> NSDictionary {
        let file = (getCurrentObject().object(forKey: "Registration") as! NSDictionary)
        
        return file
    }
    
    func getUserAndLegal() -> NSDictionary {
        let file = (getCurrentObject().object(forKey: "UserAndLegal") as! NSDictionary)
        
        return file
    }
    
    func getPopUpView() -> NSDictionary {
        let file = (getCurrentObject().object(forKey: "PopUpView") as! NSDictionary)
        
        return file
    }
    func getServicesView() -> NSDictionary {
        let file = (getCurrentObject().object(forKey: "ServicesView") as! NSDictionary)
        
        return file
    }
    func getProfileView() -> NSDictionary {
        let file = (getCurrentObject().object(forKey: "ProfileView") as! NSDictionary)
        
        return file
    }
    
    
}
