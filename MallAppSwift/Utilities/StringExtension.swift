//
//  StringExtension.swift
//  MallAppSwift
//
//  Created by Freelancer on 19/07/19.
//  Copyright © 2019 Arianne Peiso. All rights reserved.
//

import Foundation
import UIKit


extension String {
    
    func url() -> NSURL {
        
        let url:NSURL = NSURL(string:self)!
        return url
        
    }
    
    func getBlankUrl() -> NSURL {
        
        let url:NSURL = NSURL(string:self)!
        return url
        
    }
    
    func isPasswordValid() -> Bool {
        let passwordRegEx = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z!@#$%^&*?<>{}()~_\\d]{8,}$"
        
        //let passwordRegEx = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&#])[A-Za-z\\d$@$!%*?&#]{2,30}" //"^(?=.{8,}$)(?=.*\\d)(?=.*[a-zA-Z]).*$"
        // The password must contain at least 1 uppercase alphabet, 1 lowercase alphabet, 1 number & 8 characters long.
        
        //  @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&#])[A-Za-z\\d$@$!%*?&#]{8,30}";
        let password  = NSPredicate(format:"SELF MATCHES %@", passwordRegEx)
        return password.evaluate(with: self)
    }
    
    func validatePhoneNumber(value: String) -> Bool {
        let PHONE_REGEX = "^\\d{3}-\\d{3}-\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    var isValidEmail: Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with:self)
    }
   
    var length: Int {
        return self.characters.count
    }
    
    func message(_ title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .cancel) { (UIAlertAction) in }
        alert.addAction(okAction)
    }
    
    
    
}


//extension UIColor {
//    convenience init(red: Int, green: Int, blue: Int) {
//        assert(red >= 0 && red <= 255, "Invalid red component")
//        assert(green >= 0 && green <= 255, "Invalid green component")
//        assert(blue >= 0 && blue <= 255, "Invalid blue component")
//        
//        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
//    }
//    
//    convenience init(rgb: Int) {
//        self.init(
//            red: (rgb >> 16) & 0xFF,
//            green: (rgb >> 8) & 0xFF,
//            blue: rgb & 0xFF
//        )
//    }
//}
