//
//  Theme.h
//  magicxperience
//
//  Created by erka on 28/08/2018.
//  Copyright © 2018 Artech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ThemeProp.h"
#import "ThemePropText.h"
#import "CommonsUtils.h"

#define THEME_PRIMARY_COLOR @"Primary"
#define THEME_PRIMARYCLEAR_COLOR @"PrimaryClear"
#define THEME_SECUNDARY_COLOR @"Secundary"
#define THEME_AlTERNATIVE_COLOR @"Alternative"
#define THEME_HIGHLIGHT_COLOR @"Highlight"
#define THEME_TEXT_COLOR @"Text"


@interface Theme : NSObject

@property (nonatomic, readonly) ThemeProp* toolbarBackground;
@property (nonatomic, readonly) ThemeProp* toolbarUnderline;
@property (nonatomic, readonly) ThemePropText* toolbarText;
@property (nonatomic, readonly) ThemeProp* toolbarButtonBackground;



@property (nonatomic, readonly) ThemeProp* menuButtonBackground;
@property (nonatomic, readonly) ThemeProp* menuIconBackground;
@property (nonatomic, readonly) ThemePropText* menuToolbarText;
@property (nonatomic, readonly) ThemeProp* menuBackground;
@property (nonatomic, readonly) ThemePropText* menuText;
@property (nonatomic, readonly) ThemeProp* menuSeparatorBackground;
@property (nonatomic, readonly) ThemeProp* menuBagBackground;
@property (nonatomic, readonly) ThemePropText* menuBagText;

@property (nonatomic, readonly) ThemeProp* faqCollapsedHeaderBackground;
@property (nonatomic, readonly) ThemePropText* faqCollapsedHeaderText;
@property (nonatomic, readonly) ThemeProp* faqExpandedHeaderBackground;
@property (nonatomic, readonly) ThemePropText* faqExpandedHeaderText;
@property (nonatomic, readonly) ThemeProp* faqExpandedDetailBackground;
@property (nonatomic, readonly) ThemePropText* faqExpandedDetailText;
@property (nonatomic, readonly) ThemePropText* faqExpandedDetailHighlightText;

@property (nonatomic, readonly) ThemeProp* appBodyBackground;
@property (nonatomic, readonly) ThemePropText* sectionTitleText;
@property (nonatomic, readonly) ThemePropText* sectionText;
@property (nonatomic, readonly) ThemeProp* sectionButtonBackground;
@property (nonatomic, readonly) ThemePropText* sectionButtonText;
@property (nonatomic, readonly) ThemePropText* sectionHighlightText;

@property (nonatomic, readonly) ThemePropText* popupTitleText;
@property (nonatomic, readonly) ThemePropText* popupText;
@property (nonatomic, readonly) ThemePropText* popupActionText;
@property (nonatomic, readonly) ThemeProp* popupButtonOKBackground;
@property (nonatomic, readonly) ThemePropText* popupButtonOKText;
@property (nonatomic, readonly) ThemeProp* popupButtonCancelBackground;
@property (nonatomic, readonly) ThemePropText* popupButtonCancelText;
@property (nonatomic, readonly) ThemeProp* popupBorderBackground;

@property (nonatomic, readonly) ThemeProp* filterContainerBackground;
@property (nonatomic, readonly) ThemeProp* filterBackground;
@property (nonatomic, readonly) ThemePropText* filterText;


@property (nonatomic, readonly) ThemeProp* mapMarkerCluster20Background;
@property (nonatomic, readonly) ThemeProp* mapMarkerCluster50Background;
@property (nonatomic, readonly) ThemeProp* mapMarkerCluster100Background;
@property (nonatomic, readonly) ThemeProp* mapMarkerCluster200Background;
@property (nonatomic, readonly) ThemeProp* mapMarkerCluster500Background;
@property (nonatomic, readonly) ThemeProp* mapMarkerCluster1000Background;
@property (nonatomic, readonly) ThemeProp* mapMarkerClusterDefaultBackground;

@property (nonatomic, readonly) ThemeProp* newsSeparatorBackground;
@property (nonatomic, readonly) ThemeProp* newsIconBackground;
@property (nonatomic, readonly) ThemePropText* newsSumaryTitleText;
@property (nonatomic, readonly) ThemePropText* newsSumaryDescriptionText;
@property (nonatomic, readonly) ThemePropText* newsSumaryDateText;

@property (nonatomic, readonly) ThemeProp* parameterSeparatorBackground;

@property (nonatomic, readonly) ThemeProp* activityIndicatorBackground;


+ (Theme *)sharedInstance;
- (BOOL) setActiveThemeByName: (NSString*) aName;

- (UIFont*) fontOfSize: (int) aSize;
- (UIFont*) fontBoldOfSize: (int) aSize;
//- (UIColor*) getThemeColorByName: (NSString*) aPropertyName;
//- (NSString*) getThemeColorStringByName: (NSString*) aPropertyName;


@end
