//
//  ThemeProp.m
//  magicxperience
//
//  Created by erka on 30/09/2018.
//  Copyright © 2018 Artech. All rights reserved.
//

#import "ThemeProp.h"

#define UIColorFromRGBA(rgbValue, alphaValue) ([UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16)) / 255.0 \
green:((float)((rgbValue & 0xFF00) >> 8)) / 255.0 \
blue:((float)(rgbValue & 0xFF)) / 255.0 \
alpha:alphaValue])
#define UIColorFromRGB(rgbValue) (UIColorFromRGBA((rgbValue), 1.0))


@implementation ThemeProp

-(instancetype) initWithColor: (NSString*) aColor {
    self = [super init];
    if (self) {
        _color = [ThemeProp colorFromHexString:aColor];
        _stringColor = aColor;
    }
    return self;
}

+ (UIColor *) colorFromHexString:(NSString *) hexString
{
    if (!hexString) {
        return nil;
    }
    
    hexString = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    unsigned hex;
    BOOL success = [scanner scanHexInt:&hex];
    
    if (!success) return nil;
    if ([hexString length] <= 6)
        return UIColorFromRGB(hex);
    else {
        unsigned color = (hex & 0x00FFFFFF);
        CGFloat alpha = ((hex & 0xFF000000) >> 24) / 255.0; //1.0 *
        return UIColorFromRGBA(color, alpha);
    }
}


@end
