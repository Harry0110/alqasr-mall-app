//
//  MapBehaviourConfiguration.h
//  magicxperience
//
//  Created by erka on 07/10/2018.
//  Copyright © 2018 Artech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MapBehaviourConfiguration : NSObject

@property(nonatomic, assign) BOOL centerOnCoordinate;
@property(nonatomic, assign) long latitude;
@property(nonatomic, assign) long longitude;
@property(nonatomic, assign) int latitudinalMeters;
@property(nonatomic, assign) int longitudinalMeters;

-(MapBehaviourConfiguration*) initWithDictionary: (NSDictionary*) adictionary;

@end
