//
//  ThemePropText.m
//  magicxperience
//
//  Created by erka on 30/09/2018.
//  Copyright © 2018 Artech. All rights reserved.
//

#import "ThemePropText.h"

@implementation ThemePropText

-(instancetype) initWithColor: (NSString*) aColor andFont: (UIFont*) aFont {
    self = [super initWithColor:aColor];
    if (self) {
        _font = aFont;
    }
    return self;
}

@end
