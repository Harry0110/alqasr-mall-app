//
//  Theme.m
//  magicxperience
//
//  Created by erka on 28/08/2018.
//  Copyright © 2018 Artech. All rights reserved.
//

#import "Theme.h"
#import "ConfigurationManager.h"

#define APP_RESOURCES_THEME_FILE @"Theme"
#define APP_RESOURCES_THEME_KEY @"Theme"
#define APP_RESOURCES_THEME_ACTIVE_KEY @"Active"

@interface Theme ()
- (NSString*) getThemePath;
- (NSString*) getThemeDirectoryPath;
- (NSDictionary *) getBundleThemeConfigurationFile;
- (NSDictionary *) getThemeConfigurationFile;
- (BOOL) existValidThemeInConfigurationFile: (NSDictionary *) aConfigurationFile;
- (NSDictionary *) findThemeInConfiguration: (NSDictionary *) aConfiguration themeName:(NSString*) athemeName;
- (BOOL) saveThemeWithConfiguration: (NSDictionary *) aThemeConfiguration;
- (NSDictionary*) getActiveTheme;
- (void) loadThemeProperties;

@end

@implementation Theme

+ (Theme *)sharedInstance {
    static Theme *sharedInstance = nil;
    
    if (sharedInstance == nil) {
        sharedInstance = [[Theme alloc] init];
    }
    
    return sharedInstance;
}

- (NSString*) getThemePath {
    NSString* themeDirPath = [self getThemeDirectoryPath];
    NSString* themePath = [themeDirPath stringByAppendingPathComponent:@"Theme.plist"];
    return themePath;
}

- (NSString*) getThemeDirectoryPath {
    
    NSURL* themeDirPath  = [[[NSFileManager defaultManager] URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask] firstObject];
    
    NSString* appBundleID = [[NSBundle mainBundle] bundleIdentifier];
    themeDirPath = [themeDirPath URLByAppendingPathComponent:appBundleID];
    
    return [themeDirPath path];
}

- (NSDictionary *) getBundleThemeConfigurationFile {
    return [self loadResourceByConfigurationFileName:APP_RESOURCES_THEME_FILE];
}

- (NSDictionary*) loadResourceByConfigurationFileName:(NSString*) aConfigurationFileName {
    NSString* _path = [[NSBundle mainBundle] pathForResource:aConfigurationFileName ofType:@"plist"];
    NSMutableDictionary* _resourcesFile = [NSMutableDictionary dictionaryWithContentsOfFile:_path];
    return _resourcesFile;
}

- (NSDictionary *) getThemeConfigurationFile {
    
    NSDictionary* themeConfig = nil;
    NSString* themePath = [self getThemePath];
    
    if(![[NSFileManager defaultManager] fileExistsAtPath:themePath]){
        NSDictionary * themeConfigBundle =[self getBundleThemeConfigurationFile];
        [self saveThemeWithConfiguration:themeConfigBundle];
        themeConfig = themeConfigBundle;
    } else {
        themeConfig = [NSMutableDictionary dictionaryWithContentsOfFile: themePath];
    }
    
    return themeConfig;
}

- (BOOL) existValidThemeInConfigurationFile: (NSDictionary*) aConfigurationFile {
    NSDictionary* resourceThemeDic = aConfigurationFile;
    
    if(resourceThemeDic == nil) {
        NSLog(@"Resource configuration file Theme.plist do not exist.");
        return false;
    }
    
    id themes = [resourceThemeDic objectForKey:APP_RESOURCES_THEME_KEY];
    if(!themes){
        NSLog(@"Invalid configuration file: Theme.plist do not contain a theme key definition.");
        return false;
    }
    
    id activeTheme = [themes objectForKey:APP_RESOURCES_THEME_ACTIVE_KEY];
    if((activeTheme == nil || ![activeTheme isKindOfClass:[NSString class]])){
        NSLog(@"Invalid configuration file: Theme.plist do not contain a Theme.Active key definition or not contain a NSString value.");
        return false;
    }
    
    if(!([themes count] > 1)){
        NSLog(@"Invalid configuration file: Theme.plist do not contain defined theme.");
        return false;
    }
    
    return true;
}

- (NSDictionary *) findThemeInConfiguration: (NSDictionary *) aConfiguration themeName:(NSString*) athemeName {
    id themes = [aConfiguration objectForKey:APP_RESOURCES_THEME_KEY];
    
    if(![themes isKindOfClass:[NSDictionary class]])
        return nil;
        
    return [themes objectForKey:athemeName];
}

- (BOOL) saveThemeWithConfiguration: (NSDictionary *) aThemeConfiguration {
    
    return true; // remove for production
    
    NSString *themeDirPath = [self getThemeDirectoryPath];
    NSString *themePath = [self getThemePath];
    
    if(![[NSFileManager defaultManager] fileExistsAtPath:themePath]){
        NSError *error = nil;
        [[NSFileManager defaultManager] createDirectoryAtPath:themeDirPath withIntermediateDirectories:true attributes:nil error:&error];
        NSLog(@"%@", error.localizedDescription);
    }
         
    BOOL success = [aThemeConfiguration writeToFile:themePath atomically:true];
    
    return success;
}

- (BOOL) setActiveThemeByName: (NSString*) aName {
    NSDictionary* resourceThemeDic = [self getThemeConfigurationFile];
    if(![self existValidThemeInConfigurationFile:resourceThemeDic]){
        return false;
    }
    
    NSDictionary* theme = [self findThemeInConfiguration:resourceThemeDic themeName:aName];
    if(theme== nil)return false;
    
    NSDictionary* themes = [resourceThemeDic objectForKey:APP_RESOURCES_THEME_KEY];
    [themes setValue:aName forKey:APP_RESOURCES_THEME_ACTIVE_KEY];
    
    
    bool _savedActiveTheme = [self saveThemeWithConfiguration:resourceThemeDic];
    
    [self loadThemeProperties];
    
    return _savedActiveTheme;
}

- (NSDictionary*) getActiveTheme {
    NSDictionary* themeConfig = [self getThemeConfigurationFile];
    
    id themes = [themeConfig objectForKey:APP_RESOURCES_THEME_KEY];
    id activeTheme = [themes objectForKey:APP_RESOURCES_THEME_ACTIVE_KEY];
    
    return [self findThemeInConfiguration:themeConfig themeName:activeTheme];
}

- (NSDictionary*) getThemePropByName: (NSString*) aPropertyName {
    
    NSDictionary* activeTheme = [self getActiveTheme];
    id prop = [activeTheme valueForKey:aPropertyName];
    
    if (prop == nil)
        return nil;
    
    if([prop isKindOfClass:[NSString class]]){
        if([[activeTheme allKeys] containsObject:prop]){
            return [self getThemePropByName:prop];
        }
    }
    
    return prop;
}

- (ThemeProp*) createThemePropWithDictionary: (NSDictionary*) aProp {
    
    NSString* _color = [self getColorPropertyFromDictionary:aProp];
    
    return [[ThemeProp alloc] initWithColor:_color];
}

- (ThemePropText*) createThemePropTextWithDictionary: (NSDictionary*) aProp {
    
    NSString* _color = [self getColorPropertyFromDictionary:aProp];
    
    UIFont* _font = [self getFontPropertyFromDictionary:aProp];

    return [[ThemePropText alloc] initWithColor:_color andFont: _font];
}

- (NSString*) getColorPropertyFromDictionary: (NSDictionary*) aProp {
    
    NSString* _colorPropString = [aProp objectForKey:@"Color"];
    
    NSArray* _propComponents = [_colorPropString componentsSeparatedByString:@"-"];
    NSString* _palleteName = [_propComponents firstObject];
    NSString* _colorName = [_propComponents lastObject];
    
    NSDictionary* _pallete = [self getPalleteByName:_palleteName];
    NSString* _color = [_pallete objectForKey:_colorName];
    
    return _color;
}

- (UIFont*) getFontPropertyFromDictionary: (NSDictionary*) aProp {
    
    NSString* _fontPropString = [aProp objectForKey:@"Font"];
    
    NSString* _sizePropString = [aProp objectForKey:@"Size"];;
    if(IS_IPAD){
        NSString *_sizeIpad = [aProp objectForKey:@"SizeiPad"];
        if(_sizeIpad){
            _sizePropString = _sizeIpad;
        }
    }
    CGFloat fontSize = [_sizePropString floatValue];
    UIFont* _font = [UIFont fontWithName:_fontPropString size:fontSize];
    
    return _font;
}

- (NSDictionary*) getPalleteByName:(NSString*) aname {
    NSDictionary* themeConfig = [self getThemeConfigurationFile];
    NSDictionary* pallete = [themeConfig objectForKey:@"Palette"];
    return [pallete objectForKey:aname];
}

- (void) loadThemeProperties {
    
    _toolbarBackground = [self createThemePropWithDictionary:[self getThemePropByName:@"ToolbarBackground"]];
    _toolbarUnderline = [self createThemePropWithDictionary:[self getThemePropByName:@"ToolbarUnderlineBackground"]];
    _toolbarText =[self createThemePropTextWithDictionary:[self getThemePropByName:@"ToolbarText"]];
    _toolbarButtonBackground = [self createThemePropWithDictionary:[self getThemePropByName:@"ToolbarButtonBackground"]];
    
    
    _menuButtonBackground = [self createThemePropWithDictionary:[self getThemePropByName:@"MenuButtonBackground"]];
    _menuIconBackground = [self createThemePropWithDictionary:[self getThemePropByName:@"MenuIconBackground"]];
    _menuToolbarText = [self createThemePropTextWithDictionary:[self getThemePropByName:@"MenuToolbarText"]];
    _menuBackground = [self createThemePropWithDictionary:[self getThemePropByName:@"MenuBackground"]];
    _menuText = [self createThemePropTextWithDictionary:[self getThemePropByName:@"MenuText"]];
    _menuBagBackground = [self createThemePropWithDictionary:[self getThemePropByName:@"MenuBagBackground"]];
    _menuBagText = [self createThemePropTextWithDictionary:[self getThemePropByName:@"MenuBagText"]];
    _menuSeparatorBackground = [self createThemePropWithDictionary:[self getThemePropByName:@"MenuSeparatorBackground"]];
    
    
    _faqCollapsedHeaderBackground = [self createThemePropWithDictionary:[self getThemePropByName:@"FaqCollapsedHeaderBackground"]];
    _faqCollapsedHeaderText= [self createThemePropTextWithDictionary:[self getThemePropByName:@"FaqCollapsedHeaderText"]];
    _faqExpandedHeaderBackground = [self createThemePropWithDictionary:[self getThemePropByName:@"FaqExpandedHeaderBackground"]];
    _faqExpandedHeaderText= [self createThemePropTextWithDictionary:[self getThemePropByName:@"FaqExpandedHeaderText"]];
    _faqExpandedDetailBackground = [self createThemePropWithDictionary:[self getThemePropByName:@"FaqExpandedDetailBackground"]];
    _faqExpandedDetailText= [self createThemePropTextWithDictionary:[self getThemePropByName:@"FaqExpandedDetailText"]];
    _faqExpandedDetailHighlightText= [self createThemePropTextWithDictionary:[self getThemePropByName:@"FaqExpandedDetailHighlightText"]];
    
    
    _appBodyBackground= [self createThemePropWithDictionary:[self getThemePropByName:@"AppBodyBackground"]];
    _sectionTitleText= [self createThemePropTextWithDictionary:[self getThemePropByName:@"SectionTitleText"]];
    _sectionText= [self createThemePropTextWithDictionary:[self getThemePropByName:@"SectionText"]];
    _sectionButtonBackground = [self createThemePropWithDictionary:[self getThemePropByName:@"SectionButtonBackground"]];
    _sectionButtonText= [self createThemePropTextWithDictionary:[self getThemePropByName:@"SectionButtonText"]];
    _sectionHighlightText = [self createThemePropTextWithDictionary:[self getThemePropByName:@"SectionHighlightText"]];
    
    
    _popupTitleText = [self createThemePropTextWithDictionary:[self getThemePropByName:@"PopupTitleText"]];
    _popupText = [self createThemePropTextWithDictionary:[self getThemePropByName:@"PopupText"]];
    _popupActionText = [self createThemePropTextWithDictionary:[self getThemePropByName:@"PopupActionText"]];
    
    _popupButtonOKBackground = [self createThemePropWithDictionary:[self getThemePropByName:@"PopupButtonOKBackground"]];
    _popupButtonOKText = [self createThemePropTextWithDictionary:[self getThemePropByName:@"PopupButtonOKText"]];
    
    
    _popupButtonCancelBackground = [self createThemePropWithDictionary:[self getThemePropByName:@"PopupButtonCancelBackground"]];
    _popupButtonCancelText = [self createThemePropTextWithDictionary:[self getThemePropByName:@"PopupButtonCancelText"]];
    _popupBorderBackground = [self createThemePropWithDictionary:[self getThemePropByName:@"PopupBorderBackground"]];
    
    
    _filterContainerBackground = [self createThemePropWithDictionary:[self getThemePropByName:@"FilterContainerBackground"]];
    _filterBackground = [self createThemePropWithDictionary:[self getThemePropByName:@"FilterBackground"]];
    _filterText = [self createThemePropTextWithDictionary:[self getThemePropByName:@"FilterText"]];
    
    
    _mapMarkerCluster20Background = [self createThemePropWithDictionary:[self getThemePropByName:@"MapMarkerCluster20Background"]];
    _mapMarkerCluster50Background = [self createThemePropWithDictionary:[self getThemePropByName:@"MapMarkerCluster50Background"]];
    _mapMarkerCluster100Background = [self createThemePropWithDictionary:[self getThemePropByName:@"MapMarkerCluster100Background"]];
    _mapMarkerCluster200Background = [self createThemePropWithDictionary:[self getThemePropByName:@"MapMarkerCluster200Background"]];
    _mapMarkerCluster500Background = [self createThemePropWithDictionary:[self getThemePropByName:@"MapMarkerCluster500Background"]];
    _mapMarkerCluster1000Background = [self createThemePropWithDictionary:[self getThemePropByName:@"MapMarkerCluster1000Background"]];
    _mapMarkerClusterDefaultBackground = [self createThemePropWithDictionary:[self getThemePropByName:@"MapMarkerClusterDefaultBackground"]];
    
    
    _newsSeparatorBackground = [self createThemePropWithDictionary:[self getThemePropByName:@"NewsSeparatorBackground"]];
    _newsIconBackground = [self createThemePropWithDictionary:[self getThemePropByName:@"NewsIconBackground"]];
    _newsSumaryTitleText = [self createThemePropTextWithDictionary:[self getThemePropByName:@"NewsSumaryTitleText"]];
    _newsSumaryDescriptionText = [self createThemePropTextWithDictionary:[self getThemePropByName:@"NewsSumaryDescriptionText"]];
    _newsSumaryDateText = [self createThemePropTextWithDictionary:[self getThemePropByName:@"NewsSumaryDateText"]];
    
    
    _parameterSeparatorBackground = [self createThemePropWithDictionary:[self getThemePropByName:@"ParameterSeparatorBackground"]];
    
    _activityIndicatorBackground = [self createThemePropWithDictionary:[self getThemePropByName:@"ActivityIndicatorBackground"]];
}

- (UIFont*) fontOfSize: (int) aSize {
    return [UIFont fontWithName:@"Roboto-Regular" size:aSize];
}
- (UIFont*) fontBoldOfSize: (int) aSize {
    return [UIFont fontWithName:@"Roboto-BoldCondensed" size:aSize];
}

@end
