//
//  ThemeProp.h
//  magicxperience
//
//  Created by erka on 30/09/2018.
//  Copyright © 2018 Artech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ThemeProp : NSObject

@property (nonatomic, readonly) UIColor     *color;
@property (nonatomic, readonly) NSString    *stringColor;

-(instancetype) initWithColor: (NSString*) aColor;

@end
