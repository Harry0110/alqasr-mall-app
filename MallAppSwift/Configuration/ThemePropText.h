//
//  ThemePropText.h
//  magicxperience
//
//  Created by erka on 30/09/2018.
//  Copyright © 2018 Artech. All rights reserved.
//

#import "ThemeProp.h"

@interface ThemePropText : ThemeProp

@property (nonatomic, readonly) UIFont  *font;

-(instancetype) initWithColor: (NSString*) aColor andFont: (UIFont*) aFont;

@end
