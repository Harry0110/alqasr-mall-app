//
//  MapBehaviourConfiguration.m
//  magicxperience
//
//  Created by erka on 07/10/2018.
//  Copyright © 2018 Artech. All rights reserved.
//

#import "MapBehaviourConfiguration.h"

@implementation MapBehaviourConfiguration

-(MapBehaviourConfiguration*) initWithDictionary: (NSDictionary*) adictionary{
    if (self = [super init]) {
        
        self.centerOnCoordinate = [[adictionary objectForKey:@"CenterOnCoordinate"] boolValue];
        self.latitude = [[adictionary objectForKey:@"Latitude"] longValue];
        self.longitude = [[adictionary objectForKey:@"Longitude"] longValue];
        self.latitudinalMeters = [[adictionary objectForKey:@"LatitudinalMeters"] intValue];
        self.longitudinalMeters = [[adictionary objectForKey:@"LongitudinalMeters"] intValue];
    }
    return self;
}

@end
