//
//  ConfigurationManager.m
//

#import "ConfigurationManager.h"

#define APP_RESOURCES_FILE @"Configuration"
#define APP_COLOR_RESOURCES_KEY @"AppColorResources"
#define APP_PREFERENCE_RESOURCES_KEY @"AppPreferences"
#define APP_PREFERENCE_STORIES_AS_COLLECTION_RESOURCES_KEY @"StoriesAsCollection"
#define APP_PREFERENCE_MAP_BEHAVIOUR_RESOURCES_KEY @"MapBehaviour"
#define APP_PREFERENCE_STOREDETAIL_BYPASS_POPUP_RESOURCES_KEY @"StoreDetailBypassPopup"
#define APP_PREFERENCE_STORIESDETAIL_AVAILABLE_RESOURCES_KEY @"StoriesDetailAvailable"

#define ARGENIE_RESOURCES_FILE @"ARGenieResources"
#define ARGENIE_SETTINGS_RESOURCES_KEY @"ARGSettingsResources"
#define ARGENIE_SETTINGS_SHOW_BOTTOM_BAR_RESOURCES_KEY @"ARG_SHOW_BOTTOM_BAR"

#define MENU_RESOURCES_FILE @"MenuConfiguration"
#define MENU_OPTIONS_RESOURCES_KEY @"AppMenuOptions"

#define UIColorFromRGBA(rgbValue, alphaValue) ([UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16)) / 255.0 \
green:((float)((rgbValue & 0xFF00) >> 8)) / 255.0 \
blue:((float)(rgbValue & 0xFF)) / 255.0 \
alpha:alphaValue])
#define UIColorFromRGB(rgbValue) (UIColorFromRGBA((rgbValue), 1.0))



@interface ConfigurationManager ()

@end

@implementation ConfigurationManager

+ (UIColor*) getColorValue:(NSString*) aSettignsKey
{
    UIColor* _defaultColor = [UIColor clearColor]; //default color
    
    if (aSettignsKey != nil)
    {
        NSString* _value = @"";
        id _settingsValue = [ConfigurationManager loadResourcesValueForKey:aSettignsKey inResources:APP_COLOR_RESOURCES_KEY configurationFileName:APP_RESOURCES_FILE];
        
        if (_settingsValue != nil)
        {
            if ([_settingsValue isKindOfClass:[NSString class]] && [(NSString*)_settingsValue hasPrefix:@"#"])
                _value = _settingsValue;
            else //it's other color reference
                return [ConfigurationManager getColorValue:_settingsValue];

            if (![_value isEqualToString:@""])
            {
                _defaultColor = [ConfigurationManager colorFromHexString:_value];
            }
        }
    }
    
    return _defaultColor;
}

+ (NSString*) getColorValueForHtml:(NSString*) aSettignsKey
{
    NSString* _value = @"FFFFFF";
    id _settingsValue = [ConfigurationManager loadResourcesValueForKey:aSettignsKey inResources:APP_COLOR_RESOURCES_KEY configurationFileName:APP_RESOURCES_FILE];
    
    if ([_settingsValue isKindOfClass:[NSString class]] && [(NSString*)_settingsValue hasPrefix:@"#"])
        _value = _settingsValue;
    else //it's other color reference
        _value = [ConfigurationManager getColorValueForHtml:_settingsValue];
    
    return _value;
}

+ (BOOL) isMenuOptionActive: (NSString*) aMenuResourcesKey
{
    BOOL _active;
    id _activeValue = [ConfigurationManager loadResourcesValueForKey:aMenuResourcesKey inResources:MENU_OPTIONS_RESOURCES_KEY configurationFileName:MENU_RESOURCES_FILE];
    
    if ([_activeValue isKindOfClass:[NSDictionary class]])
        _active = [[[_activeValue allObjects] firstObject] boolValue];
    else
        _active = [_activeValue boolValue];
    return _active;
    
}

+ (id) loadResourcesValueForKey:(NSString*)aExternalKey inResources:(NSString*) aResourcesKey configurationFileName:(NSString*) aConfigurationFileName
{
    NSString* _path = [[NSBundle mainBundle] pathForResource:aConfigurationFileName ofType:@"plist"];
    NSDictionary* _resourcesFile = [NSDictionary dictionaryWithContentsOfFile:_path];
    id _settingsValue = nil;
    if (_resourcesFile != nil)
    {
        _resourcesFile = [_resourcesFile objectForKey:aResourcesKey]; //internal dict with specific settings or color or theme key and values
        if (_resourcesFile != nil)
        {
            _settingsValue = [_resourcesFile objectForKey:aExternalKey];
        }
    }
    return _settingsValue;
}

+ (UIColor *) colorFromHexString:(NSString *) hexString
{
    if (!hexString) {
        return nil;
    }
    
    hexString = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    unsigned hex;
    BOOL success = [scanner scanHexInt:&hex];
    
    if (!success) return nil;
    if ([hexString length] <= 6)
        return UIColorFromRGB(hex);
    else {
        unsigned color = (hex & 0x00FFFFFF);
        CGFloat alpha = ((hex & 0xFF000000) >> 24) / 255.0; //1.0 *
        return UIColorFromRGBA(color, alpha);
    }
}

+ (BOOL) enableCollectionViewForStories {
     return [[ConfigurationManager loadResourcesValueForKey:APP_PREFERENCE_STORIES_AS_COLLECTION_RESOURCES_KEY    inResources:APP_PREFERENCE_RESOURCES_KEY configurationFileName:APP_RESOURCES_FILE] boolValue];
}



+ (BOOL) storeDetailBypassPopup {
    BOOL bypassPopup = [[ConfigurationManager loadResourcesValueForKey:APP_PREFERENCE_STOREDETAIL_BYPASS_POPUP_RESOURCES_KEY    inResources:APP_PREFERENCE_RESOURCES_KEY configurationFileName:APP_RESOURCES_FILE] boolValue];
    
    return bypassPopup;
}

+ (BOOL) storiesDetailAvailable {
    BOOL available = [[ConfigurationManager loadResourcesValueForKey:APP_PREFERENCE_STORIESDETAIL_AVAILABLE_RESOURCES_KEY    inResources:APP_PREFERENCE_RESOURCES_KEY configurationFileName:APP_RESOURCES_FILE] boolValue];
    
    return available;
}

+ (BOOL) argenieShowBottomBar {
    BOOL show = [[ConfigurationManager loadResourcesValueForKey:ARGENIE_SETTINGS_SHOW_BOTTOM_BAR_RESOURCES_KEY     inResources:ARGENIE_SETTINGS_RESOURCES_KEY configurationFileName:ARGENIE_RESOURCES_FILE] boolValue];
    
    return show;
}

@end
