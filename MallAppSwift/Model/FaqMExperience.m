//
//  FaqMExperience.m
//  Unity-iPhone
//
//  Created by Magela Santana Gonzalez on 8/20/15.
//
//

#import "FaqMExperience.h"
#import "CommonsUtils.h"

#define TABLE_NAME @"faq"
#define COLUMN_QUESTION @"question"
#define COLUMN_ANSWER @"response"
#define COLUMN_ORDER @"faqorder"

@implementation FaqMExperience

+ (NSMutableArray*) getFaqContent{
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    NSMutableArray *faqList = [[NSMutableArray alloc] init];
    if (![_database open])
    {
        return faqList;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    FMResultSet *_resultSet = [_database executeQuery:@"select * from faq f inner join faq_platform fp on f.id = fp.id_faq where fp.id_platform = (select tid from platform where title = \'iPhone\') order by f.faqorder"];
    while ([_resultSet next]) {
        
        FaqMExperience *faqMExperience = [[FaqMExperience alloc] init];
        //parse data
        faqMExperience.question = [_resultSet stringForColumn:COLUMN_QUESTION];
        faqMExperience.answer = [_resultSet stringForColumn:COLUMN_ANSWER];
        faqMExperience.order = [_resultSet intForColumn:COLUMN_ORDER];
        
        [faqList addObject:faqMExperience];
    }
    
    [_resultSet close];
    [_database close];
    
    [faqList sortUsingSelector:@selector(compareOrder:)];
    return faqList;
}

- (NSComparisonResult)compareOrder:(FaqMExperience*)aOtherFaq {
    
    if (_order < aOtherFaq.order) {
        return NSOrderedAscending;
    }
    else if (_order > aOtherFaq.order) {
        return NSOrderedDescending;
    }
    else {
        return NSOrderedSame;
    }
}


@end
