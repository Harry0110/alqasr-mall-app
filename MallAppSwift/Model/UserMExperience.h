//
//  UserMExperience.h
//  Unity-iPhone
//
//  Created by Magela Santana Gonzalez
//

#import <Foundation/Foundation.h>

@interface UserMExperience : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, assign) BOOL receiveNews;
@property (nonatomic, assign) BOOL receivePartnersInfo;
@property (nonatomic, strong) NSString *serverId;


+ (BOOL) addUserToDB:(UserMExperience*) aNewUser;
+ (UserMExperience*) getUserByMail:(NSString*)aUserMail;

@end
