//
//  IntroMExperience.h
//  Unity-iPhone
//
//  Created by Magela Santana Gonzalez
//

#import <Foundation/Foundation.h>

@interface IntroMExperience : NSObject

@property(nonatomic, strong) NSString *videoUrl;
@property(nonatomic, strong) NSString *subtitleUrl;
@property(nonatomic, assign) bool showAppStart;

//+ (IntroMExperience*) getVideoIntroDataFromDb;
//+ (void) saveVideoIntroUrl:(NSString*) aVideoUrl;
//+ (BOOL) getIfVideoUpdateIsNeeded: (NSString*) aNewVideoUrl;
+ (BOOL) showDemoVideoatAppStart;
//+ (BOOL) getIfSubtitleUpdateIsNeeded: (NSString*) aNewSubtitleUrl;
//+ (void) saveSubtitleIntroUrl:(NSString*) aSubtitleUrl;
+ (BOOL) getIfDownloadedVideoIsActive;



@end
