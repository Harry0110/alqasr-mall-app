//
//  MarkerMExperience.m
//  Unity-iPhone
//


#import "MarkerMExperience.h"
#import "CommonsUtils.h"

#define TABLE_NAME @"markers"
#define COLUMN_TITLE @"title"
#define COLUMN_DISPLAY_IN_MYXPERIENCE @"displayInMyXpe"
#define COLUMN_MYXPERIENCE_THUMBNAIL @"myXpeThumb"


@implementation MarkerMExperience

+ (NSString*) getMyXpeThumbnailAssociatedToMarker: (NSString*) aMarkerName
{
    FMDatabase *database = [[CommonsUtils getCommonUtil] getLocalDB];
    NSString *_myXpeThumbnail= nil;
    
    if (![database open])
    {
        return _myXpeThumbnail;
    }
    
    FMDBQuickCheck(![database hasOpenResultSets])
    
    FMResultSet *_resultSet = [database executeQuery:[NSString stringWithFormat:@"select %@ from %@ where %@ = ? and %@ = 1", COLUMN_MYXPERIENCE_THUMBNAIL, TABLE_NAME, COLUMN_TITLE, COLUMN_DISPLAY_IN_MYXPERIENCE], aMarkerName];
    
    while ([_resultSet next]) {
        
        _myXpeThumbnail = [_resultSet stringForColumn:COLUMN_MYXPERIENCE_THUMBNAIL];
    }
    
    // close the result set.
    // it'll also close when it's dealloc'd, but we're closing the database before
    // the autorelease pool closes, so sqlite will complain about it.
    [_resultSet close];
    [database close];
    
    return _myXpeThumbnail;

}

@end
