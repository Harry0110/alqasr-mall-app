//
//  ClientMExperience.m
//  Unity-iPhone
//
//  Created by Magela Santana Gonzalez
//
//

#import "ClientMExperience.h"
#import "FMDB.h"
#import "CommonsUtils.h"

#define TABLE_NAME @"clients"
#define COLUMN_NAME_IDENTIFIER @"id"
#define COLUMN_NAME_NAME @"name"
#define COLUMN_NAME_IMAGE @"image"
#define COLUMN_NAME_THUMBNAILS @"thumbnails"
#define COLUMN_NAME_BANNER @"banner"
#define COLUMN_NAME_DETAILS_IMAGE @"detailsImage"
#define COLUMN_NAME_DESCRIPTION @"description"
#define COLUMN_NAME_SHORT_DESCRIPTION @"shortDescription"
#define COLUMN_BANNER_SMARTPHONE @"bannerSmartphone"

@implementation ClientMExperience


+ (NSMutableArray*) getClients {
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    NSMutableArray *_clientsList = [[NSMutableArray alloc] init];
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return _clientsList;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    FMResultSet *_resultSet = [_database executeQuery:[NSString stringWithFormat:@"select * from %@", TABLE_NAME]];
    
    while ([_resultSet next]) {
        
        ClientMExperience *_client = [[ClientMExperience alloc]init];
        
        //parse data
        _client.identifier = [_resultSet intForColumn:COLUMN_NAME_IDENTIFIER];
        _client.name = [_resultSet stringForColumn:COLUMN_NAME_NAME];
        _client.banner = [_resultSet stringForColumn:COLUMN_NAME_BANNER];
        _client.thumbnails = [_resultSet stringForColumn:COLUMN_NAME_THUMBNAILS];
        _client.clientDescription = [_resultSet stringForColumn:COLUMN_NAME_DESCRIPTION];
        _client.shortDescription = [_resultSet stringForColumn:COLUMN_NAME_SHORT_DESCRIPTION];
        _client.image = [_resultSet stringForColumn:COLUMN_NAME_IMAGE];
        _client.banner_smartphone = [_resultSet stringForColumn:COLUMN_BANNER_SMARTPHONE];
        if(_client.banner_smartphone != nil && ![_client.banner_smartphone isEqualToString:@""])
            _client.banner = _client.banner_smartphone;
        
        [_clientsList addObject:_client];
    }
    
    [_resultSet close];
    [_database close];
    
    return _clientsList;
}

+ (ClientMExperience*) getClientByIdentifier:(int)aClientIdentifier {
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    ClientMExperience *_client = nil;
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return _client;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    NSNumber *_clientId = [NSNumber numberWithInt:aClientIdentifier];
    
    FMResultSet *_resultSet = [_database executeQuery:[NSString stringWithFormat:@"select * from %@ where %@ = ?", TABLE_NAME, COLUMN_NAME_IDENTIFIER], _clientId];
    
    while ([_resultSet next]) {
        
        ClientMExperience *_client = [[ClientMExperience alloc]init];
        
        //parse data
        _client.identifier = [_resultSet intForColumn:COLUMN_NAME_IDENTIFIER];
        _client.name = [_resultSet stringForColumn:COLUMN_NAME_NAME];
        _client.banner = [_resultSet stringForColumn:COLUMN_NAME_BANNER];
        _client.thumbnails = [_resultSet stringForColumn:COLUMN_NAME_THUMBNAILS];
        _client.clientDescription = [_resultSet stringForColumn:COLUMN_NAME_DESCRIPTION];
        _client.shortDescription = [_resultSet stringForColumn:COLUMN_NAME_SHORT_DESCRIPTION];
        _client.image = [_resultSet stringForColumn:COLUMN_NAME_IMAGE];
        _client.banner_smartphone = [_resultSet stringForColumn:COLUMN_BANNER_SMARTPHONE];
        if(_client.banner_smartphone != nil && ![_client.banner_smartphone isEqualToString:@""])
            _client.banner = _client.banner_smartphone;
    }
    
    [_resultSet close];
    [_database close];
    
    return _client;
}

+ (BOOL) existClient {
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return false;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    NSString * _query = [NSString stringWithFormat: @"SELECT Count(%@) FROM %@", COLUMN_NAME_IDENTIFIER, TABLE_NAME];
    
    int _count = [_database intForQuery:_query];
    
    [_database close];
    
    return _count > 0;
}


@end
