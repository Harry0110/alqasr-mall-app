//
//  MarkersUsedMExperience.h
//  Unity-iPhone
//
//  Created by Magela Santana Gonzalez on 1/14/16.
//
//

#import <Foundation/Foundation.h>

//Entity of markers_used table in internaldb database. Represents the the number of used to specific marker.

@interface MarkersUsedMExperience : NSObject

@property (nonatomic, strong) NSString *markerName;
@property (nonatomic, assign) int markerId;
@property (nonatomic, assign) int markerTimesUsed;

+ (MarkersUsedMExperience*) existMarkerWithNameInDB:(NSString*) aMarkerName;
//+ (BOOL) existMarkerInDB:(NSInteger) aMarkerIdentifier;
+ (BOOL) addMarkerToDB:(MarkersUsedMExperience*) aMarkerToAdd;
- (BOOL) updateMarkerTimesUsed;
+ (NSMutableArray*) getMarkersUsedDataList;
+ (NSMutableArray*) getMarkersUsedDataListOrderedByMostUsed;

+ (int) getMarkerIdentifierByMarkerName:(NSString*)aMarkerName; //get the maker identifier from marker table in local app database.

//get the associated product identifier from marker table in local app database.
+ (int) getAssociatedProductIdentifierByMarkerName:(NSString*)aMarkerName;



@end