//
//  FloorModel.swift
//  MallAppSwift
//
//  Created by Freelancer on 25/06/19.
//  Copyright © 2019 Arianne Peiso. All rights reserved.
//

import Foundation

class FloorModel : NSObject{
    var id: Int32?
    var title :String?
    var thumbnail :String?
    var image: String?
    var weight:String?
    
}
