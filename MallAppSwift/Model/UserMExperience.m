//
//  UserMExperience.m
//  Unity-iPhone
//
//  Created by Magela Santana Gonzalez
//
//

#import "UserMExperience.h"
#import "FMDB.h"
#import "CommonsUtils.h"

#define TABLE_NAME @"local_users"

// COLUMNS NAME
#define COLUMN_NAME_IDENTIFIER @"id"
#define COLUMN_NAME_NAME @"name"
#define COLUMN_NAME_LAST_NAME @"last_name"
#define COLUMN_NAME_EMAIL @"email"
#define COLUMN_NAME_COUNTRY @"country"
#define COLUMN_NAME_RECEIVE_NEWS_LETTER @"receive_news_letter"
#define COLUMN_NAME_RECEIVE_NEWS_LETTER_PARTNERS @"receive_news_letter_from_partners"
#define COLUMN_NAME_SERVER_ID @"server_id"



@implementation UserMExperience

+ (UserMExperience*) getUserByMail:(NSString*)aUserMail {
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    UserMExperience *user = nil;
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return user;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    FMResultSet *_resultSet = [_database executeQuery:[NSString stringWithFormat:@"select * from %@ where %@ = ?", TABLE_NAME, COLUMN_NAME_EMAIL], aUserMail];
    
    while ([_resultSet next]) {
        
        user = [[UserMExperience alloc] init];
        //parse data
        user.name = [_resultSet stringForColumn:COLUMN_NAME_NAME];
        user.lastName = [_resultSet stringForColumn:COLUMN_NAME_LAST_NAME];
        user.email = [_resultSet stringForColumn:COLUMN_NAME_EMAIL];
        user.country = [_resultSet stringForColumn:COLUMN_NAME_COUNTRY];
        user.receiveNews = [_resultSet boolForColumn:COLUMN_NAME_RECEIVE_NEWS_LETTER];
        user.receivePartnersInfo = [_resultSet boolForColumn:COLUMN_NAME_RECEIVE_NEWS_LETTER];
    }
    
    [_resultSet close];
    [_database close];
    
    return user;
}

+ (BOOL) addUserToDB:(UserMExperience*) aNewUser {
    
    if([UserMExperience getUserByMail:aNewUser.email] != nil)
        return FALSE;
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getInternalDB];
    if (![_database open])
    {
        return FALSE;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    NSMutableDictionary *_dictionaryArgs = [NSMutableDictionary dictionary];
    [_dictionaryArgs setObject:aNewUser.name forKey:COLUMN_NAME_NAME];
    [_dictionaryArgs setObject:aNewUser.lastName forKey:COLUMN_NAME_LAST_NAME];
    [_dictionaryArgs setObject:aNewUser.email forKey:COLUMN_NAME_EMAIL];
    [_dictionaryArgs setObject:aNewUser.country forKey:COLUMN_NAME_COUNTRY];
    [_dictionaryArgs setObject:[NSNumber numberWithBool:aNewUser.receiveNews]  forKey:COLUMN_NAME_RECEIVE_NEWS_LETTER];
    [_dictionaryArgs setObject:[NSNumber numberWithBool:aNewUser.receivePartnersInfo]  forKey:COLUMN_NAME_RECEIVE_NEWS_LETTER_PARTNERS];
    [_dictionaryArgs setObject:aNewUser.email forKey:COLUMN_NAME_SERVER_ID];
    
    NSString* _sql = [NSString stringWithFormat:@"insert into %@ (%@, %@, %@, %@, %@, %@, %@) values (:%@, :%@, :%@, :%@, :%@, :%@, :%@)",
                      TABLE_NAME, COLUMN_NAME_NAME, COLUMN_NAME_LAST_NAME,COLUMN_NAME_EMAIL, COLUMN_NAME_COUNTRY, COLUMN_NAME_RECEIVE_NEWS_LETTER, COLUMN_NAME_RECEIVE_NEWS_LETTER_PARTNERS, COLUMN_NAME_SERVER_ID, COLUMN_NAME_NAME, COLUMN_NAME_LAST_NAME,COLUMN_NAME_EMAIL, COLUMN_NAME_COUNTRY, COLUMN_NAME_RECEIVE_NEWS_LETTER, COLUMN_NAME_RECEIVE_NEWS_LETTER_PARTNERS, COLUMN_NAME_SERVER_ID    ];
    FMDBQuickCheck([_database executeUpdate:_sql withParameterDictionary:_dictionaryArgs]);
    
    // close the result set.
    // it'll also close when it's dealloc'd, but we're closing the database before
    // the autorelease pool closes, so sqlite will complain about it.
    [_database close];
    
    return TRUE;
}


@end
