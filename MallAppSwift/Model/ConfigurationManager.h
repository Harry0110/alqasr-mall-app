//
//  ConfigurationManager.h
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define APP_TOP_BAR_BG_COLOR @"AppTopBarBgColor"
#define APP_UNDERLINE_TOP_BAR_COLOR @"AppUnderlineTopBarColor"
#define APP_TOP_BAR_TEXT_COLOR @"AppTopBarTextColor"
#define APP_PRIMARY_COLOR @"AppPrimaryColor"
#define APP_SECONDARY_COLOR @"AppSecondaryColor"
#define APP_COMPLEMENTARY_COLOR @"AppComplementaryColor"
#define APP_HIGTHLIGTH_COLOR @"AppHighlightColor"
#define APP_HTML_TEXT_COLOR @"AppHtlmlTextColor"
#define APP_HTML_LINK_TEXT_COLOR @"AppHtlmlLinkTextColor"
#define APP_FAQ_TEXT_COLOR @"AppFAQTextColor"
#define APP_MENU_BG_COLOR @"AppMenuBgColor"
#define APP_MENU_ITEM_TEXT_COLOR @"AppMenuItemTextColor"
#define APP_MENU_SEPARATOR_COLOR @"AppMenuSeparatorColor"
#define APP_STORE_DETAILS_NAME_COLOR @"AppStoreDetailsNameColor"
#define APP_STORE_DETAILS_ADDRESS_COLOR @"AppStoreDetailsAddressColor"
#define APP_STORE_DETAILS_ADDRESS_COLOR @"AppStoreDetailsAddressColor"
#define APP_STORE_DETAILS_PHONE_MAIL_SITE_LABEL_COLOR @"AppStoreDetailsPhoneMailSiteLabelColor"
#define APP_STORE_DETAILS_PHONE_MAIL_SITE_VALUE_COLOR @"AppStoreDetailsPhoneMailSiteValueColor"
#define APP_STORE_DETAILS_GPS_BUTTON_TEXT_COLOR @"AppStoreDetailsGpsButtonTextColor"
#define APP_FAQ_DETAIL_COLOR @"AppFAQDetailColor"

#define APP_CLUSTERMANAGER_20 @"APP_CLUSTERMANAGER_20"
#define APP_CLUSTERMANAGER_50 @"APP_CLUSTERMANAGER_50"
#define APP_CLUSTERMANAGER_100 @"APP_CLUSTERMANAGER_100"
#define APP_CLUSTERMANAGER_200 @"APP_CLUSTERMANAGER_200"
#define APP_CLUSTERMANAGER_500 @"APP_CLUSTERMANAGER_500"
#define APP_CLUSTERMANAGER_1000 @"APP_CLUSTERMANAGER_1000"
#define APP_CLUSTERMANAGER_Default @"APP_CLUSTERMANAGER_Default"

//menu configuration file key resources
#define MENU_RESOURCES_SCANNER_KEY @"MenuScanner"
#define MENU_RESOURCES_BIBLIO_KEY @"MenuBiblio"
#define MENU_RESOURCES_MXPERIENCES_KEY @"MenuMXperiences"
#define MENU_RESOURCES_PRODUCTS_KEY @"MenuProducts"
#define MENU_RESOURCES_EVENTS_KEY @"MenuEvents"
#define MENU_RESOURCES_STORY_KEY @"MenuStory"
#define MENU_RESOURCES_DISTRIB_KEY @"MenuDistrib"
#define MENU_RESOURCES_CLIENTS_KEY @"MenuClients"
#define MENU_RESOURCES_FAQ_KEY @"MenuFAQ"
#define MENU_RESOURCES_TUTORIAL_KEY @"MenuTutorial"
#define MENU_RESOURCES_DEMO_KEY @"MenuDemo"
#define MENU_RESOURCES_PARTAGER_KEY @"MenuPartager"
#define MENU_RESOURCES_NOTER_KEY @"MenuNoter"
#define MENU_RESOURCES_CONTACT_KEY @"MenuContact"
#define MENU_RESOURCES_NOTIFICATIONS_KEY @"MenuNotifications"
#define MENU_RESOURCES_PARAMETER_KEY @"MenuParameter"


@interface ConfigurationManager : NSObject

+ (UIColor*) getColorValue:(NSString*) aSettignsKey;
+ (NSString*) getColorValueForHtml:(NSString*) aSettignsKey;
+ (BOOL) isMenuOptionActive: (NSString*) aMenuResourcesKey;
+ (BOOL) enableCollectionViewForStories;
+ (BOOL) storeDetailBypassPopup;
+ (BOOL) storiesDetailAvailable;
+ (BOOL) argenieShowBottomBar;

@end
