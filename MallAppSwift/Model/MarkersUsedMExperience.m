//
//  MarkersUsedMExperience.m
//  Unity-iPhone
//
//  Created by Magela Santana Gonzalez on 1/14/16.
//
//

#import "MarkersUsedMExperience.h"
#import "CommonsUtils.h"

#define TABLE_NAME @"markers_used"
#define COLUMN_IDENTIFIER @"id"
#define COLUMN_NAME @"marker_name"
#define COLUMN_TIMES_USED @"times_used"

#define TABLE_MARKER_NAME @"markers"
#define COLUMN_MARKER_TITLE @"title"
#define COLUMN_PRODUCT_IDENTIFIER @"id_product"

@implementation MarkersUsedMExperience


+ (BOOL) addMarkerToDB:(MarkersUsedMExperience*) aMarkerToAdd {
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getInternalDB];
    if (![_database open])
    {
        return FALSE;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    NSMutableDictionary *_dictionaryArgs = [NSMutableDictionary dictionary];
    [_dictionaryArgs setObject:[NSNumber numberWithInteger:aMarkerToAdd.markerId] forKey:COLUMN_IDENTIFIER];
    [_dictionaryArgs setObject:[NSNumber numberWithInteger:aMarkerToAdd.markerTimesUsed] forKey:COLUMN_TIMES_USED];
    [_dictionaryArgs setObject:aMarkerToAdd.markerName forKey:COLUMN_NAME];
    
    NSString* _sql = [NSString stringWithFormat:@"insert into %@ (%@, %@) values (:%@, :%@)",
                      TABLE_NAME, COLUMN_NAME, COLUMN_TIMES_USED, COLUMN_NAME, COLUMN_TIMES_USED];
    FMDBQuickCheck([_database executeUpdate:_sql withParameterDictionary:_dictionaryArgs]);
    
    // close the result set.
    // it'll also close when it's dealloc'd, but we're closing the database before
    // the autorelease pool closes, so sqlite will complain about it.
    [_database close];
    
    return TRUE;
}

- (BOOL) updateMarkerTimesUsed {
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getInternalDB];
    if (![_database open])
    {
        return FALSE;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    NSMutableDictionary *_dictionaryArgs = [NSMutableDictionary dictionary];
    
    self.markerTimesUsed ++;
    [_dictionaryArgs setObject:[NSNumber numberWithInteger:self.markerTimesUsed] forKey:COLUMN_TIMES_USED];
    [_dictionaryArgs setObject:[NSNumber numberWithInteger:self.markerId] forKey:COLUMN_IDENTIFIER];
    
    NSString* _sql = [NSString stringWithFormat:@"update %@ set %@ = :%@ where %@ = :%@", TABLE_NAME, COLUMN_TIMES_USED, COLUMN_TIMES_USED, COLUMN_IDENTIFIER, COLUMN_IDENTIFIER];
    
    FMDBQuickCheck([_database executeUpdate:_sql withParameterDictionary:_dictionaryArgs]);
    
    // close the result set.
    // it'll also close when it's dealloc'd, but we're closing the database before
    // the autorelease pool closes, so sqlite will complain about it.
    [_database close];
    
    return TRUE;
}


//+ (BOOL) existMarkerInDB:(NSInteger) aMarkerIdentifier {
//    
//    FMDatabase *_internalDB = [[CommonsUtils getCommonUtil] getInternalDB];
//    BOOL exist = FALSE;
//    
//    if (![_internalDB open])
//    {
//        return exist;
//    }
//    
//    FMDBQuickCheck(![_internalDB hasOpenResultSets])
//    
//    FMResultSet *_resultSet = [_internalDB executeQuery:[NSString stringWithFormat:@"select * from %@ where %@ = ?", TABLE_NAME, COLUMN_IDENTIFIER],[NSNumber numberWithInteger:aMarkerIdentifier]];
//    while ([_resultSet next]) {
//        exist = TRUE;
//    }
//    
//    // close the result set.
//    // it'll also close when it's dealloc'd, but we're closing the database before
//    // the autorelease pool closes, so sqlite will complain about it.
//    [_resultSet close];
//    [_internalDB close];
//    
//    return exist;
//}

+ (MarkersUsedMExperience*) existMarkerWithNameInDB:(NSString*) aMarkerName {
    
    FMDatabase *_internalDB = [[CommonsUtils getCommonUtil] getInternalDB];
    MarkersUsedMExperience *_marker = nil;
    
    if (![_internalDB open])
    {
        return _marker;
    }
    
    FMDBQuickCheck(![_internalDB hasOpenResultSets])
    
    FMResultSet *_resultSet = [_internalDB executeQuery:[NSString stringWithFormat:@"select * from %@ where %@ = ?", TABLE_NAME, COLUMN_NAME], aMarkerName];
    while ([_resultSet next]) {
       
        _marker = [[MarkersUsedMExperience alloc] init];
        [_marker setMarkerId:[_resultSet intForColumn:COLUMN_IDENTIFIER]];
        [_marker setMarkerTimesUsed:[_resultSet intForColumn:COLUMN_TIMES_USED]];
        [_marker setMarkerName:[_resultSet stringForColumn:COLUMN_NAME]];
    }
    
    // close the result set.
    // it'll also close when it's dealloc'd, but we're closing the database before
    // the autorelease pool closes, so sqlite will complain about it.
    [_resultSet close];
    [_internalDB close];
    
    return _marker;
}

+ (NSMutableArray*) getMarkersUsedDataList
{
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getInternalDB];
    NSMutableArray *_markersUsedDataList = [[NSMutableArray alloc] init];
    if (![_database open])
    {
        return _markersUsedDataList;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    FMResultSet *_resultSet = [_database executeQuery:[NSString stringWithFormat:@"select * from %@", TABLE_NAME]];
    
    while ([_resultSet next]) {
        
        MarkersUsedMExperience *markerUsedData = [[MarkersUsedMExperience alloc]init];
        
        //parse data
        markerUsedData.markerName = [_resultSet stringForColumn:COLUMN_NAME];
        markerUsedData.markerTimesUsed = [_resultSet intForColumn:COLUMN_TIMES_USED];
        markerUsedData.markerId = [_resultSet intForColumn:COLUMN_IDENTIFIER];
        
        [_markersUsedDataList addObject:markerUsedData];
    }
    
    [_resultSet close];
    [_database close];
    
    return _markersUsedDataList;
}

+ (NSMutableArray*) getMarkersUsedDataListOrderedByMostUsed
{
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getInternalDB];
    NSMutableArray *_markersUsedDataList = [[NSMutableArray alloc] init];
    if (![_database open])
    {
        return _markersUsedDataList;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    FMResultSet *_resultSet = [_database executeQuery:[NSString stringWithFormat:@"select * from %@ order by %@ desc", TABLE_NAME, COLUMN_TIMES_USED]];
    
    while ([_resultSet next]) {
        
        MarkersUsedMExperience *markerUsedData = [[MarkersUsedMExperience alloc]init];
        
        //parse data
        markerUsedData.markerName = [_resultSet stringForColumn:COLUMN_NAME];
        markerUsedData.markerTimesUsed = [_resultSet intForColumn:COLUMN_TIMES_USED];
        markerUsedData.markerId = [_resultSet intForColumn:COLUMN_IDENTIFIER];
        
        [_markersUsedDataList addObject:markerUsedData];
    }
    
    [_resultSet close];
    [_database close];
    
    return _markersUsedDataList;
}


//get the maker identifier from marker table in local app database.
+ (int) getMarkerIdentifierByMarkerName:(NSString*)aMarkerName
{
    FMDatabase *database = [[CommonsUtils getCommonUtil] getLocalDB];
    int _markerIdentifier = -1;
    
    if (![database open])
    {
        return _markerIdentifier;
    }
    
    FMDBQuickCheck(![database hasOpenResultSets])
    
    FMResultSet *_resultSet = [database executeQuery:[NSString stringWithFormat:@"select * from %@ where %@ = ?", TABLE_MARKER_NAME, COLUMN_MARKER_TITLE], aMarkerName];
    while ([_resultSet next]) {
       
        _markerIdentifier = [_resultSet intForColumn:COLUMN_IDENTIFIER];
    }
    
    // close the result set.
    // it'll also close when it's dealloc'd, but we're closing the database before
    // the autorelease pool closes, so sqlite will complain about it.
    [_resultSet close];
    [database close];
    
    return _markerIdentifier;
}

//get the associated product identifier from marker table in local app database.
+ (int) getAssociatedProductIdentifierByMarkerName:(NSString*)aMarkerName
{
    FMDatabase *database = [[CommonsUtils getCommonUtil] getLocalDB];
    int _productIdentifier = -1;
    
    if (![database open])
    {
        return _productIdentifier;
    }
    
    FMDBQuickCheck(![database hasOpenResultSets])
    
    FMResultSet *_resultSet = [database executeQuery:[NSString stringWithFormat:@"select * from %@ where %@ = ?", TABLE_MARKER_NAME, COLUMN_MARKER_TITLE], aMarkerName];
    while ([_resultSet next]) {
        
        _productIdentifier = [_resultSet intForColumn:COLUMN_PRODUCT_IDENTIFIER];
    }
    
    // close the result set.
    // it'll also close when it's dealloc'd, but we're closing the database before
    // the autorelease pool closes, so sqlite will complain about it.
    [_resultSet close];
    [database close];
    
    return _productIdentifier;
}




@end
