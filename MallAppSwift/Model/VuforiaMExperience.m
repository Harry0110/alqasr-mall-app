//
//  VuforiaMExperience.m
//  Unity-iPhone
//
//  Created by Magela Santana Gonzalez on 9/16/15.
//
//

#import "VuforiaMExperience.h"
#import "FMDB.h"
#import "CommonsUtils.h"
#import "FMDatabaseAdditions.h"

#define TABLE_NAME @"vuforiadatabases"
#define COLUMN_DB_URL @"TrackersDataBaseUrlField"
#define COLUMN_XML_URL @"TrackersXmlUrlField"
#define COLUMN_MODIFIED_DATE @"TrackersLastModified"
#define COLUMN_ORDER @"TrackersOrder"

@implementation VuforiaMExperience

+ (NSMutableArray*) getVuforiaDbsDataList
{
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    NSMutableArray *_vuforiaDataList = [[NSMutableArray alloc]  init];
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return _vuforiaDataList;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    FMResultSet *_resultSet = [_database executeQuery:[NSString stringWithFormat:@"select * from %@ order by %@ desc", TABLE_NAME, COLUMN_ORDER]];
     while ([_resultSet next]) {
         
         VuforiaMExperience *_vuforiaData = [[VuforiaMExperience alloc] init];
         _vuforiaData = [[VuforiaMExperience alloc] init];
         _vuforiaData.dbUrl = [_resultSet stringForColumn:COLUMN_DB_URL];
         _vuforiaData.xmlUrl = [_resultSet stringForColumn:COLUMN_XML_URL];
         _vuforiaData.modifiedDate = [_resultSet stringForColumn:COLUMN_MODIFIED_DATE];
         _vuforiaData.order = [_resultSet intForColumn:COLUMN_ORDER];
         
         [_vuforiaDataList addObject:_vuforiaData];
     }
    
    [_resultSet close];
    [_database close];
    return _vuforiaDataList;
}

- (NSComparisonResult)compareDataByOrder:(VuforiaMExperience*) aOtherVuforiaData {
    
    if (_order > [aOtherVuforiaData order]) {
        return NSOrderedDescending;
    }
    else if (_order < [aOtherVuforiaData order]) {
        return NSOrderedAscending;
    }
    else {
        return NSOrderedSame;
    }
}

@end
