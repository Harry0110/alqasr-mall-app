//
//  InternalVuforiaMExperience.m
//  Unity-iPhone
//
//  Created by Magela Santana Gonzalez


#import "InternalVuforiaMExperience.h"
#import "FMDB.h"
#import "CommonsUtils.h"
#import "FMDatabaseAdditions.h"

#define TABLE_NAME @"vuforia_db_data"
#define COLUMN_NAME @"name"
#define COLUMN_MODIFICATION_DATE @"modification_date"

@implementation InternalVuforiaMExperience

+ (NSMutableArray*) getVuforiaDbsInDevice
{
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getInternalDB];
    NSMutableArray *_vuforiaDbsList = [[NSMutableArray alloc] init];
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return _vuforiaDbsList;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    FMResultSet *_resultSet = [_database executeQuery:[NSString stringWithFormat:@"select * from %@ ", TABLE_NAME]];
     while ([_resultSet next]) {
        
         InternalVuforiaMExperience *_internalVuforiaDbData = [[InternalVuforiaMExperience alloc] init];
         _internalVuforiaDbData.dbName =  [_resultSet stringForColumn:COLUMN_NAME];
         _internalVuforiaDbData.dbLastModifiedDate = [_resultSet stringForColumn:COLUMN_MODIFICATION_DATE];
       
         [_vuforiaDbsList addObject:_internalVuforiaDbData];
     }
    
    [_resultSet close];
    [_database close];
    return _vuforiaDbsList;
}

+ (BOOL) addVuforiaDataToDB:(InternalVuforiaMExperience*) aVuforiaDataToAdd {
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getInternalDB];
    if (![_database open])
    {
        return FALSE;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    NSMutableDictionary *_dictionaryArgs = [NSMutableDictionary dictionary];
    [_dictionaryArgs setObject:aVuforiaDataToAdd.dbName forKey:COLUMN_NAME];
    [_dictionaryArgs setObject:aVuforiaDataToAdd.dbLastModifiedDate forKey:COLUMN_MODIFICATION_DATE];
    
    NSString* _sql = [NSString stringWithFormat:@"insert into %@ (%@, %@) values (:%@, :%@)",
                      TABLE_NAME, COLUMN_NAME, COLUMN_MODIFICATION_DATE, COLUMN_NAME, COLUMN_MODIFICATION_DATE];
    FMDBQuickCheck([_database executeUpdate:_sql withParameterDictionary:_dictionaryArgs]);

    [_database close];
    
    return TRUE;
}


- (BOOL) updateModificationDate : (NSString*) aModificationDate {
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getInternalDB];
    if (![_database open])
    {
        return FALSE;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    NSMutableDictionary *_dictionaryArgs = [NSMutableDictionary dictionary];
    
    self.dbLastModifiedDate = aModificationDate;
    [_dictionaryArgs setObject:self.dbLastModifiedDate forKey:COLUMN_MODIFICATION_DATE];
    [_dictionaryArgs setObject:self.dbName forKey:COLUMN_NAME];
    
    NSString* _sql = [NSString stringWithFormat:@"update %@ set %@ = :%@ where %@ = :%@", TABLE_NAME, COLUMN_MODIFICATION_DATE, COLUMN_MODIFICATION_DATE, COLUMN_NAME, COLUMN_NAME];
    
    FMDBQuickCheck([_database executeUpdate:_sql withParameterDictionary:_dictionaryArgs]);
    
    [_database close];
    
    return TRUE;
}

+ (NSString*) getVuforiaDbModificationDate: (NSString *) aVuforiaDbName
{
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getInternalDB];
    NSString *_lastModifiedDate = nil;
    
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return _lastModifiedDate;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    FMResultSet *_resultSet = [_database executeQuery:[NSString stringWithFormat:@"select * from %@ where %@ = ?", TABLE_NAME, COLUMN_NAME], aVuforiaDbName];
    
    while ([_resultSet next]) {
        
        _lastModifiedDate = [_resultSet stringForColumn:COLUMN_MODIFICATION_DATE];
    }
    
    [_resultSet close];
    [_database close];
    
    return _lastModifiedDate;
}

+ (BOOL) removeVuforiaDataFromDb:(NSString*) aVuforiaDbName {
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getInternalDB];
    BOOL _result = FALSE;
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return FALSE;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    NSMutableDictionary *_dictionaryArgs = [NSMutableDictionary dictionary];
    [_dictionaryArgs setObject:aVuforiaDbName forKey:COLUMN_NAME];
    
    NSString* _sql = [NSString stringWithFormat:@"delete from %@ where %@ = :%@",
                      TABLE_NAME, COLUMN_NAME, COLUMN_NAME];
    _result = [_database executeUpdate:_sql withParameterDictionary:_dictionaryArgs];
    
    // close the result set.
    // it'll also close when it's dealloc'd, but we're closing the database before
    // the autorelease pool closes, so sqlite will complain about it.
    
    [_database close];
    return _result;
}

+ (InternalVuforiaMExperience*) getVuforiaDbData: (NSString *) aVuforiaDbName
{
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getInternalDB];
    InternalVuforiaMExperience *_vuforiaInternalDb = nil;
    
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return _vuforiaInternalDb;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    FMResultSet *_resultSet = [_database executeQuery:[NSString stringWithFormat:@"select * from %@ where %@ = ?", TABLE_NAME, COLUMN_NAME], aVuforiaDbName];
    
    while ([_resultSet next]) {
        
        _vuforiaInternalDb = [[InternalVuforiaMExperience alloc] init];
        _vuforiaInternalDb.dbName =  [_resultSet stringForColumn:COLUMN_NAME];
        _vuforiaInternalDb.dbLastModifiedDate = [_resultSet stringForColumn:COLUMN_MODIFICATION_DATE];
    }
    
    [_resultSet close];
    [_database close];
    
    return _vuforiaInternalDb;
}


@end
