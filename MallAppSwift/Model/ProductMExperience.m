//
//  ProductMExperience.m
//  Unity-iPhone
//
//  Created by Magela Santana Gonzalez on 8/9/15.
//
//

#import "ProductMExperience.h"
#import "FMDB.h"
#import "CommonsUtils.h"

#define TABLE_NAME @"products"
#define PRODUCT_GALLERY_TABLE_NAME @"products_gallery"
#define PRODUCT_STORE_TABLE_NAME @"products_stores"
#define STORE_TABLE_NAME @"stores"
#define PRODUCT_STORY_TABLE_NAME @"products_histories"
#define CATEGORY_TABLE_NAME @"category"

//TABLE PRODUCTS-GALLERY COLUMNS NAME
#define COLUMN_PRODUCT_ID @"id_product"
#define COLUMN_STORE_ID @"id_store"
#define COLUMN_IMG_URL @"image_url"

//TABLE PRODUCTS_HISTORIES COLUMNS NAME
#define COLUMN_STORY_ID @"id_history"

//TABLE PRODUCTS COLUMNS NAME
#define COLUMN_IDENTIFIER @"id"
#define COLUMN_BANNER @"banner"
#define COLUMN_CHARACTER_IMAGE @"characterImage"
#define COLUMN_DESCRIPTION @"description"
#define COLUMN_DETAIL_IMG @"detailsImage"
#define COLUMN_THUMBNAIL @"thumbnails"
#define COLUMN_SKU @"sku"
#define COLUMN_PRICE @"price"
//#define COLUMN_CURRENCY @"currency"
#define COLUMN_PROMOTED @"promoted"
#define COLUMN_SHORT_DESCRIPTION @"shortDescription"
#define COLUMN_TITLE @"title"
#define COLUMN_STATUS @"status"
#define COLUMN_WEIGHT @"weight"
#define COLUMN_NOT_TO_SELL @"not_to_sell"
#define COLUMN_CAN_RECEIVE_MARKER @"can_receive_marker"
#define COLUMN_BANNER_SMARTPHONE @"bannerSmartphone"

//TABLE CATEGORY COLUMNS NAME

#define COLUMN_CATEGORY_ID @"tid"
#define COLUMN_CATEGORY_GENERIC_ID @"genericid"

@implementation ProductMExperience


+ (NSMutableArray*) getproducts{
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    NSMutableArray *_productList = [[NSMutableArray alloc] init];
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return _productList;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    FMResultSet *_resultSet = [_database executeQuery:@"select * from products p left join products_categories pc on p.id=pc.id_product where pc.id_category IN (select c.tid FROM category c where c.genericId = ? )", GENERIC_ID_FOR_PRODUCTS];

  
    while ([_resultSet next]) {
        
        ProductMExperience *product = [ProductMExperience parseProductDataFromDbResult:_resultSet];
        [_productList addObject:product];
    }
   
    [_resultSet close];
    [_database close];
    
    return _productList;
}

+ (NSMutableArray*) getProductsOutOffGroup
{
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    NSMutableArray *_productList = [[NSMutableArray alloc] init];
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return _productList;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    FMResultSet *_resultSet = [_database executeQuery: @"select * from products p left join products_groups g on p.id=g.id_product where g.id_product is null and p.id IN ( select p.id from products p left join products_categories pc on p.id=pc.id_product where pc.id_category IN (select c.tid FROM category c where c.genericId = ?) )", GENERIC_ID_FOR_PRODUCTS];
    
    while ([_resultSet next]) {
        
        ProductMExperience *product = [ProductMExperience parseProductDataFromDbResult:_resultSet];
        [_productList addObject:product];
    }
    
    [_resultSet close];
    [_database close];
    
    return _productList;

}

+ (NSMutableArray*) getEvents{
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    NSMutableArray *_eventsList = [[NSMutableArray alloc] init];
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return _eventsList;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    FMResultSet *_resultSet = [_database executeQuery:@"select * from products p left join products_categories pc on p.id=pc.id_product where pc.id_category IN (select c.tid FROM category c where c.genericId = ? )", GENERIC_ID_FOR_EVENT];
    
    while ([_resultSet next]) {
        
        ProductMExperience *product = [ProductMExperience parseProductDataFromDbResult:_resultSet];
        [_eventsList addObject:product];
    }
    
    [_resultSet close];
    [_database close];
    
    return _eventsList;
}

+ (NSMutableArray*) getProductsForClientAndCategory:(int) aClientIdentifier category:(NSString*)aCategoryGenericId
{
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    NSMutableArray *_productList = [[NSMutableArray alloc] init];
    NSNumber *_clientId = [NSNumber numberWithInt:aClientIdentifier];
    
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return _productList;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    FMResultSet *_resultSet = [_database executeQuery: @"select * from products p inner join products_categories pc on p.id = pc.id_product where pc.id_category IN (select c.tid FROM category c where c.genericId = ? ) and p.id IN ( select prods.id from products prods inner join products_clients pcl on prods.id=pcl.id_product where pcl.id_client = ? )", aCategoryGenericId, _clientId];
    while ([_resultSet next]) {
        
        ProductMExperience *product = [ProductMExperience parseProductDataFromDbResult:_resultSet];
        [_productList addObject:product];
    }
    
    [_resultSet close];
    [_database close];
    
    return _productList;
}

+ (ProductMExperience*) getProductByIdentifier:(int)aProductIdentifier{
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    ProductMExperience *product = nil;
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return product;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    NSNumber *_productId = [NSNumber numberWithInt:aProductIdentifier];
    
    FMResultSet *_resultSet = [_database executeQuery:[NSString stringWithFormat:@"select * from %@ where %@ = ?", TABLE_NAME, COLUMN_IDENTIFIER], _productId];
    
    while ([_resultSet next]) {
        
        product = [ProductMExperience parseProductDataFromDbResult:_resultSet];
    }
    
    [_resultSet close];
    [_database close];
    
    return product;
}

+ (BOOL) exitsProductWithImage:(NSString*)aImageName{
    
    FMDatabaseQueue *_database = [[CommonsUtils getCommonUtil] getLocalQueueDB];
    
    __block BOOL _exits = FALSE;
    
    [_database inDatabase:^(FMDatabase *db) {
        
        NSString* _like = [NSString stringWithFormat:@"%%%@%%", aImageName];
        FMResultSet *_resultSet = [db executeQuery:[NSString stringWithFormat:@"select * from %@ where %@ like ? ", TABLE_NAME, COLUMN_THUMBNAIL], _like];
        _exits = [_resultSet next];
        [_resultSet close];
        
    }];
    
    [_database close];
    return _exits;
}

- (NSMutableArray*) getGalleryList{
   
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    NSMutableArray *_galleryList = [[NSMutableArray alloc] init];
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return _galleryList;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    NSNumber *identifier = [[NSNumber alloc] initWithInt:_identifier];
    
    FMResultSet *_resultSet = [_database executeQuery:[NSString stringWithFormat:@"select * from %@ where %@ = ?", PRODUCT_GALLERY_TABLE_NAME, COLUMN_PRODUCT_ID], identifier];
    while ([_resultSet next]) {
        
        NSString *imgURL;
        //parse data
        imgURL = [[NSString alloc] initWithString:[_resultSet stringForColumn:COLUMN_IMG_URL]];
        [_galleryList addObject:imgURL];
    }
    
    [_resultSet close];
    [_database close];
    
    return _galleryList;

}

+ (NSMutableArray*) getProductsSalesInStore:(int)aStoreIdentifier {
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    NSMutableArray *_productList = [[NSMutableArray alloc] init];
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return _productList;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    NSNumber *_storeId = [NSNumber numberWithInt:aStoreIdentifier];
    
    FMResultSet *_resultSet = [_database executeQuery:[NSString stringWithFormat: @"select * from %@ inner join %@ on %@.%@ = %@.%@ where %@ = ? and %@ = ?", PRODUCT_STORE_TABLE_NAME, TABLE_NAME, TABLE_NAME, COLUMN_IDENTIFIER, PRODUCT_STORE_TABLE_NAME, COLUMN_PRODUCT_ID, COLUMN_STORE_ID, COLUMN_NOT_TO_SELL], _storeId, [NSNumber numberWithInt:0]];
    
    while ([_resultSet next]) {
        
        ProductMExperience *product = [ProductMExperience parseProductDataFromDbResult:_resultSet];
        [_productList addObject:product];
    }
    
    [_resultSet close];
    [_database close];
    
    return _productList;
}

+ (NSMutableArray*) getProductsInStory:(int) aStoryIdentifier
{
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    NSMutableArray *_productList = [[NSMutableArray alloc] init];
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return _productList;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    NSNumber *_storyId = [NSNumber numberWithInt:aStoryIdentifier];
    
    FMResultSet *_resultSet = [_database executeQuery:[NSString stringWithFormat: @"select * from %@ inner join %@ on %@.%@ = %@.%@ where %@ = ? and %@ = ?", PRODUCT_STORY_TABLE_NAME, TABLE_NAME, TABLE_NAME, COLUMN_IDENTIFIER, PRODUCT_STORY_TABLE_NAME, COLUMN_PRODUCT_ID, COLUMN_STORY_ID, COLUMN_NOT_TO_SELL], _storyId, [NSNumber numberWithInt:0]];
    
    while ([_resultSet next]) {
        
        ProductMExperience *product = [ProductMExperience parseProductDataFromDbResult:_resultSet];
        [_productList addObject:product];
    }
    
    [_resultSet close];
    [_database close];
    
    return _productList;
}

+ (NSMutableArray*) getProductsOutOfGroupsInStory:(int) aStoryIdentifier
{
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    NSMutableArray *_productList = [[NSMutableArray alloc] init];
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return _productList;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    NSNumber *_storyId = [NSNumber numberWithInt:aStoryIdentifier];
    
    FMResultSet *_resultSet = [_database executeQuery: @"select p.id, p.banner, p.characterimage, p.description, p.detailsImage, p.thumbnails, p.sku, p.price,  p.promoted, p.shortDescription, p.status, p.title, p.weight, p.not_to_sell, p.can_receive_marker, p.bannerSmartphone from products p left join products_groups g on p.id=g.id_product join products_histories ph on p.id= ph.id_product where g.id_product is null and ph.id_history = ?", _storyId];
    
    
    while ([_resultSet next]) {
        
        ProductMExperience *product = [ProductMExperience parseProductDataFromDbResult:_resultSet];
        [_productList addObject:product];
    }
    
    [_resultSet close];
    [_database close];
    
    return _productList;

}

//get the associated product from marker table in local app database.
+ (ProductMExperience*) getAssociatedProductByMarkerName:(NSString*)aMarkerName
{
    FMDatabase *database = [[CommonsUtils getCommonUtil] getLocalDB];
    ProductMExperience *product = nil;
    
    if (![database open])
    {
        return product;
    }
    
    FMDBQuickCheck(![database hasOpenResultSets])
    
    FMResultSet *_resultSet = [database executeQuery: @"select p.id, p.banner, p.characterimage, p.description, p.detailsImage, p.thumbnails, p.sku, p.price,  p.promoted, p.shortDescription, p.status, p.title, p.weight, p.not_to_sell, p.can_receive_marker, p.bannerSmartphone from products p inner join markers m on p.id = m.id_product where m.title = ?", aMarkerName];
    while ([_resultSet next]) {
        
        product = [ProductMExperience parseProductDataFromDbResult:_resultSet];
        
    }
    
    // close the result set.
    // it'll also close when it's dealloc'd, but we're closing the database before
    // the autorelease pool closes, so sqlite will complain about it.
    [_resultSet close];
    [database close];
    
    return product;
    
}

- (NSComparisonResult)compare:(ProductMExperience *)otherProduct {
    
    if (_weight > otherProduct.weight) {
        return NSOrderedDescending;
    }
    else if (_weight < otherProduct.weight) {
        return NSOrderedAscending;
    }
    else {
        return NSOrderedSame;
    }
}

- (NSComparisonResult)compareWithProtocol:(id<ProductProtocol>)productProtocol {
    
    if (_weight > [productProtocol weight]) {
        return NSOrderedDescending;
    }
    else if (_weight < [productProtocol weight]) {
        return NSOrderedAscending;
    }
    else {
        return NSOrderedSame;
    }
}

- (NSComparisonResult)compareToMesXperiencesView:(ProductMExperience *)otherProduct {
    
    if (_weight > otherProduct.weight) {
        return NSOrderedAscending;
    }
    else if (_weight < otherProduct.weight) {
        return NSOrderedDescending;
    }
    else {
        return NSOrderedSame;
    }
}

- (NSString*) objectDescription{
    return _productDescription;
}

+ (ProductMExperience*) parseProductDataFromDbResult: (FMResultSet*) aResultSet
{
    ProductMExperience *product = [[ProductMExperience alloc] init];
    
    //parse data
    product.identifier = [aResultSet intForColumn:COLUMN_IDENTIFIER];
    //product.banner = [aResultSet stringForColumn:COLUMN_BANNER];
    product.characterImage = [aResultSet stringForColumn:COLUMN_CHARACTER_IMAGE];
    product.productDescription = [aResultSet stringForColumn:COLUMN_DESCRIPTION];
    product. detailImage = [aResultSet stringForColumn:COLUMN_DETAIL_IMG];
    product.thumbnails = [aResultSet stringForColumn:COLUMN_THUMBNAIL];
    product.sku = [aResultSet stringForColumn:COLUMN_SKU];
    product.price = [aResultSet doubleForColumn: COLUMN_PRICE];
    //product.currency = [aResultSet stringForColumn:COLUMN_CURRENCY];
    product. promoted = [aResultSet intForColumn:COLUMN_PROMOTED];
    product.shortDescription = [aResultSet stringForColumn:COLUMN_SHORT_DESCRIPTION];
    product. title = [aResultSet stringForColumn:COLUMN_TITLE];
    product.status = [aResultSet intForColumn:COLUMN_STATUS];
    product.weight = [aResultSet intForColumn:COLUMN_WEIGHT];
    product.isEvent = [aResultSet boolForColumn:COLUMN_NOT_TO_SELL];
    product.canReceiveMarker = [aResultSet boolForColumn:COLUMN_CAN_RECEIVE_MARKER];
    product.banner_smartphone = [aResultSet stringForColumn:COLUMN_BANNER_SMARTPHONE];
    if(product.banner_smartphone != nil && ![product.banner_smartphone isEqualToString:@""])
        product.banner = product.banner_smartphone;
    return product;
}

+ (NSMutableArray*) getAssociatedProductByEvent:(int)aEventIdentifier withProductType:(int) aProductTypeIdentifier withStory:(int) aStoryIdentifier
{
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    NSMutableArray *_productList = [[NSMutableArray alloc] init];
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return _productList;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    
    NSString * _constantQuery = @"SELECT p.id, p.banner, p.characterimage, p.description, p.detailsImage, p.thumbnails, p.sku, p.price,  p.promoted, p.shortDescription, p.status, p.title, p.weight, p.not_to_sell, p.can_receive_marker, p.bannerSmartphone FROM products p LEFT JOIN products_groups g ON p.id=g.id_product LEFT JOIN products_events pe ON pe.id_product = p.id LEFT JOIN events e ON pe.id_event = e.tid LEFT JOIN products_types psts ON psts.id_product = p.id LEFT JOIN producttype pt ON psts.id_type = pt.tid LEFT JOIN products_histories ph ON ph.id_product = p.id LEFT JOIN histories h ON h.id =  ph.id_history WHERE g.id_product IS NULL AND p.id IN (SELECT p.id FROM products p LEFT JOIN products_categories pc ON p.id=pc.id_product WHERE pc.id_category IN (SELECT c.tid FROM category c WHERE c.genericId = \'CATEGORY_PRODUCT\')) %@ ORDER BY p.weight";
    
    NSString *_conditionalQuery = @"";
   
    if(aEventIdentifier != -1)
        _conditionalQuery = [NSString stringWithFormat:@" AND e.tid = %i", aEventIdentifier];
    
    if(aProductTypeIdentifier != -1 && _conditionalQuery != nil)
        _conditionalQuery = [NSString stringWithFormat:@"%@ AND pt.tid = %i", _conditionalQuery, aProductTypeIdentifier];
    else if (aProductTypeIdentifier != -1)
             _conditionalQuery = [NSString stringWithFormat:@" AND pt.tid = %i", aProductTypeIdentifier];
    
    if(aStoryIdentifier != -1 && _conditionalQuery != nil)
        _conditionalQuery = [NSString stringWithFormat:@"%@ AND h.id = %i", _conditionalQuery, aStoryIdentifier];
    else if(aStoryIdentifier != -1)
        _conditionalQuery = [NSString stringWithFormat:@" AND h.id = %i", aStoryIdentifier];
        

    _constantQuery = [NSString stringWithFormat:_constantQuery, _conditionalQuery];
        
    FMResultSet *_resultSet = [_database executeQuery:_constantQuery];
                               
    while ([_resultSet next]) {
        ProductMExperience *product = [ProductMExperience parseProductDataFromDbResult:_resultSet];
        [_productList addObject:product];
    }
    
    [_resultSet close];
    [_database close];
    
    return _productList;
}

+ (BOOL) existProductsOutsideGroups {
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return false;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    int _count = [_database intForQuery: [NSString stringWithFormat:@"select Count(%@) from products p left join products_groups g on p.id=g.id_product where g.id_product is null and p.id IN ( select p.id from products p left join products_categories pc on p.id=pc.id_product where pc.id_category IN (select c.tid FROM category c where c.genericId = ?) )", COLUMN_IDENTIFIER], GENERIC_ID_FOR_PRODUCTS];
    
    [_database close];
    
    return _count > 0;
}

+ (BOOL) existEvents {
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return false;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets]);
    
    int _count = [_database intForQuery:[NSString stringWithFormat:@"select Count(%@) from %@ p left join products_categories pc on p.id=pc.id_product where pc.id_category IN (select c.tid FROM category c where c.genericId = ? )", COLUMN_IDENTIFIER, TABLE_NAME], GENERIC_ID_FOR_EVENT];
    
    [_database close];
    
    return _count > 0;
}

@end
