//
//  OperatorMExperience.h
//  Unity-iPhone
//
//  Created by Magela Santana Gonzalez on 1/15/16.
//
//

#import <Foundation/Foundation.h>

@interface OperatorMExperience : NSObject

@property (nonatomic, assign) int identifier;
@property (nonatomic, strong) NSString *title;

+ (OperatorMExperience*) getOperatorByIdentifier:(NSInteger) aOperatorIdentifier;
- (BOOL) getIfSatisficeRule:(int) aRuleForTimesUsed markerTimesUsed:(int) aMarkerTimesUsed;

@end
