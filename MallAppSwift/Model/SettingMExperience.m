//
//  SettingMExperience.m
//  Unity-iPhone
//
//  Created by Magela Santana Gonzalez on 8/3/15.
//
//

#import "SettingMExperience.h"
#import "FMDB.h"
#import "CommonsUtils.h"

#define TABLE_NAME @"settings"

#define COLUMN_NAME_KEY @"key"
#define COLUMN_NAME_VALUE @"value"

@implementation SettingMExperience


+ (NSString*) getSettingUrl:(NSString*) aSettingsKey {
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    NSString *_settingURL = nil;
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return _settingURL;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    FMResultSet *_resultSet = [_database executeQuery:[NSString stringWithFormat:@"select * from %@ where %@ = ?", TABLE_NAME, COLUMN_NAME_KEY], aSettingsKey];
    while ([_resultSet next]) {
        _settingURL = [[NSString alloc] init];
        
        //parse data
        
        _settingURL = [_resultSet stringForColumn:COLUMN_NAME_VALUE];
    }
    
    // close the result set.
    // it'll also close when it's dealloc'd, but we're closing the database before
    // the autorelease pool closes, so sqlite will complain about it.
    [_resultSet close];
    [_database close];
    
    return _settingURL;
}


@end
