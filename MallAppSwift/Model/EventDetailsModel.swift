//
//  EventDetailsModel.swift
//  MallAppSwift
//
//  Created by Freelancer on 28/06/19.
//  Copyright © 2019 Arianne Peiso. All rights reserved.
//

import Foundation

class EventDetailsModel : NSObject{
    var id: Int32?
    var title :String?
    var image: String?
    var shortDescription:String?
    var EDdescription:String?
    var date:Double?
    var location:String?
}
