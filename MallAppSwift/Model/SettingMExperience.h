//
//  SettingMExperience.h
//  Unity-iPhone
//
//  Created by Magela Santana Gonzalez on 8/3/15.
//
//

#import <Foundation/Foundation.h>

#define MENTIONS_LEGAL_KEY @"mentionslegal"
#define FAQ_KEY @"faq"
#define CGU_KEY @"cgu"

@interface SettingMExperience : NSObject

+ (NSString*) getSettingUrl:(NSString*) aSettingsKey;

@end
