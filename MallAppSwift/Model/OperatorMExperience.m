//
//  OperatorMExperience.m
//  Unity-iPhone
//
//  Created by Magela Santana Gonzalez on 1/15/16.
//
//

#import "OperatorMExperience.h"
#import "CommonsUtils.h"

#define TABLE_NAME @"operators"
#define COLUMN_IDENTIFIER @"id"
#define COLUMN_TITLE @"title"

@implementation OperatorMExperience

+ (OperatorMExperience*) getOperatorByIdentifier:(NSInteger) aOperatorIdentifier {
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    OperatorMExperience * _operator = nil;
    
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return _operator;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    FMResultSet *_resultSet = [_database executeQuery:[NSString stringWithFormat:@"select * from %@ where %@ = ?", TABLE_NAME, COLUMN_IDENTIFIER],[NSNumber numberWithInteger:aOperatorIdentifier]];
    while ([_resultSet next]) {
        _operator = [[OperatorMExperience alloc] init];
        [_operator setIdentifier:[_resultSet intForColumn:COLUMN_IDENTIFIER]];
        [_operator setTitle:[_resultSet stringForColumn:COLUMN_TITLE]];
      
    }
    
    // close the result set.
    // it'll also close when it's dealloc'd, but we're closing the database before
    // the autorelease pool closes, so sqlite will complain about it.
    [_resultSet close];
    [_database close];
    
    return _operator;
}

- (BOOL) getIfSatisficeRule:(int) aRuleForTimesUsed markerTimesUsed:(int) aMarkerTimesUsed {
    
    if([self.title isEqualToString:@">"])
    {
        if(aMarkerTimesUsed > aRuleForTimesUsed)
            return TRUE;
        else
            return FALSE;
    }
    else if([self.title isEqualToString:@"<"])
    {
        if(aMarkerTimesUsed < aRuleForTimesUsed)
            return TRUE;
        else
            return FALSE;
    }
    else if([self.title isEqualToString:@"="])
    {
        if(aMarkerTimesUsed == aRuleForTimesUsed)
            return TRUE;
        else
            return FALSE;
    }
    else if([self.title isEqualToString:@"!="])
    {
        if(aMarkerTimesUsed != aRuleForTimesUsed)
            return TRUE;
        else
            return FALSE;
    }
    
    return FALSE;
}


@end
