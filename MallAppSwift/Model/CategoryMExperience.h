//
//  CategoryMExperience.h
//  Unity-iPhone
//
//  Created by Magela Santana Gonzalez
//
//

#import <Foundation/Foundation.h>

@interface CategoryMExperience : NSObject

@property (nonatomic, assign) int identifier;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *categDescription;
@property (nonatomic, strong) NSString *genericId;
@property (nonatomic, assign) int weight;

+ (NSMutableArray*) getCategoriesForClient:(int) aClientIdentifier;

@end
