//
//  StoreMExperience.m
//  Unity-iPhone
//
//  Created by Magela Santana Gonzalez on 8/13/15.
//
//

#import "StoreMExperience.h"
#import "FMDB.h"
#import "CommonsUtils.h"
#import "ProductTypeMExperience.h"

#define STORE_TABLE_NAME @"stores"
#define PRODUCT_STORE_TABLE_NAME @"products_stores"
#define PRODUCTS_TYPES_TABLE_NAME @"products_types"
#define STORES_PHONES_TABLE_NAME @"stores_phones"

//STORE COLUMNS NAME
#define COLUMN_IDENTIFIER @"id"
#define COLUMN_STORE_ADDRESS @"address"
#define COLUMN_STORE_EMAIL @"email"
#define COLUMN_STORE_IMAGE @"image"
#define COLUMN_STORE_LATITUDE @"latitude"
#define COLUMN_STORE_LONGITUDE @"longitude"
#define COLUMN_STORE_NAME @"name"
#define COLUMN_STORE_WEBSITE @"websiteName"
#define COLUMN_STORE_WEBSITE_URL @"websiteUrl"
#define COLUMN_STORE_CITY @"city"
#define COLUMN_STORE_POSTALCODE @"postalCode"
#define COLUMN_STORE_IMG_DETAIL @"imageDetail"
#define COLUMN_STORE_ICON @"icon"
#define COLUMN_STORE_DESCRIPTION @"description"

//PRODUCTS_STORES COLUMNS NAME
#define COLUMN_STORE_IDENTIFIER @"id_store"
#define COLUMN_PRODUCT_IDENTIFIER @"id_product"

//PRODUCTS_TYPES COLUMNS NAME
#define COLUMN_TYPE_IDENTIFIER @"id_type"

//STORES_PHONES COLUMNS NAME
#define COLUMN_PHONE_NAME @"phone"

@implementation StoreMExperience

+ (NSMutableArray*) getStoresThatSalesProductType:(int)aProductType
{
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    NSMutableArray *_storesList = [[NSMutableArray alloc] init];
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return _storesList;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    NSNumber *_productTypeId = [NSNumber numberWithInt:aProductType];
    
    NSString *_condition = @"where stores.longitude != \"\" and stores.latitude != \"\" ";
    
    FMResultSet *_resultSet =
    [_database executeQuery:[NSString stringWithFormat: @"select distinct * from %@ inner join (select %@ as identifier from %@ inner join %@  on %@.%@ = %@.%@ where %@ = ?)  on %@.%@ = identifier %@", STORE_TABLE_NAME, COLUMN_STORE_IDENTIFIER, PRODUCT_STORE_TABLE_NAME, PRODUCTS_TYPES_TABLE_NAME, PRODUCT_STORE_TABLE_NAME, COLUMN_PRODUCT_IDENTIFIER, PRODUCTS_TYPES_TABLE_NAME, COLUMN_PRODUCT_IDENTIFIER, COLUMN_TYPE_IDENTIFIER, STORE_TABLE_NAME, COLUMN_IDENTIFIER, _condition], _productTypeId];
    
   while ([_resultSet next]) {
        
        StoreMExperience *store = [[StoreMExperience alloc]init];
        
        //parse data
        store.identifier = [_resultSet intForColumn:COLUMN_IDENTIFIER];
        store.address = [_resultSet stringForColumn:COLUMN_STORE_ADDRESS];
        store.email = [_resultSet stringForColumn:COLUMN_STORE_EMAIL];
        store.image = [_resultSet stringForColumn:COLUMN_STORE_IMAGE];
        store.latitude = [_resultSet stringForColumn:COLUMN_STORE_LATITUDE];
        store.longitude = [_resultSet stringForColumn:COLUMN_STORE_LONGITUDE];
        store.name = [_resultSet stringForColumn:COLUMN_STORE_NAME];
        store.webSiteName = [_resultSet stringForColumn:COLUMN_STORE_WEBSITE];
        store.webSiteUrl = [_resultSet stringForColumn:COLUMN_STORE_WEBSITE_URL];
        store.city = [_resultSet stringForColumn:COLUMN_STORE_CITY];
        store.postalCode = [_resultSet stringForColumn:COLUMN_STORE_POSTALCODE];
        store.imageDetail =  [_resultSet stringForColumn:COLUMN_STORE_IMG_DETAIL];
        store.icon =  [_resultSet stringForColumn:COLUMN_STORE_ICON];
        store.htmlDescription = [_resultSet stringForColumn:COLUMN_STORE_DESCRIPTION];
        [_storesList addObject:store];
    }
    
    [_resultSet close];
    [_database close];
    
    return _storesList;

}

+ (NSMutableArray*) getStoresThatSalesAtLessProductType:(NSMutableArray*)aProductTypeList
{
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    NSMutableArray *_storesList = [[NSMutableArray alloc] init];
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return _storesList;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    NSString *_condition = @"where stores.longitude != \"\" and stores.latitude != \"\" ";
    
    NSString *_prodTypeCondition = @"";
   
    if(aProductTypeList!= nil && [aProductTypeList count] > 0)
    {
        _prodTypeCondition = @"where";
        
        for (int i = 0; i < [aProductTypeList count]; i++)
        {
            ProductTypeMExperience *productType = [aProductTypeList objectAtIndex:i];
            NSNumber *_productTypeId = [NSNumber numberWithInt: [productType identifier]];
            if(i == 0)
            {
               _prodTypeCondition = [NSString stringWithFormat:@"%@ %@ = %@", _prodTypeCondition, COLUMN_TYPE_IDENTIFIER, _productTypeId];
            }
            else
            {
                _prodTypeCondition = [NSString stringWithFormat:@"%@ or %@ = %@", _prodTypeCondition, COLUMN_TYPE_IDENTIFIER, _productTypeId];
            }
                
        }
    }
    
    FMResultSet *_resultSet =
    [_database executeQuery:[NSString stringWithFormat: @"select distinct * from %@ inner join (select %@ as identifier from %@ inner join %@  on %@.%@ = %@.%@ %@)  on %@.%@ = identifier %@", STORE_TABLE_NAME, COLUMN_STORE_IDENTIFIER, PRODUCT_STORE_TABLE_NAME, PRODUCTS_TYPES_TABLE_NAME, PRODUCT_STORE_TABLE_NAME, COLUMN_PRODUCT_IDENTIFIER, PRODUCTS_TYPES_TABLE_NAME, COLUMN_PRODUCT_IDENTIFIER, _prodTypeCondition, STORE_TABLE_NAME, COLUMN_IDENTIFIER, _condition]];
    
    while ([_resultSet next]) {
        
        StoreMExperience *store = [[StoreMExperience alloc]init];
        
        //parse data
        store.identifier = [_resultSet intForColumn:COLUMN_IDENTIFIER];
        store.address = [_resultSet stringForColumn:COLUMN_STORE_ADDRESS];
        store.email = [_resultSet stringForColumn:COLUMN_STORE_EMAIL];
        store.image = [_resultSet stringForColumn:COLUMN_STORE_IMAGE];
        store.latitude = [_resultSet stringForColumn:COLUMN_STORE_LATITUDE];
        store.longitude = [_resultSet stringForColumn:COLUMN_STORE_LONGITUDE];
        store.name = [_resultSet stringForColumn:COLUMN_STORE_NAME];
        store.webSiteName = [_resultSet stringForColumn:COLUMN_STORE_WEBSITE];
        store.webSiteUrl = [_resultSet stringForColumn:COLUMN_STORE_WEBSITE_URL];
        store.city = [_resultSet stringForColumn:COLUMN_STORE_CITY];
        store.postalCode = [_resultSet stringForColumn:COLUMN_STORE_POSTALCODE];
        store.imageDetail =  [_resultSet stringForColumn:COLUMN_STORE_IMG_DETAIL];
        store.icon =  [_resultSet stringForColumn:COLUMN_STORE_ICON];
        store.htmlDescription = [_resultSet stringForColumn:COLUMN_STORE_DESCRIPTION];
        
        [_storesList addObject:store];
    }
    
    [_resultSet close];
    [_database close];
    
    return _storesList;
    
}

+ (NSMutableArray*) getStoresThatSalesProduct:(int)aProductId {
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    NSMutableArray *_storesList = [[NSMutableArray alloc] init];
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return _storesList;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    NSNumber *_productId = [NSNumber numberWithInt:aProductId];
    
    FMResultSet *_resultSet = [_database executeQuery:[NSString stringWithFormat: @"select * from %@ inner join %@ on %@.%@ = %@.%@ where %@ = ?", STORE_TABLE_NAME, PRODUCT_STORE_TABLE_NAME, STORE_TABLE_NAME, COLUMN_IDENTIFIER, PRODUCT_STORE_TABLE_NAME, COLUMN_STORE_IDENTIFIER, COLUMN_PRODUCT_IDENTIFIER], _productId];
   
    while ([_resultSet next]) {
        
        StoreMExperience *store = [[StoreMExperience alloc]init];
       
        //parse data
        store.identifier = [_resultSet intForColumn:COLUMN_IDENTIFIER];
        store.address = [_resultSet stringForColumn:COLUMN_STORE_ADDRESS];
        store.email = [_resultSet stringForColumn:COLUMN_STORE_EMAIL];
        store.image = [_resultSet stringForColumn:COLUMN_STORE_IMAGE];
        store.latitude = [_resultSet stringForColumn:COLUMN_STORE_LATITUDE];
        store.longitude = [_resultSet stringForColumn:COLUMN_STORE_LONGITUDE];
        store.name = [_resultSet stringForColumn:COLUMN_STORE_NAME];
        store.webSiteName = [_resultSet stringForColumn:COLUMN_STORE_WEBSITE];
        store.webSiteUrl = [_resultSet stringForColumn:COLUMN_STORE_WEBSITE_URL];
        store.city = [_resultSet stringForColumn:COLUMN_STORE_CITY];
        store.postalCode = [_resultSet stringForColumn:COLUMN_STORE_POSTALCODE];
        store.imageDetail =  [_resultSet stringForColumn:COLUMN_STORE_IMG_DETAIL];
        store.icon =  [_resultSet stringForColumn:COLUMN_STORE_ICON];
        store.htmlDescription = [_resultSet stringForColumn:COLUMN_STORE_DESCRIPTION];
        
        [_storesList addObject:store];
    }
    
    [_resultSet close];
    [_database close];
    
    return _storesList;
}


+ (NSMutableArray*) getStores {
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    NSMutableArray *_storesList = [[NSMutableArray alloc] init];
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return _storesList;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
   
    NSString *_condition = @"where longitude != \"\" and latitude != \"\" ";
    
    FMResultSet *_resultSet = [_database executeQuery:[NSString stringWithFormat:@"select * from %@ %@", STORE_TABLE_NAME, _condition]];
    
    while ([_resultSet next]) {
        
        StoreMExperience *store = [[StoreMExperience alloc]init];
        
        //parse data
        store.identifier = [_resultSet intForColumn:COLUMN_IDENTIFIER];
        store.address = [_resultSet stringForColumn:COLUMN_STORE_ADDRESS];
        store.email = [_resultSet stringForColumn:COLUMN_STORE_EMAIL];
        store.image = [_resultSet stringForColumn:COLUMN_STORE_IMAGE];
        store.latitude = [_resultSet stringForColumn:COLUMN_STORE_LATITUDE];
        store.longitude = [_resultSet stringForColumn:COLUMN_STORE_LONGITUDE];
        store.name = [_resultSet stringForColumn:COLUMN_STORE_NAME];
        store.webSiteName = [_resultSet stringForColumn:COLUMN_STORE_WEBSITE];
        store.webSiteUrl = [_resultSet stringForColumn:COLUMN_STORE_WEBSITE_URL];
        store.city = [_resultSet stringForColumn:COLUMN_STORE_CITY];
        store.postalCode = [_resultSet stringForColumn:COLUMN_STORE_POSTALCODE];
        store.imageDetail =  [_resultSet stringForColumn:COLUMN_STORE_IMG_DETAIL];
        store.icon =  [_resultSet stringForColumn:COLUMN_STORE_ICON];
        store.htmlDescription = [_resultSet stringForColumn:COLUMN_STORE_DESCRIPTION];
        [_storesList addObject:store];
    }
    
    [_resultSet close];
    [_database close];
    
    return _storesList;
}

+ (NSString*) getStoresPhones:(int) aStoreIdentifier
{
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    NSString *_storePhone = nil;
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return _storePhone;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    NSNumber *_storeId= [NSNumber numberWithInt:aStoreIdentifier];
    
    FMResultSet *_resultSet = [_database executeQuery:[NSString stringWithFormat:@"select * from %@ where %@ = ?", STORES_PHONES_TABLE_NAME, COLUMN_STORE_IDENTIFIER], _storeId];
    
    _storePhone = [[NSString alloc] init];
    while ([_resultSet next]) {
    if(![_storePhone isEqualToString:@""])
        _storePhone = [NSString stringWithFormat:@"%@, ", _storePhone];
       
    //parse data
    NSString *phone = [_resultSet stringForColumn:COLUMN_PHONE_NAME];
    _storePhone = [NSString stringWithFormat:@"%@%@", _storePhone, phone];
 }
    
    [_resultSet close];
    [_database close];
    
    return _storePhone;

}

+ (StoreMExperience*) getStoreByIdentifier:(int)aStoreIdentifier{
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    StoreMExperience *_store = nil;
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return _store;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    NSNumber *_storeId = [NSNumber numberWithInt:aStoreIdentifier];
    
    FMResultSet *_resultSet = [_database executeQuery:[NSString stringWithFormat:@"select * from %@ where %@ = ?", STORE_TABLE_NAME, COLUMN_IDENTIFIER], _storeId];
    
    while ([_resultSet next]) {
        
        _store = [[StoreMExperience alloc]init];
        _store.identifier = [_resultSet intForColumn:COLUMN_IDENTIFIER];
        _store.address = [_resultSet stringForColumn:COLUMN_STORE_ADDRESS];
        _store.email = [_resultSet stringForColumn:COLUMN_STORE_EMAIL];
        _store.image = [_resultSet stringForColumn:COLUMN_STORE_IMAGE];
        _store.latitude = [_resultSet stringForColumn:COLUMN_STORE_LATITUDE];
        _store.longitude = [_resultSet stringForColumn:COLUMN_STORE_LONGITUDE];
        _store.name = [_resultSet stringForColumn:COLUMN_STORE_NAME];
        _store.webSiteName = [_resultSet stringForColumn:COLUMN_STORE_WEBSITE];
        _store.webSiteUrl = [_resultSet stringForColumn:COLUMN_STORE_WEBSITE_URL];
        _store.city = [_resultSet stringForColumn:COLUMN_STORE_CITY];
        _store.postalCode = [_resultSet stringForColumn:COLUMN_STORE_POSTALCODE];
        _store.imageDetail =  [_resultSet stringForColumn:COLUMN_STORE_IMG_DETAIL];
        _store.icon =  [_resultSet stringForColumn:COLUMN_STORE_ICON];
        _store.htmlDescription = [_resultSet stringForColumn:COLUMN_STORE_DESCRIPTION];
    }
    
    [_resultSet close];
    [_database close];
    
    return _store;
}


@end
