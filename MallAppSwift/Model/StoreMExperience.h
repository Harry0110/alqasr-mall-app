//
//  StoreMExperience.h
//  Unity-iPhone
//
//  Created by Magela Santana Gonzalez on 8/13/15.
//
//

#import <Foundation/Foundation.h>

@interface StoreMExperience : NSObject

@property(nonatomic, assign) int identifier;
@property(nonatomic, strong) NSString *address;
@property(nonatomic, strong) NSString *email;
@property(nonatomic, strong) NSString *image;
@property(nonatomic, strong) NSString *latitude;
@property(nonatomic, strong) NSString *longitude;
@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSString *webSiteName;
@property(nonatomic, strong) NSString *webSiteUrl;
@property(nonatomic, strong) NSString *city;
@property(nonatomic, strong) NSString *postalCode;
@property(nonatomic, strong) NSString *imageDetail;
@property(nonatomic, strong) NSString *icon;
@property(nonatomic, strong) NSString *htmlDescription;


+ (NSMutableArray*) getStoresThatSalesProduct:(int)aProductId;
+ (NSMutableArray*) getStoresThatSalesProductType:(int)aProductType;
+ (NSMutableArray*) getStoresThatSalesAtLessProductType:(NSMutableArray*)aProductTypeList;
+ (NSMutableArray*) getStores;
+ (NSString*) getStoresPhones:(int) aStoreIdentifier;
+ (StoreMExperience*) getStoreByIdentifier:(int)aStoreIdentifier;

@end
