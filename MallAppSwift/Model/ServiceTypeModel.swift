//
//  ServiceModel.swift
//  MallAppSwift
//
//  Created by Freelancer on 24/06/19.
//  Copyright © 2019 Arianne Peiso. All rights reserved.
//

import Foundation

class ServicesTypeModel : NSObject{
    var tid: Int32?
    var ttitle :String?
    var tdescription :String?
}
