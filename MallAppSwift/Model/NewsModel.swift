//
//  NewsModel.swift
//  MallAppSwift
//
//  Created by Freelancer on 03/07/19.
//  Copyright © 2019 Arianne Peiso. All rights reserved.
//


import Foundation

class NewsModel : NSObject{
    var id: Int32?
    var title :String?
    var images: String?
    var shortDescription:String?
    var Ndescription:String?
    var date:Double?
    var created:Double?
    var thumbnails:String?
}
