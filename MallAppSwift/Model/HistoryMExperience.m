//
//  HistoryMExperience.m
//  Unity-iPhone
//
//  Created by Magela Santana Gonzalez on 9/2/15.
//
//

#import "HistoryMExperience.h"
#import "FMDB.h"
#import "CommonsUtils.h"

#define TABLE_NAME @"histories"
#define PRODUCT_HISTORY_TABLE_NAME @"products_histories"

//TABLE PRODUCTS-HISTORIES COLUMNS NAME
#define COLUMN_PRODUCT_ID @"id_product"
#define COLUMN_HISTORY_ID  @"id_history"

//TABLE HISTORIES COLUMNS NAME
#define COLUMN_IDENTIFIER @"id"
#define COLUMN_BANNER @"banner"
#define COLUMN_SHORT_DESCRIPTION @"shortDescription"
#define COLUMN_DESCRIPTION @"description"
#define COLUMN_TITLE @"title"
#define COLUMN_THUMBNAILS @"thumbnails"
#define COLUMN_WEIGHT @"weight"
#define COLUMN_BANNER_SMARTPHONE @"bannerSmartphone"

@implementation HistoryMExperience


+ (NSMutableArray*) getHistories{
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    NSMutableArray *_historiesList = [[NSMutableArray alloc] init];
    if (![_database open])
    {
        return _historiesList;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    FMResultSet *_resultSet = [_database executeQuery:[NSString stringWithFormat:@"select * from %@", TABLE_NAME]];
    while ([_resultSet next]) {
        
        HistoryMExperience *history = [[HistoryMExperience alloc] init];
        //parse data
        history.identifier = [_resultSet intForColumn:COLUMN_IDENTIFIER];
        history.banner = [_resultSet stringForColumn:COLUMN_BANNER];
        history.historyDescription = [_resultSet stringForColumn:COLUMN_DESCRIPTION];
        history.shortDescription = [_resultSet stringForColumn:COLUMN_SHORT_DESCRIPTION];
        history.title = [_resultSet stringForColumn:COLUMN_TITLE];
        history.thumbnails = [_resultSet stringForColumn:COLUMN_THUMBNAILS];
        history.weight = [_resultSet intForColumn:COLUMN_WEIGHT];
        history.banner_smartphone = [_resultSet stringForColumn:COLUMN_BANNER_SMARTPHONE];
        if(history.banner_smartphone != nil && ![history.banner_smartphone isEqualToString:@""])
            history.banner = history.banner_smartphone;

       [_historiesList addObject:history];
    }
    
    [_resultSet close];
    [_database close];
    
    return _historiesList;
}

+ (NSMutableArray*) getProductsIdentifierByHistory: (int) aHistoryIdentifier{
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    NSMutableArray *_productsIdentifierList = [[NSMutableArray alloc] init];
    if (![_database open])
    {
        return _productsIdentifierList;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    NSNumber *identifier = [[NSNumber alloc] initWithInt:aHistoryIdentifier];
    
    FMResultSet *_resultSet = [_database executeQuery:[NSString stringWithFormat:@"select * from %@ where %@ = ?", PRODUCT_HISTORY_TABLE_NAME, COLUMN_HISTORY_ID], identifier];
    while ([_resultSet next]) {
        
        NSString *productIdentifier;
        //parse data
        productIdentifier = [[NSString alloc] initWithFormat:@"%i", [_resultSet intForColumn:COLUMN_PRODUCT_ID]];
        
        
        [_productsIdentifierList addObject:productIdentifier];
    }
    
    [_resultSet close];
    [_database close];
    
    return _productsIdentifierList;
}

- (NSComparisonResult)compare:(HistoryMExperience *)otherHistory {
    
    if (_weight > otherHistory.weight) {
        return NSOrderedDescending;
    }
    else if (_weight < otherHistory.weight) {
        return NSOrderedAscending;
    }
    else {
        return NSOrderedSame;
    }
}

+ (NSMutableArray*) getStoriesAssociatedWithProducts{
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    NSMutableArray *_storyList = [[NSMutableArray alloc] init];
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return _storyList;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    FMResultSet *_resultSet = [_database executeQuery:@"SELECT DISTINCT h.id, h.title FROM histories h INNER JOIN products_histories ph ON h.id=ph.id_history ORDER BY h.id"];
    while ([_resultSet next]) {
        
       HistoryMExperience *_story = [[HistoryMExperience alloc] init];
        //parse data
        _story.identifier = [_resultSet intForColumn:COLUMN_IDENTIFIER];
        _story.title = [_resultSet stringForColumn:COLUMN_TITLE];
        [_storyList addObject:_story];
    }
    
    [_resultSet close];
    [_database close];
    return _storyList;
}


+ (NSMutableArray*) getStoriesAssociatedWithEvent: (int) aEventIdentifier  whithProductType: (int) aProductTypeIdentifier {
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    NSMutableArray *_storyList = [[NSMutableArray alloc] init];
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return _storyList;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
      
    NSString *_query = @"SELECT DISTINCT  h.id, h.title, h.weight FROM histories h INNER JOIN products_histories ph ON h.id=ph.id_history LEFT JOIN products_types pt ON ph.id_product = pt.id_product LEFT JOIN products_events pe ON ph.id_product = pe.id_product WHERE";
    
    
    NSString *_conditionalQuery;
    
    if(aEventIdentifier != -1)
        _conditionalQuery = [NSString stringWithFormat:@" pe.id_event = %i", aEventIdentifier];
    
    if(_conditionalQuery != nil && aProductTypeIdentifier != -1)
        _conditionalQuery = [NSString stringWithFormat:@"%@ AND pt.id_type = %i", _conditionalQuery, aProductTypeIdentifier];
    else if(aProductTypeIdentifier != -1)
        _conditionalQuery = [NSString stringWithFormat:@"pt.id_type = %i", aProductTypeIdentifier];
    
    
    _query = [NSString stringWithFormat:@"%@ %@", _query, _conditionalQuery];
    
    FMResultSet *_resultSet = [_database executeQuery:_query];
    
    
    while ([_resultSet next]) {
        
        HistoryMExperience *_story = [[HistoryMExperience alloc] init];
        //parse data
        _story.identifier = [_resultSet intForColumn:COLUMN_IDENTIFIER];
        _story.title = [_resultSet stringForColumn:COLUMN_TITLE];
        [_storyList addObject:_story];
    }
    
    [_resultSet close];
    [_database close];
    return _storyList;
}

+ (BOOL) existHistories {
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return false;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    NSString * _query = [NSString stringWithFormat: @"SELECT Count(%@) FROM %@", COLUMN_IDENTIFIER, TABLE_NAME];
    
    int _count = [_database intForQuery:_query];
    
    [_database close];
    
    return _count > 0;
}


@end
