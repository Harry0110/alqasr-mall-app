//
//  ProductTypeMExperience.h
//  Unity-iPhone
//
//  Created by Magela Santana Gonzalez on 9/10/15.
//
//

#import <Foundation/Foundation.h>

@interface ProductTypeMExperience : NSObject

@property (nonatomic, assign) int identifier;
@property (nonatomic, strong) NSString *title;

+ (NSMutableArray*) getProductTypes;
+ (NSMutableArray*) getProductTypesAssociatedWithProducts;
+ (NSMutableArray*) getProductTypesAssociatedWithEvents: (int) aEventIdentifier withStory: (int) aStoryIdentifier;

@end
