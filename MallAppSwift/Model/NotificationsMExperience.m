//
//  NotificationsMExperience.m
//  Unity-iPhone
//
//  Created by Magela Santana Gonzalez on 1/13/16.
//
//

#import "NotificationsMExperience.h"
#import "FMDB.h"
#import "CommonsUtils.h"
#import "MarkersUsedMExperience.h"
#import "OperatorMExperience.h"
#import "MarkerMExperience.h"

#define COLUMN_IDENTIFIER @"id" //used in markers and markers notification tables
#define COLUMN_TITLE @"title" //used in markers and markers notification tables

#define TABLE_NAME @"markers_notifications"
#define TABLE_MARKER_NAME @"markers"
#define TABLE_MARKERS_NOTIFICATIONS_MARKERS @"markers_notifications_markers"
#define TABLE_GENERIC_NOTIFICATIONS @"generic_notifications"

#define TABLE_NOTIFICATION_NOTIFICATION_STATUS @"notification_notification_status" // table in internaldb database
#define TABLE_NOTIFICATION_GENERIC_NOTIFICATION_STATUS @"notification_generic_notification_status" //table in internal database for generic notifications
#define COLUMN_NOTIFICATION_ID @"notification_id" //table in internaldb database
#define COLUMN_NOTIFICATION_STATUS_ID @"notification_status_id" //table in internaldb database
#define COLUMN_NOTIFICATION_RECEIVED_DATE @"notification_received_date"

#define COLUMN_SHORT_DESCRIPTION @"shortDescription"
#define COLUMN_TUMBNAILS @"thumbnails"
#define COLUMN_IMAGES @"images"
#define COLUMN_DATE @"date"

//columns in table markers_notification_markers
#define COLUMN_TIMESUSED @"timesUsed"
#define COLUMN_DESCRIPTION @"description"
#define COLUMN_MARKER_IDENTIFIER @"id_marker"
#define COLUMN_OPERATOR_IDENTIFIER @"id_operator" 
#define COLUMN_ID_NOTIF @"id_notif"



@implementation NotificationsMExperience


+ (NSMutableArray*) getNotifications{
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    NSMutableArray *_notificationList = [[NSMutableArray alloc] init];
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return _notificationList;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    FMResultSet *_resultSet = [_database executeQuery:[NSString stringWithFormat:@"select * from %@", TABLE_NAME]];
    
    while ([_resultSet next]) {
        
        NotificationsMExperience *_notification = [[NotificationsMExperience alloc] init];
        //parse data
        _notification.identifier = [_resultSet intForColumn:COLUMN_IDENTIFIER];
        _notification.title = [_resultSet stringForColumn:COLUMN_TITLE];
        _notification.notificationDescription = [_resultSet stringForColumn:COLUMN_DESCRIPTION];
        _notification.notificationShortDescription = [_resultSet stringForColumn:COLUMN_SHORT_DESCRIPTION];
        _notification.status = [_notification getNotificationSatus];
        _notification.notificationDate = [_resultSet dateForColumn:COLUMN_DATE];
        _notification.image = [_resultSet stringForColumn:COLUMN_TUMBNAILS];
        _notification.bannerImg = [_resultSet stringForColumn:COLUMN_IMAGES];
        _notification.receivedDate = [_notification getNotificationReceivedDate];
        [_notificationList addObject:_notification];
    }
    
    [_resultSet close];
    [_database close];
    
    return _notificationList;
}


+ (NSMutableArray*) getGenericNotifications{
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    NSMutableArray *_notificationList = [[NSMutableArray alloc] init];
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return _notificationList;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    FMResultSet *_resultSet = [_database executeQuery:[NSString stringWithFormat:@"select * from %@", TABLE_GENERIC_NOTIFICATIONS]];
    
    while ([_resultSet next]) {
        
        NotificationsMExperience *_notification = [[NotificationsMExperience alloc] init];
        //parse data
        _notification.isGenericNotification = TRUE;
        _notification.identifier = [_resultSet intForColumn:COLUMN_IDENTIFIER];
        _notification.title = [_resultSet stringForColumn:COLUMN_TITLE];
        _notification.notificationDescription = [_resultSet stringForColumn:COLUMN_DESCRIPTION];
        _notification.notificationShortDescription = [_resultSet stringForColumn:COLUMN_SHORT_DESCRIPTION];
        _notification.status = [_notification getNotificationSatus];
        _notification.notificationDate = [_resultSet dateForColumn:COLUMN_DATE];
        _notification.image = [_resultSet stringForColumn:COLUMN_TUMBNAILS];
        _notification.bannerImg = [_resultSet stringForColumn:COLUMN_IMAGES];
        _notification.receivedDate = [_notification getNotificationReceivedDate];
       
        [_notificationList addObject:_notification];
    }
    
    [_resultSet close];
    [_database close];
    
    return _notificationList;
}


+ (BOOL) existNotificationInAppDB:(NSInteger) aNotificationIdentifier {
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    BOOL exist = FALSE;
    
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return exist;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    FMResultSet *_resultSet = [_database executeQuery:[NSString stringWithFormat:@"select * from %@ where %@ = ?", TABLE_NAME, COLUMN_IDENTIFIER],[NSNumber numberWithInteger:aNotificationIdentifier]];
    while ([_resultSet next]) {
        exist = TRUE;
    }
    
    // close the result set.
    // it'll also close when it's dealloc'd, but we're closing the database before
    // the autorelease pool closes, so sqlite will complain about it.
    [_resultSet close];
    [_database close];
    
    return exist;
}


+ (BOOL) existGenericNotificationInAppDB:(NSInteger) aNotificationIdentifier {
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    BOOL exist = FALSE;
    
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return exist;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    FMResultSet *_resultSet = [_database executeQuery:[NSString stringWithFormat:@"select * from %@ where %@ = ?", TABLE_GENERIC_NOTIFICATIONS, COLUMN_IDENTIFIER],[NSNumber numberWithInteger:aNotificationIdentifier]];
    while ([_resultSet next]) {
        exist = TRUE;
    }
    
    // close the result set.
    // it'll also close when it's dealloc'd, but we're closing the database before
    // the autorelease pool closes, so sqlite will complain about it.
    [_resultSet close];
    [_database close];
    
    return exist;
}

+ (NSMutableArray*) getNotificationsIdentifiersListInInternalDB: (BOOL) aSearchGenericNotifications{
    
    FMDatabase *_internalDB = [[CommonsUtils getCommonUtil] getInternalDB];
    NSMutableArray *_notificationIdentifierList = [[NSMutableArray alloc] init];
    if (![_internalDB open])
    {
        NSLog(@"Problems to open DB, please check");
        return _notificationIdentifierList;
    }
    
    FMDBQuickCheck(![_internalDB hasOpenResultSets])
    
    NSString *_tbNotificationName = TABLE_NOTIFICATION_NOTIFICATION_STATUS;
    if(aSearchGenericNotifications)
        _tbNotificationName = TABLE_NOTIFICATION_GENERIC_NOTIFICATION_STATUS;
    
    FMResultSet *_resultSet = [_internalDB executeQuery:[NSString stringWithFormat:@"select * from %@ where %@ != ?", _tbNotificationName, COLUMN_NOTIFICATION_STATUS_ID], [NSNumber numberWithInt:ENotificationDeleted]];
    
    while ([_resultSet next]) {
        
        NSNumber *identifier = [NSNumber numberWithInt:[_resultSet intForColumn:COLUMN_NOTIFICATION_ID]];
        [_notificationIdentifierList addObject:identifier];
    }
    
    
    [_resultSet close];
    [_internalDB close];
    
    return _notificationIdentifierList;
}

- (ENotificationStatus) getNotificationSatus
{
    
    FMDatabase *_internalDB = [[CommonsUtils getCommonUtil] getInternalDB];
    ENotificationStatus _notificationStatus = ENotificationReceived;
    if (![_internalDB open])
    {
        NSLog(@"Problems to open DB, please check");
        return -1;
    }
    
    FMDBQuickCheck(![_internalDB hasOpenResultSets])
    
    NSString *_tbNotificationName = TABLE_NOTIFICATION_NOTIFICATION_STATUS;
    if(_isGenericNotification)
        _tbNotificationName = TABLE_NOTIFICATION_GENERIC_NOTIFICATION_STATUS;
    
    FMResultSet *_resultSet = [_internalDB executeQuery:[NSString stringWithFormat:@"select * from %@ where %@ = ?", _tbNotificationName, COLUMN_NOTIFICATION_ID],[NSNumber numberWithInteger:self.identifier]];
    
    while ([_resultSet next]) {
        
        int status = [_resultSet intForColumn:COLUMN_NOTIFICATION_STATUS_ID];
        switch (status) {
            case 1:
                _notificationStatus = ENotificationReaded;
                break;
            case 2:
                _notificationStatus = EnotificationUnreaded;
                break;
            case 3:
                _notificationStatus = ENotificationDeleted;
                break;
            case 4:
                _notificationStatus = ENotificationReceived;
                break;
            default:
                break;
        }
        
    }
    
    // close the result set.
    // it'll also close when it's dealloc'd, but we're closing the database before
    // the autorelease pool closes, so sqlite will complain about it.
    [_resultSet close];
    [_internalDB close];
    
    return _notificationStatus;
}

- (NSTimeInterval) getNotificationReceivedDate
{
    
    FMDatabase *_internalDB = [[CommonsUtils getCommonUtil] getInternalDB];
    
    if (![_internalDB open])
    {
        NSLog(@"Problems to open DB, please check");
        return -1;
    }
    
    FMDBQuickCheck(![_internalDB hasOpenResultSets])
    
    NSString *_tbNotificationName = TABLE_NOTIFICATION_NOTIFICATION_STATUS;
    if(_isGenericNotification)
        _tbNotificationName = TABLE_NOTIFICATION_GENERIC_NOTIFICATION_STATUS;
    
    FMResultSet *_resultSet = [_internalDB executeQuery:[NSString stringWithFormat:@"select * from %@ where %@ = ?", _tbNotificationName, COLUMN_NOTIFICATION_ID],[NSNumber numberWithInteger:self.identifier]];
    
    while ([_resultSet next]) {
        
        _receivedDate = [_resultSet doubleForColumn:COLUMN_NOTIFICATION_RECEIVED_DATE];
    }
    
    // close the result set.
    // it'll also close when it's dealloc'd, but we're closing the database before
    // the autorelease pool closes, so sqlite will complain about it.
    [_resultSet close];
    [_internalDB close];
    
    return _receivedDate;
}


- (BOOL) updateNotificationStatus: (ENotificationStatus)aNotificationStatus {
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getInternalDB];
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return FALSE;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    NSMutableDictionary *_dictionaryArgs = [NSMutableDictionary dictionary];
    
    self.status = aNotificationStatus;
    [_dictionaryArgs setObject:[NSNumber numberWithInteger:self.status] forKey:COLUMN_NOTIFICATION_STATUS_ID];
    [_dictionaryArgs setObject:[NSNumber numberWithInteger:self.identifier] forKey:COLUMN_NOTIFICATION_ID];
    
    NSString *_tbNotificationName = TABLE_NOTIFICATION_NOTIFICATION_STATUS;
    if(_isGenericNotification)
        _tbNotificationName = TABLE_NOTIFICATION_GENERIC_NOTIFICATION_STATUS;
    
    NSString* _sql = [NSString stringWithFormat:@"update %@ set %@ = :%@ where %@ = :%@", _tbNotificationName, COLUMN_NOTIFICATION_STATUS_ID, COLUMN_NOTIFICATION_STATUS_ID, COLUMN_NOTIFICATION_ID, COLUMN_NOTIFICATION_ID];
    
    FMDBQuickCheck([_database executeUpdate:_sql withParameterDictionary:_dictionaryArgs]);
    
    // close the result set.
    // it'll also close when it's dealloc'd, but we're closing the database before
    // the autorelease pool closes, so sqlite will complain about it.
    [_database close];
    
    return TRUE;
}

- (BOOL) setNotificationReceivedDate {
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getInternalDB];
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return FALSE;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    NSMutableDictionary *_dictionaryArgs = [NSMutableDictionary dictionary];
    
    [_dictionaryArgs setObject:[NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]] forKey:COLUMN_NOTIFICATION_RECEIVED_DATE];
    [_dictionaryArgs setObject:[NSNumber numberWithInteger:self.identifier] forKey:COLUMN_NOTIFICATION_ID];
    
    NSString *_tbNotificationName = TABLE_NOTIFICATION_NOTIFICATION_STATUS;
    if(_isGenericNotification)
        _tbNotificationName = TABLE_NOTIFICATION_GENERIC_NOTIFICATION_STATUS;
    
    NSString* _sql = [NSString stringWithFormat:@"update %@ set %@ = :%@ where %@ = :%@", _tbNotificationName, COLUMN_NOTIFICATION_RECEIVED_DATE, COLUMN_NOTIFICATION_RECEIVED_DATE, COLUMN_NOTIFICATION_ID, COLUMN_NOTIFICATION_ID];
    
    FMDBQuickCheck([_database executeUpdate:_sql withParameterDictionary:_dictionaryArgs]);
    
    // close the result set.
    // it'll also close when it's dealloc'd, but we're closing the database before
    // the autorelease pool closes, so sqlite will complain about it.
    [_database close];
    
    return TRUE;
}

- (NSMutableArray*) getAssociatedMarkersListToNotification
{
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    NSMutableArray *_markersList = [[NSMutableArray alloc] init];
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return _markersList;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    FMResultSet *_resultSet = [_database executeQuery: [NSString stringWithFormat: @"select %@.%@, %@.%@, %@.%@, %@.%@, %@.%@, %@.%@ from %@ inner join %@ on %@.%@ = %@.%@ where %@.%@ = ?", TABLE_MARKERS_NOTIFICATIONS_MARKERS, COLUMN_ID_NOTIF, TABLE_MARKERS_NOTIFICATIONS_MARKERS, COLUMN_MARKER_IDENTIFIER, TABLE_MARKERS_NOTIFICATIONS_MARKERS, COLUMN_TIMESUSED, TABLE_MARKERS_NOTIFICATIONS_MARKERS, COLUMN_OPERATOR_IDENTIFIER, TABLE_MARKER_NAME, COLUMN_TITLE, TABLE_MARKER_NAME, COLUMN_IDENTIFIER, TABLE_MARKERS_NOTIFICATIONS_MARKERS, TABLE_MARKER_NAME, TABLE_MARKERS_NOTIFICATIONS_MARKERS, COLUMN_MARKER_IDENTIFIER, TABLE_MARKER_NAME, COLUMN_IDENTIFIER, TABLE_MARKERS_NOTIFICATIONS_MARKERS, COLUMN_ID_NOTIF], [NSNumber numberWithInt:self.identifier]];
                                                       
    
    while ([_resultSet next]) {
        
        MarkerMExperience * _marker = [[MarkerMExperience alloc] init];
        _marker.markerName = [_resultSet stringForColumn:COLUMN_TITLE];
        _marker.markerId = [_resultSet intForColumn:COLUMN_MARKER_IDENTIFIER];
        _marker.markerTimesUsed = [_resultSet intForColumn:COLUMN_TIMESUSED];
        _marker.markerOperator = [_resultSet intForColumn:COLUMN_OPERATOR_IDENTIFIER];
        [_markersList addObject:_marker];
    }
    
    [_resultSet close];
    [_database close];
    
    return _markersList;
}

+ (BOOL) addNotificationToInternalDB:(NotificationsMExperience*) aNotificationToAdd {
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getInternalDB];
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return FALSE;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    NSMutableDictionary *_dictionaryArgs = [NSMutableDictionary dictionary];
    [_dictionaryArgs setObject:[NSNumber numberWithInteger:aNotificationToAdd.identifier] forKey:COLUMN_NOTIFICATION_ID];
    [_dictionaryArgs setObject:[NSNumber numberWithInteger:aNotificationToAdd.status] forKey:COLUMN_NOTIFICATION_STATUS_ID];
    
    NSString *_tbNotificationName = TABLE_NOTIFICATION_NOTIFICATION_STATUS;
    if(aNotificationToAdd.isGenericNotification)
        _tbNotificationName = TABLE_NOTIFICATION_GENERIC_NOTIFICATION_STATUS;
    
    NSString* _sql = [NSString stringWithFormat:@"insert into %@ (%@, %@) values (:%@, :%@)",
                      _tbNotificationName, COLUMN_NOTIFICATION_ID, COLUMN_NOTIFICATION_STATUS_ID, COLUMN_NOTIFICATION_ID, COLUMN_NOTIFICATION_STATUS_ID];
    FMDBQuickCheck([_database executeUpdate:_sql withParameterDictionary:_dictionaryArgs]);
    
    // close the result set.
    // it'll also close when it's dealloc'd, but we're closing the database before
    // the autorelease pool closes, so sqlite will complain about it.
    [_database close];
    
    return TRUE;
}

+ (BOOL) removeNotificationFromInternalDB:(NSInteger) aNotificationIdentifier {
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getInternalDB];
    BOOL _result = FALSE;
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return FALSE;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    NSMutableDictionary *_dictionaryArgs = [NSMutableDictionary dictionary];
    [_dictionaryArgs setObject:[NSNumber numberWithInteger:aNotificationIdentifier] forKey:COLUMN_NOTIFICATION_ID];
    
    
    NSString* _sql = [NSString stringWithFormat:@"delete from %@ where %@ = :%@",
                      TABLE_NOTIFICATION_NOTIFICATION_STATUS, COLUMN_NOTIFICATION_ID, COLUMN_NOTIFICATION_ID];
    _result = [_database executeUpdate:_sql withParameterDictionary:_dictionaryArgs];
    
    // close the result set.
    // it'll also close when it's dealloc'd, but we're closing the database before
    // the autorelease pool closes, so sqlite will complain about it.
    
    [_database close];
    return _result;
}

+ (BOOL) existNotificationInInternalDB:(NSInteger) aNotificationIdentifier {
    
    FMDatabase *_internalDb = [[CommonsUtils getCommonUtil] getInternalDB];
    BOOL exist = FALSE;
    
    if (![_internalDb open])
    {
        NSLog(@"Problems to open DB, please check");
        return exist;
    }
    
    FMDBQuickCheck(![_internalDb hasOpenResultSets])
    
    FMResultSet *_resultSet = [_internalDb executeQuery:[NSString stringWithFormat:@"select * from %@ where %@ = ?", TABLE_NOTIFICATION_NOTIFICATION_STATUS, COLUMN_NOTIFICATION_ID],[NSNumber numberWithInteger:aNotificationIdentifier]];
    while ([_resultSet next]) {
        exist = TRUE;
    }
    
    // close the result set.
    // it'll also close when it's dealloc'd, but we're closing the database before
    // the autorelease pool closes, so sqlite will complain about it.
    [_resultSet close];
    [_internalDb close];
    
    return exist;
}


+ (BOOL) removeGenericNotificationFromInternalDB:(NSInteger) aNotificationIdentifier {
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getInternalDB];
    BOOL _result = FALSE;
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return FALSE;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    NSMutableDictionary *_dictionaryArgs = [NSMutableDictionary dictionary];
    [_dictionaryArgs setObject:[NSNumber numberWithInteger:aNotificationIdentifier] forKey:COLUMN_NOTIFICATION_ID];
    
    
    NSString* _sql = [NSString stringWithFormat:@"delete from %@ where %@ = :%@",
                      TABLE_NOTIFICATION_GENERIC_NOTIFICATION_STATUS, COLUMN_NOTIFICATION_ID, COLUMN_NOTIFICATION_ID];
    _result = [_database executeUpdate:_sql withParameterDictionary:_dictionaryArgs];
    
    // close the result set.
    // it'll also close when it's dealloc'd, but we're closing the database before
    // the autorelease pool closes, so sqlite will complain about it.
    
    [_database close];
    return _result;
}

+ (BOOL) existGenericNotificationInInternalDB:(NSInteger) aNotificationIdentifier {
    
    FMDatabase *_internalDb = [[CommonsUtils getCommonUtil] getInternalDB];
    BOOL exist = FALSE;
    
    if (![_internalDb open])
    {
        NSLog(@"Problems to open DB, please check");
        return exist;
    }
    
    FMDBQuickCheck(![_internalDb hasOpenResultSets])
    
    FMResultSet *_resultSet = [_internalDb executeQuery:[NSString stringWithFormat:@"select * from %@ where %@ = ?", TABLE_NOTIFICATION_GENERIC_NOTIFICATION_STATUS, COLUMN_NOTIFICATION_ID],[NSNumber numberWithInteger:aNotificationIdentifier]];
    while ([_resultSet next]) {
        exist = TRUE;
    }
    
    // close the result set.
    // it'll also close when it's dealloc'd, but we're closing the database before
    // the autorelease pool closes, so sqlite will complain about it.
    [_resultSet close];
    [_internalDb close];
    
    return exist;
}


+ (void) updateInternalDbWithServerNotifications
{
    NSMutableArray * serverNotificationList = [NotificationsMExperience getNotifications];
    NSMutableArray * localNotificationList = [NotificationsMExperience getNotificationsIdentifiersListInInternalDB:FALSE];
    
    if(serverNotificationList == nil || [serverNotificationList count] == 0) return;
   
    //remove notifications in internal db that not exist in server anymore
    if(localNotificationList != nil && [localNotificationList count] > 0)
        for (NSNumber *notificationId in localNotificationList) {
        if(![NotificationsMExperience existNotificationInAppDB:[notificationId integerValue]])
            [NotificationsMExperience removeNotificationFromInternalDB:[notificationId integerValue]];
    }
    
    //add new server notifications to internal db
    for (NotificationsMExperience* notification in serverNotificationList) {
        if(![NotificationsMExperience existNotificationInInternalDB:notification.identifier])
        {
            notification.status = ENotificationReceived;
            [NotificationsMExperience addNotificationToInternalDB:notification];
        }
        else
        {
            if(notification.status == EnotificationUnreaded)
            {
                BOOL _canShow = [notification canNotificationShowToUser];
                if(!_canShow)
                    [notification updateNotificationStatus:ENotificationReceived];
            }
 
        }
    }
}

+ (void) updateInternalDbWithGenericServerNotifications
{
    NSMutableArray * serverNotificationList = [NotificationsMExperience getGenericNotifications];
    NSMutableArray * localNotificationList = [NotificationsMExperience getNotificationsIdentifiersListInInternalDB: TRUE];
    
    if(serverNotificationList == nil || [serverNotificationList count] == 0) return;
    
    //remove notifications in internal db that not exist in server anymore
    if(localNotificationList != nil && [localNotificationList count] > 0)
        for (NSNumber *notificationId in localNotificationList) {
            if(![NotificationsMExperience existGenericNotificationInAppDB:[notificationId integerValue]])
                [NotificationsMExperience removeGenericNotificationFromInternalDB:[notificationId integerValue]];
        }
    
    //add new server notifications to internal db
    for (NotificationsMExperience* notification in serverNotificationList) {
        if(![NotificationsMExperience existGenericNotificationInInternalDB:notification.identifier])
        {
            notification.status = ENotificationReceived;
            [NotificationsMExperience addNotificationToInternalDB:notification];
            [notification setNotificationReceivedDate];
            
        }
    }
    
}


-(BOOL) canNotificationShowToUser
{
    if([self getNotificationSatus] == ENotificationDeleted)
        return FALSE;
    else
    {
        if(_isGenericNotification)
            return TRUE;
        else
        {
            BOOL _canShow = FALSE;
            for (MarkerMExperience *_marker in _associatedMarkersList) {
                
                MarkersUsedMExperience *markerUsedObject= [MarkersUsedMExperience existMarkerWithNameInDB:_marker.markerName];
                if(markerUsedObject != nil)
                {
                    int timesUsed = markerUsedObject.markerTimesUsed;
                    OperatorMExperience *_associatedOperator = [OperatorMExperience getOperatorByIdentifier:_marker.markerOperator];
                    if(_associatedOperator != nil)
                    {
                        _canShow = [_associatedOperator getIfSatisficeRule:_marker.markerTimesUsed markerTimesUsed:timesUsed];
                        if(_canShow) return _canShow;
                        
                    }
                }
            }
            return _canShow;
        }
    }
}


- (NSComparisonResult)compare:(NotificationsMExperience *)otherNotification {
    
    NSTimeInterval _otherNotificationDate = otherNotification.receivedDate;
    if(_otherNotificationDate == 0)
        _otherNotificationDate = [otherNotification.notificationDate timeIntervalSince1970];
    NSTimeInterval _currentNotificationDate= _receivedDate;
    if(_currentNotificationDate == 0)
        _currentNotificationDate = [_notificationDate timeIntervalSince1970];
    if (_currentNotificationDate > _otherNotificationDate) {
        return NSOrderedAscending;
    }
    else if (_currentNotificationDate <_otherNotificationDate) {
        return NSOrderedDescending;
    }
    else {
        return NSOrderedSame;
    }
}



@end
