//
//  ProductGroupMExperience.h
//  Unity-iPhone
//
//  Created by Magela Santana Gonzalez on 8/9/15.
//
//

#import <Foundation/Foundation.h>
#import "ProductMExperience.h"

@interface ProductGroupMExperience : NSObject <ProductProtocol>

@property (nonatomic, assign) int identifier;
@property (nonatomic, strong) NSString *banner;
@property (nonatomic, strong) NSString *groupDescription;
@property (nonatomic, strong) NSString *shortDescription;
@property (nonatomic, strong) NSString *thumbnails;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *characterImage;
@property (nonatomic, strong) NSString *detailImage;
@property (nonatomic, assign) int weight;

+ (NSMutableArray*) getGroups;
+ (NSMutableArray*) getProductsIdentifierByGroup: (int) aGroupIdentifier;
+ (BOOL) isProductBelogToGroup: (int) aProductIdentifier;
+ (BOOL) exitsGroupWithImage:(NSString*)aImageName;
- (NSString*) objectDescription;
+ (NSMutableArray*) getGroupsInStory: (int) aStoryIdentifier;
+ (BOOL) existGroup;
@end
