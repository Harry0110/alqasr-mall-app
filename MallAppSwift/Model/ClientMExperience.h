//
//  ClientMExperience.h
//  Unity-iPhone
//
//  Created by Magela Santana Gonzalez
//
//

#import <Foundation/Foundation.h>

@interface ClientMExperience : NSObject

@property (nonatomic, assign) int identifier;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *image;
@property (nonatomic, strong) NSString *banner;
@property (nonatomic, strong) NSString *thumbnails;
@property (nonatomic, strong) NSString *detailImage;
@property (nonatomic, strong) NSString *clientDescription;
@property (nonatomic, strong) NSString *shortDescription;
@property (nonatomic, strong) NSString *banner_smartphone;

+ (NSMutableArray*) getClients;
+ (ClientMExperience*) getClientByIdentifier:(int)aClientIdentifier;
+ (BOOL) existClient;
@end
