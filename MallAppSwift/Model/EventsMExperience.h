//
//  EventsMExperience.h
//  Unity-iPhone
//
//  Created by Magela Santana Gonzalez on 21/12/17.
//
//

#import <Foundation/Foundation.h>

@interface EventsMExperience : NSObject

@property (nonatomic, assign) int identifier;
@property (nonatomic, strong) NSString *title;

+ (NSMutableArray*) getEventsAssociatedWithProducts;
+ (NSMutableArray*) getEventsByProducttype: (int) aProductTypeIdentifier withStory:(int) aStoryIdentifier;
+ (BOOL) existEvents;
@end
