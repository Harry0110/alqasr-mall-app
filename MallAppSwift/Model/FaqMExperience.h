//
//  FaqMExperience.h
//  Unity-iPhone
//
//  Created by Magela Santana Gonzalez on 8/20/15.
//
//

#import <Foundation/Foundation.h>

@interface FaqMExperience : NSObject

@property (nonatomic, strong) NSString * question;
@property (nonatomic, strong) NSString *answer;
@property (nonatomic, assign) int order;

+ (NSMutableArray*) getFaqContent;

@end
