//
//  VuforiaMExperience.h
//  Unity-iPhone
//
//  Created by Magela Santana Gonzalez on 9/16/15.
//
//

#import <Foundation/Foundation.h>

@interface VuforiaMExperience : NSObject

@property (nonatomic, strong) NSString *dbUrl;
@property (nonatomic, strong) NSString *xmlUrl;
@property (nonatomic, strong) NSString *modifiedDate;
@property (nonatomic, assign) int order;

+ (NSMutableArray*) getVuforiaDbsDataList;
- (NSComparisonResult)compareDataByOrder:(VuforiaMExperience*) aOtherVuforiaData;

@end
