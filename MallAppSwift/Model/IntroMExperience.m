//
//  StoreMExperience.m
//  Unity-iPhone
//
//  Created by Magela Santana Gonzalez on 8/13/15.
//
//

#import "IntroMExperience.h"
#import "FMDB.h"
#import "CommonsUtils.h"

#define TABLE_NAME @"intro_videos"

// COLUMNS NAME
#define COLUMN_VIDEO_URL @"videoUrl"
#define COLUMN_SUBTITLE_URL @"subtitleUrl"
#define COLUMN_ACTIVE @"active"
#define COLUMN_SHOW_APP_START @"showAppStart"


@implementation IntroMExperience


+ (IntroMExperience*) getVideoIntroDataFromDb {
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    IntroMExperience *_videoIntro = nil;
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return _videoIntro;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    FMResultSet *_resultSet = [_database executeQuery:[NSString stringWithFormat:@"select * from %@ where %@ = ?", TABLE_NAME, COLUMN_ACTIVE], [NSNumber numberWithInt:1]];
    
    while ([_resultSet next]) {
        
        _videoIntro = [[IntroMExperience alloc] init];
        //parse data
        _videoIntro.videoUrl = [_resultSet stringForColumn:COLUMN_VIDEO_URL];
        _videoIntro.subtitleUrl = [_resultSet stringForColumn:COLUMN_SUBTITLE_URL];
        _videoIntro.showAppStart = [_resultSet boolForColumn:COLUMN_SHOW_APP_START];
    }
    
    [_resultSet close];
    [_database close];
    
    return _videoIntro;
}

+ (BOOL) showDemoVideoatAppStart
{
    BOOL _showVideo = FALSE;
    IntroMExperience *_introVideo = [IntroMExperience getVideoIntroDataFromDb];
    if(_introVideo != nil && _introVideo.showAppStart && [IntroMExperience getSavedVideoUrl] != nil && [_introVideo.videoUrl isEqualToString:[IntroMExperience getSavedVideoUrl]])
        _showVideo = TRUE;
    return _showVideo;
}


+ (BOOL) getIfDownloadedVideoIsActive
{
    BOOL _isActive = FALSE;
    IntroMExperience *_introVideo = [IntroMExperience getVideoIntroDataFromDb];
    if(_introVideo != nil && [IntroMExperience getSavedVideoUrl] != nil && [_introVideo.videoUrl isEqualToString:[IntroMExperience getSavedVideoUrl]])
        _isActive = TRUE;
    return _isActive;
}

+ (void) saveVideoIntroUrl:(NSString*) aVideoUrl
{
    NSString* lang = [CommonsUtils getAppLanguage];
    NSString *key = [NSString stringWithFormat:@"%@%@", lang, COLUMN_VIDEO_URL];
    [CommonsUtils saveValueAsPreference:aVideoUrl key:key];
}

+ (void) saveSubtitleIntroUrl:(NSString*) aSubtitleUrl
{
    NSString* lang = [CommonsUtils getAppLanguage];
    NSString *key = [NSString stringWithFormat:@"%@%@", lang, COLUMN_SUBTITLE_URL];
    [CommonsUtils saveValueAsPreference:aSubtitleUrl key:key];
}


+ (NSString*) getSavedVideoUrl
{
    NSString* lang = [CommonsUtils getAppLanguage];
    NSString *key = [NSString stringWithFormat:@"%@%@", lang, COLUMN_VIDEO_URL];
    return [CommonsUtils getPreferenceValueForKey:key];
}

+ (BOOL) getIfVideoUpdateIsNeeded: (NSString*) aNewVideoUrl
{
    NSString* lang = [CommonsUtils getAppLanguage];
    NSString *key = [NSString stringWithFormat:@"%@%@", lang, COLUMN_VIDEO_URL];
    
    NSString *videoUrlSavedValue = [CommonsUtils getPreferenceValueForKey:key];
    if(videoUrlSavedValue != nil && [videoUrlSavedValue isEqualToString:aNewVideoUrl])
        return FALSE;
    else
        return TRUE;
    return FALSE;
}

+ (BOOL) getIfSubtitleUpdateIsNeeded: (NSString*) aNewSubtitleUrl
{
    NSString* lang = [CommonsUtils getAppLanguage];
    NSString *key = [NSString stringWithFormat:@"%@%@", lang, COLUMN_SUBTITLE_URL];
    
    NSString *subtitleUrlSavedValue = [CommonsUtils getPreferenceValueForKey:key];
    if(subtitleUrlSavedValue != nil && [subtitleUrlSavedValue isEqualToString:aNewSubtitleUrl])
        return FALSE;
    else
        return TRUE;
    return FALSE;
}


@end
