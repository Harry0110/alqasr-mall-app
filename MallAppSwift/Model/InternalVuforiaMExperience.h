//
//  InternalVuforiaMExperience.h
//  Unity-iPhone
//
//  Created by Magela Santana Gonzalez
//


#import <Foundation/Foundation.h>

@interface InternalVuforiaMExperience : NSObject

@property (nonatomic, strong) NSString *dbName;
@property (nonatomic, strong) NSString *dbLastModifiedDate;
@property (nonatomic, assign) int downloadCount;

+ (NSMutableArray*) getVuforiaDbsInDevice;
+ (BOOL) addVuforiaDataToDB:(InternalVuforiaMExperience*) aVuforiaDataToAdd;
- (BOOL) updateModificationDate : (NSString*) aModificationDate;
+ (NSString*) getVuforiaDbModificationDate: (NSString *) aVuforiaDbName;
+ (BOOL) removeVuforiaDataFromDb:(NSString*) aVuforiaDbName;
+ (InternalVuforiaMExperience*) getVuforiaDbData: (NSString *) aVuforiaDbName;

@end
