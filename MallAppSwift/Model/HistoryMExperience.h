//
//  HistoryMExperience.h
//  Unity-iPhone
//
//  Created by Magela Santana Gonzalez on 9/2/15.
//
//

#import <Foundation/Foundation.h>

@interface HistoryMExperience : NSObject

@property (nonatomic, assign) int identifier;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *shortDescription;
@property (nonatomic, strong) NSString *historyDescription;
@property (nonatomic,strong) NSString *banner;
@property (nonatomic, strong) NSString *thumbnails;
@property (nonatomic, assign) int weight;
@property (nonatomic, strong) NSString *banner_smartphone;

+ (NSMutableArray*) getHistories;
+ (NSMutableArray*) getProductsIdentifierByHistory: (int) aHistoryIdentifier;
- (NSComparisonResult)compare:(HistoryMExperience *)otherHistory;
+ (NSMutableArray*) getStoriesAssociatedWithProducts;
+ (NSMutableArray*) getStoriesAssociatedWithEvent: (int) aEventIdentifier  whithProductType: (int) aProductTypeIdentifier;
+ (BOOL) existHistories;

@end
