//
//  CategoryMExperience.m
//  Unity-iPhone
//
//  Created by Magela Santana Gonzalez
//
//

#import "CategoryMExperience.h"
#import "FMDB.h"
#import "CommonsUtils.h"

#define TABLE_NAME @"category"
#define COLUMN_NAME_IDENTIFIER @"tid"
#define COLUMN_NAME_NAME @"title"
#define COLUMN_NAME_DESCRIPTION @"description"
#define COLUMN_NAME_GENERIC_ID @"genericId"
#define COLUMN_NAME_WEIGHT @"weight"

@implementation CategoryMExperience


+ (NSMutableArray*) getCategoriesForClient:(int) aClientIdentifier
{
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    NSMutableArray *_categoriesList = [[NSMutableArray alloc] init];
    
     NSNumber *_clientId = [NSNumber numberWithInt:aClientIdentifier];
    
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return _categoriesList;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    FMResultSet *_resultSet = [_database executeQuery:@"select distinct title, tid, genericId, weight from category c inner join products_categories pc on pc.id_category = c.tid where pc.id_product IN ( select p.id from products p inner join products_clients pcl on p.id=pcl.id_product where pcl.id_client = ? ) order by weight", _clientId];
    
    while ([_resultSet next]) {
        
        CategoryMExperience *_category = [[CategoryMExperience alloc]init];
        
        //parse data
        _category.identifier = [_resultSet intForColumn:COLUMN_NAME_IDENTIFIER];
        _category.title = [_resultSet stringForColumn:COLUMN_NAME_NAME];
//        _category.categDescription = [_resultSet stringForColumn:COLUMN_NAME_DESCRIPTION];
        _category.genericId = [_resultSet stringForColumn:COLUMN_NAME_GENERIC_ID];
        
        [_categoriesList addObject:_category];
    }
    
    [_resultSet close];
    [_database close];
    
    return _categoriesList;
}



@end
