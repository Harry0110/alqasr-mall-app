//
//  MarkerMExperience.h
//  Unity-iPhone
//

#import <Foundation/Foundation.h>

@interface MarkerMExperience : NSObject

@property (nonatomic, strong) NSString *markerName;
@property (nonatomic, assign) int markerId;
@property (nonatomic, assign) int markerTimesUsed;
@property (nonatomic, assign) int markerOperator;

+ (NSString*) getMyXpeThumbnailAssociatedToMarker: (NSString*) aMarkerName;

@end
