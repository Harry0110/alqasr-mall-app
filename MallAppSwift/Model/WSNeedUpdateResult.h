//
//  WSNeedUpdateResult.h
//  Unity-iPhone
//
//

#import <Foundation/Foundation.h>

@interface WSNeedUpdateResult : NSObject

@property (nonatomic, assign) BOOL required;
@property (nonatomic, assign) BOOL updateAvailable;
@end
