//
//  NotificationsMExperience.h
//  Unity-iPhone
//
//  Created by Magela Santana Gonzalez on 1/13/16.
//
//

#import <Foundation/Foundation.h>


typedef enum {
    ENotificationReaded = 1,
    EnotificationUnreaded = 2,
    ENotificationDeleted = 3,
    ENotificationReceived = 4, //this is an local state that means the notification was copy to db from server, no means that was present to user
} ENotificationStatus;


//Entity of markers_notification table in app local database. Represents a notification associated to 1..* markers.


@interface NotificationsMExperience : NSObject

@property (nonatomic, assign) int identifier;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *notificationDescription;
@property (nonatomic, strong) NSString *notificationShortDescription;
@property (nonatomic, assign) int status; //readed from notification_notification_status from internaldb database
@property (nonatomic, strong) NSString *image;
@property (nonatomic, strong) NSString *bannerImg;
@property (nonatomic, strong) NSDate *notificationDate;
@property (nonatomic, assign) NSTimeInterval receivedDate;
@property (nonatomic, strong) NSMutableArray* associatedMarkersList;
@property (nonatomic, assign) BOOL isGenericNotification;

//this method use app local database
+ (NSMutableArray*) getNotifications;
+ (NSMutableArray*) getGenericNotifications;
+ (BOOL) existNotificationInAppDB:(NSInteger) aNotificationIdentifier;

//this methods use internaldb database

+ (NSMutableArray*) getNotificationsIdentifiersListInInternalDB: (BOOL) aSearchGenericNotifications;
+ (BOOL) existNotificationInInternalDB:(NSInteger) aNotificationIdentifier;
+ (BOOL) addNotificationToInternalDB:(NotificationsMExperience*) aNotificationToAdd;
+ (BOOL) removeNotificationFromInternalDB:(NSInteger) aNotificationIdentifier;
- (BOOL) updateNotificationStatus: (ENotificationStatus)aNotificationStatus;
- (ENotificationStatus) getNotificationSatus;
- (NSTimeInterval) getNotificationReceivedDate;

+ (void) updateInternalDbWithServerNotifications;
- (BOOL) canNotificationShowToUser;
- (NSComparisonResult)compare:(NotificationsMExperience *)otherNotification;
- (BOOL) setNotificationReceivedDate;
- (NSMutableArray*) getAssociatedMarkersListToNotification;
+ (BOOL) removeGenericNotificationFromInternalDB:(NSInteger) aNotificationIdentifier;
+ (BOOL) existGenericNotificationInInternalDB:(NSInteger) aNotificationIdentifier;

+ (void) updateInternalDbWithGenericServerNotifications;






@end
