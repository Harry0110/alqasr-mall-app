//
//  EventsMExperience.m
//  Unity-iPhone
//
//  Created by Magela Santana Gonzalez on 21/12/2017.
//
//

#import "EventsMExperience.h"
#import "FMDB.h"
#import "CommonsUtils.h"
#import "FMDatabaseAdditions.h"

#define TABLE_NAME @"events"
#define COLUMN_IDENTIFIER @"tid"
#define COLUMN_TITLE @"title"

@implementation EventsMExperience

+ (NSMutableArray*) getEventsAssociatedWithProducts{
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    NSMutableArray *_eventsList = [[NSMutableArray alloc] init];
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return _eventsList;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    FMResultSet *_resultSet = [_database executeQuery:@"SELECT DISTINCT e.tid, e.title FROM events e INNER JOIN products_events pe ON e.tid=pe.id_event ORDER BY e.tid"];
    while ([_resultSet next]) {
        
        EventsMExperience *_event = [[EventsMExperience alloc] init];
        //parse data
        _event.identifier = [_resultSet intForColumn:COLUMN_IDENTIFIER];
        _event.title = [_resultSet stringForColumn:COLUMN_TITLE];
        [_eventsList addObject:_event];
    }
    
    [_resultSet close];
    [_database close];
    return _eventsList;
}

+ (NSMutableArray*) getEventsByProducttype: (int) aProductTypeIdentifier withStory:(int) aStoryIdentifier{
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    NSMutableArray *_eventsList = [[NSMutableArray alloc] init];
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return _eventsList;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    NSString *_query = @"SELECT DISTINCT e.tid, e.title FROM events e INNER JOIN products_events pe ON e.tid=pe.id_event LEFT JOIN products_histories ph ON pe.id_product = ph.id_product LEFT JOIN products_types pt ON pe.id_product = pt.id_product WHERE";
    
    NSString *_conditionalQuery;
    
    if(aStoryIdentifier != -1)
        _conditionalQuery = [NSString stringWithFormat:@"ph.id_history = %i", aStoryIdentifier];
    
    if(_conditionalQuery != nil && aProductTypeIdentifier != -1)
        _conditionalQuery = [NSString stringWithFormat:@"%@ AND pt.id_type = %i", _conditionalQuery, aProductTypeIdentifier];
    else if (aProductTypeIdentifier != -1)
        _conditionalQuery = [NSString stringWithFormat:@"pt.id_type = %i", aProductTypeIdentifier];
    
    
    _query = [NSString stringWithFormat:@"%@ %@", _query, _conditionalQuery];
    
    FMResultSet *_resultSet = [_database executeQuery:_query];
   
    while ([_resultSet next]) {
        
        EventsMExperience *_event = [[EventsMExperience alloc] init];
        //parse data
        _event.identifier = [_resultSet intForColumn:COLUMN_IDENTIFIER];
        _event.title = [_resultSet stringForColumn:COLUMN_TITLE];
        [_eventsList addObject:_event];
    }
    
    [_resultSet close];
    [_database close];
    return _eventsList;
}

+ (BOOL) existEvents {

    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return false;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    NSString * _query = [NSString stringWithFormat: @"SELECT Count(%@) FROM %@", COLUMN_IDENTIFIER, TABLE_NAME];
    
    int _count = [_database intForQuery:_query];
    
    [_database close];
    
    return _count > 0;
}


@end
