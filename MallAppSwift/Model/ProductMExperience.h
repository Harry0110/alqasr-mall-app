//
//  ProductMExperience.h
//  Unity-iPhone
//
//  Created by Magela Santana Gonzalez on 8/9/15.
//
//

#import <Foundation/Foundation.h>

#define GENERIC_ID_FOR_PRODUCTS @"CATEGORY_PRODUCT"
#define GENERIC_ID_FOR_EVENT @"CATEGORY_EVENT"

@protocol ProductProtocol <NSObject>

@required
- (NSString*) banner;
- (int) identifier;
- (NSString*) objectDescription;
- (NSString*) shortDescription;
- (NSString*) thumbnails;
- (NSString*) title;
- (NSString*) characterImage;
- (NSString*) detailImage;
- (int) weight;

- (NSComparisonResult)compareWithProtocol:(id<ProductProtocol>)productProtocol;

@end

@interface ProductMExperience : NSObject <ProductProtocol>

@property (nonatomic, assign) int identifier;
@property (nonatomic, strong) NSString *banner;
@property (nonatomic, strong) NSString *characterImage;
@property (nonatomic, strong) NSString *productDescription;
@property (nonatomic, strong) NSString *detailImage;
@property (nonatomic, strong) NSString *thumbnails;
@property (nonatomic, strong) NSString *sku;
@property (nonatomic, assign) float price;
//@property (nonatomic, strong) NSString *currency;
@property (nonatomic, assign) int promoted;
@property (nonatomic, strong) NSString *shortDescription;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) int status;
@property (nonatomic, assign) int weight;
@property (nonatomic, assign) BOOL isEvent;
@property (nonatomic, assign) BOOL canReceiveMarker;
@property (nonatomic, strong) NSString *banner_smartphone;


+ (NSMutableArray*) getproducts;
+ (NSMutableArray*) getEvents;
+ (ProductMExperience*) getProductByIdentifier:(int)aProductIdentifier;   
- (NSMutableArray*) getGalleryList;
+ (NSMutableArray*) getProductsSalesInStore:(int)aStoreIdentifier;
+ (BOOL) exitsProductWithImage:(NSString*)aImageName;
+ (NSMutableArray*) getProductsInStory:(int) aStoryIdentifier;
- (NSComparisonResult)compare:(ProductMExperience *)otherProduct;
- (NSString*) objectDescription;
+ (NSMutableArray*) getProductsForClientAndCategory:(int) aClientIdentifier category:(NSString*)aCategoryGenericId;
+ (ProductMExperience*) getAssociatedProductByMarkerName:(NSString*)aMarkerName;
- (NSComparisonResult)compareToMesXperiencesView:(ProductMExperience *)otherProduct;
+ (NSMutableArray*) getProductsOutOffGroup;
+ (NSMutableArray*) getProductsOutOfGroupsInStory:(int) aStoryIdentifier;
+ (NSMutableArray*) getAssociatedProductByEvent:(int)aEventIdentifier withProductType:(int) aProductTypeIdentifier withStory:(int) aStoryIdentifier;
+ (BOOL) existProductsOutsideGroups;
+ (BOOL) existEvents;

@end
