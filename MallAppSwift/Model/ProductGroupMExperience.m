//
//  ProductGroupMExperience.m
//  Unity-iPhone
//
//  Created by Magela Santana Gonzalez on 8/9/15.
//
//

#import "ProductGroupMExperience.h"
#import "FMDB.h"
#import "CommonsUtils.h"
#import "FMDatabaseAdditions.h"

#define GROUPS_TABLE_NAME @"groups"
#define PRODUCTS_GROUPS_TABLE_NAME @"products_groups"


//GROUP TABLE COLUMNS NAME
#define COLUMN_IDENTIFIER @"id"
#define COLUMN_BANNER @"banner"
#define COLUMN_CHARACTER_IMAGE @"characterImage"
#define COLUMN_DESCRIPTION @"description"
#define COLUMN_DETAIL_IMG @"detailsImage"
#define COLUMN_THUMBNAIL @"thumbnails"
#define COLUMN_SHORT_DESCRIPTION @"shortDescription"
#define COLUMN_TITLE @"title"
#define COLUMN_WEIGHT @"weight"

//PRODUCTS-GROUPS COLUMN NAME
#define COLUMN_PRODUCT_IDENTIFIER @"id_product"
#define COLUMN_GROUP_IDENTIFIER @"id_group"


@implementation ProductGroupMExperience

+ (NSMutableArray*) getGroups{
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    NSMutableArray *_groupList = [[NSMutableArray alloc] init];
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return _groupList;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    FMResultSet *_resultSet = [_database executeQuery:[NSString stringWithFormat:@"select * from %@", GROUPS_TABLE_NAME]];
    while ([_resultSet next]) {
        
        ProductGroupMExperience *group = [[ProductGroupMExperience alloc] init];
        //parse data
        group.identifier = [_resultSet intForColumn:COLUMN_IDENTIFIER];
        group.banner = [_resultSet stringForColumn:COLUMN_BANNER];
        group.characterImage = [_resultSet stringForColumn:COLUMN_CHARACTER_IMAGE];
        group.groupDescription = [_resultSet stringForColumn:COLUMN_DESCRIPTION];
        group. detailImage = [_resultSet stringForColumn:COLUMN_DETAIL_IMG];
        group.thumbnails = [_resultSet stringForColumn:COLUMN_THUMBNAIL];
        group.shortDescription = [_resultSet stringForColumn:COLUMN_SHORT_DESCRIPTION];
        group. title = [_resultSet stringForColumn:COLUMN_TITLE];
        group.weight = [_resultSet intForColumn:COLUMN_WEIGHT];
     
        [_groupList addObject:group];
    }
    
    [_resultSet close];
    [_database close];
    
    return _groupList;
}

+ (NSMutableArray*) getGroupsInStory: (int)aStoryIdentifier {
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    NSMutableArray *_groupList = [[NSMutableArray alloc] init];
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return _groupList;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    
    NSNumber *_storyIdentifier = [[NSNumber alloc] initWithInt:aStoryIdentifier];
    
    FMResultSet *_resultSet = [_database executeQuery: @"select DISTINCT g.id, g.thumbnails, g.title, g.weight, g.description, g.banner, g.detailsImage from groups g left join products_groups pg on g.id=pg.id_group where pg.id_product in ( select ph.id_product from products_histories ph left join products p on p.id = ph.id_product where ph.id_history = ?)", _storyIdentifier];
  
    while ([_resultSet next]) {
        
        ProductGroupMExperience *group = [[ProductGroupMExperience alloc] init];
        //parse data
        group.identifier = [_resultSet intForColumn:COLUMN_IDENTIFIER];
        group.thumbnails = [_resultSet stringForColumn:COLUMN_THUMBNAIL];
        group. title = [_resultSet stringForColumn:COLUMN_TITLE];
        group.weight = [_resultSet intForColumn:COLUMN_WEIGHT];
        group.groupDescription = [_resultSet stringForColumn:COLUMN_DESCRIPTION];
        group.banner = [_resultSet stringForColumn:COLUMN_BANNER];
        group. detailImage = [_resultSet stringForColumn:COLUMN_DETAIL_IMG];
        [_groupList addObject:group];
    }
    
    [_resultSet close];
    [_database close];
    
    return _groupList;
}


+ (NSMutableArray*) getProductsIdentifierByGroup: (int) aGroupIdentifier{
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    NSMutableArray *_productsIdentifierList = [[NSMutableArray alloc] init];
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return _productsIdentifierList;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
     NSNumber *identifier = [[NSNumber alloc] initWithInt:aGroupIdentifier];
    
   FMResultSet *_resultSet = [_database executeQuery:[NSString stringWithFormat:@"select * from %@ where %@ = ?", PRODUCTS_GROUPS_TABLE_NAME, COLUMN_GROUP_IDENTIFIER], identifier];
    while ([_resultSet next]) {
        
        NSString *productIdentifier;
        //parse data
        productIdentifier = [[NSString alloc] initWithFormat:@"%i", [_resultSet intForColumn:COLUMN_PRODUCT_IDENTIFIER]];
    
        
        [_productsIdentifierList addObject:productIdentifier];
    }
    
    [_resultSet close];
    [_database close];
    
    return _productsIdentifierList;
}

+ (BOOL) exitsGroupWithImage:(NSString*)aImageName{
    
    FMDatabaseQueue *_database = [[CommonsUtils getCommonUtil] getLocalQueueDB];
    
    __block BOOL _exits = FALSE;
    
    [_database inDatabase:^(FMDatabase *db) {
        
        NSString* _like = [NSString stringWithFormat:@"%%%@%%", aImageName];
        FMResultSet *_resultSet = [db executeQuery:[NSString stringWithFormat:@"select * from %@ where %@ like ? ", GROUPS_TABLE_NAME, COLUMN_THUMBNAIL], _like];
        _exits = [_resultSet next];
        [_resultSet close];
        
    }];
    
    [_database close];
    return _exits;
}

+ (BOOL) isProductBelogToGroup: (int) aProductIdentifier{
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return FALSE;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    NSNumber *identifier = [[NSNumber alloc] initWithInt:aProductIdentifier];
    //NSInteger count = [_database intForQuery:[NSString stringWithFormat:@"select count(%@) from %@ where %@ = ?", COLUMN_PRODUCT_IDENTIFIER, PRODUCTS_GROUPS_TABLE_NAME, COLUMN_PRODUCT_IDENTIFIER], aProductIdentifier];
    
    NSInteger count = [_database intForQuery:@"select count(id_product) from products_groups where id_product = ?", identifier];
    
    [_database close];
    
    return count > 0 ? TRUE : FALSE;
}

- (NSString*) objectDescription{
    return _groupDescription;
}

- (NSComparisonResult)compareWithProtocol:(id<ProductProtocol>)productProtocol {
    
    if (_weight > [productProtocol weight]) {
        return NSOrderedDescending;
    }
    else if (_weight < [productProtocol weight]) {
        return NSOrderedAscending;
    }
    else {
        return NSOrderedSame;
    }
}

+ (BOOL) existGroup {
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return false;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    int _count = [_database intForQuery:[NSString stringWithFormat:@"select Count(%@) from %@", COLUMN_IDENTIFIER, GROUPS_TABLE_NAME]];
    
    [_database close];
    
    return _count > 0;
}

@end
