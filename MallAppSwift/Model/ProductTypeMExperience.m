//
//  ProductTypeMExperience.m
//  Unity-iPhone
//
//  Created by Magela Santana Gonzalez on 9/10/15.
//
//

#import "ProductTypeMExperience.h"
#import "FMDB.h"
#import "CommonsUtils.h"
#import "FMDatabaseAdditions.h"

#define TABLE_NAME @"producttype"
#define COLUMN_IDENTIFIER @"tid"
#define COLUMN_TITLE @"title"

@implementation ProductTypeMExperience

+ (NSMutableArray*) getProductTypes{
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    NSMutableArray *_productTypesList = [[NSMutableArray alloc] init];
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return _productTypesList;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    FMResultSet *_resultSet = [_database executeQuery:[NSString stringWithFormat:@"select * from %@", TABLE_NAME]];
    while ([_resultSet next]) {
        
        ProductTypeMExperience *productType = [[ProductTypeMExperience alloc] init];
        //parse data
        productType.identifier = [_resultSet intForColumn:COLUMN_IDENTIFIER];
        productType.title = [_resultSet stringForColumn:COLUMN_TITLE];
        [_productTypesList addObject:productType];
    }
    
    [_resultSet close];
    [_database close];
    return _productTypesList;
}


+ (NSMutableArray*) getProductTypesAssociatedWithProducts{
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    NSMutableArray *_productTypesList = [[NSMutableArray alloc] init];
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return _productTypesList;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    FMResultSet *_resultSet = [_database executeQuery:@"SELECT DISTINCT t.tid, t.title FROM producttype t INNER JOIN products_types pt ON t.tid=pt.id_type ORDER BY t.tid"];
    while ([_resultSet next]) {
        
        ProductTypeMExperience *productType = [[ProductTypeMExperience alloc] init];
        //parse data
        productType.identifier = [_resultSet intForColumn:COLUMN_IDENTIFIER];
        productType.title = [_resultSet stringForColumn:COLUMN_TITLE];
        [_productTypesList addObject:productType];
    }
    
    [_resultSet close];
    [_database close];
    return _productTypesList;
}

+ (NSMutableArray*) getProductTypesAssociatedWithEvents: (int) aEventIdentifier withStory: (int) aStoryIdentifier {
    
    FMDatabase *_database = [[CommonsUtils getCommonUtil] getLocalDB];
    NSMutableArray *_productTypesList = [[NSMutableArray alloc] init];
    if (![_database open])
    {
        NSLog(@"Problems to open DB, please check");
        return _productTypesList;
    }
    
    FMDBQuickCheck(![_database hasOpenResultSets])
    
    NSString *_query = @"SELECT DISTINCT t.tid, t.title FROM producttype t INNER JOIN products_types pt ON t.tid=pt.id_type LEFT JOIN products_events pe ON pt.id_product = pe.id_product LEFT JOIN products_histories ph ON pt.id_product = ph.id_product WHERE";
    
    
    NSString *_conditionalQuery;
    
    if(aEventIdentifier != -1)
        _conditionalQuery = [NSString stringWithFormat:@" pe.id_event = %i", aEventIdentifier];
    
    if(_conditionalQuery != nil && aStoryIdentifier != -1)
        _conditionalQuery = [NSString stringWithFormat:@"%@ AND ph.id_history = %i", _conditionalQuery, aStoryIdentifier];
    else if (aStoryIdentifier != -1)
        _conditionalQuery = [NSString stringWithFormat:@"ph.id_history = %i", aStoryIdentifier];
    
    _query = [NSString stringWithFormat:@"%@ %@", _query, _conditionalQuery];
    
    FMResultSet *_resultSet = [_database executeQuery:_query];
    
    while ([_resultSet next]) {
        
        ProductTypeMExperience *productType = [[ProductTypeMExperience alloc] init];
        //parse data
        productType.identifier = [_resultSet intForColumn:COLUMN_IDENTIFIER];
        productType.title = [_resultSet stringForColumn:COLUMN_TITLE];
        [_productTypesList addObject:productType];
    }
    
    [_resultSet close];
    [_database close];
    return _productTypesList;
}

@end
