//
//  AppDelegate.swift
//  SampleSDKSwift
//
//  Created by Arianne Peiso on 12/06/2019.
//  Copyright © 2019 Arianne Peiso. All rights reserved.
//

import UIKit
import ARGenieSDK
import IQKeyboardManagerSwift


//@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,ARGenieManagerDownloadCallback {
   
    let bottom = BottomBar()
    let exbottom = ExpandedBottomBar()
    
    
    func didARGenieManagerDownloadFinish(_ error: Error!) {
        
        print("Donwload Complete.")
        
        if "\(UserDefaults.standard.value(forKey: "DeviceLanguage")!)" == "en" ||  "\(UserDefaults.standard.value(forKey: "DeviceLanguage")!)" == "fr" || "\(UserDefaults.standard.value(forKey: "DeviceLanguage")!)" == "es"{
            
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            
        }else if  "\(UserDefaults.standard.value(forKey: "DeviceLanguage")!)" == "ar"{
            
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            
        }
        else
        {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            
        }
        
        let mainViewController = MainViewController()
        
        // Init navigator controller
        let navigatorController = UINavigationController.init(rootViewController: mainViewController)
        
       // navigatorController.setNavigationBarHidden(false, animated: true)
        
        // Set the root view controller of the app's window
        window?.rootViewController = navigatorController
        
        // Make the window visible
        window?.makeKeyAndVisible()
    }
    

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        
        let pre = Locale.preferredLanguages[0]
        
       let string = pre.components(separatedBy: "-")[0]
        
        print(string)
        
        UserDefaults.standard.set(string, forKey: "DeviceLanguage")
        
        print("\(UserDefaults.standard.value(forKey: "DeviceLanguage")!)")
        
        Theme.sharedInstance()?.setActiveThemeByName("Current")
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        
        ARGenieManager.notifyAppStarted(application, options: launchOptions)
        
        // Initialize the window
        window = UIWindow.init(frame: UIScreen.main.bounds)
        
        let emptyViewController = UIViewController()
        
        // Init navigator controller
        let navigatorController = UINavigationController.init(rootViewController: emptyViewController)
        
        navigatorController.setNavigationBarHidden(true, animated: true)
        
        // Set the root view controller of the app's window
        window?.rootViewController = navigatorController
        
        // Make the window visible
        window?.makeKeyAndVisible()
        
        ARGenieManager.shared()?.loadAugmentedRealityData(self)

       
        // Allocate memory for an instance of MinViewController class
       
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        ARGenieManager.shared()?.requestAugmentedRealityStop()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        ARGenieManager.shared()?.requestAugmentedRealityStart()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    
}

