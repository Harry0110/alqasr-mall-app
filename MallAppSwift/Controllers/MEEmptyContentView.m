//
//  MEEmptyContentView.m
//  MagicXperience
//

#import "MEEmptyContentView.h"
#import "Languages.h"
#import "Utility.h"

@implementation MEEmptyContentView


#pragma mark - Instance Creation and Initialization

- (void)internalEmptyViewInitializer
{
    [self setBackgroundColor:[CommonsUtils getPrimaryColor]];
    [_lbMessage setTextColor:[CommonsUtils getComplementaryColor]];
    [_lbMessage setText:NOTIFY_UPDATE_NEEDED];
    [self layoutIfNeeded];
}

+ (instancetype) initEmptyContentView
{
    MEEmptyContentView* _emptyContentView = [[[NSBundle mainBundle] loadNibNamed:@"MEEmptyContentView" owner:nil options:nil] objectAtIndex:0];
    [_emptyContentView internalEmptyViewInitializer];
    return _emptyContentView;
}



#pragma mark - Presentation and Dismiss

- (void)centerInSuperview
{
    if (!self.superview)
    {
        return;
    }
    
    NSArray *constraintArray = [self.superview.constraints copy];
    for (NSLayoutConstraint *constraint in constraintArray)
    {
        if ((constraint.firstItem == self) || (constraint.secondItem == self))
            [self.superview removeConstraint:constraint];
    }
    [self.superview addConstraints:CONSTRAINTS_CENTERING(self)];
}


@end
