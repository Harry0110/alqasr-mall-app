//
//  EventsDetailsVC.swift
//  MallAppSwift
//
//  Created by Freelancer on 28/06/19.
//  Copyright © 2019 Arianne Peiso. All rights reserved.
//

import Foundation
import UIKit

class EventsDetailsVC : BaseViewController, BotoomViewDelegate, ExpandBottomViewDelegate {
        
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var TimeLbl: UILabel!
    @IBOutlet weak var DescLl: UILabel!
    @IBOutlet weak var DateLbl: UILabel!
    @IBOutlet weak var TitleLbl: UILabel!
    @IBOutlet weak var ImageView: UIImageView!
    @IBOutlet weak var timeimage : UIImageView!
    @IBOutlet weak var locationimage : UIImageView!
    var imageString: String?
    var date:String?
    var descriptionString: String?
    var time: Double?
    var location: String?
    var EDtitle: String?
    
    let theme = ThemeClass()
    
    @IBOutlet weak var bottomExpanded: ExpandedBottomBar!
    @IBOutlet weak var bottombar: BottomBar!
    
    override func viewDidLoad() {
    
        if NetworkConnection.isConnectedToNetwork(){
            
        }else {
            
            self.showAlert(Alertmsg: "Alert", msg: networkAlert.networkmsg)
        }
        
      self.bottomExpanded.isHidden = true
        self.bottombar.delegate = self
        self.bottomExpanded.delegate = self
        
        setUpdetails()
        self.showLoader()
        self.bottombar.HomeButton.setImage(UIImage(named: "Home-9"), for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
    }
    
    func setUpdetails(){
        
        let Object:NSDictionary = theme.getButtonView()
        
        let Object2:NSDictionary = theme.getEventsView()
        
        let color = hexStringToUIColor(hex: Object.object(forKey: "BackGroundColor")! as! String)
        
        let color1 = hexStringToUIColor(hex: Object2.object(forKey: "TextColor")! as! String)
        
        let color2 =  hexStringToUIColor(hex: Object2.object(forKey: "DescColor")! as! String)
      
    
        if self.EDtitle != ""{
            
            self.TitleLbl?.text = self.EDtitle
            self.TitleLbl.textColor = color1
            self.TitleLbl.font =  UIFont(name:Object2.object(forKey: "TextFont")! as! String, size: Object2.object(forKey: "TextSize")! as! CGFloat)
        }else{
            
        }
        
        if self.date != "" {
            
            self.DateLbl.text = self.date!
            self.DateLbl.font = UIFont(name:Object2.object(forKey: "DateFont")! as! String, size: Object2.object(forKey: "DateSize")! as! CGFloat)
            self.DateLbl.textColor = color1
            
        }else {}
        if self.descriptionString != "" {
            
            self.DescLl?.text = self.descriptionString?.htmlToString
            self.DescLl.textColor = color2
            self.DescLl.font =  UIFont(name:Object2.object(forKey: "DescFont")! as! String, size: Object2.object(forKey: "DescSize")! as! CGFloat)
            
        }else{}
        
        if self.time != nil {
            
            let date = self.time
            let dateVar = Date(timeIntervalSince1970: TimeInterval(date!))
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone() as TimeZone
            dateFormatter.locale = NSLocale.current
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
            dateFormatter.dateFormat = "mm-dd-yyyy"
            print(dateFormatter.string(from: dateVar))
            dateFormatter.date(from: String(describing: dateVar))
            let updatedTimeStamp = dateVar
            let converteddate = DateFormatter.localizedString(from: updatedTimeStamp as Date, dateStyle: DateFormatter.Style.medium, timeStyle: .none)
            
            self.TimeLbl.text = converteddate
            self.TimeLbl.textColor = color1
            self.TimeLbl.font = UIFont(name:Object2.object(forKey: "OtherFont")! as! String, size: Object2.object(forKey: "DateSize")! as! CGFloat)
            
        }else{
            
            self.timeimage.isHidden = true
            self.TimeLbl.isHidden = true
            
        }
        
        if self.location != nil {
            self.locationLbl.text = self.location
            self.locationLbl.font =  UIFont(name:Object2.object(forKey: "OtherFont")! as! String, size: Object2.object(forKey: "DateSize")! as! CGFloat)
            self.locationLbl.textColor = color1
            
        }else{
            
            self.locationimage.isHidden = true
            self.locationLbl.isHidden = true
        }
        
        if self.imageString != "" {
            
            let catPictureURL = URL(string: self.imageString!)!
            
            // Creating a session object with the default configuration.
            // You can read more about it here https://developer.apple.com/reference/foundation/urlsessionconfiguration
            let session = URLSession(configuration: .default)
            
            // Define a download task. The download task will download the contents of the URL as a Data object and then you can do what you wish with that data.
            let downloadPicTask = session.dataTask(with: catPictureURL) { (data, response, error) in
                // The download has finished.
                if let e = error {
                    print("Error downloading cat picture: \(e)")
                } else {
                    // No errors found.
                    // It would be weird if we didn't have a response, so check for that too.
                    if let res = response as? HTTPURLResponse {
                        print("Downloaded cat picture with response code \(res.statusCode)")
                        if let imageData = data {
                            // Finally convert that Data into an image and do what you wish with it.
                            let image = UIImage(data: imageData)
                            
                            DispatchQueue.main.async {
                                self.ImageView?.image = image
                                self.hideLoader()
                            }
                            // Do something with your image.
                        } else {
                            print("Couldn't get image: Image is nil")
                            self.hideLoader()
                        }
                    } else {
                        print("Couldn't get response code for some reason")
                        self.hideLoader()
                    }
                }
            }
            
            downloadPicTask.resume()
        }
        
    
    }
    
   
    
    func ServicesAction(sender: AnyObject) {
        
        let vc = ServicesViewController()
        
         vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func FloorAction(sender: AnyObject) {
        
        let vc = FloorViewController()
        
         vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func CollapseButtonTapped(sender: AnyObject) {
        
        self.bottombar?.isHidden = false
        
        self.bottomExpanded?.isHidden = true
        
    }
    
    func ShareButtonTapped(sender: AnyObject) {
        
        let textToShare = "Check out this app"
        
        if let myWebsite = NSURL(string: "https://www.google.com/") {
            let objectsToShare: [Any] = [textToShare, myWebsite]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            activityVC.popoverPresentationController?.sourceView = self.bottomExpanded.ShareButton
            self.present(activityVC, animated: true, completion: nil)
        }
        
    }
    
    func ExpandButtonTapped(sender: AnyObject) {
        
        self.bottombar?.isHidden = true
        
        self.bottomExpanded?.isHidden = false
    }
    
    func HomeButtonTapped(sender: AnyObject) {
        
        let vc = MainViewController()
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func ProfilebuttonTapped(sender: AnyObject) {
        
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            let vc = LoginViewController()
            
             vc.ProfileBool = true
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else {
            
            let vc = ProfileVC()
            
             vc.selected = true
            
            self.navigationController?.pushViewController(vc, animated: false)
        }
        
    }
    func ArbuttonTapped(sender: AnyObject) {
        
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            let vc = Tuto1ViewController()
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else {
            
            ARGenieManager.shared()?.loadAugmentedRealityData(nil)
            
        }
    }
    func ArExbuttonTapped(sender: AnyObject) {
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            let vc = Tuto1ViewController()
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else {
            
            ARGenieManager.shared()?.loadAugmentedRealityData(nil)
            
        }
    }
    func FaqbuttonTaped(sender: AnyObject) {
        let vc = FAQViewController()
        
         vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
        
    }
    
    func UserAgreeTapped(sender: AnyObject) {
        
        let vc = UserAgreementVC()
        
         vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func LegalTapped(sender: AnyObject) {
        let vc = LeagalViewController()
        
         vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
    }

    @IBAction func CloseAction(_ sender : UIButton){
        
        //let vc = MainViewController()
        self.navigationController?.popViewController(animated: false)
        //self.navigationController?.popToRootViewController(animated: false)

    }
    
    func LibraryTapped(sender: AnyObject) {
        let vc = LibraryVC()
        
         vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
        
    }
 
    func showsettings(sender: AnyObject) {
        
        self.alertToEncourageCameraAccessInitially()
        
    }
    func showsetting(sender: AnyObject) {
        
        self.alertToEncourageCameraAccessInitially()
        
    }
    func showLibrary(sender: AnyObject) {
        
        self.alertToEncourageLibraryAccessInitially()
    }
}

