//
//  NewsTableViewCell.swift
//  MallAppSwift
//
//  Created by Freelancer on 01/07/19.
//  Copyright © 2019 Arianne Peiso. All rights reserved.
//
import UIKit

class NewsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var DetailButtonOutlet: CurvedButton!
    @IBOutlet weak var DescLbl: UILabel!
    @IBOutlet weak var DateLbl: UILabel!
    @IBOutlet weak var TitleLbl: UILabel!
    @IBOutlet weak var ImageView: UIImageView!
    
    weak var delegate : NewsCellDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    @IBAction func DetailsAction(_ sender: UIButton) {
        
        delegate?.btnTapped(cell: self)
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
