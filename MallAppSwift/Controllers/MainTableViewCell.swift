//
//  MainTableViewCell.swift
//  MallAppSwift
//
//  Created by Freelancer on 21/06/19.
//  Copyright © 2019 Arianne Peiso. All rights reserved.
//

import UIKit

class MainTableViewCell: UITableViewCell {
        
        
        @IBOutlet weak var ImageView: UIImageView!
        
        @IBOutlet weak var TitleLbl: UILabel!
        

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
