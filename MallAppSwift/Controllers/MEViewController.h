//  Abstract class that implements the basis of application screen,
//  with a top bar, gesture to open menu and content view, each
//  view controller that is displayed by menu must subclass it
#import <UIKit/UIKit.h>

#define KTAG_ALERT_POPUP_NO_CNX 456789
#define KTAG_ALERT_POPUP_UPD_AVAILABLE 567890

@class TopBarView, CustomAlert, MenuViewController;

@interface MEViewController : UIViewController


@property (nonatomic, strong) TopBarView *topView;
@property (nonatomic, weak) IBOutlet UIView *contentView;
@property (nonatomic, strong) UIPanGestureRecognizer *navigationBarPanGestureRecognizer;
@property (nonatomic, strong) MenuViewController* menuViewController;
@property (nonatomic, strong) UIView *activityUpdateView;

@property (nonatomic, assign, getter=hasScanButton) BOOL hasScanButton;
@property (nonatomic, assign, getter=showScanButton) BOOL showScanButton;

- (void) enableScanButton:(BOOL) enable;
- (void) setTopViewValues;
- (void) setGestureRecognizeForMenu:(BOOL)addGesture;
- (void) ShowConexionErrorPopup;
-(void) forceOrientationToPortraitMode;
-(void) HideConexionErrorPopup;

@end
