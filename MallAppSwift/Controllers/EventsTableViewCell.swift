//
//  EventsTableViewCell.swift
//  MallAppSwift
//
//  Created by Freelancer on 28/06/19.
//  Copyright © 2019 Arianne Peiso. All rights reserved.
//

import UIKit

class EventsTableViewCell: UITableViewCell {

    @IBOutlet weak var EventsImage: UIImageView!
    @IBOutlet weak var TitleLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var DescriptionLbl: UILabel!
    
    @IBOutlet weak var DetailButtonOutlet: CurvedButton!
    weak var delegate: MyCellDelegate?
    
   
    @IBAction func DetaislAction(_ sender: EventsTableViewCell) {
        
        delegate?.btnTapped(cell: self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()


    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
}
