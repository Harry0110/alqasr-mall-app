//
//  ProfileVC.swift
//  MallAppSwift
//
//  Created by Freelancer on 27/06/19.
//  Copyright © 2019 Arianne Peiso. All rights reserved.
//

import UIKit

class ProfileVC: BaseViewController, UITextFieldDelegate,ExpandBottomViewDelegate,BotoomViewDelegate {
    
    var bool = false
    
    var updatebool = false
    
    @IBOutlet weak var PointsText: UILabel!
    @IBOutlet weak var EditLbl: CurvedButton!
    @IBOutlet weak var ChangePasswordOutlet: UIButton!
    @IBOutlet weak var LoggedInLbl: UILabel!
    @IBOutlet weak var UserProfileLbl: UILabel!
    @IBOutlet weak var bottomExpanded: ExpandedBottomBar!
    @IBOutlet weak var HelloLbl: UILabel!
    
    @IBOutlet weak var LogoutOutlet: CurvedButton!
    @IBOutlet weak var FirstNameLbl: UITextField!
    let service  = UserService()
    
    @IBOutlet weak var bottombar: BottomBar!
    @IBOutlet weak var lastLbl: UITextField!
    
    @IBOutlet weak var LocationLbl: UITextField!
    
    @IBOutlet weak var EmailLbl: UITextField!
    
    @IBOutlet weak var SubmitOutlet: CurvedButton!
    
    var selected : Bool!
    
    let theme = ThemeClass()
    
    override func viewDidLoad() {
        self.navigationController?.navigationBar.isHidden = false
        
        setProfilePlaceholder()
        
        setUi()
        
        getUserInfo()
        
        self.bottomExpanded.isHidden = true
        
        self.bottombar.delegate = self
        self.bottomExpanded.delegate = self
        
        self.EmailLbl.delegate = self
        self.FirstNameLbl.delegate = self
        self.lastLbl.delegate = self
        
        disableInteraction()
        
        if selected == true{
            
            self.bottomExpanded.MypointsButton.setImage(UIImage(named: "My points-9"), for: .normal)
            self.bottombar.HomeButton.setImage(UIImage(named: "Home-9"), for: .normal)
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.CheckIfAccessTokenExpired()
        
        self.ChangePasswordOutlet.isHidden = false
        
    }
    
    func CheckIfAccessTokenExpired(){
        
        let date = Date(timeIntervalSince1970: Double("\(UserDefaults.standard.value(forKey: "Access_Token_Expires")!)")!)
        
        let now = NSDate()
        if( now.timeIntervalSince(date) > 0 ) {
            print("now > dateTimeStamp")
            self.getUserInfo()
        } else {
            
            self.getAccessToken()
        }
        
    }
    
    func getUserInfo(){
        
        if NetworkConnection.isConnectedToNetwork(){
            
            
            service.getuserinfo(with:UserDefaults.standard.value(forKey: "App_Token") as! String,complition:{(myresult) in
                
                DispatchQueue.main.async {
                    
                    if let result = myresult {
                        
                        print(result)
                        
                        if result["error"] != nil
                        {
                            
                            self.getAccessToken()
                        }
                            
                        else
                            
                        {
                            
                            self.disableInteraction()
                            
                            let email = result["email"] as! String
                            
                            let firstname = result["first_name"] as! String
                            
                            let lastname = result["last_name"] as! String
                            
                            let optin = result["optin_newsletters"] as! String
                            
                            let points = result["points"] as! String
                            
                            print(optin)
                            UserDefaults.standard.set(optin, forKey: "Opt")
                            
                            self.FirstNameLbl.text = firstname
                            
                            self.EmailLbl.text = email
                            
                            self.lastLbl.text = lastname
                            
                            self.HelloLbl.text = NSLocalizedString("HELLO \(firstname)", comment: "")
                            
                            self.PointsText.text = NSLocalizedString("YOU HAVE \(points) POINTS", comment: "")
                            
                            if self.bool == true{
                                
                                self.showAlert(Alertmsg: "Success", msg: "Your Profile is updated.")
                                
                            }
                            
                        }
                        
                    }
                }
                
            })
            
        }else {
            
            self.showAlert(Alertmsg: "Alert", msg: networkAlert.networkmsg)
            
            
        }
        
        
    }
    
    
    func Updatepasword(){
        
        if NetworkConnection.isConnectedToNetwork(){
            
            
            print(self.FirstNameLbl.text!)
            
            
            service.UpdateuserPassword(with:self.FirstNameLbl.text!, new_password: self.lastLbl.text!, app_token: UserDefaults.standard.value(forKey: "App_Token") as! String,complition:{(myresult) in
                
                DispatchQueue.main.async {
                    
                    if let result = myresult {
                        print(result)
                        
                        
                        if result["error"] != nil {
                            
                            let error = result["error"] as! String
                            
                            let errordesc = result["error_description"] as! String
                            
                            if error == "invalid_old_password"{
                                
                                self.showAlert(Alertmsg:"Alert" , msg: "Old password is missing or incorrect")
                            }
                            else
                            {
                                
                                self.showAlert(Alertmsg:error , msg: errordesc)

                            }
                            
                        } else {
                            
                            
                            self.setUi()
                            
                            self.setProfilePlaceholder()
                            
                            self.disableInteraction()
                            
                            self.bool = false
                            
                            self.getUserInfo()
                            
                            self.showAlert(Alertmsg: "Success", msg: "Password Modified.")
                            
                        }
                        
                    }
                }
                
                
                
            })
            
        }else {
            
            
            self.showAlert(Alertmsg: "Alert", msg: networkAlert.networkmsg)
            
        }
        
        //
    }
    
    func getAccessToken() {
        
        if NetworkConnection.isConnectedToNetwork(){
            
            
            service.getaccessTokenRevised(with: "refresh_token", client_id:"alqasrid", client_secret: "@lq@sr", refresh_token: UserDefaults.standard.value(forKey: "RefreshToken") as! String, target:self,complition:{(myresult) in
                
                
                DispatchQueue.main.async {
                    
                    if let result = myresult {
                        print(result)
                        
                        if result["error"] != nil {
                            
                            let error = result["error"] as! String
                            
                            let errordesc = result["error_description"] as! String
                            
                            
                            self.showAlert(Alertmsg:"Alert" , msg: "Oops!! unable to get your profile data, please try after some time.")
                            
                        }
                        else
                        {
                            
                            if let accessToken = result["access_token"] as? String {
                                
                                UserDefaults.standard.set(accessToken, forKey: "DeviceId")
                                
                                let refreshToken = result["refresh_token"] as? String
                                
                                UserDefaults.standard.set(refreshToken, forKey: "RefreshToken")
                                
                                self.getUserInfo()
                            }
                            
                            
                        }
                        
                        
                    }
                }
                
                
                
            })
            
        }else {
            
            self.showAlert(Alertmsg: "Alert", msg: networkAlert.networkmsg)
            
            
        }
        
        //
        
        
    }
    
    
    func updateUserinfo () {
        
        
        if NetworkConnection.isConnectedToNetwork(){
            
            
            service.getUpdateuserinfo(with: self.FirstNameLbl.text!, last_name: self.lastLbl.text!, email: self.EmailLbl.text!, optin_newsletters: UserDefaults.standard.value(forKey: "Opt") as! String , app_token: UserDefaults.standard.value(forKey: "App_Token") as! String,target:self,complition:{(myresult) in
                
                DispatchQueue.main.async {
                    
                    if let result = myresult {
                        print(result)
                        
                        self.bool = true
                        
                        self.SubmitOutlet.setTitle("EDIT", for: .normal)
                        
                        self.getUserInfo()
                        
                    }
                }
                
                
                
            })
        }else {
            
            
            self.showAlert(Alertmsg: "Alert", msg: networkAlert.networkmsg)
            
        }
        
    }
    
    func disableInteraction() {
        
        self.FirstNameLbl.isUserInteractionEnabled = false
        self.lastLbl.isUserInteractionEnabled = false
        self.EmailLbl.isUserInteractionEnabled = false
        
    }
    
    
    func enableInteraction() {
        
        self.FirstNameLbl.isUserInteractionEnabled = true
        self.lastLbl.isUserInteractionEnabled = true
        self.EmailLbl.isUserInteractionEnabled = true
        
    }
    
    
    
    @IBAction func SubmitAction(_ sender: UIButton) {
        
        if SubmitOutlet.title(for: .normal) == "EDIT" {
            
            enableInteraction()
            
            self.UserProfileLbl.text = NSLocalizedString("MODIFY YOUR PROFILE", comment: "")
            self.SubmitOutlet.setTitle(NSLocalizedString("SUBMIT", comment: ""), for: .normal)
        }
        else
        {
            
            if self.updatebool == true
            {
                if self.lastLbl.text != self.EmailLbl.text {
                    self.showAlert(Alertmsg: "Alert", msg: "Passwords do not match.")
                    
                }else{
                    
                    self.Updatepasword()
                    
                }
                
            }
            else {
                
                updateUserinfo ()
                
            }
            
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        
        return true
    }
    
    
    @IBAction func LogoutAction(_ sender: UIButton) {
        
        if LogoutOutlet.title(for: .normal) == "LOGOUT"{
            
            UserDefaults.standard.removeObject(forKey: "DeviceId")
            UserDefaults.standard.removeObject(forKey: "App_Token")
            UserDefaults.standard.removeObject(forKey: "refresh_token")
            
            
            let vc = MainViewController()
            
            vc.loggedOut = true
            
            self.navigationController?.pushViewController(vc, animated: false)
        }
        else
        {
            
            let vc = MainViewController()
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        
        
    }
    
    
    func CollapseButtonTapped(sender: AnyObject) {
        
        self.bottombar?.isHidden = false
        
        self.bottomExpanded?.isHidden = true
        //self.bottomExpanded.MypointsButton.setImage(UIImage(named: "My points-9"), for: .normal)
    }
    
    func ShareButtonTapped(sender: AnyObject) {
        
        let textToShare = "Check out this app"
        
        if let myWebsite = NSURL(string: "https://www.google.com/") {
            let objectsToShare: [Any] = [textToShare, myWebsite]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            activityVC.popoverPresentationController?.sourceView = self.bottomExpanded.ShareButton
            self.present(activityVC, animated: true, completion: nil)
        }
        
        
    }
    
    func ExpandButtonTapped(sender: AnyObject) {
        
        self.bottombar?.isHidden = true
        
        self.bottomExpanded?.isHidden = false
        
        self.bottomExpanded.MypointsButton.setImage(UIImage(named: "My points-9"), for: .normal)
    }
    
    func HomeButtonTapped(sender: AnyObject) {
        
        let vc = MainViewController()
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func ServicesAction(sender: AnyObject) {
        
        let vc = ServicesViewController()
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func FloorAction(sender: AnyObject) {
        
        let vc = FloorViewController()
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func ProfilebuttonTapped(sender: AnyObject) {
        
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            let vc = LoginViewController()
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else {
            
            let vc = ProfileVC()
            
            self.navigationController?.pushViewController(vc, animated: false)
        }
        
    }
    
    func ArbuttonTapped(sender: AnyObject) {
        
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            let vc = Tuto1ViewController()
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else {
            
            ARGenieManager.shared()?.loadAugmentedRealityData(nil)
            
        }
    }
    func ArExbuttonTapped(sender: AnyObject) {
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            let vc = Tuto1ViewController()
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else {
            
            ARGenieManager.shared()?.loadAugmentedRealityData(nil)
            
        }
    }
    
    func FaqbuttonTaped(sender: AnyObject) {
        
        let vc = FAQViewController()
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func UserAgreeTapped(sender: AnyObject) {
        let vc = UserAgreementVC()
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func LegalTapped(sender: AnyObject) {
        let vc = LeagalViewController()
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    
    @IBAction func ChangePasswordAction(_ sender: UIButton) {
        
        
        setUiChangePasswordScreen()
        
        setModifyPlaceholder()
        
        self.updatebool = true
        
        enableInteraction()
        
    }
    
    func setUiChangePasswordScreen(){
        
        self.UserProfileLbl.text = NSLocalizedString("MODIFY PASSWORD", comment: "")
        //self.LoggedInLbl.text = ""
        self.SubmitOutlet.setTitle(NSLocalizedString("SUBMIT",comment: ""), for: .normal)
        self.LogoutOutlet.setTitle(NSLocalizedString("CANCEL",comment: ""), for: .normal)
        self.ChangePasswordOutlet.isHidden = true
        self.FirstNameLbl.text = ""
        self.lastLbl.text = ""
        self.EmailLbl.text = ""
        
    }
    
    
    func setUi(){
        
        let Object:NSDictionary = theme.getProfileView()
        
        
        let color = hexStringToUIColor(hex: Object.object(forKey: "TextColor")! as! String)
       
        let color2 = hexStringToUIColor(hex: Object.object(forKey: "TextColor2")! as! String)
        let color3 = hexStringToUIColor(hex: Object.object(forKey: "TextColor3")! as! String)
    
        
        self.HelloLbl.text = NSLocalizedString("HELLO ,", comment: "")
        self.HelloLbl.textColor = color
         self.HelloLbl.font =  UIFont(name:Object.object(forKey: "TextFont")! as! String, size: Object.object(forKey: "TextSize")! as! CGFloat)
        
        self.PointsText.text = NSLocalizedString("YOU HAVE POINTS", comment: "")
        self.PointsText.textColor = color2
        self.PointsText.font = UIFont(name:Object.object(forKey: "TextFont2")! as! String, size: Object.object(forKey: "TextSize2")! as! CGFloat)
        
        
        self.UserProfileLbl.text = NSLocalizedString("USER PROFILE", comment: "")
        self.UserProfileLbl.textColor = color
        self.UserProfileLbl.font = UIFont(name:Object.object(forKey: "TextFont3")! as! String, size: Object.object(forKey: "TextSize3")! as! CGFloat)
        
        
        self.LogoutOutlet.setTitle(NSLocalizedString("LOGOUT", comment: ""), for: .normal)
        self.SubmitOutlet.setTitle(NSLocalizedString("EDIT", comment: ""), for: .normal)
        self.ChangePasswordOutlet.isHidden = false
        
    }
    
    func setProfilePlaceholder(){
        
        self.FirstNameLbl.isSecureTextEntry = false
        self.lastLbl.isSecureTextEntry = false
        self.EmailLbl.isSecureTextEntry = false
        self.FirstNameLbl.placeholder = NSLocalizedString("First Name", comment: "")
        self.lastLbl.placeholder = NSLocalizedString("Last Name", comment: "")
        self.EmailLbl.placeholder = NSLocalizedString("Email", comment: "")
    }
    
    func setModifyPlaceholder(){
        
        self.FirstNameLbl.isSecureTextEntry = false
        self.lastLbl.isSecureTextEntry = true
        self.EmailLbl.isSecureTextEntry = true
        self.FirstNameLbl.placeholder = NSLocalizedString("Old Password", comment: "")
        self.lastLbl.placeholder = NSLocalizedString("New Password", comment: "")
        self.EmailLbl.placeholder = NSLocalizedString("Confirm Password", comment: "")
        
    }
    func LibraryTapped(sender: AnyObject) {
        let vc = LibraryVC()
        
        vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
        
    }
    
    func showsettings(sender: AnyObject) {
        
        self.alertToEncourageCameraAccessInitially()
        
    }
    func showsetting(sender: AnyObject) {
        
        self.alertToEncourageCameraAccessInitially()
        
    }
    
    func showLibrary(sender: AnyObject) {
        
        self.alertToEncourageLibraryAccessInitially()
    }
    
    
}
