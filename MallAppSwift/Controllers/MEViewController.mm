#import "MEViewController.h"
#import "TopBarView.h"
#import "CommonsUtils.h"
#import "NotificationsName.h"
#import "CustomAlert.h"
#import "Languages.h"
#import <ARGenieSDK/ARGConstants.h>
#import <ARGenieSDK/ARGenieManager.h>
#import "MEEmptyContentView.h"

#define kMETOPBAR_TAG 9999

@interface MEViewController ()

//@property (nonatomic, strong) CustomAlert * alertView;
@property (nonatomic, strong) MEEmptyContentView *emptyContentView;
@property (nonatomic, strong) UIButton* scanButton;

@end

@implementation MEViewController

#pragma mark - View Methods

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setTopViewValues];
    [self setGestureRecognizeForMenu:YES];
    
    [self.view setBackgroundColor:[CommonsUtils getPrimaryColor]];
    [_contentView setBackgroundColor:[CommonsUtils getPrimaryColor]];
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_SHOW_CONEXION_ERROR_POPUP object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ShowConexionErrorPopup) name:NOTIFICATION_SHOW_CONEXION_ERROR_POPUP object:nil];
    
    self.view.frame = [UIScreen mainScreen].bounds;
    [self.navigationController setNavigationBarHidden:YES];
    
    if(self.hasScanButton){
        _showScanButton = TRUE;
        _scanButton = [self createScanBtn];
        NSArray<NSLayoutConstraint*>* _scanButtonConstraint = [self getConstraintForScanButton];
        
        [self.contentView addSubview:_scanButton];
        [self.contentView addConstraints:_scanButtonConstraint];
        [self addObserver:self forKeyPath:NSStringFromSelector(@selector(showScanButton)) options:0
                  context:nil];
    }
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self forceOrientationToPortraitMode];
    
}

/*!
 *  Method to initialize the top view with default values
 *  Subclass must call super setTopViewValues and after that
 *  make custom initialization
 */
- (void) setTopViewValues
{
//    UIView* _oldTopView = [self.view viewWithTag:kMETOPBAR_TAG];
//    if (_oldTopView != nil)
//        [_oldTopView removeFromSuperview];
    
    NSArray *_topViews = [[NSBundle mainBundle] loadNibNamed:@"TopBarView" owner:self options:nil];
    _topView = [_topViews objectAtIndex:0];
   // _topView.tag = kMETOPBAR_TAG;
    [self.view addSubview:_topView];
    [_topView setFrame:CGRectMake(_topView.frame.origin.x, _topView.frame.origin.y, self.view.bounds.size.width, TOP_BAR_HEIGHT)];
    [_topView initializeTopBar];
    // _topView.targetController = self.navigationController.parentViewController;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void) forceOrientationToPortraitMode
{
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    [CommonsUtils getCommonUtil].lastOrientation = UIDeviceOrientationPortrait;
}

-(void) viewWillDisappear:(BOOL)animated
{
    [self HideConexionErrorPopup];
//    [self HideAvailableUpdatePopup];
    [super viewWillDisappear:animated];
    
    if(self.hasScanButton){
        [self removeObserver:self forKeyPath:NSStringFromSelector(@selector(showScanButton))];
        [self.scanButton removeFromSuperview];
        self.scanButton = nil;
    }
    
}

/*!
 *  Method to activate/deactivate gesture to open/close menu
 *
 *  @param addGesture YES/NO to activate/deactivate
 */
- (void) setGestureRecognizeForMenu:(BOOL)addGesture
{
    if (addGesture)
    {
        if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)])
        {
            if (_navigationBarPanGestureRecognizer == nil)
                _navigationBarPanGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self.navigationController.parentViewController action:@selector(revealGesture:)];
            
            [self.view removeGestureRecognizer:_navigationBarPanGestureRecognizer];
            [self.view addGestureRecognizer:_navigationBarPanGestureRecognizer];
        }
    }
    else
    {
        if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)])
        {
            [self.view removeGestureRecognizer:_navigationBarPanGestureRecognizer];
        }
    }
}

#pragma mark - No content available popup
-(void) ShowConexionErrorPopup
{
    
    if(![[CommonsUtils getCommonUtil] isLocalDbExits])
    {
        _emptyContentView = [MEEmptyContentView initEmptyContentView];
        [self.contentView addSubview:_emptyContentView];
        [_emptyContentView setFrame:[UIScreen mainScreen].bounds];
        [_emptyContentView centerInSuperview];
    }
  
}

-(void) HideConexionErrorPopup
{
    [_emptyContentView removeFromSuperview];
    _emptyContentView = nil;
    
}

//-(void) ShowAlertPopup:(NSString*)message
//{
//    UIView *_updatePopupView =[self.view viewWithTag:KTAG_ALERT_POPUP_UPD_AVAILABLE];
//    if(_updatePopupView != nil)
//        [self HideAvailableUpdatePopup];
//    
//    if(_alertView == nil)
//        _alertView = [CustomAlert initCustomAlertView];
//    
//    _alertView.center = self.view.center;
//    _alertView.tag = KTAG_ALERT_POPUP_NO_CNX;
//   
//    if(IS_IPHONE)
//        _alertView.nslcPopupHeigth.constant = 140.0;
//    
//    [self.view addSubview:_alertView];
//    
//    NSString *_msg = message;
//    
//    _alertView.label.textAlignment = NSTextAlignmentCenter;
//    _alertView.label.text = _msg;
//    [_alertView.button setTitle: UPDATE_ACTION_TEXT forState:UIControlStateNormal];
//    [_alertView.button addTarget:self action:@selector(reintentDBDownload) forControlEvents:UIControlEventTouchUpInside];
//    
//    [_alertView sizeThatFits:CGSizeMake(_alertView.frame.size.width, 0)];
//    [_alertView systemLayoutSizeFittingSize:CGSizeMake(_alertView.frame.size.width, 0)];
//    
//    UITapGestureRecognizer *_hidePopup = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(HideConexionErrorPopup)];
//    [_alertView addGestureRecognizer:_hidePopup];
//    
//    
//    [_alertView layoutIfNeeded];
//}


//
//-(void) HideAvailableUpdatePopup
//{
//    UIView *_popupView =[self.view viewWithTag:KTAG_ALERT_POPUP_UPD_AVAILABLE];
//    if(_popupView != nil)
//        [_popupView removeFromSuperview];
//}


//#pragma App Update Methods
//
//-(void) RequestAppUpdate
//{
//    //Enter in RequestAppUpdate method: user accept execute update
//    
//    [CommonsUtils getCommonUtil].isUserAcceptUpdate = TRUE;
//    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_EXECUTE_APP_UPDATE object:nil];
//    [self HideConexionErrorPopup];
//    [self HideAvailableUpdatePopup];
//}
//
//-(void) reintentDBDownload
//{
//    [[ARGenieManager sharedManager] updateSDKData:nil];
//    
//    //Enter in reintentDBDownload method: user accept to reintent download
//                                                                                                                                                                 
//    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REINTENT_DB_DOWNLOAD object:nil];
//    [self HideConexionErrorPopup];
//    [self HideAvailableUpdatePopup];
//}


- (UIButton*) createScanBtn {
    CGSize mainScreenBounds = UIScreen.mainScreen.bounds.size;
    
    UIImage* scanImage = [UIImage imageNamed:@"ScanImage"];
    
    //UIButton* _menuScannerBtn = [[UIButton alloc] initWithFrame:CGRectMake(mainScreenBounds.width - 81, mainScreenBounds.height - 81, 63, 63)];
    
    UIButton* _menuScannerBtn = [[UIButton alloc] initWithFrame:CGRectMake(mainScreenBounds.width - 83, mainScreenBounds.height - TOP_BAR_HEIGHT - 83  , 73, 73)];
    
    [_menuScannerBtn setImage:scanImage forState:UIControlStateNormal];
    _menuScannerBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    
    
    [_menuScannerBtn addTarget:self action:@selector(goToScanView) forControlEvents:UIControlEventTouchUpInside];
    
    return _menuScannerBtn;
}

-(void) goToScanView {
    //[self.menuViewController scannerMenuAction:self];
    //[self.menuViewController openHideMenu];
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:NOTIFICATION_OPEN_SCANNER
     object:self
     userInfo:nil];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:NOTIFICATION_OPEN_HIDE_MENU
     object:self
     userInfo:nil];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (object == self && [keyPath isEqualToString:NSStringFromSelector(@selector(showScanButton))]) {
        _scanButton.hidden = !self.showScanButton;
    }
}

- (void) enableScanButton:(BOOL) enable {
    if(self.scanButton != nil)
        self.scanButton.enabled = enable;
}

- (NSArray<NSLayoutConstraint*>*) getConstraintForScanButton {
    NSLayoutConstraint *bottomConstraint = [NSLayoutConstraint
                                            constraintWithItem:_scanButton
                                            attribute:NSLayoutAttributeBottom
                                            relatedBy:NSLayoutRelationEqual
                                            toItem:self.contentView
                                            attribute:NSLayoutAttributeBottom
                                            multiplier:1.0
                                            constant:10];
    NSLayoutConstraint *rightConstraint = [NSLayoutConstraint
                                           constraintWithItem:_scanButton
                                           attribute:NSLayoutAttributeRight
                                           relatedBy:NSLayoutRelationEqual
                                           toItem:self.contentView
                                           attribute:NSLayoutAttributeRight
                                           multiplier:1.0
                                           constant:10];
    
    NSLayoutConstraint *withConstraint = [NSLayoutConstraint
                                          constraintWithItem:_scanButton
                                          attribute:NSLayoutAttributeWidth
                                          relatedBy:NSLayoutRelationEqual
                                          toItem:nil
                                          attribute:NSLayoutAttributeWidth
                                          multiplier:1.0
                                          constant:73];
    
    NSLayoutConstraint *heightConstraint = [NSLayoutConstraint
                                            constraintWithItem:_scanButton
                                            attribute:NSLayoutAttributeHeight
                                            relatedBy:NSLayoutRelationEqual
                                            toItem:nil
                                            attribute:NSLayoutAttributeHeight
                                            multiplier:1.0
                                            constant:73];
    
    return @[bottomConstraint, rightConstraint, withConstraint, heightConstraint];
}




@end
