//
//  Tuto1ViewController.swift
//  MallAppSwift
//
//  Created by Uday kanth Bangaru on 06/07/19.
//  Copyright © 2019 Arianne Peiso. All rights reserved.
//

import UIKit

class Tuto1ViewController: UIViewController,UIScrollViewDelegate,BotoomViewDelegate,ExpandBottomViewDelegate,SlideDelegate {
   

    @IBOutlet weak var ScrollView : UIScrollView!
    
    @IBOutlet weak var Pagecontroll : UIPageControl!
    
    @IBOutlet weak var BottomViewoutlet : BottomBar!
    
    @IBOutlet weak var ExpandBottomoutlet : ExpandedBottomBar!
    
    @IBOutlet weak var IndicatorImg : UIImageView!
    
    private var lastcontentOffset  : CGFloat = 0
    
    var currentpos = 0
    //var slide : Slide1 = Slide1()
    
    var slides:[Slide1] = [];
    
    var TreasureHuntBool = false
    
    //    @IBOutlet weak var slideview : Slide1?
    //
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        slides = createSlides()
        ScrollView.delegate = self
        setupSlideScrollView(slides: slides)
        Pagecontroll.numberOfPages = slides.count
        Pagecontroll.currentPage = 0
        view.bringSubviewToFront(Pagecontroll)
        Pagecontroll.backgroundColor = UIColor.clear
        Pagecontroll.pageIndicatorTintColor = #colorLiteral(red: 0.2196078431, green: 0.2196078431, blue: 0.5647058824, alpha: 1)
         //img = [UIImage outlinedEllipse:CGSizeMake(7, 7) :Theme.sharedInstance.sectionTitleText.color :1]
        
        let img = UIImage.outlinedEllipse(CGSize(width: 7, height: 7),#colorLiteral(red: 0.2196078431, green: 0.2196078431, blue: 0.5647058824, alpha: 1), 1)
        Pagecontroll.currentPageIndicatorTintColor = UIColor(patternImage: img!)
        Pagecontroll.transform = CGAffineTransform(scaleX: 2, y: 2)
        
        self.ExpandBottomoutlet.isHidden = true
        
        self.BottomViewoutlet.delegate = self
        self.ExpandBottomoutlet.delegate = self
        //self.slideview?.delegate = self
        
         self.BottomViewoutlet.HomeButton.setImage(UIImage(named: "Home-9"), for: .normal)
        
        
        if UserDefaults.standard.object(forKey: "DeviceLanguage") != nil {
            if "\(UserDefaults.standard.value(forKey: "DeviceLanguage")!)" == "en" ||  "\(UserDefaults.standard.value(forKey: "DeviceLanguage")!)" == "fr" || "\(UserDefaults.standard.value(forKey: "DeviceLanguage")!)" == "es"{
                
                self.ScrollView?.transform = CGAffineTransform(scaleX: 1, y: 1)
                
            }
            else if  "\(UserDefaults.standard.value(forKey: "DeviceLanguage")!)" == "ar"{
                
                self.ScrollView?.transform = CGAffineTransform(scaleX: -1, y: 1)
                
            }
            else
            {
                
                self.ScrollView?.transform = CGAffineTransform(scaleX: 1, y: 1)
                
                
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        //ScrollView.setContentOffset(CGPoint(x: 0, y: ScrollView.contentOffset.y), animated: true)
    }
    
    
    
    func createSlides() -> [Slide1] {
        
        
        let slide1: Slide1 = Bundle.main.loadNibNamed("Slide1", owner: self, options: nil)?.first as! Slide1
        
        slide1.SlideNo.image = UIImage(named:"number 1-8" )
        slide1.ImageView.image = UIImage(named: "Ar_icn-8")
        slide1.MessageLbl.text = "Use the AR button to open the Augmented Reality."
        slide1.StartHuntBtn.isHidden = true
        
        let slide2: Slide1 = Bundle.main.loadNibNamed("Slide1", owner: self, options: nil)?.first as! Slide1
        
        slide2.SlideNo.image = UIImage(named:"Number 2-8" )
        slide2.ImageView.image = UIImage(named: "scan_icon-8")
        slide2.MessageLbl.text = "Scan the compatible tokens scattered all over the mall."
        slide2.StartHuntBtn.isHidden = true
        
        let slide3: Slide1 = Bundle.main.loadNibNamed("Slide1", owner: self, options: nil)?.first as! Slide1
        
        slide3.SlideNo.image = UIImage(named:"number 3-8" )
        slide3.ImageView.image = UIImage(named: "Asset 65-8")
        slide3.MessageLbl.text = "Treasure Hunt screen 3 ."
        slide3.StartHuntBtn.isHidden = true
        
        let slide4: Slide1 = Bundle.main.loadNibNamed("Slide1", owner: self, options: nil)?.first as! Slide1
        
        slide4.SlideNo.image = UIImage(named:"number 4-8" )
        slide4.ImageView.image = UIImage(named: "Asset 66-8")
        slide4.MessageLbl.text = "Redeem your gifts in the Kiosk on level G, if you didn't win it's ok! with each scan you cumulate tokens that can be re-deemed for gifts."
        slide4.StartHuntBtn.isHidden = false
        slide4.StartHuntBtn.setTitle(NSLocalizedString("START HUNTING", comment: ""), for: .normal)
        
        
        return [slide1,slide2,slide3,slide4]
        
    }
    func setupSlideScrollView(slides: [Slide1]){
        
        let contentwidth = UIScreen.main.bounds.width
        //let contentheight = UIScreen.main.bounds.height - 200
        
        ScrollView.frame = CGRect(x: 0, y: 0, width: contentwidth, height:ScrollView.frame.height)
        
        print(ScrollView.frame)
        ScrollView.contentSize = CGSize(width: contentwidth * CGFloat(slides.count), height:ScrollView.frame.height)
        ScrollView.isPagingEnabled = true
        
        print(slides.count)
        print(ScrollView.contentSize)
        
        print(ScrollView.contentSize)
        for i in 0 ..< slides.count{
            
            slides[i].frame = CGRect(x: contentwidth * CGFloat(i), y: 0, width: contentwidth, height: ScrollView.frame.height)
            self.slides[i].delegate = self
            
            if UserDefaults.standard.object(forKey: "DeviceLanguage") != nil {
                if "\(UserDefaults.standard.value(forKey: "DeviceLanguage")!)" == "en" ||  "\(UserDefaults.standard.value(forKey: "DeviceLanguage")!)" == "fr" || "\(UserDefaults.standard.value(forKey: "DeviceLanguage")!)" == "es"{
                    
                    self.slides[i].transform = CGAffineTransform(scaleX: 1, y: 1)
                    
                }
                else if  "\(UserDefaults.standard.value(forKey: "DeviceLanguage")!)" == "ar"{
                    
                    self.slides[i].transform = CGAffineTransform(scaleX: -1, y: 1)
                    
                }
                else
                {
                    
                    self.slides[i].transform = CGAffineTransform(scaleX: 1, y: 1)
                    
                    
                }
            }
            ScrollView.addSubview(slides[i])
            print(slides[i].frame)
        }
    }
    
    /*
     * default function called when view is scolled. In order to enable callback
     * when scrollview is scrolled, the below code needs to be called:
     * slideScrollView.delegate = self or
     */
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        Pagecontroll.currentPage = Int(pageIndex)
        
        let maximumHorizontalOffset : CGFloat = scrollView.contentSize.width - scrollView.frame.width
        let currentHorizontalOffset : CGFloat = scrollView.contentOffset.x
        
        //Vertical
        
        let maximumVerticalOffset : CGFloat = scrollView.contentSize.height - scrollView.frame.height
        let currentverticalOffset : CGFloat = scrollView.contentOffset.y
        
        let percentageHorizontalOffset : CGFloat = currentHorizontalOffset/maximumHorizontalOffset
        
        let percentageVerticalOffset : CGFloat = currentverticalOffset/maximumVerticalOffset
        
        /*
         * below code changes the background color of view on paging the scrollview
         */
        self.scrollView(scrollView, didScrollToPercentageOffset: percentageHorizontalOffset)
        
        
        /*
         * below code scales the imageview on paging the scrollview
         */
        
        let percentOffset : CGPoint = CGPoint(x: percentageHorizontalOffset, y: percentageVerticalOffset)

//        let newPos = Int(ScrollView.contentOffset.x / ScrollView.frame.size.width)
//
//        if newPos + 1 == currentpos{
//            return
//        }
//        currentpos = newPos + 1
//
//        switch currentpos {
//        case 1: IndicatorImg.image = UIImage(named: "ind 1-8")
//        case 2: IndicatorImg.image = UIImage(named: "indicator-8")
//        case 3: IndicatorImg.image = UIImage(named: "ind 3-8")
//        case 4: IndicatorImg.image = UIImage(named: "ind 4-8")
//
//        default:
//            break
//        }

        
        if (percentOffset.x>0 && percentOffset.x <= 0.33) {
            
            slides[0].ImageView.transform = CGAffineTransform(scaleX: (0.33-percentOffset.x)/0.33, y: (0.33-percentOffset.x)/0.33 )
            slides[1].ImageView.transform = CGAffineTransform(scaleX: percentOffset.x/0.33, y: percentOffset.x/0.33)
            
            print(slides[0].frame)
            print(slides[1].frame)
            //print(slides[1].ImageView)
        }else if (percentOffset.x > 0.33 && percentOffset.x <= 0.66){
            slides[1].ImageView.transform = CGAffineTransform(scaleX: (0.66-percentOffset.x)/0.33, y: (0.66-percentOffset.x)/0.33)
            slides[2].ImageView.transform = CGAffineTransform(scaleX: percentOffset.x/0.66, y: percentOffset.x/0.66)
            print(slides[1].ImageView.transform)
            print(slides[2].ImageView.transform)
            
            print(slides[1].frame)
            print(slides[2].frame)
        }
            
        else if(percentOffset.x > 0.66 && percentOffset.x <= 1) {
            slides[2].ImageView.transform = CGAffineTransform(scaleX: (1-percentOffset.x)/0.33, y: (1-percentOffset.x)/0.33)
            slides[3].ImageView.transform = CGAffineTransform(scaleX: percentOffset.x, y: percentOffset.x)
            
            print(slides[2].frame)
            print(slides[3].frame)
        }
        //  else if (percentOffset.x > 0.75 && percentOffset.x <= 1) {
        //            slides[3].ImageView.transform = CGAffineTransform(scaleX: (1-percentOffset.x)/0.25, y: (1-percentOffset.x)/0.25)
        //            slides[4].ImageView.transform = CGAffineTransform(scaleX: percentOffset.x, y: percentOffset.x)
        //        }
        
        //Pagecontroll.currentPage = currentpos - 1
    }
    
    
    func scrollView(_ scrollView: UIScrollView, didScrollToPercentageOffset percentageHorizontalOffset: CGFloat) {
//        if(Pagecontroll.currentPage == 0) {
//            //Change background color to toRed: 103/255, fromGreen: 58/255, fromBlue: 183/255, fromAlpha: 1
//            //Change pageControl selected color to toRed: 103/255, toGreen: 58/255, toBlue: 183/255, fromAlpha: 0.2
//            //Change pageControl unselected color to toRed: 255/255, toGreen: 255/255, toBlue: 255/255, fromAlpha: 1
//
//            let pageUnselectedColor: UIColor = fade(fromRed: 255/255, fromGreen: 255/255, fromBlue: 255/255, fromAlpha: 1, toRed: 103/255, toGreen: 58/255, toBlue: 183/255, toAlpha: 1, withPercentage: percentageHorizontalOffset * 3)
//            Pagecontroll.pageIndicatorTintColor = pageUnselectedColor
//
//
//            //let bgColor: UIColor = fade(fromRed: 103/255, fromGreen: 58/255, fromBlue: 183/255, fromAlpha: 1, toRed: 255/255, toGreen: 255/255, toBlue: 255/255, toAlpha: 1, withPercentage: percentageHorizontalOffset * 3)
//            //slides[Pagecontroll.currentPage].backgroundColor = bgColor
//
//            let pageSelectedColor: UIColor = fade(fromRed: 81/255, fromGreen: 36/255, fromBlue: 152/255, fromAlpha: 1, toRed: 103/255, toGreen: 58/255, toBlue: 183/255, toAlpha: 1, withPercentage: percentageHorizontalOffset * 3)
//            Pagecontroll.currentPageIndicatorTintColor = pageSelectedColor
//        }
//
    }
//    func fade(fromRed: CGFloat,
//              fromGreen: CGFloat,
//              fromBlue: CGFloat,
//              fromAlpha: CGFloat,
//              toRed: CGFloat,
//              toGreen: CGFloat,
//              toBlue: CGFloat,
//              toAlpha: CGFloat,
//              withPercentage percentage: CGFloat) -> UIColor {
//
//        let red: CGFloat = (toRed - fromRed) * percentage + fromRed
//        let green: CGFloat = (toGreen - fromGreen) * percentage + fromGreen
//        let blue: CGFloat = (toBlue - fromBlue) * percentage + fromBlue
//        let alpha: CGFloat = (toAlpha - fromAlpha) * percentage + fromAlpha
//
//        // return the fade colour
//        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    // }
    
    
    func ServicesAction(sender: AnyObject) {
        
        let vc = ServicesViewController()
        
        vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func FloorAction(sender: AnyObject) {
        
        let vc = FloorViewController()
        
        vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    
    
    func ShareButtonTapped(sender: AnyObject) {
        
        let textToShare = "Check out this app"
        
        //self.ExpandBottomoutlet.ShareButton.setImage(UIImage(named: "Share-10"), for: .normal)

        
        if let myWebsite = NSURL(string: "https://www.google.com/") {
            let objectsToShare: [Any] = [textToShare, myWebsite]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            activityVC.popoverPresentationController?.sourceView = self.ExpandBottomoutlet.ShareButton
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    
    
    func HomeButtonTapped(sender: AnyObject) {
        
        
        let vc = MainViewController()
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func ProfilebuttonTapped(sender: AnyObject) {
        
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            let vc = LoginViewController()
            
             vc.ProfileBool = true
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else {
            
            let vc = ProfileVC()
            
            self.navigationController?.pushViewController(vc, animated: false)
        }
        
    }
    func ArbuttonTapped(sender: AnyObject) {
        
        
    //    self.showAlert(Alertmsg: "Alert", msg: "Please scroll through the tutorials first.")
//        if UserDefaults.standard.value(forKey: "UserID") == nil {
//
//            let vc = LoginViewController()
//
//            self.navigationController?.pushViewController(vc, animated: false)
//
//        }else {
//
//            ARGenieManager.shared()?.loadAugmentedRealityData(nil)
//
//        }
    }
    func ArExbuttonTapped(sender: AnyObject) {
        
  //      self.showAlert(Alertmsg: "Alert", msg: "Please scroll through the tutorials first.")
//        if UserDefaults.standard.value(forKey: "UserID") == nil {
//
//            let vc = LoginViewController()
//
//            self.navigationController?.pushViewController(vc, animated: false)
//
//        }else {
//
//            ARGenieManager.shared()?.loadAugmentedRealityData(nil)
//        }
    }
    
    func starthunttapped(sender: AnyObject) {
        
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            let vc = LoginViewController()
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else {
            if NetworkConnection.isConnectedToNetwork(){
                ARGenieManager.shared()?.loadAugmentedRealityData(nil)

            }else {
                self.showAlert(Alertmsg: "Alert", msg: networkAlert.networkmsg)
                
            }
        }
    }
    
    func ExpandButtonTapped(sender: AnyObject) {
        
        self.BottomViewoutlet.isHidden = true
        self.ExpandBottomoutlet.isHidden = false
    }
    
    
    func CollapseButtonTapped(sender: AnyObject) {
        
        self.BottomViewoutlet.isHidden = false
        self.ExpandBottomoutlet.isHidden = true
    }
    
    func FaqbuttonTaped(sender: AnyObject) {
        
        let vc = FAQViewController()
        
         vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func UserAgreeTapped(sender: AnyObject) {
        
        let vc = UserAgreementVC()
        
         vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func LegalTapped(sender: AnyObject) {
        
        let vc = LeagalViewController()
        
         vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func LibraryTapped(sender: AnyObject) {
        
        let vc = LibraryVC()
        
         vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func showsettings(sender: AnyObject) {
        
        self.alertToEncourageCameraAccessInitially()
        
    }
    func showsetting(sender: AnyObject) {
        
        self.alertToEncourageCameraAccessInitially()
        
    }
    
    func showLibrary(sender: AnyObject) {
        
        self.alertToEncourageLibraryAccessInitially()
    }

}


extension UIImage {
    class func outlinedEllipse(_ size: CGSize, _ color: UIColor?, _ lineWidth: CGFloat) -> UIImage? {
        
        UIGraphicsBeginImageContextWithOptions(size, _: false, _: 0)
        let context = UIGraphicsGetCurrentContext()
        
        if let CGColor = color?.cgColor {
            context?.setStrokeColor(CGColor)
        }
        context?.setLineWidth(lineWidth)
        
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        context?.addEllipse(in: rect)
        context?.strokePath()
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
}
