//
//  Slide1.swift
//  MallAppSwift
//
//  Created by Uday kanth Bangaru on 06/07/19.
//  Copyright © 2019 Arianne Peiso. All rights reserved.
//

import UIKit


protocol SlideDelegate
{
    func starthunttapped(sender : AnyObject)
}

class Slide1: UIView {

    
    @IBOutlet weak var SlideNo : UIImageView!
    
    @IBOutlet weak var ImageView : UIImageView!
    
    @IBOutlet weak var MessageLbl : UILabel!
    
    @IBOutlet weak var StartHuntBtn : UIButton!
    
    var delegate : SlideDelegate?
    
    
    @IBAction func startHuntAction(_ sender: UIButton) {
        print("calling")
        
        delegate?.starthunttapped(sender: StartHuntBtn)
        
    }

    
}
