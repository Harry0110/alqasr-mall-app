//
//  ServiceDetailsViewController.swift
//  MallAppSwift
//
//  Created by Freelancer on 25/06/19.
//  Copyright © 2019 Arianne Peiso. All rights reserved.
//

import UIKit

class ServiceDetailsViewController: BaseViewController, BotoomViewDelegate, ExpandBottomViewDelegate{
    
    @IBAction func CloseAction(_ sender : UIButton){
        
        //let vc = MainViewController()
        self.navigationController?.popViewController(animated: false)
       // self.navigationController?.popToRootViewController(animated: false)

    }
    
    @IBOutlet weak var ScrollView: UIScrollView!
    
    let theme = ThemeClass()
    
    //Constraints Outlets
    @IBOutlet weak var locatinimagetop: NSLayoutConstraint!
    @IBOutlet weak var locationlabelheight: NSLayoutConstraint!
    @IBOutlet weak var Locationlabeltop: NSLayoutConstraint!
    @IBOutlet weak var locationimageheight: NSLayoutConstraint!
    
    
    @IBOutlet weak var phoneimagetop: NSLayoutConstraint!
    @IBOutlet weak var phonelabelheight: NSLayoutConstraint!
    @IBOutlet weak var phonelabeltop: NSLayoutConstraint!
    @IBOutlet weak var phoneimageheight: NSLayoutConstraint!
    
    @IBOutlet weak var mailimagetop: NSLayoutConstraint!
    @IBOutlet weak var maillabelheight: NSLayoutConstraint!
    @IBOutlet weak var maillabeltop: NSLayoutConstraint!
    @IBOutlet weak var mailimageheight: NSLayoutConstraint!
    
    @IBOutlet weak var webimagetop: NSLayoutConstraint!
    @IBOutlet weak var weblabelheight: NSLayoutConstraint!
    @IBOutlet weak var weblabeltop: NSLayoutConstraint!
    @IBOutlet weak var webimageheight: NSLayoutConstraint!
    
    @IBOutlet weak var webimagebottom: NSLayoutConstraint!
    
    @IBOutlet weak var weblabelbottom: NSLayoutConstraint!
    
    @IBOutlet weak var ServiceView: UIView!
    var phoneString:String?
    var mailString:String?
    var locationString: String?
    var webString:String?
    var descString:String?
    var titleString: String?
    var floorImageString:String?
    
    var imageStringArray = [String]()
    
    var imageString: String?
    
    var selected : Bool!
    
    @IBOutlet weak var bottombar: BottomBar!
    
    @IBOutlet weak var bottomExpanded: ExpandedBottomBar!
    @IBOutlet weak var TitleLbl: UILabel!
    @IBOutlet weak var Image: UIImageView!
    
    @IBOutlet weak var DescTextView: UITextView!
    
    @IBOutlet weak var Label1: UILabel!
    
    @IBOutlet weak var Label2: UILabel!
    
    @IBOutlet weak var Label3: UILabel!
    
    @IBOutlet weak var Label4: UILabel!
    
    @IBOutlet weak var Image4: UIImageView!
    @IBOutlet weak var Image2: UIImageView!
    @IBOutlet weak var Image3: UIImageView!
    @IBOutlet weak var FloorImage: UIImageView!
    @IBOutlet weak var Image1: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.showLoader()
        
        if NetworkConnection.isConnectedToNetwork(){
            
        }else {
            
            self.showAlert(Alertmsg: "Alert", msg: networkAlert.networkmsg)
        }

        self.bottombar.delegate = self
        self.bottomExpanded.delegate = self
        setupdetails()
        
        self.bottomExpanded.isHidden = true
        // Do any additional setup after loading the view.
        
        if selected == true{
            
             self.bottombar.ServiceButton.setImage(UIImage(named: "Services-8"), for: .normal)
             self.bottombar.HomeButton.setImage(UIImage(named: "Home-9"), for: .normal)
            
        }
        ScrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 51, right: 0)
       
    }

    func setupdetails() {
                
        let Object:NSDictionary = theme.getServicesView()
        
       
        let color1 = hexStringToUIColor(hex: Object.object(forKey: "TextColor")! as! String)
        
        let color2 =  hexStringToUIColor(hex: Object.object(forKey: "DescColor")! as! String)
        
        let color3 = hexStringToUIColor(hex: Object.object(forKey: "OtherColor")! as! String)
        
        self.TitleLbl.text = self.titleString
        self.TitleLbl.textColor = color1
        self.TitleLbl.font =  UIFont(name:Object.object(forKey: "TextFont")! as! String, size: Object.object(forKey: "TextSize")! as! CGFloat)
        self.TitleLbl.textColor = color1
        
        
        print(self.TitleLbl.text!)
        self.DescTextView.text = self.descString?.htmlToString
        self.DescTextView.font = UIFont(name:Object.object(forKey: "DescFont")! as! String, size: Object.object(forKey: "DescSize")! as! CGFloat)
        self.DescTextView.textColor = color2


        
        if self.locationString == "" {
            
            self.Locationlabeltop.constant = 0
            self.locationlabelheight.constant = 0
            self.locatinimagetop.constant = 0
            self.locationimageheight.constant = 0
            self.Image1.isHidden = true
            self.Label1.isHidden = true
        }else{
            self.Label1.text = self.locationString
            self.Label1.textColor = color3
            self.Label1.font = UIFont(name:Object.object(forKey: "OtherFont")! as! String, size: Object.object(forKey: "OtherSize")! as! CGFloat)
            //self.Image1.isHidden = false
        }
        if self.phoneString == "" {
            
            self.phoneimagetop.constant = 0
            self.phonelabeltop.constant = 0
            self.phoneimageheight.constant = 0
            self.phonelabelheight.constant = 0
            self.Image2.isHidden = true
            self.Label2.isHidden = true
        }else{
            
            self.Label2.text = self.phoneString
            self.Label2.textColor = color3
            self.Label2.font = UIFont(name:Object.object(forKey: "OtherFont")! as! String, size: Object.object(forKey: "OtherSize")! as! CGFloat)
            //self.Image2.isHidden = false
        }
        if self.mailString == ""{
            
            self.mailimagetop.constant = 0
            self.maillabeltop.constant = 0
            self.mailimageheight.constant = 0
            self.maillabelheight.constant = 0
            self.Image3.isHidden = true
            self.Label3.isHidden = true
        }else{
            
            self.Label3.text = self.mailString
            self.Label3.textColor = color3
            self.Label3.font = UIFont(name:Object.object(forKey: "OtherFont")! as! String, size: Object.object(forKey: "OtherSize")! as! CGFloat)
            //self.Image3.isHidden = false
        }
        if self.webString == "" {
            
            self.webimagetop.constant = 0
            self.weblabeltop.constant = 0
            self.webimageheight.constant = 0
            self.weblabelheight.constant = 0
            self.webimagebottom.constant = 10
            self.weblabelbottom.constant = 10
            self.Image4.isHidden = true
            self.Label4.isHidden = true
        }else{
            
            self.Label4.text = self.webString
            self.Label4.textColor = color3
            self.Label4.font = UIFont(name:Object.object(forKey: "OtherFont")! as! String, size: Object.object(forKey: "OtherSize")! as! CGFloat)
        }
        
        self.imageStringArray.append(self.imageString!)
        
        if self.floorImageString != "" {
            
            self.imageStringArray.append(self.floorImageString!)
            
        }
        
        for i in imageStringArray{
            
            let catPictureURL = URL(string: i)!
            
            let session = URLSession(configuration: .default)
            
            let downloadPicTask = session.dataTask(with: catPictureURL) { (data, response, error) in
                // The download has finished.
                if let e = error {
                    print("Error downloading cat picture: \(e)")
                } else {
                    // No errors found.
                    // It would be weird if we didn't have a response, so check for that too.
                    if let res = response as? HTTPURLResponse {
                        print("Downloaded cat picture with response code \(res.statusCode)")
                        if let imageData = data {
                            // Finally convert that Data into an image and do what you wish with it.
                            let image = UIImage(data: imageData)
                            
                            DispatchQueue.main.async {
                                if i == self.imageString{
                                    
                                    self.Image.image = image
                                    self.hideLoader()
                                }else
                                {
                                    self.FloorImage.image = image
                                    self.hideLoader()
                                    
                                }
                            }
                            // Do something with your image.
                        } else {
                            print("Couldn't get image: Image is nil")
                        }
                    } else {
                        print("Couldn't get response code for some reason")
                    }
                }
            }
            
            downloadPicTask.resume()
            self.hideLoader()
        }
    }
    
    func CollapseButtonTapped(sender: AnyObject) {
        
        self.bottombar?.isHidden = false
        
        self.bottomExpanded?.isHidden = true
         ScrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 51, right: 0)
        
        
    }
    
    func ShareButtonTapped(sender: AnyObject) {
        
        let textToShare = "Check out this app"
        
        if let myWebsite = NSURL(string: "https://www.google.com/") {
            let objectsToShare: [Any] = [textToShare, myWebsite]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            activityVC.popoverPresentationController?.sourceView = self.bottomExpanded.ShareButton
            self.present(activityVC, animated: true, completion: nil)
        }
        
    }
    
    func ExpandButtonTapped(sender: AnyObject) {
        
        self.bottombar?.isHidden = true
        
        self.bottomExpanded?.isHidden = false
        
        ScrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)

    }
    
    func HomeButtonTapped(sender: AnyObject) {
        
        let vc = MainViewController()
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func ServicesAction(sender: AnyObject) {
        
        let vc = ServicesViewController()
        
        vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func FloorAction(sender: AnyObject) {
        
        let vc = FloorViewController()
        
        vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func ProfilebuttonTapped(sender: AnyObject) {
        
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            let vc = LoginViewController()
            
             vc.ProfileBool = true
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else {
            
            let vc = ProfileVC()
            
            vc.selected = true
            
            self.navigationController?.pushViewController(vc, animated: false)
        }
        
    }
    func ArbuttonTapped(sender: AnyObject) {
        
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            let vc = Tuto1ViewController()
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else {
            
            ARGenieManager.shared()?.loadAugmentedRealityData(nil)
            
        }
    }
    func ArExbuttonTapped(sender: AnyObject) {
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            let vc = Tuto1ViewController()
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else {
            
            ARGenieManager.shared()?.loadAugmentedRealityData(nil)
            
        }
    }
    
    func FaqbuttonTaped(sender: AnyObject) {
        let vc = FAQViewController()
        
        vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
        
    }
    
    func UserAgreeTapped(sender: AnyObject) {
        
        let vc = UserAgreementVC()
        
        vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func LegalTapped(sender: AnyObject) {
        let vc = LeagalViewController()
        
        vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func LibraryTapped(sender: AnyObject) {
        let vc = LibraryVC()
        
        vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
        
    }
    func showsettings(sender: AnyObject) {
        
        self.alertToEncourageCameraAccessInitially()
        
    }

    func showsetting(sender: AnyObject) {
        
        self.alertToEncourageCameraAccessInitially()
        
    }
    
    func showLibrary(sender: AnyObject) {
        
        self.alertToEncourageLibraryAccessInitially()
    }
}
