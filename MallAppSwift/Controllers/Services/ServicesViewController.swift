//
//  ServicesViewController.swift
//  MallAppSwift
//
//  Created by Freelancer on 24/06/19.
//  Copyright © 2019 Arianne Peiso. All rights reserved.
//

import Foundation
import UIKit


class ServicesViewController : BaseViewController,UICollectionViewDelegate, UICollectionViewDataSource, UITextFieldDelegate,UICollectionViewDelegateFlowLayout,BotoomViewDelegate,ExpandBottomViewDelegate {
    
    @IBOutlet weak var dropDown: DropDown!
    
    @IBOutlet weak var BottomBarOutlet: BottomBar!
    @IBOutlet weak var ExpandedBottomOutlet: ExpandedBottomBar!
    
    let TABLE_NAME = "services"
    let TABLE_NAME1 = "servicetype"
    let COLUMN_IDENTIFIER = "id"
    let COLUMN_TIDENTIFIER = "tid"
    let COLUMN_DESCRIPTION = "description"
    let COLUMN_TITLE = "title"
    let COLUMN_BANNER = "banner"
    let COLUMN_THUMBNAIL = "thumbnail"
    let COLUMN_TYPESERVICE = "id_typeservice"
    let COLUMN_LOCATION = "location"
    let COLUMN_PHONE = "phone"
    let COLUMN_EMAIL = "email"
    let COLUMN_WEBSITE = "website"
    let COLUMN_FLOORPLAN = "floorplan"
    let COLUMN_WEIGHT = "weight"
    
    
    var filteredArray = [ServicesModel]()
    var ServiceTypeArray = [ServicesTypeModel]()
    var typeArray = ["SHOW ALL CATEGORIES"]
    var titleArray = [String]()
    var idArray = [0]
    
    var serviceList: [AnyHashable] = []
    var serviceTypeList: [AnyHashable] = []
    
    var arraycount = 0
    
    var searchActive : Bool = false
    
    var myPickerView : UIPickerView!
    
    var selectedID :Int = 0
        
    var selected : Bool?
    
    var titleName: String?
    
    var bool = false
    
    @IBOutlet weak var CategoryTF: UITextField!
    
    @IBOutlet weak var ServicesCV: UICollectionView!
    override func viewDidLoad() {
        
        self.showLoader()
        
        if titleName == nil {
            
            self.CategoryTF.text = self.typeArray[0]
            
        }else {
            
            self.CategoryTF.text = self.titleName
        }
        
         self.CategoryTF.inputView = UIView()
         self.CategoryTF.inputAccessoryView = UIView()
        self.CategoryTF.tintColor = UIColor.clear
        
        if NetworkConnection.isConnectedToNetwork(){
            self.ServiceTypeArray = self.getServicesType() as! [ServicesTypeModel]
            
            print(self.idArray)
            dropDown.optionArray = self.typeArray
            //Its Id Values and its optional
            dropDown.optionIds = self.idArray
            
            // Image Array its optional
            // The the Closure returns Selected Index and String
            dropDown.didSelect{(selectedText , index ,id) in
                
                let selectedIndex = index
                
                self.CategoryTF.text = self.typeArray[selectedIndex]
                
                self.selectedID = self.idArray[selectedIndex]
                
                self.searchActive = true
                
                self.titleArray = []
                
                self.filteredArray = self.getServices() as! [ServicesModel]
                
                self.ServicesCV.reloadData()
            }
        }
        else{
            
            self.showAlert(Alertmsg: "Alert", msg: networkAlert.networkmsg)
        }
        
        self.filteredArray = self.getServices() as! [ServicesModel]
        
        self.ServicesCV.reloadData()
        
        self.BottomBarOutlet.delegate = self
        
        self.ExpandedBottomOutlet.delegate = self
        
        self.ExpandedBottomOutlet.isHidden = true
        
        if selected == true{
            
            self.BottomBarOutlet.ServiceButton.setImage(UIImage(named: "Services-8"), for: .normal)
            self.BottomBarOutlet.HomeButton.setImage(UIImage(named: "Home-9"), for: .normal)
        }
        print(self.filteredArray)
        print(self.ServiceTypeArray)
        
        self.CategoryTF.delegate = self
        
        let nibName = UINib(nibName: "ServicesCollectionViewCell", bundle:nil)
        
        ServicesCV.register(nibName, forCellWithReuseIdentifier: "cell")
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 0, bottom: 10, right: 0)
        layout.itemSize = CGSize(width: self.ServicesCV.frame.height/3, height: self.ServicesCV.frame.height/3)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        ServicesCV!.collectionViewLayout = layout
        
        ServicesCV.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 45, right: 0)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        if serviceList.count == 0 {
            
            self.hideLoader()
        }
        
        
//        self.filteredArray.sort {
//            if let One = Int($0.weight!) {
//                if let Two = Int($1.weight!) {
//
//                }
//
//            }
//
//            return true
//        }
        
    }
    
    
//    func sortedKeysByValue(isOrderedBefore:(Value, Value) -> Bool) -> [Key] {
//        return sortedKeys {
//            isOrderedBefore(self[$0]!, self[$1]!)
//        }
//    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.serviceList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ServicesCollectionViewCell
        
        print(selectedID)
        print(self.filteredArray[indexPath.row].id_typeservice!)
        
        if self.filteredArray[indexPath.row].title != nil {
            
            print(self.filteredArray[indexPath.row].title!)
            print(self.filteredArray[indexPath.row].thumbnail!)
            
            self.titleArray.append(self.filteredArray[indexPath.row].title!)
            
            let catPictureURL = URL(string: self.filteredArray[indexPath.row].thumbnail!)!
            
            // Creating a session object with the default configuration.
            // You can read more about it here https://developer.apple.com/reference/foundation/urlsessionconfiguration
            let session = URLSession(configuration: .default)
            
            // Define a download task. The download task will download the contents of the URL as a Data object and then you can do what you wish with that data.
            let downloadPicTask = session.dataTask(with: catPictureURL) { (data, response, error) in
                // The download has finished.
                if let e = error {
                    print("Error downloading cat picture: \(e)")
                } else {
                    // No errors found.
                    // It would be weird if we didn't have a response, so check for that too.
                    if let res = response as? HTTPURLResponse {
                        print("Downloaded cat picture with response code \(res.statusCode)")
                        if let imageData = data {
                            // Finally convert that Data into an image and do what you wish with it.
                            let image = UIImage(data: imageData)
                            
                            DispatchQueue.main.async {
                                cell.ServicesImage.image = image
                                self.hideLoader()
                            }
                            // Do something with your image.
                        } else {
                            print("Couldn't get image: Image is nil")
                            self.hideLoader()

                        }
                    } else {
                        print("Couldn't get response code for some reason")
                        self.hideLoader()

                    }
                }
            }
            downloadPicTask.resume()
        }
        

        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let minItemSpacing: CGFloat = 0.25
        return CGSize(width: ((self.ServicesCV.frame.width/2) - minItemSpacing), height: ((self.ServicesCV.frame.width/2) - minItemSpacing))
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let vc = ServiceDetailsViewController()
        
        vc.imageString = self.filteredArray[indexPath.row].thumbnail!
        print(self.filteredArray[indexPath.row].thumbnail!)
        vc.descString = self.filteredArray[indexPath.row].Sdescription!
        vc.titleString = self.filteredArray[indexPath.row].title!
        vc.floorImageString = self.filteredArray[indexPath.row].floorplan!
        vc.phoneString = self.filteredArray[indexPath.row].phone!
        vc.mailString = self.filteredArray[indexPath.row].email!
        vc.locationString = self.filteredArray[indexPath.row].location!
        vc.webString = self.filteredArray[indexPath.row].website!
        
        print(self.filteredArray[indexPath.row].floorplan!)
        
        self.navigationController?.pushViewController(vc, animated: false)
        
        
    }
    
    
    
    func getServices() -> [AnyHashable]? {
        
        self.serviceList = []
        let database: FMDatabase? = CommonsUtils.getCommonUtil().getLocalDB()
        if database?.open() == nil {
            print("Problems to open DB, please check")
            return serviceList
        }
        
        // FMDBQuickCheck(!database?.hasOpenResultSets())
        
        let resultSet = database?.executeQuery("select * from \(TABLE_NAME)", withArgumentsIn: [])
        //        let resultSet: FMResultSet? = database?.execute("SELECT DISTINCT e.tid, e.title FROM events e INNER JOIN products_events pe ON e.tid=pe.id_event ORDER BY e.tid")
        while ((resultSet?.next())!) {
            
            let service = ServicesModel()
            //parse data
            service.id = (resultSet?.int(forColumn: self.COLUMN_IDENTIFIER))!
            service.title = (resultSet?.string(forColumn: self.COLUMN_TITLE))!.uppercased()
            service.banner = (resultSet?.string(forColumn: self.COLUMN_BANNER))!
            service.thumbnail = (resultSet?.string(forColumn: self.COLUMN_THUMBNAIL))!
            service.id_typeservice = (resultSet?.string(forColumn: self.COLUMN_TYPESERVICE))!
            service.location = (resultSet?.string(forColumn: self.COLUMN_LOCATION))!
            service.phone = (resultSet?.string(forColumn: self.COLUMN_PHONE))!
            service.email = (resultSet?.string(forColumn: self.COLUMN_EMAIL))!
            service.website = (resultSet?.string(forColumn: self.COLUMN_WEBSITE))!
            service.floorplan = (resultSet?.string(forColumn: self.COLUMN_FLOORPLAN))!
            service.weight = (resultSet?.string(forColumn: self.COLUMN_WEIGHT))!
            service.Sdescription = (resultSet?.string(forColumn: self.COLUMN_DESCRIPTION))!
            
            if self.selectedID == Int(service.id_typeservice!){
                serviceList.append(service as AnyHashable)
            }else if self.selectedID == 0
            {
                serviceList.append(service as AnyHashable)
            }
            
        }
        
        resultSet?.close()
        database?.close()
        return serviceList
    }
    
    func getServicesType() -> [AnyHashable]? {
        
        let database: FMDatabase? = CommonsUtils.getCommonUtil().getLocalDB()
        if database?.open() == nil {
            print("Problems to open DB, please check")
            return serviceTypeList
        }
        
        // FMDBQuickCheck(!database?.hasOpenResultSets())
        
        let resultSet = database?.executeQuery("select * from \(TABLE_NAME1)", withArgumentsIn: [])
        //        let resultSet: FMResultSet? = database?.execute("SELECT DISTINCT e.tid, e.title FROM events e INNER JOIN products_events pe ON e.tid=pe.id_event ORDER BY e.tid")
        while ((resultSet?.next())!) {
            
            let service = ServicesTypeModel()
            //parse data
            service.tid = (resultSet?.int(forColumn: self.COLUMN_TIDENTIFIER))!
            service.ttitle = (resultSet?.string(forColumn: self.COLUMN_TITLE))!.uppercased()
            service.tdescription = (resultSet?.string(forColumn: self.COLUMN_DESCRIPTION))!
            self.typeArray.append(service.ttitle!)
            self.idArray.append(Int(service.tid!))
            serviceTypeList.append(service as AnyHashable)
            
        }
        
        resultSet?.close()
        database?.close()
        
        return serviceTypeList
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    
    func CollapseButtonTapped(sender: AnyObject) {
        
        self.BottomBarOutlet?.isHidden = false
        
        self.ExpandedBottomOutlet?.isHidden = true
        
        ServicesCV.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 45, right: 0)
        
    }
    
    func ShareButtonTapped(sender: AnyObject) {
        
        let textToShare = "Check out this app"
        
        if let myWebsite = NSURL(string: "https://www.google.com/") {
            let objectsToShare: [Any] = [textToShare, myWebsite]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            activityVC.popoverPresentationController?.sourceView = self.ExpandedBottomOutlet.ShareButton
            self.present(activityVC, animated: true, completion: nil)
        }
        
    }
    
    func ExpandButtonTapped(sender: AnyObject) {
        
        self.BottomBarOutlet?.isHidden = true
        
        self.ExpandedBottomOutlet?.isHidden = false
        
        ServicesCV.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 95, right: 0)
    }
    
    func HomeButtonTapped(sender: AnyObject) {
        
        let vc = MainViewController()
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func ServicesAction(sender: AnyObject) {
        
        if bool == true {
            
            let vc = ServicesViewController()
            
            vc.selected = true
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        
    }
    
    func FloorAction(sender: AnyObject) {
        
        let vc = FloorViewController()
        
        vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func ProfilebuttonTapped(sender: AnyObject) {
        
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            let vc = LoginViewController()
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else {
            
            let vc = ProfileVC()
            
            vc.selected = true
            
            self.navigationController?.pushViewController(vc, animated: false)
        }
        
    }
    func ArbuttonTapped(sender: AnyObject) {
        
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            let vc = Tuto1ViewController()
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else {
            
            ARGenieManager.shared()?.loadAugmentedRealityData(nil)
            
        }
    }
    func ArExbuttonTapped(sender: AnyObject) {
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            let vc = Tuto1ViewController()
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else {
            
            ARGenieManager.shared()?.loadAugmentedRealityData(nil)
            
        }
    }
    
    func FaqbuttonTaped(sender: AnyObject) {
        let vc = FAQViewController()
        
        vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func UserAgreeTapped(sender: AnyObject) {
        
        let vc = UserAgreementVC()
        
        vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func LegalTapped(sender: AnyObject) {
        let vc = LeagalViewController()
        
        vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func LibraryTapped(sender: AnyObject) {
        let vc = LibraryVC()
        
        vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
        
    }
    func showsettings(sender: AnyObject) {
        
        self.alertToEncourageCameraAccessInitially()
        
    }
    func showsetting(sender: AnyObject) {
        
        self.alertToEncourageCameraAccessInitially()
        
    }
    
    func showLibrary(sender: AnyObject) {
        
         self.alertToEncourageLibraryAccessInitially()
    }
}

