//
//  EventsViewController.swift
//  MallAppSwift
//
//  Created by Freelancer on 28/06/19.
//  Copyright © 2019 Arianne Peiso. All rights reserved.
//

import UIKit


protocol MyCellDelegate: AnyObject {
    
    func btnTapped(cell: EventsTableViewCell)
    
}

class EventsViewController: BaseViewController,UITableViewDelegate, UITableViewDataSource,MyCellDelegate, BotoomViewDelegate, ExpandBottomViewDelegate {
    
    @IBOutlet weak var bottomExpanded: ExpandedBottomBar!
    @IBOutlet weak var bottombar: BottomBar!
    var EventList: [AnyHashable] = []
    var EventDetailslist: [AnyHashable] = []
    
    var Indexpath : Int = 0
    
    let theme = ThemeClass()
    
    @IBOutlet weak var EventsTV: UITableView!
    
    let TABLE_NAME = "events"
    let TABLE_NAME1 = "events_agenda"
    let COLUMN_TIDENTIFIER = "tid"
    let COLUMN_DESCRIPTION = "description"
    let COLUMN_TITLE = "title"
    
    let COLUMN_IDENTIFIER = "id"
    let COLUMN_ETITLE = "title"
    let COLUMN_EDESCRIPTION = "description"
    let COLUMN_IMAGE = "image"
    let COLUMN_SHORT_DESCRIPTION = "shortDescription"
    let COLUMN_DATE = "date"
    let COLUMN_LOCATION = "location"
    
    var filteredArray = [EventModel]()
    var EventsDetailsArray = [EventDetailsModel]()
    
    var bottom = BottomBar()
    
    var exbottom = ExpandedBottomBar()

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        if "\(UserDefaults.standard.value(forKey: "DeviceLanguage")!)" == "en" ||  "\(UserDefaults.standard.value(forKey: "DeviceLanguage")!)" == "fr" || "\(UserDefaults.standard.value(forKey: "DeviceLanguage")!)" == "es"{
//            
//            UIView.appearance().semanticContentAttribute = .forceLeftToRight
//            
//        }
//        else
//        {
//            
//            UIView.appearance().semanticContentAttribute = .forceRightToLeft
//            
//            DispatchQueue.main.async {
//                
//                self.bottombar.semanticContentAttribute = .forceLeftToRight
//                self.bottomExpanded.semanticContentAttribute = .forceLeftToRight
//                
//            }
//        }
        
        if NetworkConnection.isConnectedToNetwork(){
            self.EventsDetailsArray = self.getEventdetails() as! [EventDetailsModel]

        }
        else{
            
            self.showAlert(Alertmsg: "Alert", msg: networkAlert.networkmsg)
        }
        // Do any additional setup after loading the view.
        self.bottomExpanded.isHidden = true
        
        self.bottomExpanded.delegate = self
        self.bottombar.delegate = self
        print(self.EventsDetailsArray)
        
        
        self.EventsTV.separatorStyle = .none
                
        EventsTV.register(UINib(nibName: "EventsTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        self.showLoader()
        self.bottombar.HomeButton.setImage(UIImage(named: "Home-9"), for: .normal)
        
        EventsTV.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 52, right: 0)
    }


    
   
    
    func getEventdetails() -> [AnyHashable]? {
        
        
        let database: FMDatabase? = CommonsUtils.getCommonUtil().getLocalDB()
        if database?.open() == nil {
            print("Problems to open DB, please check")
            return EventDetailslist
        }
        
        // FMDBQuickCheck(!database?.hasOpenResultSets())
        
        let resultSet = database?.executeQuery("select * from \(TABLE_NAME1)", withArgumentsIn: [])
        //        let resultSet: FMResultSet? = database?.execute("SELECT DISTINCT e.tid, e.title FROM events e INNER JOIN products_events pe ON e.tid=pe.id_event ORDER BY e.tid")
        while ((resultSet?.next())!) {
            
            let EventsDetails = EventDetailsModel()
            //parse data
         
            EventsDetails.id = (resultSet?.int(forColumn: self.COLUMN_IDENTIFIER))!
            EventsDetails.image = (resultSet?.string(forColumn: self.COLUMN_IMAGE))!
            EventsDetails.shortDescription = (resultSet?.string(forColumn: self.COLUMN_SHORT_DESCRIPTION))!
            EventsDetails.EDdescription = (resultSet?.string(forColumn: self.COLUMN_DESCRIPTION))!
            EventsDetails.date = (resultSet?.double(forColumn: self.COLUMN_DATE))!
            EventsDetails.location = (resultSet?.string(forColumn: self.COLUMN_LOCATION))!
            EventsDetails.title = (resultSet?.string(forColumn: self.COLUMN_TITLE))!
            
            EventDetailslist.append(EventsDetails)
            
        }
        
        resultSet?.close()
        database?.close()
        return EventDetailslist
    }
    
        
    func btnTapped(cell: EventsTableViewCell) {
        
        let vc = EventsDetailsVC()

        let indexPath = self.EventsTV.indexPath(for: cell)
        print(indexPath!.row)

        self.Indexpath = indexPath!.row
        
        print(self.EventsDetailsArray[self.Indexpath].image!)

        vc.imageString = self.EventsDetailsArray[self.Indexpath].image
        vc.date = "\(cell.dateLbl.text!)"
        vc.descriptionString = self.EventsDetailsArray[self.Indexpath].EDdescription?.htmlToString
      //  vc.time = self.EventsDetailsArray[self.Indexpath].date 
        vc.EDtitle = self.EventsDetailsArray[self.Indexpath].title

        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.EventsDetailsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! EventsTableViewCell
        
        cell.selectionStyle = .none

        let Object:NSDictionary = theme.getButtonView()
        
        let Object2:NSDictionary = theme.getEventsView()
        
        let color = hexStringToUIColor(hex: Object.object(forKey: "BackGroundColor")! as! String)
        
        let color1 = hexStringToUIColor(hex: Object.object(forKey: "TextColor")! as! String)
        
        let color2 =  hexStringToUIColor(hex: Object2.object(forKey: "DescColor")! as! String)
        
        cell.DetailButtonOutlet.backgroundColor = color
        
        cell.TitleLbl.font = UIFont(name:Object2.object(forKey: "TextFont")! as! String, size: Object2.object(forKey: "TextSize")! as! CGFloat)
        
         cell.TitleLbl.textColor = color
        
         cell.dateLbl.textColor = color
        
        cell.dateLbl.font = UIFont(name:Object2.object(forKey: "DateFont")! as! String, size: Object2.object(forKey: "DateSize")! as! CGFloat)
        
        cell.DetailButtonOutlet.setTitleColor(color1, for: .normal)
        
        cell.DescriptionLbl.textColor = color2
        
        cell.DescriptionLbl.font = UIFont(name:Object2.object(forKey: "DescFont")! as! String, size: Object2.object(forKey: "DescSize")! as! CGFloat)
        
        
       
        
        cell.delegate = self
                
        let catPictureURL = URL(string: self.EventsDetailsArray[indexPath.row].image!)!
        
        // Creating a session object with the default configuration.
        // You can read more about it here https://developer.apple.com/reference/foundation/urlsessionconfiguration
        let session = URLSession(configuration: .default)
        
        // Define a download task. The download task will download the contents of the URL as a Data object and then you can do what you wish with that data.
        let downloadPicTask = session.dataTask(with: catPictureURL) { (data, response, error) in
            // The download has finished.
            if let e = error {
                print("Error downloading cat picture: \(e)")
            } else {
                // No errors found.
                // It would be weird if we didn't have a response, so check for that too.
                if let res = response as? HTTPURLResponse {
                    print("Downloaded cat picture with response code \(res.statusCode)")
                    if let imageData = data {
                        // Finally convert that Data into an image and do what you wish with it.
                        let image = UIImage(data: imageData)
                        
                        DispatchQueue.main.async {
                            cell.EventsImage.image = image
                            self.hideLoader()
                        }
                        // Do something with your image.
                    } else {
                        print("Couldn't get image: Image is nil")
                        self.hideLoader()
                    }
                } else {
                    print("Couldn't get response code for some reason")
                    self.hideLoader()
                }
            }
        }
        
        downloadPicTask.resume()
        
        cell.TitleLbl.text = self.EventsDetailsArray[indexPath.row].title?.uppercased()


        let date = (self.EventsDetailsArray[indexPath.row].date!)
        
        let dateVar = Date(timeIntervalSince1970: TimeInterval(date))
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone() as TimeZone
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        dateFormatter.dateFormat = "mm/dd/yyyy"
        print(dateFormatter.string(from: dateVar))
        //dateFormatter.date(from: String(describing: dateVar))
        //let updatedTimeStamp = dateVar
        //let converteddate = DateFormatter.localizedString(from: updatedTimeStamp as Date, dateStyle: DateFormatter.Style.medium, timeStyle: .none)
        let convertedate = dateFormatter.string(from: dateVar)
        cell.dateLbl.text = convertedate
        
        cell.DescriptionLbl.text = self.EventsDetailsArray[indexPath.row].shortDescription?.htmlToString
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
   
    func ServicesAction(sender: AnyObject) {
        
        let vc = ServicesViewController()
        
         vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func FloorAction(sender: AnyObject) {
        
        let vc = FloorViewController()
        
         vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func CollapseButtonTapped(sender: AnyObject) {
        
        self.bottombar?.isHidden = false
        
        self.bottomExpanded?.isHidden = true
        
        EventsTV.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 52, right: 0)
        
    }
    
    func ShareButtonTapped(sender: AnyObject) {
        
        let textToShare = "Check out this app"
        
        if let myWebsite = NSURL(string: "https://www.google.com/") {
            let objectsToShare: [Any] = [textToShare, myWebsite]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            activityVC.popoverPresentationController?.sourceView = self.bottomExpanded.ShareButton
            self.present(activityVC, animated: true, completion: nil)
        }
        
    }
    
    func ExpandButtonTapped(sender: AnyObject) {
        
        self.bottombar?.isHidden = true
        
        self.bottomExpanded?.isHidden = false
        
        EventsTV.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 120, right: 0)
    }
    
    func HomeButtonTapped(sender: AnyObject) {
        
        let vc = MainViewController()
        
         self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func ProfilebuttonTapped(sender: AnyObject) {
        
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            let vc = LoginViewController()
            
             vc.ProfileBool = true
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else {
            
            let vc = ProfileVC()
            
             vc.selected = true
            
            self.navigationController?.pushViewController(vc, animated: false)
        }
        
    }
    func ArbuttonTapped(sender: AnyObject) {
        
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            let vc = Tuto1ViewController()
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else {
            
            ARGenieManager.shared()?.loadAugmentedRealityData(nil)
            
        }
    }
    func ArExbuttonTapped(sender: AnyObject) {
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            let vc = Tuto1ViewController()
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else {
            
            ARGenieManager.shared()?.loadAugmentedRealityData(nil)
            
        }
    }
    
    func FaqbuttonTaped(sender: AnyObject) {
        let vc = FAQViewController()
        
         vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
        
    }
    
    func UserAgreeTapped(sender: AnyObject) {
        
        let vc = UserAgreementVC()
        
         vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func LegalTapped(sender: AnyObject) {
        let vc = LeagalViewController()
        
         vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
    }

    func LibraryTapped(sender: AnyObject) {
        let vc = LibraryVC()
        
         vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
        
    }
    
    func showsettings(sender: AnyObject) {
        
        self.alertToEncourageCameraAccessInitially()
        
    }
    func showsetting(sender: AnyObject) {
        
        self.alertToEncourageCameraAccessInitially()
        
    }
    
    func showLibrary(sender: AnyObject) {
        
        self.alertToEncourageLibraryAccessInitially()
    }
    
    @IBAction func CloseAction(_ sender : UIButton){
        
        self.navigationController?.popToRootViewController(animated: false)
    }
    
    func setbuttonView(){
        
        
        
        
        
    }
}
