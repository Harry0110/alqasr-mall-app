//
//  FAQViewController.swift
//  MallAppSwift
//
//  Created by Uday kanth Bangaru on 08/07/19.
//  Copyright © 2019 Arianne Peiso. All rights reserved.
//

import UIKit

class FAQViewController: BaseViewController,ExpandBottomViewDelegate,BotoomViewDelegate {
    
    @IBOutlet  var faqview : UIView!
    
    //var faqview : FAQView!
    @IBOutlet weak var BottomViewOutlet : BottomBar!
    
    var faqmodel = FaqItems()
    
    let TABLE_NAME = "faq"
    
    let COLUMN_QUESTION = "question"
    let COLUMN_ANSWER = "response"
    
    var faqarray  = [FaqItems]()
    
    @IBOutlet weak var EXpandedBottomOutlet : ExpandedBottomBar!
    
    var FaqList: [AnyHashable] = []

    var faqview1 : FAQView!
    
    var selected : Bool!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.automaticallyAdjustsScrollViewInsets = false
        
        if NetworkConnection.isConnectedToNetwork(){
            self.faqarray = self.getFaq() as! [FaqItems]

        }
        else{
            
            self.showAlert(Alertmsg: "Alert", msg: networkAlert.networkmsg)
        }

        
//        let items = [FAQItem(question: "What is reddit?", answer: "reddit is a source for what's new and popular on the web. Users like you provide all of the content and decide, through voting, what's good and what's junk. Links that receive community approval bubble up towards #1, so the front page is constantly in motion and (hopefully) filled with fresh, interesting links."), FAQItem(question: "What does the name \"reddit\" mean?", answer: "It's (sort of) a play on words -- i.e., \"I read it on reddit.\" Also, there are some unintentional but interesting Latin meanings to the word \"reddit\"."), FAQItem(question: "How is a submission's score determined?", answer: "A submission's score is simply the number of upvotes minus the number of downvotes. If five users like the submission and three users don't it will have a score of 2. Please note that the vote numbers are not \"real\" numbers, they have been \"fuzzed\" to prevent spam bots etc. So taking the above example, if five users upvoted the submission, and three users downvote it, the upvote/downvote numbers may say 23 upvotes and 21 downvotes, or 12 upvotes, and 10 downvotes. The points score is correct, but the vote totals are \"fuzzed\".")]
        self.BottomViewOutlet.isHidden = false
        self.EXpandedBottomOutlet.isHidden = true
        self.faqview1 = FAQView(frame: self.faqview.frame, items: self.faqarray)
        
        self.faqview.addSubview(faqview1)

        //faqview.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.backgroundColor = #colorLiteral(red: 0.797960043, green: 0.797960043, blue: 0.797960043, alpha: 1)
//        self.BottomViewOutlet.backgroundColor = UIColor.lightGray
//        self.EXpandedBottomOutlet.backgroundColor = UIColor.lightGray
        self.BottomViewOutlet.delegate = self
        self.EXpandedBottomOutlet.delegate = self
        view.addSubview(faqview)
        //addFaqViewConstraints()
        
        if selected == true {
            
            self.EXpandedBottomOutlet.FAQButton.setImage(UIImage(named: "FAQ-9"), for: .normal)
            self.BottomViewOutlet.HomeButton.setImage(UIImage(named: "Home-9"), for: .normal)

        }
        
        faqview.bringSubviewToFront(BottomViewOutlet)
        faqview.bringSubviewToFront(EXpandedBottomOutlet)
        faqview.backgroundColor = #colorLiteral(red: 0.797960043, green: 0.797960043, blue: 0.797960043, alpha: 1)
        
        
        faqview1.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 60, right: 0)
    }

    
    func getFaq() -> [AnyHashable]? {
        
        let database: FMDatabase? = CommonsUtils.getCommonUtil().getLocalDB()
        if database?.open() == nil {
            print("Problems to open DB, please check")
            return FaqList
        }
        
        // FMDBQuickCheck(!database?.hasOpenResultSets())
        
        let resultSet = database?.executeQuery("select * from \(TABLE_NAME)", withArgumentsIn: [])
        //        let resultSet: FMResultSet? = database?.execute("SELECT DISTINCT e.tid, e.title FROM events e INNER JOIN products_events pe ON e.tid=pe.id_event ORDER BY e.tid")
        while ((resultSet?.next())!) {
            
            let faq = FaqItems()
            //parse data
            faq.question = (resultSet?.string(forColumn: self.COLUMN_QUESTION))!
            faq.response = (resultSet?.string(forColumn: self.COLUMN_ANSWER))!
           
            FaqList.append(faq as AnyHashable)
            
        }
        
        resultSet?.close()
        database?.close()
        return FaqList
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func addFaqViewConstraints() {
        let faqViewTrailing = NSLayoutConstraint(item: faqview, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailingMargin, multiplier: 1, constant: 17)
        let faqViewLeading = NSLayoutConstraint(item: faqview, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leadingMargin, multiplier: 1, constant: -17)
        let faqViewTop = NSLayoutConstraint(item: faqview, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 0)
        let faqViewBottom = NSLayoutConstraint(item: self.view, attribute: .bottom, relatedBy: .equal, toItem: faqview, attribute: .bottom, multiplier: 1, constant: 0)
        //let faqViewheight = NSLayoutConstraint(item:faqview , attribute: .height, relatedBy: .equal, toItem: faqview, attribute: .notAnAttribute, multiplier: 1, constant: UIScreen.main.bounds.height - 140)
       
    NSLayoutConstraint.activate([faqViewTop, faqViewBottom, faqViewLeading, faqViewTrailing])
    }

    
    
    
    
    func ServicesAction(sender: AnyObject) {
        
        let vc = ServicesViewController()
        
        vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func FloorAction(sender: AnyObject) {
        
        let vc = FloorViewController()
        
        vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func CollapseButtonTapped(sender: AnyObject) {
        
        self.BottomViewOutlet?.isHidden = false
        
        self.EXpandedBottomOutlet?.isHidden = true
        
        faqview1.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 60, right: 0)
    }
    
    func ShareButtonTapped(sender: AnyObject) {
        
        //self.EXpandedBottomOutlet.ShareButton.setImage(UIImage(named: "Share-10"), for: .normal)
        let textToShare = "Check out this app"
        
        if let myWebsite = NSURL(string: "https://www.google.com/") {
            let objectsToShare: [Any] = [textToShare, myWebsite]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            activityVC.popoverPresentationController?.sourceView = self.EXpandedBottomOutlet.ShareButton
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    
    func ExpandButtonTapped(sender: AnyObject) {
        
        self.BottomViewOutlet?.isHidden = true
        
        self.EXpandedBottomOutlet?.isHidden = false
        
        self.EXpandedBottomOutlet.FAQButton.setImage(UIImage(named: "FAQ-9"), for: .normal)
        
        faqview1.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 110, right: 0)
    }
    
    func HomeButtonTapped(sender: AnyObject) {
        
        let vc = MainViewController()
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func ProfilebuttonTapped(sender: AnyObject) {
        
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            let vc = LoginViewController()
            
             vc.ProfileBool = true
                        
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else {
            
            let vc = ProfileVC()
            
             vc.selected = true
            
            self.navigationController?.pushViewController(vc, animated: false)
        }
        
    }
    func ArbuttonTapped(sender: AnyObject) {
        
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            let vc = Tuto1ViewController()
            
             //vc.selected = true
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else {
            
            ARGenieManager.shared()?.loadAugmentedRealityData(nil)
            
        }
    }
    func ArExbuttonTapped(sender: AnyObject) {
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            let vc = Tuto1ViewController()
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else {
            
            ARGenieManager.shared()?.loadAugmentedRealityData(nil)
            
        }
    }
    func FaqbuttonTaped(sender: AnyObject) {
        
    }
    
    func UserAgreeTapped(sender: AnyObject) {
        
        let vc = UserAgreementVC()
        
         vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func LegalTapped(sender: AnyObject) {
        
        let vc = LeagalViewController()
        
         vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func LibraryTapped(sender: AnyObject) {
        let vc = LibraryVC()
        
        vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
        
    }

    func showsettings(sender: AnyObject) {
        
        self.alertToEncourageCameraAccessInitially()
        
    }
    func showsetting(sender: AnyObject) {
        
        self.alertToEncourageCameraAccessInitially()
        
    }
    
    func showLibrary(sender: AnyObject) {
        
        self.alertToEncourageLibraryAccessInitially()
    }
}
