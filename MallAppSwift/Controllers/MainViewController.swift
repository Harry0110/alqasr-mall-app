//
//  MainViewController.swift
//  SampleSDKSwift
//
//  Created by Arianne Peiso on 16/06/2019.
//  Copyright © 2019 Arianne Peiso. All rights reserved.
//

import UIKit
import ARGenieSDK
import AVFoundation


class MainViewController: BaseViewController, ARGenieManagerDownloadCallback, UITableViewDelegate, UITableViewDataSource,BotoomViewDelegate,ExpandBottomViewDelegate {
    
    
    func didARGenieManagerDownloadFinish(_ error: Error!)
    {
        
        
        
    }
    
    
    @IBOutlet weak var MainViewTV: UITableView!
    @IBOutlet weak var bottomView: BottomBar!
    @IBOutlet weak var bottomExpandView: ExpandedBottomBar!
    
    let theme = ThemeClass()
    
    var arrayofImages = [String]()
    
    var arrayofTitles = [String]()
    
    let service = UserService()
    
    var loggedOut = false
        
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setTheme()
        self.bottomView.semanticContentAttribute = .forceLeftToRight
        self.bottomExpandView.semanticContentAttribute = .forceLeftToRight
        
        self.bottomView?.delegate = self
        
        self.bottomExpandView?.delegate = self
        
        //self.bottomView?.isHidden = false
        
        self.bottomExpandView?.isHidden = true
        MainViewTV.register(UINib(nibName: "MainTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        self.navigationController?.isNavigationBarHidden = true
        
        MainViewTV.reloadData()
        
        MainViewTV.contentInset  = UIEdgeInsets(top: 0, left: 0, bottom: 52, right: 0)
        

        let path: String = Bundle.main.path(forResource: "Home", ofType: "plist")!
        let ConfigFile: NSDictionary = NSDictionary(contentsOfFile: path)!
        print(ConfigFile.object(forKey: "ImageArray") as! NSArray)
        
        self.arrayofImages = (ConfigFile.object(forKey: "ImageArray") as! NSArray) as! [String]
        self.arrayofTitles = (ConfigFile.object(forKey: "TitleArray") as! NSArray) as! [String]
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        if UserDefaults.standard.value(forKey: "App_Token_Expires") == nil{
            
            self.getaccesstoken{ () -> () in
                self.CheckIfAppTokenExpired()
            }
            
        }else {
            
            self.CheckIfAppTokenExpired()
            
        }
        
        if loggedOut == true {
            
            self.getaccesstoken{ () -> () in
                self.CheckIfAppTokenExpired()
            }
            
        }
        
        self.bottomExpandView?.isHidden = true
        self.bottomView?.isHidden = false
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayofImages.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! MainTableViewCell
        
        cell.selectionStyle = .none
        
        let Object:NSDictionary = theme.getMainView()
        
        print(Object.object(forKey: "TextSize")!)
        
        cell.TitleLbl.font = UIFont(name:Object.object(forKey: "TextFont")! as! String, size: Object.object(forKey: "TextSize")! as! CGFloat)
        
        cell.ImageView.image = UIImage.init(named: arrayofImages[indexPath.row])
        
        cell.TitleLbl.text = NSLocalizedString(arrayofTitles[indexPath.row], comment: "")
        
        let color = hexStringToUIColor(hex: Object.object(forKey: "TextColor")! as! String)
        
        print(Object.object(forKey: "TextColor")! as! String)
        
        print(color)
        
        cell.TitleLbl.textColor = color
        
        return cell
        
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            
            let vc = Tuto1ViewController()
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
            
        else if indexPath.row == 1 {
            
            let vc = EventsViewController()
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        else if indexPath.row == 2 {
            
            let vc = ServicesViewController()
            
            vc.bool = true
            vc.titleName = self.arrayofTitles[indexPath.row]
            vc.selectedID = 151
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
            
        else if indexPath.row == 3 {
            
            let vc = NewsViewController()
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        } else if indexPath.row == 4 {
            
            let vc = ServicesViewController()
            vc.bool = true
            vc.titleName = self.arrayofTitles[indexPath.row]
            vc.selectedID = 152
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        else if indexPath.row == 5 {
            
            let vc = ServicesViewController()
            vc.bool = true
            vc.titleName = self.arrayofTitles[indexPath.row]
            vc.selectedID = 153
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        else if indexPath.row == 6 {
            
            let vc = ServicesViewController()
            vc.bool = true
            vc.titleName = self.arrayofTitles[indexPath.row]
            vc.selectedID = 154
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        else if indexPath.row == 7 {
            
            let vc = ServicesViewController()
            vc.bool = true
            vc.titleName = self.arrayofTitles[indexPath.row]
            vc.selectedID = 155
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        else if indexPath.row == 8 {
            
            let vc = ServicesViewController()
            vc.bool = true
            vc.titleName = self.arrayofTitles[indexPath.row]
            vc.selectedID = 156
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        
        
    }
    
    
    func getaccesstoken(handleComplete:@escaping (()->())){
        
        if NetworkConnection.isConnectedToNetwork(){
            
            
            self.showLoader()
            
            service.getaccesstoken(complition:{(myresult) in
                
                DispatchQueue.main.async {
                    
                    if let result = myresult {
                        print(result)
                        
                        
                        let token = result["token"] as? String
                        
                        let Expires = result["expires_in"] as? String
                        
                        print(Expires!)
                        
                        UserDefaults.standard.set(Expires!, forKey: "App_Token_Expires")
                        
                        UserDefaults.standard.set(token!, forKey: "App_Token")
                        
                        print(UserDefaults.standard.value(forKey: "App_Token")!)
                        
                        handleComplete()
                        
                    }
                    
                    
                }
                
                
                
            })
            
            self.hideLoader()
            
        }else {
            
            self.showAlert(Alertmsg: "Alert", msg: networkAlert.networkmsg)
            
        }
    }
    
    func CheckIfAppTokenExpired(){
        
        let date = Date(timeIntervalSince1970: Double("\(UserDefaults.standard.value(forKey: "App_Token_Expires")!)")!)
        
        let now = NSDate()
        if( now.timeIntervalSince(date) > 0 ) {
            print("now > dateTimeStamp")
        } else {
            
            self.getaccesstoken{ () -> () in
                
            }
        }
        
        
        
    }
    
    func ServicesAction(sender: AnyObject) {
        
        let vc = ServicesViewController()
        
        vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func FloorAction(sender: AnyObject) {
        
        //self.bottomView?.FloorButton.setBackgroundImage(UIImage(named: "Flor plans-8"), for: .normal)
        
        let vc = FloorViewController()
        
        vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func CollapseButtonTapped(sender: AnyObject) {
        
        self.bottomView?.isHidden = false
        
        self.bottomExpandView?.isHidden = true
        
        MainViewTV.contentInset  = UIEdgeInsets(top: 0, left: 0, bottom: 52, right: 0)
        

        
    }
    
    func ShareButtonTapped(sender: AnyObject) {
        
        let textToShare = "Check out this app"
        
        if let myWebsite = NSURL(string: "https://www.google.com/") {
            let objectsToShare: [Any] = [textToShare, myWebsite]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            //            activityVC.popoverPresentationController?.delegate = self as? UIPopoverPresentationControllerDelegate
            
            activityVC.popoverPresentationController?.sourceView = self.bottomExpandView?.ShareButton
            self.present(activityVC, animated: true, completion: nil)
        }
        
    }
    
    func ExpandButtonTapped(sender: AnyObject) {
        
        self.bottomView?.isHidden = true
    
        self.bottomExpandView?.isHidden = false
        
        MainViewTV.contentInset  = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)
        

    }
    
    
    func HomeButtonTapped(sender: AnyObject) {
        
        
    }
    
    func ProfilebuttonTapped(sender: AnyObject) {
        
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            let vc = LoginViewController()
            
            vc.ProfileBool = true
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else {
            
            let vc = ProfileVC()
            
            vc.selected = true
            self.navigationController?.pushViewController(vc, animated: false)
        }
        
    }
    func ArbuttonTapped(sender: AnyObject) {
        
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            let vc = Tuto1ViewController()
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else {
            
            ARGenieManager.shared()?.loadAugmentedRealityData(nil)
            
        }
    }
    func ArExbuttonTapped(sender: AnyObject) {
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            let vc = Tuto1ViewController()
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else {
            
            ARGenieManager.shared()?.loadAugmentedRealityData(nil)
            
        }
    }
    
    func FaqbuttonTaped(sender: AnyObject) {
        
        let vc = FAQViewController()
        
        vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func UserAgreeTapped(sender: AnyObject) {
        
        let vc = UserAgreementVC()
        
        vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func LegalTapped(sender: AnyObject) {
        let vc = LeagalViewController()
        
        vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func LibraryTapped(sender: AnyObject) {
        
        let vc = LibraryVC()
        
        vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    func showsettings(sender: AnyObject) {
        
        self.alertToEncourageCameraAccessInitially()
        
    }
    
    func showsetting(sender: AnyObject) {
        
        self.alertToEncourageCameraAccessInitially()
        
    }
    func showLibrary(sender: AnyObject) {
        
        self.alertToEncourageLibraryAccessInitially()
    }
    
    func setTheme(){
        
        let Object:NSDictionary = theme.getMainView()
    
        print(Object.object(forKey: "TextSize")!)
        
    }
    
}
