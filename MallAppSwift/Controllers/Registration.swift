//
//  Registration.swift
//  MallAppSwift
//
//  Created by Uday kanth Bangaru on 08/07/19.
//  Copyright © 2019 Arianne Peiso. All rights reserved.
//

import Foundation
import  UIKit

class Registration : BaseViewController,BotoomViewDelegate,ExpandBottomViewDelegate,UITextFieldDelegate {
   
    
    @IBOutlet weak var RegisterButtonText: UIButton!
    @IBOutlet weak var CheckBoxText: UILabel!
    @IBOutlet weak var InscriptionText: UILabel!
    @IBOutlet weak var WelcomeText: UILabel!
    @IBOutlet weak var RecievePromotionsText: UILabel!
    @IBOutlet weak var Scrollview : UIScrollView!

    
    var checkboxBool1 = false
    
    var checkboxBool2 = false
    
    let service = UserService()
    
    let theme = ThemeClass()
    
    @IBOutlet weak var ConfirmPassLbl: UITextField!
    @IBOutlet weak var PasswordLbl: UITextField!
    @IBOutlet weak var ConfirmLbl: UITextField!
    @IBOutlet weak var EmailLbl: UITextField!
    @IBOutlet weak var LastNameLbl: UITextField!
    @IBOutlet weak var FirstNamelbl: UITextField!
    @IBOutlet weak var CheckBox1: UIButton!
    @IBOutlet weak var CheckBox2: UIButton!
    
    @IBOutlet weak var BottomviewOutlet : BottomBar!
    @IBOutlet weak var ExpandBottomviewOutlet : ExpandedBottomBar!
    
    
    
    override func viewDidLoad() {
        
        settext()
        ConfirmPassLbl.delegate = self
        PasswordLbl.delegate = self
        ConfirmLbl.delegate = self
        EmailLbl.delegate = self
        LastNameLbl.delegate = self
        FirstNamelbl.delegate = self
        
        self.BottomviewOutlet.isHidden = false
        
        self.ExpandBottomviewOutlet.isHidden = true
        
        self.BottomviewOutlet.delegate = self
        
        self.ExpandBottomviewOutlet.delegate = self
        
        UserDefaults.standard.set("0", forKey: "Opt")
        self.BottomviewOutlet.HomeButton.setImage(UIImage(named: "Home-9"), for: .normal)
        
        Scrollview.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 51, right: 0)
    }
    
    func ServicesAction(sender: AnyObject) {
        
        let vc = ServicesViewController()
        
        vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func FloorAction(sender: AnyObject) {
        
        let vc = FloorViewController()
        
        vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
   
    func ShareButtonTapped(sender: AnyObject) {
        
        let textToShare = "Check out this app"
        
        if let myWebsite = NSURL(string: "https://www.google.com/") {
            let objectsToShare: [Any] = [textToShare, myWebsite]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            activityVC.popoverPresentationController?.sourceView = self.ExpandBottomviewOutlet.ShareButton
            self.present(activityVC, animated: true, completion: nil)
        }
        
    }
    
  
    
    func HomeButtonTapped(sender: AnyObject) {
        let vc = MainViewController()
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func ProfilebuttonTapped(sender: AnyObject) {
        
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            let vc = LoginViewController()
            
             vc.ProfileBool = true
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else {
            
            let vc = ProfileVC()
            
             vc.selected = true
            
            self.navigationController?.pushViewController(vc, animated: false)
        }
        
    }
    func ArbuttonTapped(sender: AnyObject) {
        
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            let vc = Tuto1ViewController()
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else {
            
            ARGenieManager.shared()?.loadAugmentedRealityData(nil)
            
        }
    }
    func ArExbuttonTapped(sender: AnyObject) {
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            let vc = Tuto1ViewController()
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else {
            
            ARGenieManager.shared()?.loadAugmentedRealityData(nil)
            
        }
    }
  
    
    func ExpandButtonTapped(sender: AnyObject) {
        
        self.BottomviewOutlet.isHidden = true
        
        self.ExpandBottomviewOutlet.isHidden = false
    }
    

    
    func CollapseButtonTapped(sender: AnyObject) {
        self.BottomviewOutlet.isHidden = false
        
        self.ExpandBottomviewOutlet.isHidden = true
        
    }
    

    func FaqbuttonTaped(sender: AnyObject) {
        
        let vc = FAQViewController()
        
         vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func LegalTapped(sender: AnyObject) {
        let vc = LeagalViewController()
        
         vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
   
    
    
    @IBAction func CheckBox1(_ sender: UIButton) {
        if checkboxBool1 == false {
            
            self.checkboxBool1 = true
            self.CheckBox1.setImage(UIImage(named: "Asset 56-8"), for: .normal)
            UserDefaults.standard.set("1", forKey: "Opt")
        }
        else {
            
             self.checkboxBool1 = false
             self.CheckBox1.setImage(UIImage(named: "radio_btn-9"), for: .normal)
            UserDefaults.standard.set("0", forKey: "Opt")

        }
        
        
    }
    
    
    @IBAction func CheckBox2(_ sender: UIButton) {
        if checkboxBool2 == false {
            
            self.checkboxBool2 = true
            self.CheckBox2.setImage(UIImage(named: "Asset 56-8"), for: .normal)
            
        }
        else {
            
            self.checkboxBool2 = false
            self.CheckBox2.setImage(UIImage(named: "radio_btn-9"), for: .normal)
        }
        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        //or
        //self.view.endEditing(true)
        return true
    }
    
    
    func CreateUser(){
        
        if NetworkConnection.isConnectedToNetwork(){
            
            self.showLoader()
            
            print(UserDefaults.standard.value(forKey: "Opt") as! String)
            print(UserDefaults.standard.value(forKey: "App_Token") as! String)
        
            
            service.createUser(with: self.FirstNamelbl.text!, last_name: self.LastNameLbl.text!, email: self.EmailLbl.text!, password: self.PasswordLbl.text!, optin_newsletters: UserDefaults.standard.value(forKey: "Opt") as! String , app_token:UserDefaults.standard.value(forKey: "App_Token") as! String, target:self,complition:{(myresult) in
                
                
                DispatchQueue.main.async {
                    
                    if let result = myresult {
                        print(result)
                        
                        
                        if result["error"] != nil {
                            
                            let error = result["error"] as! String
                            
                            let errordesc = result["error_description"] as! String
                            
                            self.showAlert(Alertmsg:error , msg: errordesc)
                            
                        }
                        else{
                            
                            UserDefaults.standard.set(self.FirstNamelbl.text, forKey: "UserFirstName")
                            UserDefaults.standard.set(self.LastNameLbl.text, forKey: "UserLastName")
                            UserDefaults.standard.set(self.EmailLbl.text, forKey: "UserEmail")
                            
                            self.authenticateUser()
                            
                        }
                        
                    }
                }
                
                
                
            })
            
            self.hideLoader()
        }else {
            
            self.showAlert(Alertmsg: "Alert", msg: networkAlert.networkmsg)
            
        }
    }
    
    
    func authenticateUser(){
        
        if NetworkConnection.isConnectedToNetwork(){
            
            
            self.showLoader()
            
            service.authenticateUser(with: "password", client_id:"alqasrid", username: self.EmailLbl.text!, password:self.PasswordLbl.text!, client_secret:"@lq@sr", target:self,complition:{(myresult) in
                
                
                DispatchQueue.main.async {
                    
                    if let result = myresult {
                        print(result)
                        
                        
                        if result["error"] != nil {
                            
                            let error = result["error"] as! String
                            
                            let errordesc = result["error_description"] as! String
                            
                            
                            self.showAlert(Alertmsg: error, msg: errordesc)
                            
                        }
                        else{
                            
                            let accessToken = result["access_token"] as? String
                            
                            UserDefaults.standard.set(accessToken, forKey: "DeviceId")
                            
                            let Expires = result["expires_in"] as? Int
                            
                            print(Expires!)
                            
                            UserDefaults.standard.set(Expires!, forKey: "Access_Token_Expires")
                            
                            let refreshToken = result["refresh_token"] as? String
                            
                            UserDefaults.standard.set(refreshToken, forKey: "RefreshToken")
                            
                            let vc = ProfileVC()
                            
                            self.navigationController?.pushViewController(vc, animated: false)
                            
                        }
                        
                    }
                }
                
            })
            
        }else {
            
            self.showAlert(Alertmsg: "Alert", msg: networkAlert.networkmsg)
            
        }
        
        self.hideLoader()
    }
    
    
    @IBAction func RegisterAction(_ sender: UIButton) {
        let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
       
        if self.FirstNamelbl.text == "" || self.LastNameLbl.text == "" || self.PasswordLbl.text == "" || self.ConfirmPassLbl.text == "" || self.EmailLbl.text == "" || self.ConfirmLbl.text == ""
        {
            
            self.showAlert(Alertmsg: "Alert", msg: "Please fill all the fields.")
            
        }
        else if self.FirstNamelbl.text?.rangeOfCharacter(from: characterset.inverted) != nil  ||  self.LastNameLbl.text?.rangeOfCharacter(from: characterset.inverted) != nil{
            
             self.showAlert(Alertmsg: "Alert", msg: "Name cannot contain special characters.")
        }
            
        else if self.PasswordLbl.text != self.ConfirmPassLbl.text {
            
            self.showAlert(Alertmsg: "Alert", msg: "Passwords mismatch.")
            
        }
        else if !(self.EmailLbl.text?.isValidEmail)! {
            
            self.showAlert(Alertmsg: "Alert", msg: "Please enter a correct email.")
            
        }
        else if self.EmailLbl.text != self.ConfirmLbl.text {
            
            self.showAlert(Alertmsg: "Alert", msg: "Emails mismatch.")
            
        }
        else if checkboxBool2 == false {
            
            self.showAlert(Alertmsg: "Alert", msg: "Please accept the Game rules agreement.")
        }
        else{
            
            CreateUser()
            
        }
        
    }
    
    func UserAgreeTapped(sender: AnyObject) {
        
        let vc = UserAgreementVC()
        
         vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func CancelAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: false)
        
    }
    func LibraryTapped(sender: AnyObject) {
        let vc = LibraryVC()
        
         vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
        
    }
    
    func showsettings(sender: AnyObject) {
        
        self.alertToEncourageCameraAccessInitially()
        
    }
    
    func showsetting(sender: AnyObject) {
        
        self.alertToEncourageCameraAccessInitially()
        
    }
    
    func showLibrary(sender: AnyObject) {
        
        self.alertToEncourageLibraryAccessInitially()
    }
    
    
    func settext(){
        let Object:NSDictionary = theme.getRegisterView()
        
        let color = hexStringToUIColor(hex: Object.object(forKey: "TextColor")! as! String)
        let color1 = hexStringToUIColor(hex: Object.object(forKey: "TextColor1")! as! String)
       
        
        self.InscriptionText.text = NSLocalizedString("INSCRIPTION", comment: "")
        self.InscriptionText.textColor = color
        self.InscriptionText.font = UIFont(name:Object.object(forKey: "TextFont")! as! String, size: Object.object(forKey: "TextSize")! as! CGFloat)
        
        self.WelcomeText.text = NSLocalizedString("Welcome to the treasurehunt .To participate,please create an account with the following information", comment: "")
        self.WelcomeText.font = UIFont(name:Object.object(forKey: "TextFont1")! as! String, size: Object.object(forKey: "TextSize1")! as! CGFloat)
        self.WelcomeText.textColor = color1
        
        
        self.RecievePromotionsText.text = NSLocalizedString("Receive promotions and news from Al Qasr", comment: "")
        self.RecievePromotionsText.font = UIFont(name:Object.object(forKey: "PromoTextFont")! as! String, size: Object.object(forKey: "PromoTextSize")! as! CGFloat)
        self.RecievePromotionsText.textColor = color1
        
        
        self.CheckBoxText.text = NSLocalizedString("By checking this box I accept and recognize the", comment: "")
        self.CheckBoxText.font = UIFont(name:Object.object(forKey: "PromoTextFont")! as! String, size: Object.object(forKey: "PromoTextSize")! as! CGFloat)
        self.CheckBoxText.textColor = color1
        
         self.RegisterButtonText.setTitle(NSLocalizedString("S'INSCRIBE", comment: ""), for: .normal)
        
        
    }
}
