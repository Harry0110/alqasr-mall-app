//
//  LeagalViewController.swift
//  MallAppSwift
//
//  Created by apple on 7/11/19.
//  Copyright © 2019 Arianne Peiso. All rights reserved.
//

import UIKit

class LeagalViewController: BaseViewController,ExpandBottomViewDelegate,BotoomViewDelegate {

    @IBOutlet weak var WebView: UIWebView!
    var LegalList: [AnyHashable] = []
    
    var legalArray = [UserAgreeModel]()
    
    let theme = ThemeClass()
    
    @IBOutlet weak var descLbl: UILabel!
    
    @IBOutlet weak var bottomView: BottomBar!
    @IBOutlet weak var bottomExpandedView: ExpandedBottomBar!
    
    @IBOutlet weak var TitleLbl: UILabel!
    
    let TABLE_NAME = "settings"
    let COLUMN_KEY = "key"
    let COLUMN_VALUE = "value"
    let COLUMN_LASTMODIFIED = "lastmodified"

    var selected : Bool!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let Object:NSDictionary = theme.getUserAndLegal()
        
        let color = hexStringToUIColor(hex: Object.object(forKey: "TextColor")! as! String)
        
        self.TitleLbl.textColor = color
        self.TitleLbl.font = UIFont(name:Object.object(forKey: "TextFont")! as! String, size: Object.object(forKey: "TextSize")! as! CGFloat)
        
        if NetworkConnection.isConnectedToNetwork(){
            
            self.legalArray = getLegal() as! [UserAgreeModel]
            
            loadHTMLStringinWebview()
        }
        else
        {
            
            self.showAlert(Alertmsg: "Alert", msg: networkAlert.networkmsg)
        }
        self.bottomView.delegate = self
        self.bottomExpandedView.delegate = self
      
       self.bottomExpandedView.isHidden = true
        
        if selected == true{
            
            self.bottomExpandedView.LegalButton.setImage(UIImage(named: "Legal notice-8"), for: .normal)
            self.bottomView.HomeButton.setImage(UIImage(named: "Home-9"), for: .normal)
        }
        
        self.view.bringSubviewToFront(bottomView)
        self.view.bringSubviewToFront(bottomExpandedView)
        WebView.scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 52, right: 0)
    }

    func getLegal() -> [AnyHashable]? {
        
        let database: FMDatabase? = CommonsUtils.getCommonUtil().getLocalDB()
        if database?.open() == nil {
            print("Problems to open DB, please check")
            return LegalList
        }
        
        // FMDBQuickCheck(!database?.hasOpenResultSets())
        
        let resultSet = database?.executeQuery("select * from \(TABLE_NAME)", withArgumentsIn: [])
        
        while ((resultSet?.next())!) {
            
            let User = UserAgreeModel()
            //parse data
            User.key = (resultSet?.string(forColumn: self.COLUMN_KEY))!
            User.value = (resultSet?.string(forColumn: self.COLUMN_VALUE))!
            User.lastmodified = (resultSet?.int(forColumn: self.COLUMN_LASTMODIFIED))!
            
            LegalList.append(User as AnyHashable)
            
        }
        
        resultSet?.close()
        database?.close()
        return LegalList
    }
    
    func ServicesAction(sender: AnyObject) {
        
        let vc = ServicesViewController()
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func FloorAction(sender: AnyObject) {
        
        let vc = FloorViewController()
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    
    func CollapseButtonTapped(sender: AnyObject) {
        
        self.bottomView?.isHidden = false
        
        self.bottomExpandedView?.isHidden = true
        
        WebView.scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 52, right: 0)
        
    }
    
    func ShareButtonTapped(sender: AnyObject) {
        
        let textToShare = "Check out this app"
        
        if let myWebsite = NSURL(string: "https://www.google.com/") {
            let objectsToShare: [Any] = [textToShare, myWebsite]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            activityVC.popoverPresentationController?.sourceView = self.bottomExpandedView.ShareButton
            self.present(activityVC, animated: true, completion: nil)
        }
        
    }
    
    func ExpandButtonTapped(sender: AnyObject) {
        
        self.bottomView?.isHidden = true
        
        self.bottomExpandedView?.isHidden = false
        
         self.bottomExpandedView.LegalButton.setImage(UIImage(named: "Legal notice-8"), for: .normal)
        
        WebView.scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)
    }
    
    func HomeButtonTapped(sender: AnyObject) {
        
        let vc = MainViewController()
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func ProfilebuttonTapped(sender: AnyObject) {
        
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            let vc = LoginViewController()
            
            vc.ProfileBool = true
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else {
            
            let vc = ProfileVC()
            
            vc.selected = true
            
            self.navigationController?.pushViewController(vc, animated: false)
        }
        
    }
    func ArbuttonTapped(sender: AnyObject) {
        
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            let vc = Tuto1ViewController()
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else {
            
            ARGenieManager.shared()?.loadAugmentedRealityData(nil)
            
        }
    }
    func ArExbuttonTapped(sender: AnyObject) {
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            let vc = Tuto1ViewController()
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else {
            
            ARGenieManager.shared()?.loadAugmentedRealityData(nil)
            
        }
    }
    
    func FaqbuttonTaped(sender: AnyObject) {
        let vc = FAQViewController()
        
        vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
        
    }
    func UserAgreeTapped(sender: AnyObject) {
        
        let vc = UserAgreementVC()
        
        vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func LegalTapped(sender: AnyObject) {
        
        
    }
    
    func LibraryTapped(sender: AnyObject) {
        let vc = LibraryVC()
        
        vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
        
    }
    func showsettings(sender: AnyObject) {
        
        self.alertToEncourageCameraAccessInitially()
        
    }
    
    func showsetting(sender: AnyObject) {
        
        self.alertToEncourageCameraAccessInitially()
        
    }
   
    func showLibrary(sender: AnyObject) {
        
        self.alertToEncourageLibraryAccessInitially()
    }
    func loadHTMLStringinWebview() -> Void {
        let htmlString = self.legalArray[0].value
        print(htmlString)
        WebView.loadHTMLString(htmlString!, baseURL: nil)
    }
}
