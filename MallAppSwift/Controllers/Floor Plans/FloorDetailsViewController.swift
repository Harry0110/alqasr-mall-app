//
//  FloorDetailsViewController.swift
//  MallAppSwift
//
//  Created by Freelancer on 25/06/19.
//  Copyright © 2019 Arianne Peiso. All rights reserved.
//

import UIKit

class FloorDetailsViewController: BaseViewController,UIScrollViewDelegate {

    
    @IBOutlet weak var ScrollView: UIScrollView!
    var imageString: String?
    
    @IBAction func CancelAction(_ sender: UIButton) {
                
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBOutlet weak var Image: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if NetworkConnection.isConnectedToNetwork(){
            
        }else {
            
            self.showAlert(Alertmsg: "Alert", msg: networkAlert.networkmsg)
        }
        
        self.showLoader()

        ScrollView.minimumZoomScale = 1.0
        ScrollView.maximumZoomScale = 6.0
                
        let catPictureURL = URL(string: self.imageString!)!
        
        let session = URLSession(configuration: .default)
    
        let downloadPicTask = session.dataTask(with: catPictureURL) { (data, response, error) in
            // The download has finished.
            if let e = error {
                print("Error downloading cat picture: \(e)")
            } else {
                // No errors found.
                // It would be weird if we didn't have a response, so check for that too.
                if let res = response as? HTTPURLResponse {
                    print("Downloaded cat picture with response code \(res.statusCode)")
                    if let imageData = data {
                        // Finally convert that Data into an image and do what you wish with it.
                        let image = UIImage(data: imageData)
                        
                        DispatchQueue.main.async {
                            self.Image.image = image
                            self.hideLoader()
                        }
                        // Do something with your image.
                    } else {
                        print("Couldn't get image: Image is nil")
                    }
                } else {
                    print("Couldn't get response code for some reason")
                }
            }
        }
        
        downloadPicTask.resume()
        
    }

    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        
        return Image
    }

}
