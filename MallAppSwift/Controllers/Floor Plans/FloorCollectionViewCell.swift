//
//  FloorCollectionViewCell.swift
//  MallAppSwift
//
//  Created by Freelancer on 25/06/19.
//  Copyright © 2019 Arianne Peiso. All rights reserved.
//

import UIKit

class FloorCollectionViewCell: UICollectionViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var TitleLbl: UILabel!
    @IBOutlet weak var Image: UIImageView!
    
    
}
