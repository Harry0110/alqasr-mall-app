//
//  FloorViewController.swift
//  MallAppSwift
//
//  Created by Freelancer on 25/06/19.
//  Copyright © 2019 Arianne Peiso. All rights reserved.
//

import Foundation
import UIKit




class FloorViewController :BaseViewController,UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,ExpandBottomViewDelegate,BotoomViewDelegate {
    
    let TABLE_NAME = "floor_plans"

    @IBOutlet weak var BottomOutlet: BottomBar!
    @IBOutlet weak var ExpandedBottomOutlet: ExpandedBottomBar!
    let COLUMN_TITLE = "title"
    let COLUMN_IDENTIFIER = "id"
    let COLUMN_THUMBNAIL = "thumbnail"
    let COLUMN_WEIGHT = "weight"
    let COLUMN_IMAGE = "image"
    
    var FloorList: [AnyHashable] = []
    
    var filteredArray = [FloorModel]()
    
    var selected : Bool?

    override func viewDidLoad() {

        
        let nibName = UINib(nibName: "FloorCollectionViewCell", bundle:nil)

        FloorCV.register(nibName, forCellWithReuseIdentifier: "cell")

        if NetworkConnection.isConnectedToNetwork(){
            self.filteredArray = self.getFloors() as! [FloorModel]

        }
        else{
            
            self.showAlert(Alertmsg: "Alert", msg: networkAlert.networkmsg)
        }

        
        self.BottomOutlet.delegate = self
        self.ExpandedBottomOutlet.delegate = self
        self.ExpandedBottomOutlet.isHidden = true
        self.showLoader()
        
        if selected == true{
            
            self.BottomOutlet.FloorButton.setImage(UIImage(named: "Flor plans-8"), for: .normal)
            
            self.BottomOutlet.HomeButton.setImage(UIImage(named: "Home-9"), for: .normal)
        }
        
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 0, bottom: 10, right: 0)
        layout.itemSize = CGSize(width: self.FloorCV.frame.height/3, height: self.FloorCV.frame.height/3)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        FloorCV!.collectionViewLayout = layout
    }


    override func viewWillAppear(_ animated: Bool) {




    }

    @IBOutlet weak var FloorCV: UICollectionView!

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return filteredArray.count
        
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{


        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! FloorCollectionViewCell

        if self.filteredArray[indexPath.row].title != nil {

            cell.TitleLbl.text = self.filteredArray[indexPath.row].title
            let catPictureURL = URL(string: self.filteredArray[indexPath.row].thumbnail!)!

            // Creating a session object with the default configuration.
            // You can read more about it here https://developer.apple.com/reference/foundation/urlsessionconfiguration
            let session = URLSession(configuration: .default)

            // Define a download task. The download task will download the contents of the URL as a Data object and then you can do what you wish with that data.
            let downloadPicTask = session.dataTask(with: catPictureURL) { (data, response, error) in
                // The download has finished.
                if let e = error {
                    print("Error downloading cat picture: \(e)")
                } else {
                    // No errors found.
                    // It would be weird if we didn't have a response, so check for that too.
                    if let res = response as? HTTPURLResponse {
                        print("Downloaded cat picture with response code \(res.statusCode)")
                        if let imageData = data {
                            // Finally convert that Data into an image and do what you wish with it.
                            let image = UIImage(data: imageData)

                            DispatchQueue.main.async {
                                cell.Image.image = image
                                self.hideLoader()
                            }
                            // Do something with your image.
                        } else {
                            print("Couldn't get image: Image is nil")
                        }
                    } else {
                        print("Couldn't get response code for some reason")
                    }
                }
            }

            downloadPicTask.resume()
        }

        return cell

    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let minItemSpacing: CGFloat = 0.25
        return CGSize(width: ((self.FloorCV.frame.width/2) - minItemSpacing), height: ((self.FloorCV.frame.width/2) - minItemSpacing))
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let vc = FloorDetailsViewController()
        vc.imageString = self.filteredArray[indexPath.row].image
        self.navigationController?.pushViewController(vc, animated: false)
        
    }


    func getFloors() -> [AnyHashable]? {


        let database: FMDatabase? = CommonsUtils.getCommonUtil().getLocalDB()
        if database?.open() == nil {
            print("Problems to open DB, please check")
            return FloorList
        }

        // FMDBQuickCheck(!database?.hasOpenResultSets())

        let resultSet = database?.executeQuery("select * from \(TABLE_NAME)", withArgumentsIn: [])
        //        let resultSet: FMResultSet? = database?.execute("SELECT DISTINCT e.tid, e.title FROM events e INNER JOIN products_events pe ON e.tid=pe.id_event ORDER BY e.tid")
        while ((resultSet?.next())!) {

            let Floors = FloorModel()
            //parse data
            Floors.id = (resultSet?.int(forColumn: self.COLUMN_IDENTIFIER))!
            Floors.title = (resultSet?.string(forColumn: self.COLUMN_TITLE))!
            Floors.thumbnail = (resultSet?.string(forColumn: self.COLUMN_THUMBNAIL))!
            Floors.image = (resultSet?.string(forColumn: self.COLUMN_IMAGE))!
            Floors.weight = (resultSet?.string(forColumn: self.COLUMN_WEIGHT ))
           
            FloorList.append(Floors as AnyHashable)

        }

        resultSet?.close()
        database?.close()
        return FloorList
    }

    func CollapseButtonTapped(sender: AnyObject) {
        
        self.BottomOutlet?.isHidden = false
        
        self.ExpandedBottomOutlet?.isHidden = true
        
    }
    
    func ShareButtonTapped(sender: AnyObject) {
        let textToShare = "Check out this app"
        
        if let myWebsite = NSURL(string: "https://www.google.com/") {
            let objectsToShare: [Any] = [textToShare, myWebsite]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            activityVC.popoverPresentationController?.sourceView = self.ExpandedBottomOutlet.ShareButton
            self.present(activityVC, animated: true, completion: nil)
        }
        
    }
    
    func ExpandButtonTapped(sender: AnyObject) {
        
        self.BottomOutlet?.isHidden = true
        
        self.ExpandedBottomOutlet?.isHidden = false
        
        self.BottomOutlet.FloorButton.setImage(UIImage(named: "Flor plans-8"), for: .normal)

    }
    
    func HomeButtonTapped(sender: AnyObject) {
        
        let vc = MainViewController()
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func ServicesAction(sender: AnyObject) {
        
        let vc = ServicesViewController()
        
        vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func FloorAction(sender: AnyObject) {
        
        
    }
    
    
    func ProfilebuttonTapped(sender: AnyObject) {
        
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            let vc = LoginViewController()
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else {
            
            let vc = ProfileVC()
            
             vc.selected = true
            
            self.navigationController?.pushViewController(vc, animated: false)
        } 
    }
    
    func ArbuttonTapped(sender: AnyObject) {
        
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            let vc = Tuto1ViewController()
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else {
            
            ARGenieManager.shared()?.loadAugmentedRealityData(nil)
            
        }
    }
    func ArExbuttonTapped(sender: AnyObject) {
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            let vc = Tuto1ViewController()
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else {
            
            ARGenieManager.shared()?.loadAugmentedRealityData(nil)
            
        }
    }
    
    func FaqbuttonTaped(sender: AnyObject) {
        
        let vc = FAQViewController()
        
         vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func UserAgreeTapped(sender: AnyObject) {
        
        let vc = UserAgreementVC()
        
         vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func LegalTapped(sender: AnyObject) {
        let vc = LeagalViewController()
        
         vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
    func LibraryTapped(sender: AnyObject) {
        let vc = LibraryVC()
        
         vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
        
    }

    func showsettings(sender: AnyObject) {
        
        self.alertToEncourageCameraAccessInitially()
        
    }
    func showsetting(sender: AnyObject) {
        
        self.alertToEncourageCameraAccessInitially()
        
    }
    
    func showLibrary(sender: AnyObject) {
        
        self.alertToEncourageLibraryAccessInitially()
    }
}

