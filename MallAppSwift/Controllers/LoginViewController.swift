
//
//  LoginViewController.swift
//  MallAppSwift
//
//  Created by Freelancer on 26/06/19.
//  Copyright © 2019 Arianne Peiso. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController, UITextFieldDelegate,BotoomViewDelegate,ExpandBottomViewDelegate {
    
    
    
    @IBOutlet weak var OneHereOutlet: UIButton!
    
    @IBOutlet weak var LoginButtonText: CurvedButton!
    @IBOutlet weak var ClickHereOutlet: UIButton!
    @IBOutlet weak var PopButton: CurvedButton!
    @IBOutlet weak var EnterEmailRestText: UILabel!
    @IBOutlet weak var PopForgotText: UILabel!
    @IBOutlet weak var ForgotPassText: UILabel!
    @IBOutlet weak var DontHaveLoginText: UILabel!
    @IBOutlet weak var LoginText: UILabel!
    @IBOutlet weak var WelcomeText: UILabel!
    let service = UserService()
    
    var ProfileBool = false
    
    let theme = ThemeClass()
    
    @IBOutlet weak var EmailText: UITextField!
    
    @IBOutlet weak var LoginView: UIView!
    
    @IBOutlet weak var EmailtextField: NSLayoutConstraint!
    
    
    @IBAction func PopUp(_ sender: UIButton) {
        
        self.PopupView.isHidden = false
        LoginView.addBlurEffect()
        LoginView.bringSubviewToFront(self.PopupView)
        
    }
    
    
    
    @IBOutlet weak var PopupView: PopUpView!
    
    @IBOutlet weak var BottomBarExpanded: ExpandedBottomBar!
    @IBOutlet weak var BottomView: BottomBar!
    @IBOutlet weak var Emailtext: UITextField!
    @IBOutlet weak var Passwordtext: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setText()
        self.Emailtext.delegate = self
        self.EmailText.delegate = self
        self.Passwordtext.delegate = self
        
        self.BottomView.delegate = self
        self.BottomBarExpanded.delegate = self
        
        self.BottomBarExpanded.isHidden = true
        self.Emailtext.delegate = self
        self.Passwordtext.delegate = self
        
        self.Passwordtext.isSecureTextEntry = true
        // Do any additional setup after loading the view.
        self.PopupView.isHidden = true
        
        self.BottomView.HomeButton.setImage(UIImage(named: "Home-9"), for: .normal)
        
        
    }
    
    
    @IBAction func LoginAction(_ sender: UIButton) {
        
        if self.Emailtext.text == "" || self.Passwordtext.text == "" {
            
            let alert = UIAlertController(title: "Alert", message: "Fields can't be empty", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }else {
            
            self.LoginUser()
            
        }
    }
    
   
    func resetPassword(){
        
        
        self.PopupView.isHidden = true
        
        self.LoginView.removeBlurEffect()
        
        if NetworkConnection.isConnectedToNetwork(){
            
            self.showLoader()
            
            service.resetPassword(with: self.Emailtext.text!, app_token:UserDefaults.standard.value(forKey: "App_Token")! as! String ,target:self, complition:{(myresult) in
                
                DispatchQueue.main.async {
                    
                    if let result = myresult {
                        print(result)
                        
                        
                    }
                    
                }
                
            })
            
            self.hideLoader()
            
        }else {
            
            self.showAlert(Alertmsg: "Alert", msg: networkAlert.networkmsg)
            
            
        }
    }
    
    
    func LoginUser(){
        
        if NetworkConnection.isConnectedToNetwork(){
            
            
            self.showLoader()
            
            service.authenticateUser(with: "password", client_id:"alqasrid", username: self.Emailtext.text!, password:self.Passwordtext.text!, client_secret:"@lq@sr", target:self,complition:{(myresult) in
                
                
                DispatchQueue.main.async {
                    
                    if let result = myresult {
                        print(result)
                        
                        
                        if result["error"] != nil {
                            
                            let error = result["error"] as! String
                            
                            let errordesc = result["error_description"] as! String
                            
                            if errordesc == "Invalid username and password combination"

                            {
                                
                                self.showAlert(Alertmsg: "Error", msg: "Your login or your password are invalid")

                            }else {
                                
                                self.showAlert(Alertmsg: "Error", msg: "Login Failed.")

                            }
                            
                            
                            
                        }
                        else{
                            
                            let accessToken = result["access_token"] as? String
                            
                            UserDefaults.standard.set(accessToken, forKey: "DeviceId")
                            
                            let Expires = result["expires_in"] as? Int
                            
                            print(Expires!)
                            
                            UserDefaults.standard.set(Expires!, forKey: "Access_Token_Expires")
                            
                            let refreshToken = result["refresh_token"] as? String
                            
                            UserDefaults.standard.set(refreshToken, forKey: "RefreshToken")
                            
                            if self.ProfileBool == true {
                                
                                DispatchQueue.main.async {
                                    
                                    let vc = ProfileVC()
                                    self.navigationController?.pushViewController(vc, animated: false)
                                    
                                }
                                
                            }else {
                                ARGenieManager.shared()?.loadAugmentedRealityData(nil)
                                
                            }
                            
                        }
                        
                    }
                }
                
            })
            
        }else {
            
            self.showAlert(Alertmsg: "Alert", msg: networkAlert.networkmsg)
            
        }
        
        self.hideLoader()
    }
    
    @IBAction func CloseAction(_ sender: UIButton) {
        
        self.PopupView.isHidden = true
        
        self.LoginView.removeBlurEffect()
        
    }
    
    @IBAction func Registeration(_ sender: UIButton) {
        
        let vc = Registration()
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func setText(){
        
        let Object:NSDictionary = theme.getLoginView()
        let Object1:NSDictionary = theme.getPopUpView()
        
        
        let color = hexStringToUIColor(hex: Object.object(forKey: "TextColor")! as! String)
        let color1 = hexStringToUIColor(hex: Object.object(forKey: "TextColor1")! as! String)
        let color2 = hexStringToUIColor(hex: Object.object(forKey: "TextColor2")! as! String)
        let color3 = hexStringToUIColor(hex: Object.object(forKey: "TextColor3")! as! String)
        let color4 = hexStringToUIColor(hex: Object1.object(forKey: "TextColor")! as! String)
        let color5 = hexStringToUIColor(hex: Object1.object(forKey: "BackGroundColor")! as! String)
        
        self.PopupView.backgroundColor = color5
        
        self.LoginText.text = NSLocalizedString("LOGIN", comment: "")
        self.LoginText.textColor = color
        self.LoginText.font =  UIFont(name:Object.object(forKey: "TextFont")! as! String, size: Object.object(forKey: "TextSize")! as! CGFloat)
    
        self.WelcomeText.text = NSLocalizedString("Welcome to the treasure hunt", comment: "")
        self.WelcomeText.textColor = color1
        self.WelcomeText.font  = UIFont(name:Object.object(forKey: "TextFont1")! as! String, size: Object.object(forKey: "TextSize1")! as! CGFloat)
        
        self.ForgotPassText.text = NSLocalizedString("Forgot your Password? ", comment: "")
        self.ForgotPassText.textColor = color2
        self.ForgotPassText.font = UIFont(name:Object.object(forKey: "TextFont2")! as! String, size: Object.object(forKey: "TextSize2")! as! CGFloat)
        
        self.DontHaveLoginText.text = NSLocalizedString("If you don't have account please create", comment: "")
        self.DontHaveLoginText.textColor = color3
        self.DontHaveLoginText.font =  UIFont(name:Object.object(forKey: "TextFont3")! as! String, size: Object.object(forKey: "TextSize3")! as! CGFloat)
        
        self.EnterEmailRestText.text = NSLocalizedString("Enter your email. We will send you an email with instructions on how to reset your password.", comment: "")
        self.EnterEmailRestText.font = UIFont(name:Object1.object(forKey: "TextFont")! as! String, size: Object1.object(forKey: "TextSize")! as! CGFloat)
        self.EnterEmailRestText.textColor = color4
        
        self.PopButton.setTitle(NSLocalizedString("Ok", comment: ""), for: .normal)
        
        self.PopForgotText.text = NSLocalizedString("Forgot your Password", comment: "")
        self.PopForgotText.font = UIFont(name:Object1.object(forKey: "TextFont")! as! String, size: Object1.object(forKey: "TextSize")! as! CGFloat)
        self.PopForgotText.textColor = color4
        
        
        self.LoginButtonText.setTitle(NSLocalizedString("LOGIN", comment: ""), for: .normal)
        
        self.Emailtext.placeholder = NSLocalizedString("E-mail", comment: "")
        self.Passwordtext.placeholder = NSLocalizedString("Password", comment: "")
        self.ClickHereOutlet.setTitle(NSLocalizedString("Click Here", comment: ""), for: .normal)
        self.OneHereOutlet.setTitle(NSLocalizedString("One Here", comment: ""), for: .normal)
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        //or
        //self.view.endEditing(true)
        return true
    }
    
    func ServicesAction(sender: AnyObject) {
        
        let vc = ServicesViewController()
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func FloorAction(sender: AnyObject) {
        
        let vc = FloorViewController()
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func CollapseButtonTapped(sender: AnyObject) {
        
        self.BottomView?.isHidden = false
        
        self.BottomBarExpanded?.isHidden = true
        
    }
    
    func ShareButtonTapped(sender: AnyObject) {
        
        let textToShare = "Check out this app"
        
        if let myWebsite = NSURL(string: "https://www.google.com/") {
            let objectsToShare: [Any] = [textToShare, myWebsite]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            activityVC.popoverPresentationController?.sourceView = self.BottomBarExpanded.ShareButton
            self.present(activityVC, animated: true, completion: nil)
        }
        
        
    }
    
    func ExpandButtonTapped(sender: AnyObject) {
        
        self.BottomView?.isHidden = true
        
        self.BottomBarExpanded?.isHidden = false
    }
    
    func HomeButtonTapped(sender: AnyObject) {
        
        let vc = MainViewController()
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func ProfilebuttonTapped(sender: AnyObject) {
        
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            
        }else {
            
            let vc = ProfileVC()
            
            vc.selected = true
            
            self.navigationController?.pushViewController(vc, animated: false)
        }
        
    }
    func ArbuttonTapped(sender: AnyObject) {
        
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            
        }else {
            
            ARGenieManager.shared()?.loadAugmentedRealityData(nil)
            
        }
    }
    func ArExbuttonTapped(sender: AnyObject) {
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
           
            
            
        }else {
            
            ARGenieManager.shared()?.loadAugmentedRealityData(nil)
            
        }
    }
    
    func FaqbuttonTaped(sender: AnyObject) {
        let vc = FAQViewController()
        
        vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func UserAgreeTapped(sender: AnyObject) {
        
        let vc = UserAgreementVC()
        
        vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func LegalTapped(sender: AnyObject) {
        let vc = LeagalViewController()
        
        vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    
    @IBAction func OKAction(_ sender: UIButton) {
        
        if EmailText.text == "" {
            
            self.showAlert(Alertmsg: "Alert", msg: "Please enter email.")
        }
        else
            
        {
            
            if !(self.EmailText.text?.isValidEmail)!
            {
                
                self.showAlert(Alertmsg: "Alert", msg: "Please enter a valid email.")
                
            }
                
            else
                
            {
                
                resetPassword()
                
                
            }
        }
    }
    
    func LibraryTapped(sender: AnyObject) {
        let vc = LibraryVC()
        
        vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
        
    }
    
    func showsettings(sender: AnyObject) {
        
        self.alertToEncourageCameraAccessInitially()
        
    }
    
    func showsetting(sender: AnyObject) {
        
        self.alertToEncourageCameraAccessInitially()
        
    }
    func showLibrary(sender: AnyObject) {
        
        self.alertToEncourageLibraryAccessInitially()
    }
    
}


extension UIView {
    
    /// Remove UIBlurEffect from UIView
    func removeBlurEffect() {
        let blurredEffectViews = self.subviews.filter{$0 is UIVisualEffectView}
        blurredEffectViews.forEach{ blurView in
            blurView.removeFromSuperview()
        }
    }
    
    func addBlurEffect()
    {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.regular)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
        self.addSubview(blurEffectView)
    }
    
    
    
}

