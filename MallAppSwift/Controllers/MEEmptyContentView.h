//
//  MEEmptyContentView.h
//  MagicXperience
//


#import <UIKit/UIKit.h>

@interface MEEmptyContentView : UIView

@property (nonatomic, weak) IBOutlet UILabel *lbMessage;


+ (instancetype) initEmptyContentView;
- (void)centerInSuperview;

@end
