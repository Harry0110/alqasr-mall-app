//
//  NewsViewController.swift
//  MallAppSwift
//
//  Created by Freelancer on 01/07/19.
//  Copyright © 2019 Arianne Peiso. All rights reserved.
//

import Foundation

protocol NewsCellDelegate: AnyObject {
    func btnTapped(cell: NewsTableViewCell)
    
}

class NewsViewController: BaseViewController,UITableViewDelegate, UITableViewDataSource,NewsCellDelegate, ExpandBottomViewDelegate,BotoomViewDelegate{
    
    var NewsList: [AnyHashable] = []
    
    let theme = ThemeClass()
    
    @IBOutlet weak var NewsTV: UITableView!
    @IBOutlet weak var bottmExpanded: ExpandedBottomBar!
    
    @IBOutlet weak var bottombar: BottomBar!
    let TABLE_NAME = "generic_notifications"
    
     var Indexpath : Int = 0
    
    let COLUMN_IDENTIFIER = "id"
    let COLUMN_TITLE = "title"
    let COLUMN_NDESCRIPTION = "description"
    let COLUMN_IMAGE = "images"
    let COLUMN_SHORT_DESCRIPTION = "shortDescription"
    let COLUMN_DATE = "date"
    let COLUMN_LOCATION = "thumnails"
    let COLUMN_CREATED = "created"
    
    
    var NewsArray = [NewsModel]()
    
    
    override func viewDidLoad() {
        
        
        if NetworkConnection.isConnectedToNetwork(){
            self.NewsArray = self.getNews() as! [NewsModel]

        }
        else{
            
            self.showAlert(Alertmsg: "Alert", msg: networkAlert.networkmsg)
        }
        // Do any additional setup after loading the view.
        
        self.bottmExpanded.isHidden = true
        self.bottmExpanded.delegate = self
        self.bottombar.delegate = self
        self.NewsTV.separatorStyle = .none
        
        
        NewsTV.register(UINib(nibName: "NewsTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        self.showLoader()
        
        self.bottombar.HomeButton.setImage(UIImage(named: "Home-9"), for: .normal)
        
        NewsTV.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 52, right: 0)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.NewsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! NewsTableViewCell
        
        cell.selectionStyle = .none
        
        let Object:NSDictionary = theme.getButtonView()
        
        let Object2:NSDictionary = theme.getNewsView()
        
        let color = hexStringToUIColor(hex: Object.object(forKey: "BackGroundColor")! as! String)
        
        let color1 = hexStringToUIColor(hex: Object2.object(forKey: "TextColor")! as! String)
        
        let color2 =  hexStringToUIColor(hex: Object2.object(forKey: "DescColor")! as! String)
        
        cell.DetailButtonOutlet.backgroundColor = color
        
        cell.TitleLbl.font = UIFont(name:Object2.object(forKey: "TextFont")! as! String, size: Object2.object(forKey: "TextSize")! as! CGFloat)
        
        cell.TitleLbl.textColor = color
        
        cell.DateLbl.textColor = color
        
        cell.DateLbl.font = UIFont(name:Object2.object(forKey: "DateFont")! as! String, size: Object2.object(forKey: "DateSize")! as! CGFloat)
        
        cell.DetailButtonOutlet.setTitleColor(color1, for: .normal)
        
        cell.DescLbl.textColor = color2
        
        cell.DescLbl.font = UIFont(name:Object2.object(forKey: "DescFont")! as! String, size: Object2.object(forKey: "DescSize")! as! CGFloat)
        
        
        
         cell.delegate = self
        
        let catPictureURL = URL(string: self.NewsArray[indexPath.row].images!)!
        
        // Creating a session object with the default configuration.
        // You can read more about it here https://developer.apple.com/reference/foundation/urlsessionconfiguration
        let session = URLSession(configuration: .default)
        
        // Define a download task. The download task will download the contents of the URL as a Data object and then you can do what you wish with that data.
        let downloadPicTask = session.dataTask(with: catPictureURL) { (data, response, error) in
            // The download has finished.
            if let e = error {
                print("Error downloading cat picture: \(e)")
            } else {
                // No errors found.
                // It would be weird if we didn't have a response, so check for that too.
                if let res = response as? HTTPURLResponse {
                    print("Downloaded cat picture with response code \(res.statusCode)")
                    if let imageData = data {
                        // Finally convert that Data into an image and do what you wish with it.
                        let image = UIImage(data: imageData)
                        
                        DispatchQueue.main.async {
                            cell.ImageView.image = image
                            self.hideLoader()
                        }
                        // Do something with your image.
                    } else {
                        print("Couldn't get image: Image is nil")
                        self.hideLoader()

                    }
                } else {
                    print("Couldn't get response code for some reason")
                    self.hideLoader()
                }
            }
        }
        
        downloadPicTask.resume()
        
        
        cell.TitleLbl.text = self.NewsArray[indexPath.row].title
        
        let milisecond = Double(self.self.NewsArray[indexPath.row].date!)
        let dateVar = Date(timeIntervalSince1970: TimeInterval(milisecond))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        print(dateFormatter.string(from: dateVar))
        
        cell.DateLbl.text = dateFormatter.string(from: dateVar)
        
        cell.DescLbl.text = self.NewsArray[indexPath.row].shortDescription?.htmlToString
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    
    func convertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return  dateFormatter.string(from: date!)
        
    }
    
    func btnTapped(cell: NewsTableViewCell) {
        
        let vc = NewsDetailsVC()
        
        let indexPath = self.NewsTV.indexPath(for: cell)
        print(indexPath!.row)
        
        self.Indexpath = indexPath!.row
        
        print(self.NewsArray[self.Indexpath].images!)
        
        vc.imageString = self.NewsArray[self.Indexpath].images
        vc.date = self.NewsArray[self.Indexpath].date
        vc.descriptionString = self.NewsArray[self.Indexpath].Ndescription
        vc.time = self.NewsArray[self.Indexpath].date
        vc.EDtitle = self.NewsArray[self.Indexpath].title
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    
    func getNews() -> [AnyHashable]? {
        
        
        let database: FMDatabase? = CommonsUtils.getCommonUtil().getLocalDB()
        if database?.open() == nil {
            print("Problems to open DB, please check")
            return NewsList
        }
        
        // FMDBQuickCheck(!database?.hasOpenResultSets())
        
        let resultSet = database?.executeQuery("select * from \(TABLE_NAME)", withArgumentsIn: [])
        //        let resultSet: FMResultSet? = database?.execute("SELECT DISTINCT e.tid, e.title FROM events e INNER JOIN products_events pe ON e.tid=pe.id_event ORDER BY e.tid")
        while ((resultSet?.next())!) {
            
            let NewsDetails = NewsModel()
            //parse data
            
            NewsDetails.id = (resultSet?.int(forColumn: self.COLUMN_IDENTIFIER))!
            NewsDetails.images = (resultSet?.string(forColumn: self.COLUMN_IMAGE))!
            NewsDetails.shortDescription = (resultSet?.string(forColumn: self.COLUMN_SHORT_DESCRIPTION))!
            NewsDetails.Ndescription = (resultSet?.string(forColumn: self.COLUMN_NDESCRIPTION))!
            NewsDetails.date = (resultSet?.double(forColumn: self.COLUMN_DATE))!
            NewsDetails.title = (resultSet?.string(forColumn: self.COLUMN_TITLE))!
            NewsDetails.created = (resultSet?.double(forColumn: self.COLUMN_CREATED))!
            
            NewsList.append(NewsDetails)
            
        }
        
        resultSet?.close()
        database?.close()
        return NewsList
    }
    
    
    func ServicesAction(sender: AnyObject) {
        
        let vc = ServicesViewController()
        
         vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func FloorAction(sender: AnyObject) {
        
        let vc = FloorViewController()
        
         vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func CollapseButtonTapped(sender: AnyObject) {
        
        self.bottombar?.isHidden = false
        
        self.bottmExpanded?.isHidden = true
        
        NewsTV.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 52, right: 0)

        
    }
    
    func ShareButtonTapped(sender: AnyObject) {
        
        let textToShare = "Check out this app"
        
        if let myWebsite = NSURL(string: "https://www.google.com/") {
            let objectsToShare: [Any] = [textToShare, myWebsite]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            activityVC.popoverPresentationController?.sourceView = self.bottmExpanded.ShareButton
            self.present(activityVC, animated: true, completion: nil)
        }
        
    }
    
    func ExpandButtonTapped(sender: AnyObject) {
        
        self.bottombar?.isHidden = true
        
        self.bottmExpanded?.isHidden = false
        
        NewsTV.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 120, right: 0)

    }
    
    func HomeButtonTapped(sender: AnyObject) {
        
        let vc = MainViewController()
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func ProfilebuttonTapped(sender: AnyObject) {
        
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            let vc = LoginViewController()
            
             vc.ProfileBool = true
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else {
            
            let vc = ProfileVC()
            
             vc.selected = true
            
            self.navigationController?.pushViewController(vc, animated: false)
        }
        
    }
    func ArbuttonTapped(sender: AnyObject) {
        
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            let vc = Tuto1ViewController()
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else {
            
            ARGenieManager.shared()?.loadAugmentedRealityData(nil)
            
        }
    }
    func ArExbuttonTapped(sender: AnyObject) {
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            let vc = Tuto1ViewController()
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else {
            
            ARGenieManager.shared()?.loadAugmentedRealityData(nil)
            
        }
    }
    
    func FaqbuttonTaped(sender: AnyObject) {
        let vc = FAQViewController()
        
         vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
        
    }
    
    func UserAgreeTapped(sender: AnyObject) {
        
        let vc = UserAgreementVC()
        
         vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func LegalTapped(sender: AnyObject) {
        let vc = LeagalViewController()
        
         vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
    }

    func LibraryTapped(sender: AnyObject) {
        let vc = LibraryVC()
        
         vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
        
    }
    
    func showsettings(sender: AnyObject) {
        
        self.alertToEncourageCameraAccessInitially()
        
    }
    
    func showsetting(sender: AnyObject) {
        
        self.alertToEncourageCameraAccessInitially()
        
    }
    
    func showLibrary(sender: AnyObject) {
        
        self.alertToEncourageLibraryAccessInitially()
    }
    
    
    @IBAction func CloseAction(_ sender : UIButton){
        
        self.navigationController?.popToRootViewController(animated: false)
    }
}
