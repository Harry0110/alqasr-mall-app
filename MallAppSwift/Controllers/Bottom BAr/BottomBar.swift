//
//  BottomBar.swift
//  MallAppSwift
//
//  Created by Freelancer on 26/06/19.
//  Copyright © 2019 Arianne Peiso. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

protocol BotoomViewDelegate {
    
    func ExpandButtonTapped(sender : AnyObject)
    func HomeButtonTapped(sender : AnyObject)
    func ServicesAction(sender : AnyObject)
    func FloorAction(sender : AnyObject)
    func ArbuttonTapped(sender:AnyObject)
    func showsettings(sender:AnyObject)
}

@IBDesignable class BottomBar : UIView {
    
    @IBOutlet weak var HomeText: UILabel!
    @IBOutlet var bottomBar: UIView!
    @IBOutlet weak var ServicesText: UILabel!
    
    @IBOutlet weak var FloorText: UILabel!
    @IBOutlet weak var ExpandButton : UIButton!
    
    @IBOutlet weak var ServiceButton : UIButton!
    
    @IBOutlet weak var HomeButton : UIButton!
    
    @IBOutlet weak var FloorButton : UIButton!
    
    @IBOutlet weak var imageview: UIImageView!
    
    
    var delegate : BotoomViewDelegate?

    var pressed = false
    
    let theme = ThemeClass()
    //var view : UIView!
    
    var nibName : String = "BottomBar"
    
    //init
    
    override init(frame: CGRect) {
        //Properties
        super.init(frame:frame)
        //Set Anything that uses the view or visible bounds
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        //Properties
        super.init(coder: aDecoder)
        
        //setup
        setup()
    }
    
    func setup(){
    
        let Object:NSDictionary = theme.getBottomBar()
        
        
        let color = hexStringToUIColor(hex: Object.object(forKey: "ItemColor")! as! String)
        
        bottomBar = loadViewFromNib()
        
        addSubview(bottomBar)
        
        bottomBar.frame = bounds
        
        bottomBar.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        
        self.HomeText.text = NSLocalizedString("Home", comment: "")
        self.ServicesText.text = NSLocalizedString("Services", comment: "")
        self.FloorText.text = NSLocalizedString("Floor Plans", comment: "")
        self.HomeText.font = UIFont(name:Object.object(forKey: "ItemFont")! as! String, size: Object.object(forKey: "ItemSize")! as! CGFloat)
         self.FloorText.font = UIFont(name:Object.object(forKey: "ItemFont")! as! String, size: Object.object(forKey: "ItemSize")! as! CGFloat)
         self.ServicesText.font = UIFont(name:Object.object(forKey: "ItemFont")! as! String, size: Object.object(forKey: "ItemSize")! as! CGFloat)
        self.HomeText.textColor = color
        self.ServicesText.textColor = color
        self.FloorText.textColor = color

    }
    func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        
        if UserDefaults.standard.object(forKey: "DeviceLanguage") != nil {
            if "\(UserDefaults.standard.value(forKey: "DeviceLanguage")!)" == "en" ||  "\(UserDefaults.standard.value(forKey: "DeviceLanguage")!)" == "fr" || "\(UserDefaults.standard.value(forKey: "DeviceLanguage")!)" == "es"{
                
                self.imageview?.transform = CGAffineTransform(scaleX: 1, y: 1)
                
            }
            else if  "\(UserDefaults.standard.value(forKey: "DeviceLanguage")!)" == "ar"{
                
                self.imageview?.transform = CGAffineTransform(scaleX: -1, y: 1)
                
            }
            else
            {
                
                self.imageview?.transform = CGAffineTransform(scaleX: 1, y: 1)
                
                
            }
        }else {
            
            print("no")
            
            let pre = Locale.preferredLanguages[0]
            
            let string = pre.components(separatedBy: "-")[0]
            
            print(string)
            
            UserDefaults.standard.set(string, forKey: "DeviceLanguage")
            
        }
        
    
        
        
        return view
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    @IBAction func HomeAction(_ sender: UIButton) {
        
//        self.Home.setImage("", for: .normal)
//        self.Services.setImage("", for: .normal)
        
        delegate?.HomeButtonTapped(sender: HomeButton)
    }
    
    @IBAction func expandButtonAction(_ sender: UIButton) {
        print("calling")
        
        delegate?.ExpandButtonTapped(sender: ExpandButton)
        
        
    }
    
    @IBAction func ServicesAction(_ sender: UIButton) {
        
//        DispatchQueue.main.async {
//            self.delegate?.ServicesAction(sender: self.ServiceButton)
//        }
        //sender.setImage(UIImage(named: "Services-8"), for: .normal)
        
//        if sender.isSelected == false {
//            sender.isSelected = true
//        }
        
        self.delegate?.ServicesAction(sender: self.ServiceButton)
    }
    
    @IBAction func FloorAction(_ sender: UIButton) {
        
        delegate?.FloorAction(sender: FloorButton)
    }
    
    @IBAction func ArAction(_ sender: UIButton) {
        
        cameraAuthorisation()
       
        
    }
    
    func cameraAuthorisation(){
        
        if AVCaptureDevice.authorizationStatus(for: AVMediaType.video) ==  AVAuthorizationStatus.authorized {
            
            DispatchQueue.main.async {
                self.delegate?.ArbuttonTapped(sender: BottomBar.self)
            }
        }
        else
        {
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted: Bool) -> Void in
                if granted == true {
                    
                    DispatchQueue.main.async {
                        self.delegate?.ArbuttonTapped(sender: BottomBar.self)
                    }
                } else {
                    
                    DispatchQueue.main.async {
                        
                        self.delegate?.showsettings(sender: BottomBar.self)
                        
                    }
                    
                }
            })
        }
        
    }
    
}


public extension UIImage { var flippedHorizontally: UIImage { return UIImage(cgImage: self.cgImage!, scale: self.scale, orientation: .upMirrored) } }
