//
//  ExpandedBottomBar.swift
//  MallAppSwift
//
//  Created by Uday kanth Bangaru on 03/07/19.
//  Copyright © 2019 Arianne Peiso. All rights reserved.
//

import Foundation
import UIKit

protocol ExpandBottomViewDelegate {
    
    func CollapseButtonTapped(sender : AnyObject)
    func ShareButtonTapped(sender : AnyObject)
    func ProfilebuttonTapped(sender: AnyObject)
    func ArExbuttonTapped(sender:AnyObject)
    func FaqbuttonTaped(sender: AnyObject)
    func UserAgreeTapped(sender:AnyObject)
    func LegalTapped(sender:AnyObject)
    func LibraryTapped(sender:AnyObject)
    func showsetting(sender:AnyObject)
    func showLibrary(sender:AnyObject)

}


@IBDesignable class ExpandedBottomBar : UIView {
    
    @IBOutlet weak var ShareText: UILabel!
    
    @IBOutlet weak var FaqText: UILabel!
    @IBOutlet weak var UserAgreeText: UILabel!
    @IBOutlet weak var LegalNoticeText: UILabel!
    @IBOutlet weak var MyPointsText: UILabel!
    @IBOutlet var bottomExpandView: UIView!
    
    @IBOutlet weak var RateText: UILabel!
    @IBOutlet weak var ContactsText: UILabel!
    @IBOutlet weak var LibraryText: UILabel!
    var delegate : ExpandBottomViewDelegate?
    
    var delegate2 : BotoomViewDelegate?
    
    var contactbool : Bool = false
    
    let theme = ThemeClass()
    
    @IBOutlet weak var CollapseButton : UIButton!
    @IBOutlet weak var ShareButton : UIButton!
    @IBOutlet weak var MypointsButton : UIButton!
    @IBOutlet weak var LibraryButton : UIButton!
    @IBOutlet weak var ContactButton : UIButton!
    @IBOutlet weak var RateButton : UIButton!
    @IBOutlet weak var LegalButton : UIButton!
    @IBOutlet weak var UseragreementButton : UIButton!
    @IBOutlet weak var FAQButton : UIButton!
    
    @IBOutlet weak var imageview: UIImageView!
    
    
    //var view : UIView!
    
    var nibName : String = "ExpandedBottomBar"
    
    //init
    
    override init(frame: CGRect) {
        //Properties
        super.init(frame:frame)
        //Set Anything that uses the view or visible bounds
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        //Properties
        super.init(coder: aDecoder)
        
        //setup
        setup()
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func setup(){
        
        let Object:NSDictionary = theme.getBottomBar()
        
        let color = hexStringToUIColor(hex: Object.object(forKey: "ItemColor")! as! String)
        
        bottomExpandView = loadViewFromNib()
        
        addSubview(bottomExpandView)
        
        bottomExpandView.frame = bounds
        
        bottomExpandView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        
        self.ShareText.text = NSLocalizedString("Share", comment: "")
        
        self.MyPointsText.text = NSLocalizedString("My Points", comment: "")
        self.LibraryText.text = NSLocalizedString("Library", comment: "")
        self.ContactsText.text = NSLocalizedString("Contact", comment: "")
        self.RateText.text = NSLocalizedString("Rate", comment: "")
        self.LegalNoticeText.text = NSLocalizedString("Legal notice", comment: "")
        self.UserAgreeText.text = NSLocalizedString("User agreement", comment: "")
        self.FaqText.text = NSLocalizedString("FAQ", comment: "")
        
        self.ShareText.font = UIFont(name:Object.object(forKey: "ItemFont")! as! String, size: Object.object(forKey: "ItemSize")! as! CGFloat)
        self.MyPointsText.font = UIFont(name:Object.object(forKey: "ItemFont")! as! String, size: Object.object(forKey: "ItemSize")! as! CGFloat)
        self.LibraryText.font = UIFont(name:Object.object(forKey: "ItemFont")! as! String, size: Object.object(forKey: "ItemSize")! as! CGFloat)
        self.ContactsText.font = UIFont(name:Object.object(forKey: "ItemFont")! as! String, size: Object.object(forKey: "ItemSize")! as! CGFloat)
        self.RateText.font = UIFont(name:Object.object(forKey: "ItemFont")! as! String, size: Object.object(forKey: "ItemSize")! as! CGFloat)
        self.LegalNoticeText.font = UIFont(name:Object.object(forKey: "ItemFont")! as! String, size: Object.object(forKey: "ItemSize")! as! CGFloat)
        self.UserAgreeText.font = UIFont(name:Object.object(forKey: "ItemFont")! as! String, size: Object.object(forKey: "ItemSize")! as! CGFloat)
        self.FaqText.font = UIFont(name:Object.object(forKey: "ItemFont")! as! String, size: Object.object(forKey: "ItemSize")! as! CGFloat)
        self.ShareText.textColor = color
        self.MyPointsText.textColor = color
        self.LibraryText.textColor = color
        self.ContactsText.textColor = color
        self.RateText.textColor = color
        self.LegalNoticeText.textColor = color
        self.UserAgreeText.textColor = color
        self.FaqText.textColor = color
        
    }
    func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        if UserDefaults.standard.object(forKey: "DeviceLanguage") != nil {
            if "\(UserDefaults.standard.value(forKey: "DeviceLanguage")!)" == "en" ||  "\(UserDefaults.standard.value(forKey: "DeviceLanguage")!)" == "fr" || "\(UserDefaults.standard.value(forKey: "DeviceLanguage")!)" == "es"{
                
                self.imageview?.transform = CGAffineTransform(scaleX: 1, y: 1)
                
            }
            else if  "\(UserDefaults.standard.value(forKey: "DeviceLanguage")!)" == "ar"{
                
                self.imageview?.transform = CGAffineTransform(scaleX: -1, y: 1)
                
            }
            else
            {
                
                self.imageview?.transform = CGAffineTransform(scaleX: 1, y: 1)
                
                
            }
        }else {
            
            print("no")
            
            let pre = Locale.preferredLanguages[0]
            
            let string = pre.components(separatedBy: "-")[0]
            
            print(string)
            
            UserDefaults.standard.set(string, forKey: "DeviceLanguage")
            
        }
        
        
        return view
    }
    
    @IBAction func CollapseButtonAction(_ sender: UIButton) {
        print("calling")
        
        delegate?.CollapseButtonTapped(sender: CollapseButton)
        
        ContactButton.setImage(UIImage(named: "Contact-8"), for: .normal)
        ShareButton.setImage(UIImage(named:"share-8"), for: .normal)
        MypointsButton.setImage(UIImage(named:"My points-10" ), for: .normal)
        LibraryButton.setImage(UIImage(named: "Library-9"), for: .normal)
        RateButton.setImage(UIImage(named: "Note-9"), for: .normal)
        LegalButton.setImage(UIImage(named: "Legal notices-8"), for: .normal)
        UseragreementButton.setImage(UIImage(named: "User Agreement-8"), for: .normal)
        FAQButton.setImage(UIImage(named: "FAQ-8"), for: .normal)
        
    }
    
    @IBAction func ProfileAction(_ sender: UIButton) {
        
        delegate?.ProfilebuttonTapped(sender: ExpandedBottomBar.self)
    }
    
    @IBAction func FaqAction(_ sender: UIButton) {
        
        delegate?.FaqbuttonTaped(sender: ExpandedBottomBar.self)
        
    }
    
    @IBAction func ArAction(_ sender: UIButton) {
        
        cameraAuthorisation()
        
        
    }
    
    @IBAction func ShareAction(_ sender: UIButton) {
        
        delegate?.ShareButtonTapped(sender: ExpandedBottomBar.self)
        
        sender.setImage(UIImage(named: "Share-10"), for: .normal)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
            
            self.ShareButton.setImage(UIImage(named:"share-8"), for: .normal)
            // Change `2.0` to the
        }
        
    }
    
    @IBAction func UserAgreeAction(_ sender: UIButton) {
        
         delegate?.UserAgreeTapped(sender: ExpandedBottomBar.self)
    }
    @IBAction func LegalAction(_ sender: UIButton) {
        
        delegate?.LegalTapped(sender: ExpandedBottomBar.self)
    }
    @IBAction func RateAction(_ sender: UIButton) {
        
        rateApp()
        
        sender.setImage(UIImage(named: "Note-10"), for: .normal)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
            
            sender.setImage(UIImage(named:"Note-8"), for: .normal)
            // Change `2.0` to the
        }
    }
    
    func rateApp() {
        guard let url = URL(string: "itms-apps://itunes.apple.com/app/" + Constants.AppID) else {
            return
        }
        if #available(iOS 10, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
            
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    @IBAction func ContactAction(_ sender: UIButton) {
        
        self.contactbool = true
        let email = Constants.Email
        if let url = URL(string: "mailto:\(email)") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        sender.setImage(UIImage(named: "Contact-10"), for: .normal)

        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
            
            sender.setImage(UIImage(named:"Contact-8"), for: .normal)
            // Change `2.0` to the
        }
        
        
    }
    
    @IBAction func LibraryAction(_ sender: UIButton) {
        
        checkPhotoLibraryPermission()
        
    }
    
    func cameraAuthorisation(){
        
        if AVCaptureDevice.authorizationStatus(for: AVMediaType.video) ==  AVAuthorizationStatus.authorized {
            
            DispatchQueue.main.async {
                self.delegate?.ArExbuttonTapped(sender: ExpandedBottomBar.self)
            }
        }
        else
        {
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted: Bool) -> Void in
                if granted == true {
                    
                    DispatchQueue.main.async {
                        self.delegate?.ArExbuttonTapped(sender: ExpandedBottomBar.self)                    }
                } else {
                    
                    DispatchQueue.main.async {
                        
                        self.delegate?.showsetting(sender: ExpandedBottomBar.self)
                        
                    }
                    
                }
            })
        }
        
    }
    
    func checkPhotoLibraryPermission() {
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            
            DispatchQueue.main.async {
                self.delegate?.LibraryTapped(sender: ExpandedBottomBar.self)
            }
            break
        //handle authorized status
        case .denied, .restricted :
            
            DispatchQueue.main.async {
                self.delegate?.showLibrary(sender: ExpandedBottomBar.self)
            }
            break
        //handle denied status
        case .notDetermined:
            // ask for permissions
            PHPhotoLibrary.requestAuthorization { status in
                switch status {
                case .authorized:
                    DispatchQueue.main.async {
                        self.delegate?.LibraryTapped(sender: ExpandedBottomBar.self)
                    }
                    
                    break
                // as above
                case .denied, .restricted:
                    DispatchQueue.main.async {
                        self.delegate?.showLibrary(sender: ExpandedBottomBar.self)
                    }
                    
                    break
                // as above
                case .notDetermined:
                    
                    
                    break
                    // won't happen but still
                }
            }
        }
    }
    
}
