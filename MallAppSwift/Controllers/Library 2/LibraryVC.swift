//
//  LibraryVC.swift
//  MallAppSwift
//
//  Created by apple on 7/12/19.
//  Copyright © 2019 Arianne Peiso. All rights reserved.
//

import UIKit
import Photos
import AVKit
import AVFoundation

class LibraryVC : BaseViewController,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,ExpandBottomViewDelegate,BotoomViewDelegate,UIImagePickerControllerDelegate{
    
    
    
    var player = AVPlayer()
    var playbutton = UIButton()
    
//    var asset1 : PHAsset!
    
    var tap : UITapGestureRecognizer!
    
    var closebutton = UIButton()
    
    var sharebutton = UIButton()
    
    var deletebutton = UIButton()
    var assetcollection : PHAssetCollection!
    
    var deleteasset : PHAsset!
    
    var photoasset : PHFetchResult<AnyObject>!
    var assettumbnailsize : CGSize!
    var albumfound : Bool = false
    var images = [UIImage]()
    
    var selected : Bool!
    
    var imageview : UIImageView!
    
    @IBOutlet weak var PhotosCollectionView : UICollectionView!
    @IBOutlet weak var BottomviewOutlet : BottomBar!
    @IBOutlet weak var ExapandBottomOutlet : ExpandedBottomBar!
    
   // @IBOutlet weak var photo: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        

        let albumname = "ARGenie Sample"
        let fetchoptions = PHFetchOptions()
        fetchoptions.predicate = NSPredicate(format: "title = %@", albumname)
        let collection : PHFetchResult = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: fetchoptions)
        if let first_obj : AnyObject = collection.firstObject{
            
            self.assetcollection = first_obj as? PHAssetCollection
            albumfound = true
            self.photoasset = (PHAsset.fetchAssets(in: self.assetcollection, options: nil) as? PHFetchResult<AnyObject>)
        }
        else{
            albumfound = false
            
            let alertController = UIAlertController(title: "Alert", message: "No photos or videos found.", preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                UIAlertAction in
                
                self.navigationController?.popToRootViewController(animated: true)
            }
            
            // Add the actions
            alertController.addAction(okAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
            
        }
        
    // Do any additional setup after loading the view.
     self.BottomviewOutlet.delegate = self
    self.ExapandBottomOutlet.delegate = self
        
    self.BottomviewOutlet.isHidden = false
    self.ExapandBottomOutlet.isHidden = true
        let nibName = UINib(nibName: "LibraryCollectionViewCell", bundle:nil)
        
        PhotosCollectionView.register(nibName, forCellWithReuseIdentifier: "cell")
        
        if selected == true{
        
        self.ExapandBottomOutlet.LibraryButton.setImage(UIImage(named:"Library-10" ), for: .normal)
        self.BottomviewOutlet.HomeButton.setImage(UIImage(named: "Home-9"), for: .normal)
        }
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 0, bottom: 10, right: 0)
        layout.itemSize = CGSize(width: self.PhotosCollectionView.frame.height/3, height: self.PhotosCollectionView.frame.height/3)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        PhotosCollectionView!.collectionViewLayout = layout
        
        PhotosCollectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 52, right: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {

        
        let layout = self.PhotosCollectionView!.collectionViewLayout as? UICollectionViewFlowLayout
        let cellsize = layout!.itemSize
        //let cell = layout?.estimatedItemSize
        
        self.assettumbnailsize = CGSize(width: cellsize.width, height: cellsize.height)
        
        self.PhotosCollectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        var count:Int = 0
        if (self.photoasset != nil){
            
            count = self.photoasset.count
            
        }
        
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! LibraryCollectionViewCell
        
        let asset:PHAsset = self.photoasset[indexPath.item] as! PHAsset
        PHImageManager.default().requestImage(for: asset, targetSize: self.assettumbnailsize, contentMode: .aspectFill, options: nil,resultHandler: { (result, info) in
            
            if asset.mediaType == .video && result != nil{
                
                cell.imageview.image = result
                cell.playimage.isHidden = false
                cell.playimage.isHighlighted = true
            }else if asset.mediaType == .image && result != nil{
                
                cell.imageview.image = result
                cell.playimage.isHidden = true
            }
            
//            if result != nil{
//
//                cell.imageview.image = result
//            }
        })
        
//        PHImageManager.default().requestAVAsset(forVideo: , options: <#T##PHVideoRequestOptions?#>, resultHandler: <#T##(AVAsset?, AVAudioMix?, [AnyHashable : Any]?) -> Void#>)
        return cell
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let minItemSpacing: CGFloat = 0.25
        return CGSize(width: ((self.PhotosCollectionView.frame.width/2) - minItemSpacing), height: ((self.PhotosCollectionView.frame.width/2) - minItemSpacing))
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.deleteasset = self.photoasset[indexPath.item] as? PHAsset
        let asset:PHAsset = self.photoasset[indexPath.item] as! PHAsset
        if asset.mediaType == PHAssetMediaType .image{
            
            self.imageview = UIImageView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
            //            let imageview = UIImageView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height))
            imageview.isUserInteractionEnabled = false
            PHImageManager.default().requestImage(for: asset, targetSize: CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height), contentMode: .aspectFit, options: nil, resultHandler:{(result,info) in
                
                self.imageview.image = result
                
            })
            self.BottomviewOutlet.isHidden = true
            self.ExapandBottomOutlet.isHidden = true
            self.view.addSubview(imageview)
            self.setupButtons()
        }else if asset.mediaType == PHAssetMediaType.video {
            
            
            //self.view.addSubview(player)
            
            self.playVideo(view: self, videoAsset: asset)
            
            //self.setupButtons()
            
        }
    
    }

    
    func setupButtons(){
        
        self.closebutton = UIButton(frame: CGRect(x: UIScreen.main.bounds.width - 50, y: 35, width: 30, height: 30))
        
        print(closebutton.frame)
        
        self.closebutton.setBackgroundImage(UIImage(named: "close"), for: .normal)
        
        self.closebutton.backgroundColor =  #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
        
        self.closebutton.addTarget(self, action: #selector(self.closepressed(sender:)), for: .touchUpInside)
        
        self.sharebutton = UIButton(frame: CGRect(x: UIScreen.main.bounds.width - 50  , y: UIScreen.main.bounds.height - 50, width: 30, height: 30))
        self.sharebutton.setBackgroundImage(UIImage(named: "share"), for: .normal)
        self.sharebutton.addTarget(self, action: #selector(self.sharepressed(sender:)), for: .touchUpInside)
        print(sharebutton.frame)
        
        self.deletebutton = UIButton(frame: CGRect(x: 25, y: UIScreen.main.bounds.height - 50, width: 30 , height: 30))
        
        self.deletebutton.backgroundColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
        
        self.sharebutton.backgroundColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
        
        self.deletebutton.setBackgroundImage(UIImage(named: "delete"), for: .normal)
        self.deletebutton.addTarget(self, action: #selector(self.deletebutton(_:)), for: .touchUpInside)
        
        self.view.addSubview(closebutton)
        self.view.addSubview(sharebutton)
        self.view.addSubview(deletebutton)
        
        self.BottomviewOutlet.isHidden = true
        self.ExapandBottomOutlet.isHidden = true
    }
    
    @objc func closepressed(sender:AnyObject)  {
        
        let vc = LibraryVC()
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @objc func sharepressed(sender:AnyObject){
        
        //let textToShare = "Check out this image/video"
        
        let bounds = imageview.bounds
        UIGraphicsBeginImageContextWithOptions(bounds.size, true, 0.0)
        self.view.drawHierarchy(in: bounds, afterScreenUpdates: false)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        //let image = imageview.image as Any
        let video = player.currentItem as Any
        print(video)
        let image = [img!]
        if self.imageview.image != nil || self.player.currentItem != nil {
            //let objectsToShare: [Any] = [textToShare, img,video]
            let activityVC = UIActivityViewController(activityItems: image, applicationActivities: nil)
            
            activityVC.popoverPresentationController?.sourceView = sender as? UIView
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    
    
    
    @objc func deletebutton(_: AnyObject){
        
        PHPhotoLibrary.shared().performChanges({
            
            PHAssetChangeRequest.deleteAssets([self.deleteasset] as NSArray)
        }, completionHandler:{success,error in
            print (success ? "Success" : error as Any)
            DispatchQueue.main.async {
                let vc = LibraryVC()
                self.navigationController?.pushViewController(vc, animated: false)            }
        })
        
        
    }
    func playVideo (view: UIViewController, videoAsset: PHAsset) {
        
        guard (videoAsset.mediaType == .video) else {
            print("Not a valid video media type")
            return
        }
        
        PHCachingImageManager().requestAVAsset(forVideo: videoAsset, options: nil) { (asset, audioMix, args) in
            let asset = asset as! AVURLAsset
            
            DispatchQueue.main.async {
                self.player =  AVPlayer(url: asset.url)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = self.player
                playerViewController.delegate = self as? AVPlayerViewControllerDelegate
                self.addChild(playerViewController)
                self.view.addSubview(playerViewController.view)
                playerViewController.view.frame = self.view.frame
                //player.play()
                playerViewController.showsPlaybackControls = false
                self.setupButtons()
                self.tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
                playerViewController.contentOverlayView?.gestureRecognizers = [self.tap]
                playerViewController.view.subviews.first?.addGestureRecognizer(self.tap)
                //self.view.addGestureRecognizer(self.tap)
                //self.tap.isEnabled = true
                print(self.view.frame)
                self.playbutton = UIButton(frame: CGRect(x: (self.view.bounds.width - 80)/2, y: (self.view.bounds.height - 80)/2,width: 80 , height: 80))
                
                print(self.playbutton.frame)
                self.playbutton.setBackgroundImage(UIImage(named: "Play_icn"), for: .normal)
                self.playbutton.addTarget(self, action: #selector(self.playButtonAction(sender:)), for: .touchUpInside)
                self.view.addSubview(self.playbutton)
                
                self.BottomviewOutlet.isHidden = true
                self.ExapandBottomOutlet.isHidden = true
                
            }
        }
    }
    @objc func playButtonAction(sender:UIButton){
        
        //        sender.isHidden = false
        
        self.player.play()
        sender.isHidden = true
        self.tap.isEnabled = true
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        if self.player.rate > 0.0 {
            self.player.pause()
            self.tap.isEnabled = false
            self.playbutton.isHidden = false
            self.closebutton.isEnabled = true
        }
        //self.tap.isEnabled = false
        self.playbutton.isHidden = false
        
        self.playbutton.setBackgroundImage(UIImage(named: "Play_icn"), for: .normal)
    }
   
    func ServicesAction(sender: AnyObject) {
        
        let vc = ServicesViewController()
        
        vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func FloorAction(sender: AnyObject) {
        
        let vc = FloorViewController()
        
        vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func CollapseButtonTapped(sender: AnyObject) {
        
        self.BottomviewOutlet?.isHidden = false
        
        
        self.ExapandBottomOutlet?.isHidden = true
        
        PhotosCollectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 52, right: 0)

        
    }
    
    func ShareButtonTapped(sender: AnyObject) {
        
        //self.ExapandBottomOutlet.ShareButton.setImage(UIImage(named: "Share-10"), for: .normal)
        
        //self.ExapandBottomOutlet.LibraryButton.setImage(UIImage(named: "Library-8"), for: .normal)

        let textToShare = "Check out this app"
        
        if let myWebsite = NSURL(string: "https://www.google.com/") {
            let objectsToShare: [Any] = [textToShare, myWebsite]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            activityVC.popoverPresentationController?.sourceView = self.ExapandBottomOutlet.ShareButton
            self.present(activityVC, animated: true, completion: nil)
        }
        
    }
    
    func ExpandButtonTapped(sender: AnyObject) {
        
        self.BottomviewOutlet?.isHidden = true
        
        self.ExapandBottomOutlet?.isHidden = false
        
        self.ExapandBottomOutlet.LibraryButton.setImage(UIImage(named:"Library-10" ), for: .normal)
        
        PhotosCollectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)


    }
    
    func HomeButtonTapped(sender: AnyObject) {
        
        let vc = MainViewController()
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func ProfilebuttonTapped(sender: AnyObject) {
        
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            let vc = LoginViewController()
            
            vc.ProfileBool = true
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else {
            
            let vc = ProfileVC()
            
            vc.selected = true
            
            self.navigationController?.pushViewController(vc, animated: false)
        }
        
    }
    func ArbuttonTapped(sender: AnyObject) {
        
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            let vc = Tuto1ViewController()
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else {
            
            ARGenieManager.shared()?.loadAugmentedRealityData(nil)
            
        }
    }
    func ArExbuttonTapped(sender: AnyObject) {
        if UserDefaults.standard.value(forKey: "DeviceId") == nil {
            
            let vc = Tuto1ViewController()
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else {
            
            ARGenieManager.shared()?.loadAugmentedRealityData(nil)
            
        }
    }
    
    func FaqbuttonTaped(sender: AnyObject) {
        let vc = FAQViewController()
        
        vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
        
    }
    
    func UserAgreeTapped(sender: AnyObject) {
        let vc = UserAgreementVC()
        
        vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
        
        
    }
    
    func LegalTapped(sender: AnyObject) {
        let vc = LeagalViewController()
        
         vc.selected = true
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func LibraryTapped(sender: AnyObject) {
//        let vc = LibraryVC()
//        
//         vc.selected = true
//        
//        self.navigationController?.pushViewController(vc, animated: false)
        
        
    }
    
    func showsettings(sender: AnyObject) {
        
        self.alertToEncourageCameraAccessInitially()
        
    }

    func showsetting(sender: AnyObject) {
        
        self.alertToEncourageCameraAccessInitially()
        
    }
    
    func showLibrary(sender: AnyObject) {
        self.alertToEncourageLibraryAccessInitially()
    }
    
}
