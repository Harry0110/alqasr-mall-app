//
//  LibraryCollectionViewCell.swift
//  MallAppSwift
//
//  Created by Freelancer on 15/07/19.
//  Copyright © 2019 Arianne Peiso. All rights reserved.
//

import UIKit

class LibraryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageview : UIImageView!
    @IBOutlet weak var playimage : UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
