//
//  BaseViewController.swift
//  MallAppSwift
//
//  Created by Freelancer on 27/06/19.
//  Copyright © 2019 Arianne Peiso. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    
    
    var container: UIView = UIView()
    let myLabel = UILabel()
    var loadingView: UIView = UIView()
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(true)
    }
  
    
    func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
    
    
    //    func showAlertWithAction(msg : String)
    //    {
    //        let alertController = UIAlertController(title:StringTxt.appName, message:StringTxt.noMerchant, preferredStyle: .alert)
    //        alertController.view.tintColor = ColorConverter.hexStringToUIColor(hex: ColorCode.BackgroundViewColor)
    //        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
    //            UIAlertAction in
    //            let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeTabViewController") as! HomeTabViewController
    //            vc.selectedIndex = 0
    //            self.navigationController?.pushViewController(vc, animated: true)
    //
    //        }
    //        let cancelAction = UIAlertAction(title: "Not Now", style: UIAlertActionStyle.cancel) {
    //            UIAlertAction in
    //
    //
    //            }
    //
    //        // Add the actions
    //        alertController.addAction(okAction)
    //        alertController.addAction(cancelAction)
    //
    //        // Present the controller
    //        self.present(alertController, animated: true, completion: nil)
    //
    //
    //    }
    //MARK: - function to get IndexPath
    
    func getIndexPathFor(sender : AnyObject, tblView : UITableView) -> NSIndexPath?
    {
        let rect = sender.convert(sender.bounds.origin, to: tblView)
        if let indexPath = tblView.indexPathForRow(at: rect)
        {
            return indexPath as NSIndexPath
        }
        return nil
    }
    
    func getIndexPathForCollection(sender : AnyObject, collectionView : UICollectionView) -> NSIndexPath?
    {
        let rect = sender.convert(sender.bounds.origin, to: collectionView)
        if let indexPath = collectionView.indexPathForItem(at: rect)
        {
            return indexPath as NSIndexPath
        }
        return nil
    }
    
    //MARK: - Fuction to set corner radius
    func setCornerRadius(sender : AnyObject, radius : CGFloat)
    {
        sender.layer.cornerRadius = radius
    }
    
    //MARK: - Function to set border
    func setBorder(sender : AnyObject, borderWidth : CGFloat, borderColor: CGColor)
    {
        sender.layer.borderColor = borderColor
        sender.layer.borderWidth = borderWidth
    }
    
    //MARK: - Function to add Done toolbar
    func addDoneToolBar(onTextField textfield:UITextField, target:Any, selector:Selector)
    {
        let toolBar = UIToolbar()
        
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: selector)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        toolBar.setItems([ spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        textfield.inputAccessoryView = toolBar
    }
    
    //MARK: - Function to add check for valid email
    class func isValidEmail(emailString:String, strictFilter:Bool) -> Bool {
        
        let strictEmailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let laxString = ".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*"
        let emailRegex = strictFilter ? strictEmailRegEx : laxString
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegex)
        return emailTest.evaluate(with: emailString)
    }
    
    //MARK: - Function to add check for valid string
    class func validString(string: Any?) -> String
    {
        let str: String? = string as? String
        
        if(str == nil)
        {
            return ""
        }
        else if(str == "<null>" || str == "<NULL>")
        {
            return ""
        }
        else if(str == "<nil>" || str == "<NIL>")
        {
            return ""
        }
        else if(str == "null" || str == "NULL")
        {
            return ""
        }
        else if(str == "NIL" || str == "nil")
        {
            return ""
        }
        else if(str == "(null)")
        {
            return ""
        }
        return str!
    }
    
    //MARK: - Function to add check for valid mobile number
    class func isValidMobileNumber(str:String?) -> Bool
    {
        if(str == nil){return false}
        
        if(str!.count < 4){return false}
        if(str!.count > 13){return false}
        
        return true
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func UTCToLocal(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "H:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "h:mm a"
        
        return dateFormatter.string(from: dt!)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func showLoader() {
        
         self.loadingView = CommonsUtils.showActivityViewer(withCustomLoading: UIImage(named: "Loading"), message: "", showIn: self.view, colorForView: nil)
    }
    
    func hideLoader() {
        
        CommonsUtils.hideActivityViewer(loadingView)
        
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
}

extension UIViewController {
    
  
     
    
    func storeString() -> String{
        
       let string = "Its taking longer than usual"
        
       return string
    }
    
//    func showLoader() {
//
////        let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
////
////        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
////        loadingIndicator.hidesWhenStopped = true
////        loadingIndicator.style = UIActivityIndicatorView.Style.gray
////        loadingIndicator.startAnimating();
////
////        alert.view.addSubview(loadingIndicator)
////
////        present(alert, animated: true, completion: nil)
////
////        let when = DispatchTime.now() + 15
////        DispatchQueue.main.asyncAfter(deadline: when){
////            // your code with delay
////            alert.dismiss(animated: true, completion: nil)
////        }
////        let ind = MyIndicator(frame: CGRect(x:self.view.center.x , y:self.view.center.y , width: 50, height: 50), image: UIImage(named: "Loading")!)
////        view.addSubview(ind)
////        ind.startAnimating()
//
//        loadview = CommonsUtils.showActivityViewer(withCustomLoading: UIImage(named: "Loading"), message: "", showIn: self.view, colorForView: UIColor.green)
//
//
//    }
//
//    func hideLoader() {
//
////        dismiss(animated: true, completion: nil)
////        let ind = MyIndicator(frame: CGRect(x:self.view.center.x , y:self.view.center.y , width: 50, height: 50), image: UIImage(named: "Loading")!)
////        ind.stopAnimating()
//
//        CommonsUtils.hideActivityViewer(loadview)
//
//    }
    
    
    func showAlert(Alertmsg: String,msg: String) {
        
        let alert = UIAlertController(title: Alertmsg, message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func alertToEncourageCameraAccessInitially() {
        let alert = UIAlertController(
            title: "Alert",
            message: "Camera access is required.",
            preferredStyle: UIAlertController.Style.alert
        )
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Go to settings.", style: .cancel, handler: { (alert) -> Void in
            UIApplication.shared.openURL(URL(string: UIApplication.openSettingsURLString)!)
        }))
        present(alert, animated: true, completion: nil)
    }
    
    
    func alertToEncourageLibraryAccessInitially() {
        let alert = UIAlertController(
            title: "Alert",
            message: "Library access required.",
            preferredStyle: UIAlertController.Style.alert
        )
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (alert) -> Void in
            let vc = MainViewController()
            self.navigationController?.pushViewController(vc, animated: false)
        }))
        alert.addAction(UIAlertAction(title: "Go to settings.", style: .cancel, handler: { (alert) -> Void in
            UIApplication.shared.openURL(URL(string: UIApplication.openSettingsURLString)!)
        }))
        present(alert, animated: true, completion: nil)
    }

}


extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}


class MyIndicator: UIView {
    
    let imageView = UIImageView()
    
    init(frame: CGRect, image: UIImage) {
        super.init(frame: frame)
        
        imageView.frame = bounds
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        imageView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(imageView)
    }
    
    required init(coder: NSCoder) {
        fatalError()
    }
    
    func startAnimating() {
        isHidden = false
        rotate()
    }
    
    func stopAnimating() {
        isHidden = true
        removeRotation()
    }
    
    private func rotate() {
        let rotation : CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotation.toValue = NSNumber(value: Double.pi * 2)
        rotation.duration = 1
        rotation.isCumulative = true
        rotation.repeatCount = Float.greatestFiniteMagnitude
        self.imageView.layer.add(rotation, forKey: "rotationAnimation")
    }
    
    private func removeRotation() {
        self.imageView.layer.removeAnimation(forKey: "rotationAnimation")
    }
}
