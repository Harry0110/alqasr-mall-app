//
//  FAQView.swift
//  FAQView
//
//  Created by udaykanth bangaru on 09/07/2019.
// Copyright © 2019 udaykanth bangaru . All rights reserved.
//
import Foundation

public class FAQView: UIView {
    
    // MARK: Public Properties
    
    public var items: [FaqItems]
    
    public var questionTextColor: UIColor!  {
        get {
            return configuration.questionTextColor
        }
        set(value) {
            configuration.questionTextColor = value
        }
    }
    
    public var answerTextColor: UIColor! {
        get {
            return configuration.answerTextColor
        }
        set(value) {
            configuration.answerTextColor = value
        }
    }
    
    public var questionTextFont: UIFont! {
        get {
            return configuration.questionTextFont
        }
        set(value) {
            configuration.questionTextFont = value
        }
    }
    
    public var answerTextFont: UIFont! {
        get {
            return configuration.answerTextFont
        }
        set(value) {
            configuration.answerTextFont = value
        }
    }
    
    public var titleLabelTextColor: UIColor! {
        get {
            return configuration.titleTextColor
        }
        set(value) {
            configuration.titleTextColor = value
            titleLabel.textColor = configuration.titleTextColor
        }
    }
    
    public var titleLabelTextFont: UIFont! {
        get {
            return configuration.titleTextFont
        }
        set(value) {
            configuration.titleTextFont = value
            titleLabel.font = configuration.titleTextFont
        }
    }
    
    public var titleLabelBackgroundColor: UIColor! {
        get {
            return configuration.titleLabelBackgroundColor
        }
        set(value) {
            configuration.titleLabelBackgroundColor = value
            titleLabel.backgroundColor = configuration.titleLabelBackgroundColor
        }
    }
    
    public var viewBackgroundColor: UIColor! {
        get {
            return configuration.viewBackgroundColor
        }
        set(value) {
            configuration.viewBackgroundColor = value
            self.backgroundColor = configuration.viewBackgroundColor
        }
    }
    
    public var cellBackgroundColor: UIColor! {
        get {
            return configuration.cellBackgroundColor
        }
        set(value) {
            configuration.cellBackgroundColor = value
        }
    }
    
    public var separatorColor: UIColor! {
        get {
            return configuration.separatorColor
        }
        set(value) {
            configuration.separatorColor = value
        }
    }
    
    public var dataDetectorTypes: UIDataDetectorTypes? {
        get {
            return configuration.dataDetectorTypes
        }
        set(value) {
            configuration.dataDetectorTypes = value
        }
    }
    
    public var answerTintColor: UIColor! {
        get {
            return configuration.tintColor
        }
        set(value) {
            configuration.tintColor = value
        }
    }
    
    // MARK: Internal Properties
    
    var tableView: UITableView = {
        let tableview = UITableView()
        tableview.translatesAutoresizingMaskIntoConstraints = false
        //tableview.backgroundColor = UIColor.clear
        tableview.backgroundColor = #colorLiteral(red: 0.797960043, green: 0.797960043, blue: 0.797960043, alpha: 1)
        tableview.allowsSelection = false
        tableview.separatorStyle = .none
        tableview.estimatedRowHeight = 50
        tableview.tableFooterView = UIView()
        return tableview
    }()
    
    var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    var expandedCells = [CellOperation]()
    var configuration = FAQConfiguration()
    var heightAtIndexPath = NSMutableDictionary()
    
    // MARK: Initialization
    
    public init(frame: CGRect, items: [FaqItems]) {
        self.items = items
        super.init(frame: frame)
        expandedCells = Array(repeating: CellOperation.collapsed, count: items.count)
        //setupTitleView(title: title)
        setupTableView()
        setupView()
        self.addSubview(tableView)
        //self.addSubview(titleLabel)
        addConstraintsForTableViewAndTitleLabel()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    //
    //  //required public init?(coder aDecoder: NSCoder) {
    //    fatalError("init(coder:) has not been implemented")
    //    //super.init(coder: aDecoder)
    //  }
    
    // MARK: Internal Methods
    
    func updateSection(_ section: Int) {
        if expandedCells[section] == .expanded {
            expandedCells[section] = .collapse
        } else {
            expandedCells[section] = .expand
        }
        tableView.reloadSections(IndexSet(integer: section), with: .fade)
        tableView.scrollToRow(at: IndexPath(row: 0, section: section), at: .top, animated: true)
    }
    
    func updateCellOperation(section: Int, cellOperation: CellOperation) {
        if cellOperation == .expand {
            expandedCells[section] = .expanded
        } else if cellOperation == .collapse {
            expandedCells[section] = .collapsed
        }
    }
    
    // MARK: Private Methods
    
    //  private func setupTitleView(title: String) {
    //    self.titleLabel.textColor = configuration.titleTextColor
    //    self.titleLabel.font = configuration.titleTextFont
    //    self.titleLabel.backgroundColor = configuration.titleLabelBackgroundColor
    //    self.titleLabel.text = title
    //  }
    
    private func setupTableView() {
        self.tableView.register(FAQViewCell.self, forCellReuseIdentifier: "cell")
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }
    
    private func setupView() {
        self.backgroundColor = configuration.viewBackgroundColor
    }
    
    private func addConstraintsForTableViewAndTitleLabel() {
        
        //    let titleLabelTrailing = NSLayoutConstraint(item: titleLabel, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailingMargin, multiplier: 1, constant: 0)
        //    let titleLabelLeading = NSLayoutConstraint(item: titleLabel, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leadingMargin, multiplier: 1, constant: 0)
        //    let titleLabelTop = NSLayoutConstraint(item: titleLabel, attribute: .top, relatedBy: .equal, toItem: self, attribute: .topMargin, multiplier: 1, constant: 20)
        
        
        let tableViewTrailing = NSLayoutConstraint(item: tableView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailingMargin, multiplier: 1, constant: 0)
        let tableViewLeading = NSLayoutConstraint(item: tableView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leadingMargin, multiplier: 1, constant: 0)
        let tableViewTop = NSLayoutConstraint(item: tableView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .topMargin, multiplier: 1, constant: 0)
        let tableViewBottom = NSLayoutConstraint(item: tableView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottomMargin, multiplier: 1, constant: 0)
        NSLayoutConstraint.activate([tableViewTrailing, tableViewLeading, tableViewTop, tableViewBottom,]) //titleLabelLeading, titleLabelTrailing, titleLabelTop])
    }
    
}

extension FAQView: UITableViewDelegate, UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = self.heightAtIndexPath.object(forKey: indexPath)
        if ((height) != nil) {
            return CGFloat(height as! CGFloat)
        } else {
            #if swift(>=4.2)
            return UITableView.automaticDimension
            #else
            return UITableViewAutomaticDimension
            #endif
        }
    }
    
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let height = cell.frame.size.height
        self.heightAtIndexPath.setObject(height, forKey: indexPath as NSCopying)
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! FAQViewCell
        cell.configuration = configuration
        let currentItem = items[indexPath.section]
        let cellOperation = expandedCells[indexPath.section]
        cell.configure(currentItem: currentItem, indexPath: indexPath, cellOperation: cellOperation)
        updateCellOperation(section: indexPath.section, cellOperation: cellOperation)
        cell.didSelectQuestion = { [weak self] cell in
            guard let faqView = self else {
                return
            }
            faqView.updateSection(indexPath.section)
        }
        return cell
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: 10))
        headerView.backgroundColor = configuration.separatorColor
        return headerView
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

public struct FAQItem {
    public let question: String
    public let answer: String?
    public let attributedAnswer: NSAttributedString?
    
    public init(question: String, answer: String) {
        self.question = question
        self.answer = answer
        self.attributedAnswer = nil
    }
    
    public init(question: String, attributedAnswer: NSAttributedString) {
        self.question = question
        self.attributedAnswer = attributedAnswer
        self.answer = nil
    }
}

public class FAQConfiguration {
    
    public var questionTextColor: UIColor?
    public var answerTextColor: UIColor?
    public var questionTextFont: UIFont?
    public var answerTextFont: UIFont?
    public var titleTextColor: UIColor?
    public var titleTextFont: UIFont?
    public var viewBackgroundColor: UIColor?
    public var cellBackgroundColor: UIColor?
    public var separatorColor: UIColor?
    public var titleLabelBackgroundColor: UIColor?
    public var dataDetectorTypes: UIDataDetectorTypes?
    public var tintColor: UIColor?
    
    init() {
        defaultValue()
    }
    
    func defaultValue() {
        self.questionTextColor = UIColor.white
        self.answerTextColor = #colorLiteral(red: 1, green: 0.294183135, blue: 0.8343199492, alpha: 1)
        self.questionTextFont = UIFont(name: "HelveticaNeue-Bold", size: 16)
        self.answerTextFont = UIFont(name: "HelveticaNeue-Light", size: 15)
        self.titleTextColor = UIColor.green
        //self.titleTextFont = UIFont(name: "HelveticaNeue-Light", size: 20)
        //self.titleLabelBackgroundColor = UIColor.lightGray
        //let colorValue: CGFloat = 210/255
        self.viewBackgroundColor = #colorLiteral(red: 0.797960043, green: 0.797960043, blue: 0.797960043, alpha: 1)
        self.cellBackgroundColor = #colorLiteral(red: 0.797960043, green: 0.797960043, blue: 0.797960043, alpha: 1)
        //self.separatorColor = UIColor(red: colorValue, green: colorValue, blue: colorValue, alpha: 1)
        self.separatorColor = #colorLiteral(red: 0.797960043, green: 0.797960043, blue: 0.797960043, alpha: 1)
    }
}


class FAQViewCell: UITableViewCell {
    
    // MARK: Internal Properties
    
    private var questionview : UIView = {
        
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = #colorLiteral(red: 0.284263134, green: 0.3032021224, blue: 0.6330097318, alpha: 1)
        view.layer.cornerRadius = 5
        return view
    }()
    
    
    var questionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.sizeToFit()
        return label
    }()
    
    var answerTextView: UITextView = {
        let textView = UITextView()
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.backgroundColor = UIColor.clear
        textView.isScrollEnabled = false
        textView.textContainerInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        textView.isEditable = false
        textView.dataDetectorTypes = []
        textView.layer.cornerRadius = 3
        //textView.textAlignment = .center
        //textView.sizeToFit() = true
        //textView.sizeToFit()
        return textView
    }()
    
    var indicatorImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        let indicatorImage = UIImage(named: "drop_down-8", in: Bundle(for: FAQView.self), compatibleWith: nil)
        imageView.image = indicatorImage
        return imageView
    }()
    
    var answerTextViewBottom = NSLayoutConstraint()
    
    var configuration: FAQConfiguration! {
        didSet {
            setup(with: configuration)
        }
    }
    
    var didSelectQuestion: ((_ cell: FAQViewCell) ->())?
    
    // MARK: Private Properties
    
    private let actionByQuestionTap = #selector(didTapQuestion)
    
    private var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        //view.backgroundColor = UIColor.blue
        return view
    }()
    
    
    // MARK: Initialization
    #if swift(>=4.2)
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        viewSetup()
    }
    #else
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        viewSetup()
    }
    #endif
    
    private func viewSetup() {
        selectionSetup()
        
        self.containerView.addSubview(indicatorImageView)
        contentView.addSubview(questionview)
        self.questionview.addSubview(questionLabel)
        self.questionview.addSubview(containerView)
        //contentView.addSubview(questionLabel)
        contentView.addSubview(answerTextView)
        //contentView.addSubview(containerView)
        addLabelConstraints()
        contentView.addSubview(questionview)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(currentItem: FaqItems, indexPath: IndexPath, cellOperation: CellOperation) {
        questionLabel.text = currentItem.question
        switch cellOperation {
        case .collapsed:
            collapse(animated: false)
        case .expand:
            if let answer = currentItem.response?.htmlToString {
                expand(withAnswer: answer, animated: true)
            } else if let attributedAnswer = currentItem.response?.htmlToString {
                expand(withAttributedAnswer: attributedAnswer, animated: true)
            }
        case .collapse:
            collapse(animated: true)
        case .expanded:
            if let answer = currentItem.response?.htmlToString {
                expand(withAnswer: answer, animated: false)
            } else if let attributedAnswer = currentItem.response?.htmlToString {
                expand(withAttributedAnswer: attributedAnswer, animated: false)
            }
        }
    }
    
    // MARK: Private Methods
    
    private func selectionSetup() {
        questionLabel.isUserInteractionEnabled = true
        indicatorImageView.isUserInteractionEnabled = true
        let questionLabelGestureRecognizer = UITapGestureRecognizer(target: self, action
            : actionByQuestionTap)
        questionLabel.addGestureRecognizer(questionLabelGestureRecognizer)
        let imageGestureRecognizer = UITapGestureRecognizer(target: self, action: actionByQuestionTap)
        indicatorImageView.addGestureRecognizer(imageGestureRecognizer)
    }
    
    private func setup(with configuration: FAQConfiguration) {
        self.backgroundColor = configuration.cellBackgroundColor
        self.questionLabel.textColor = configuration.questionTextColor
        //questionLabel.backgroundColor = UIColor.blue
        self.answerTextView.textColor = configuration.answerTextColor
        self.answerTextView.backgroundColor = UIColor.white
        self.questionLabel.font = configuration.questionTextFont
        self.answerTextView.font = configuration.answerTextFont
        if let dataDetectorTypes = configuration.dataDetectorTypes {
            self.answerTextView.dataDetectorTypes = dataDetectorTypes
        }
        if let tintColor = configuration.tintColor {
            self.answerTextView.tintColor = tintColor
        }
    }
    
    private func addLabelConstraints() {
        
        let questionviewtrailing = NSLayoutConstraint(item: questionview, attribute: .trailing, relatedBy: .equal, toItem: contentView, attribute: .trailing, multiplier: 1, constant: 0)
        let questionviewLeading = NSLayoutConstraint(item: questionview
            , attribute: .leading, relatedBy: .equal, toItem: contentView, attribute: .leading, multiplier: 1, constant: 0)
        let questionviewTop = NSLayoutConstraint(item: questionview, attribute: .top, relatedBy: .equal, toItem: contentView, attribute: .top, multiplier: 1, constant: 0)
        let questionviewheight = NSLayoutConstraint(item: questionview, attribute:.height, relatedBy: .equal, toItem: questionLabel, attribute: .height, multiplier: 1, constant: 20)
        //let questionviewbottom = NSLayoutConstraint(item: questionview, attribute: .bottom, relatedBy: .equal, toItem: answerTextView, attribute: .top, multiplier: 1, constant: 10)
        
        let questionLabelTrailing = NSLayoutConstraint(item: questionLabel, attribute: .trailing, relatedBy: .equal, toItem: questionview, attribute: .trailingMargin, multiplier: 1, constant: 5)
        //let questionLabelLeading = NSLayoutConstraint(item: questionLabel, attribute: .leading, relatedBy: .equal, toItem: questionview, attribute: .leadingMargin, multiplier: 1, constant: 0)
        let questionLabelTop = NSLayoutConstraint(item: questionLabel, attribute: .top, relatedBy: .equal, toItem: questionview, attribute: .top, multiplier: 1, constant: 10)
        
        let answerTextViewTrailing = NSLayoutConstraint(item: answerTextView, attribute: .trailing, relatedBy: .equal, toItem: contentView, attribute: .trailing, multiplier: 1, constant: 0)
        let answerTextViewLeading = NSLayoutConstraint(item: answerTextView, attribute: .leading, relatedBy: .equal, toItem: contentView, attribute: .leading, multiplier: 1, constant: 0)
        let answerTextViewTop = NSLayoutConstraint(item: answerTextView, attribute: .top, relatedBy: .equal, toItem: questionview, attribute: .bottom, multiplier: 1, constant: 0)
        answerTextViewBottom = NSLayoutConstraint(item: contentView, attribute: .bottom, relatedBy: .equal, toItem: answerTextView, attribute: .bottom, multiplier: 1, constant: 0)
        
        let indicatorHorizontalCenter = NSLayoutConstraint(item: indicatorImageView, attribute: .centerX, relatedBy: .equal, toItem: containerView, attribute: .centerX, multiplier: 1, constant: 0)
        let indicatorVerticalCenter = NSLayoutConstraint(item: indicatorImageView, attribute: .centerY, relatedBy: .equal, toItem: containerView, attribute: .centerY, multiplier: 1, constant: 0)
        let indicatorWidth = NSLayoutConstraint(item: indicatorImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 15)
        let indicatorHeight = NSLayoutConstraint(item: indicatorImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 15)
        
        let containerTrailing = NSLayoutConstraint(item: containerView, attribute: .trailing, relatedBy: .equal, toItem: questionLabel, attribute: .leadingMargin, multiplier: 1, constant: -15)
        let containerLeading = NSLayoutConstraint(item: containerView, attribute: .leading, relatedBy: .equal, toItem: questionview, attribute: .leadingMargin, multiplier: 1, constant: 5)
        let containerWidth = NSLayoutConstraint(item: containerView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 30)
        let containerTop = NSLayoutConstraint(item: containerView, attribute: .top, relatedBy: .equal, toItem: contentView,attribute: .top, multiplier: 1, constant: 10)
        let containerHeight = NSLayoutConstraint(item: containerView, attribute: .height, relatedBy: .equal, toItem: questionLabel, attribute: .height, multiplier: 1, constant: 0)
        
        
        NSLayoutConstraint.activate([questionviewtrailing,questionviewLeading,questionviewTop,questionviewheight, questionLabelTrailing, questionLabelTop, answerTextViewLeading
            , answerTextViewTrailing, answerTextViewTop ,answerTextViewBottom, indicatorVerticalCenter, indicatorHorizontalCenter, indicatorWidth, indicatorHeight, containerTrailing,containerLeading, containerTop, containerWidth, containerHeight])
    }
    
    @objc private func didTapQuestion(_ recognizer: UIGestureRecognizer) {
        self.didSelectQuestion?(self)
    }
    
    private func expand(withAnswer answer: String, animated: Bool) {
        answerTextView.text = answer
        expand(animated: animated)
    }
    
    private func expand(withAttributedAnswer answer: String, animated: Bool) {
        answerTextView.text = answer
        expand(animated: animated)
    }
    
    private func expand(animated: Bool) {
        answerTextView.isHidden = false
        if animated {
            answerTextView.alpha = 0
            UIView.animate(withDuration: 0.5, animations: {
                self.answerTextView.alpha = 1
            })
        }
        answerTextViewBottom.constant = 0
        update(arrow: .up, animated: animated)
    }
    
    private func collapse(animated: Bool) {
        answerTextView.text = ""
        answerTextView.isHidden = true
        answerTextViewBottom.constant = -20
        update(arrow: .down, animated: animated)
    }
    
    private func update(arrow: Arrow, animated: Bool) {
        switch arrow {
        case .up:
            if animated {
                // Change direction from down to up with animation
                self.indicatorImageView.rotate(withAngle: CGFloat(0), animated: false)
                self.indicatorImageView.rotate(withAngle: (CGFloat(Double.pi)/2) * 3, animated: true)
            } else {
                // Change direction from down to up without animation
                self.indicatorImageView.rotate(withAngle: (CGFloat(Double.pi)/2) * 3, animated: false)
            }
        case .down:
            if animated {
                // Change direction from up to down with animation
                self.indicatorImageView.rotate(withAngle: (CGFloat(Double.pi)/2) * 3, animated: false)
                self.indicatorImageView.rotate(withAngle: CGFloat(0), animated: true)
            } else {
                // Change direction from up to down without animation
                self.indicatorImageView.rotate(withAngle: CGFloat(0), animated: false)
            }
        }
    }
}

enum Arrow: String {
    case up
    case down
}

enum CellOperation {
    case collapsed
    case expand
    case expanded
    case collapse
}

extension UIImageView {
    func rotate(withAngle angle: CGFloat, animated: Bool) {
        UIView.animate(withDuration: animated ? 0.5 : 0, animations: {
            self.transform = CGAffineTransform(rotationAngle: angle)
        })
    }
}

