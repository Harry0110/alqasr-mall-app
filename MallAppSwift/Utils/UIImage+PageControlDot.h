//
//  UIImage+PageControlDot.h
//  magicxperience
//
//  Created by baromir on 06/09/2018.
//  Copyright © 2018 Artech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (PageControlDot)

+(UIImage*) outlinedEllipse:(CGSize) size : (UIColor*) color : (CGFloat) lineWidth;

@end
