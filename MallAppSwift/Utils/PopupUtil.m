//
//  PopupUtil.m
//  magicxperience
//
//  Created by erka on 03/10/2018.
//  Copyright © 2018 Artech. All rights reserved.
//

#import "PopupUtil.h"

#define KTAG_POPUP_BLUR_VIEW 13526

@interface PopupUtil()

@property (strong, nonatomic) UIGestureRecognizer *gestureHidePopup;

-(void) onTap;


@end

@implementation PopupUtil

+ (PopupUtil *)sharedInstance {
    static PopupUtil *sharedInstance = nil;
    
    if (sharedInstance == nil) {
        sharedInstance = [PopupUtil new];
        sharedInstance.gestureHidePopup = nil;
    }
    
    return sharedInstance;
}

-(void) addBlurEffectViewToView:(UIView*) aView {
    
    if([self findBlurEffectViewInView:aView] != nil)
        return;
    
    UIView *blurVisualEffectView = [self getBlurView];
    
    [aView addSubview:blurVisualEffectView];
    
    _gestureHidePopup = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap)];
    [blurVisualEffectView addGestureRecognizer:_gestureHidePopup];
}

-(void) insertBlurEffectViewToView:(UIView*) aView belowSubView:(UIView*) aSubview {
    if([self findBlurEffectViewInView:aView] != nil)
        return;
    
    UIView *blurVisualEffectView = [self getBlurView];
    
    [aView insertSubview:blurVisualEffectView belowSubview:aSubview];
    
    _gestureHidePopup = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap)];
    [blurVisualEffectView addGestureRecognizer:_gestureHidePopup];
}

-(void) removeBlurEffectViewFromView:(UIView*) aView {
    UIView *blurEffectView = [self findBlurEffectViewInView:aView];
    if(blurEffectView != nil){
        [blurEffectView removeGestureRecognizer:_gestureHidePopup];
        [blurEffectView removeFromSuperview];
        _gestureHidePopup = nil;
        if(_delegate) {
            _delegate=nil;
        }
    }
}

-(void) onTap {
    if(_delegate){
        [_delegate onTap];
        _delegate=nil;
    }
}

-(UIView*) getBlurView {//UIVisualEffectView
    
    /*UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *blurVisualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];*/
    
    UIView* blurVisualEffectView = [UIView new];
    blurVisualEffectView.backgroundColor = UIColor.blackColor;
    
    
    
    blurVisualEffectView.alpha = 0.8;
    blurVisualEffectView.frame = [UIScreen mainScreen].bounds;
    blurVisualEffectView.tag = KTAG_POPUP_BLUR_VIEW;
    
    return blurVisualEffectView;
}

-(UIView*) findBlurEffectViewInView:(UIView*) aView {
    UIView *blurEffectView = [aView viewWithTag:KTAG_POPUP_BLUR_VIEW];
    return blurEffectView;
}

@end
