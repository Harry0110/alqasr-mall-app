//
//  Languages.h
//  MagicXperience
//

#import <Foundation/Foundation.h>
#import "CommonsUtils.h"


#define GET_LOCALIZABLE [NSString stringWithFormat:@"%@_Localizable", [CommonsUtils getAppLanguage]]
#define GET_CUSTOM_APP_LOCALIZABLE [NSString stringWithFormat:@"%@_AppLocalizable", [CommonsUtils getAppLanguage]]


//General
#pragma mark - General

#define TITLE_MESAG_ERROR NSLocalizedStringWithDefaultValue(@"tittle_msg_error", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"tittle_msg_error", GET_LOCALIZABLE ,"tittle_msg_error"), "tittle_msg_error")
#define TITLE_MESAG_INFO NSLocalizedStringWithDefaultValue(@"tittle_msg_info", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"tittle_msg_info", GET_LOCALIZABLE ,"tittle_msg_info"), "tittle_msg_info")
#define INVALID_MAIL_ERROR NSLocalizedStringWithDefaultValue(@"invalid_mail_error", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"invalid_mail_error", GET_LOCALIZABLE ,"invalid_mail_error"), "invalid_mail_error")
#define MAIL_SERVICE_NOT_CONFIGURED NSLocalizedStringWithDefaultValue(@"mail_service_error", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"mail_service_error", GET_LOCALIZABLE ,"mail_service_error"), "mail_service_error")
#define OK_ACTION NSLocalizedStringWithDefaultValue(@"ok_action", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"ok_action", GET_LOCALIZABLE ,"ok_action"), "ok_action")
#define APP_NAME NSLocalizedStringFromTable(@"app_name", GET_CUSTOM_APP_LOCALIZABLE ,"app_name")
#define ACCEPT_TEXT NSLocalizedStringWithDefaultValue(@"accept_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"accept_text", GET_LOCALIZABLE ,"accept_text"), "accept_text")
#define CANCEL_TEXT NSLocalizedStringWithDefaultValue(@"cancel_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"cancel_text", GET_LOCALIZABLE ,"cancel_text"), "cancel_text")

#define CONEXION_ERROR_MSG NSLocalizedStringWithDefaultValue(@"conexion_error_msg", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"conexion_error_msg", GET_LOCALIZABLE ,"conexion_error_msg"), "conexion_error_msg")
#define SERVER_CONEXION_ERROR_MSG NSLocalizedStringWithDefaultValue(@"server_conexion_error_msg", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"server_conexion_error_msg", GET_LOCALIZABLE ,"server_conexion_error_msg"), "server_conexion_error_msg")
#define SPLASH_LOADING_TEXT NSLocalizedStringWithDefaultValue(@"loading_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"loading_text", GET_LOCALIZABLE ,"loading_text"), "loading_text")
#define UPDATE_ACTION_TEXT NSLocalizedStringWithDefaultValue(@"update_action_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"update_action_text", GET_LOCALIZABLE ,"update_action_text"), "update_action_text")

//Menu
#pragma mark - Menu

#define MENU_LOGIN NSLocalizedStringWithDefaultValue(@"menu_login", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"menu_login", GET_LOCALIZABLE ,"menu_login"), "menu_login")
#define MENU_SCANNER NSLocalizedStringWithDefaultValue(@"menu_scanner", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"menu_scanner", GET_LOCALIZABLE ,"menu_scanner"), "menu_scanner")
#define MENU_BIBLIO NSLocalizedStringWithDefaultValue(@"menu_biblio", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"menu_biblio", GET_LOCALIZABLE ,"menu_biblio"), "menu_biblio")
#define MENU_MEXPERIENCE NSLocalizedStringWithDefaultValue(@"menu_mexperience", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"menu_mexperience", GET_LOCALIZABLE ,"menu_mexperience"), "menu_mexperience")
#define MENU_PRODUCT NSLocalizedStringWithDefaultValue(@"menu_product", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"menu_product", GET_LOCALIZABLE ,"menu_product"), "menu_product")
#define MENU_HISTORY NSLocalizedStringWithDefaultValue(@"menu_history", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"menu_history", GET_LOCALIZABLE ,"menu_history"), "menu_history")
#define MENU_DISTRIB NSLocalizedStringWithDefaultValue(@"menu_distrib", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"menu_distrib", GET_LOCALIZABLE ,"menu_distrib"), "menu_distrib")
#define MENU_FAQ NSLocalizedStringWithDefaultValue(@"menu_faq", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"menu_faq", GET_LOCALIZABLE ,"menu_faq"), "menu_faq")
#define MENU_TUTORIAL NSLocalizedStringWithDefaultValue(@"menu_tutorial", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"menu_tutorial", GET_LOCALIZABLE ,"menu_tutorial"), "menu_tutorial")
#define MENU_DEMO NSLocalizedStringWithDefaultValue(@"menu_demo", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"menu_demo", GET_LOCALIZABLE ,"menu_demo"), "menu_demo")
#define MENU_PARTAGER NSLocalizedStringWithDefaultValue(@"menu_partager", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"menu_partager", GET_LOCALIZABLE ,"menu_partager"), "menu_partager")
#define MENU_NOTER NSLocalizedStringWithDefaultValue(@"menu_noter", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"menu_noter", GET_LOCALIZABLE ,"menu_noter"), "menu_noter")
#define MENU_CONTACT NSLocalizedStringWithDefaultValue(@"menu_contact", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"menu_contact", GET_LOCALIZABLE ,"menu_contact"), "menu_contact")
#define MENU_CGU NSLocalizedStringWithDefaultValue(@"menu_cgu", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"menu_cgu", GET_LOCALIZABLE ,"menu_cgu"), "menu_cgu")
#define MENU_MENTION NSLocalizedStringWithDefaultValue(@"menu_mention", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"menu_mention", GET_LOCALIZABLE ,"menu_mention"), "menu_mention")
#define MENU_NOTIFICATIONS NSLocalizedStringWithDefaultValue(@"menu_notifications", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"menu_notifications", GET_LOCALIZABLE ,"menu_notifications"), "menu_notifications")
#define MENU_EVENTS NSLocalizedStringWithDefaultValue(@"menu_events", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"menu_events", GET_LOCALIZABLE ,"menu_events"), "menu_events")
#define MENU_CONFIGURATION NSLocalizedStringWithDefaultValue(@"menu_configuration", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"menu_configuration", GET_LOCALIZABLE ,"menu_configuration"), "menu_configuration")
#define MENU_CLIENT NSLocalizedStringWithDefaultValue(@"menu_client", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"menu_client", GET_LOCALIZABLE ,"menu_client"), "menu_client")
#define MENU_PROFILE NSLocalizedStringWithDefaultValue(@"menu_profile", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"menu_profile", GET_LOCALIZABLE ,"menu_profile"), "menu_profile")
#define MENU_LOGOUT NSLocalizedStringWithDefaultValue(@"menu_logout", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"menu_logout", GET_LOCALIZABLE ,"menu_logout"), "menu_logout")

//CGU Texts
#pragma mark - CGU Texts

#define CGU_HEADER NSLocalizedStringWithDefaultValue(@"cgu_header", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"cgu_header", GET_LOCALIZABLE ,"cgu_header"), "cgu_header")
#define FAQ_HEADER NSLocalizedStringWithDefaultValue(@"faq_header", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"faq_header", GET_LOCALIZABLE ,"faq_header"), "faq_header")
#define MENTIONS_LEGAL_HEADER NSLocalizedStringWithDefaultValue(@"mentions_legals_header", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"mentions_legals_header", GET_LOCALIZABLE ,"mentions_legals_header"), "mentions_legals_header")

//Login Texts
#pragma mark - Login Texts

#define LOGIN_HEADER NSLocalizedStringWithDefaultValue(@"login_header", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"login_header", GET_LOCALIZABLE ,"login_header"), "login_header")
#define TEXT_OU_INDICATOR NSLocalizedStringWithDefaultValue(@"text_ou_indicator", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"text_ou_indicator", GET_LOCALIZABLE ,"text_ou_indicator"), "text_ou_indicator")
#define LOGIN_INDICATOR NSLocalizedStringWithDefaultValue(@"login_indicator", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"login_indicator", GET_LOCALIZABLE ,"login_indicator"), "login_indicator")
#define MAIL_TEXT NSLocalizedStringWithDefaultValue(@"mail_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"mail_text", GET_LOCALIZABLE ,"mail_text"), "mail_text")
#define PASSWORD_TEXT NSLocalizedStringWithDefaultValue(@"password_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"password_text", GET_LOCALIZABLE ,"password_text"), "password_text")
#define REMEMBER_PASS_TEXT NSLocalizedStringWithDefaultValue(@"remember_pass_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"remember_pass_text", GET_LOCALIZABLE ,"remember_pass_text"), "remember_pass_text")
#define REMEMBER_PASS_TEXT_UNDERLINE NSLocalizedStringWithDefaultValue(@"remember_pass_text_underline", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"remember_pass_text_underline", GET_LOCALIZABLE ,"remember_pass_text_underline"), "remember_pass_text_underline")
#define REGISTER_TEXT NSLocalizedStringWithDefaultValue(@"register_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"register_text", GET_LOCALIZABLE ,"register_text"), "register_text")
#define REGISTER_TEXT_UNDERLINE NSLocalizedStringWithDefaultValue(@"register_text_underline", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"register_text_underline", GET_LOCALIZABLE ,"register_text_underline"), "register_text_underline")
#define AUTHENTICATION_USER_FAIL NSLocalizedStringWithDefaultValue(@"authentication_user_fail", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"authentication_user_fail", GET_LOCALIZABLE ,"authentication_user_fail"), "authentication_user_fail")

//password recover popup
#pragma mark - password recover popup

#define PASS_RECOVER_POPUP_HEADER NSLocalizedStringWithDefaultValue(@"pass_recover_popup_header", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"pass_recover_popup_header", GET_LOCALIZABLE ,"pass_recover_popup_header"), "pass_recover_popup_header")
#define PASS_RECOVER_POPUP_MSG NSLocalizedStringWithDefaultValue(@"pass_recover_popup_msg", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"pass_recover_popup_msg", GET_LOCALIZABLE ,"pass_recover_popup_msg"), "pass_recover_popup_msg")
#define VALIDER_TEXT NSLocalizedStringWithDefaultValue(@"valider_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"valider_text", GET_LOCALIZABLE ,"valider_text"), "valider_text")

//ecover_sucess popup
#pragma mark - recover_sucess popup

#define RECOVER_SUCESS_HEADER NSLocalizedStringWithDefaultValue(@"recover_sucess_header", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"recover_sucess_header", GET_LOCALIZABLE ,"recover_sucess_header"), "recover_sucess_header")
#define RECOVER_SUCESS_MSG NSLocalizedStringWithDefaultValue(@"recover_sucess_msg", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"recover_sucess_msg", GET_LOCALIZABLE ,"recover_sucess_msg"), "recover_sucess_msg")

//Inscription texts
#pragma mark - Inscription texts

#define INSCRIPTION_HEADER NSLocalizedStringWithDefaultValue(@"inscription_header", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"inscription_header", GET_LOCALIZABLE ,"inscription_header"), "inscription_header")
#define INSCRIPTION_REGISTER_TEXT NSLocalizedStringWithDefaultValue(@"inscription_register_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"inscription_register_text", GET_LOCALIZABLE ,"inscription_register_text"), "inscription_register_text")
#define CONFIRM_PASS_TEXT NSLocalizedStringWithDefaultValue(@"confirm_pass_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"confirm_pass_text", GET_LOCALIZABLE ,"confirm_pass_text"), "confirm_pass_text")
#define NAME_TEXT NSLocalizedStringWithDefaultValue(@"name_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"name_text", GET_LOCALIZABLE ,"name_text"), "name_text")
#define LAST_NAME_TEXT NSLocalizedStringWithDefaultValue(@"last_name_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"last_name_text", GET_LOCALIZABLE ,"last_name_text"), "last_name_text")
#define OPTION1_TEXT NSLocalizedStringWithDefaultValue(@"option1_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"option1_text", GET_LOCALIZABLE ,"option1_text"), "option1_text")
#define OPTION3_TEXT NSLocalizedStringWithDefaultValue(@"option3_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"option3_text", GET_LOCALIZABLE ,"option3_text"), "option3_text")
#define OPTION2_TEXT NSLocalizedStringWithDefaultValue(@"option2_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"option2_text", GET_LOCALIZABLE ,"option2_text"), "option2_text")
#define OPTION2_PART1 NSLocalizedStringWithDefaultValue(@"option2_part1", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"option2_part1", GET_LOCALIZABLE ,"option2_part1"), "option2_part1")
#define OPTION2_PART2 NSLocalizedStringWithDefaultValue(@"option2_part2", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"option2_part2", GET_LOCALIZABLE ,"option2_part2"), "option2_part2")
#define SELECT_COUNTRY NSLocalizedStringWithDefaultValue(@"select_country", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"select_country", GET_LOCALIZABLE ,"select_country"), "select_country")
#define SELECT_COUNTRY_INDICATOR NSLocalizedStringWithDefaultValue(@"select_country_indicator", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"select_country_indicator", GET_LOCALIZABLE ,"select_country_indicator"), "select_country_indicator")
#define FILL_DATA_TEXT NSLocalizedStringWithDefaultValue(@"fill_data_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"fill_data_text", GET_LOCALIZABLE ,"fill_data_text"), "fill_data_text")
#define CONFIRM_PASS_INCORRECT NSLocalizedStringWithDefaultValue(@"confirm_pass_incorrect", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"confirm_pass_incorrect", GET_LOCALIZABLE ,"confirm_pass_incorrect"), "confirm_pass_incorrect")
#define ACCEPT_UTILIZATION_CONDITIONS NSLocalizedStringWithDefaultValue(@"accept_utilization_conditions", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"accept_utilization_conditions", GET_LOCALIZABLE ,"accept_utilization_conditions"), "accept_utilization_conditions")
#define ERROR_CLIENT_INSCRIPTION NSLocalizedStringWithDefaultValue(@"error_client_inscription", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"error_client_inscription", GET_LOCALIZABLE ,"error_client_inscription"), "error_client_inscription")
#define SUCESSFULLY_CLIENT_INSCRIPTION NSLocalizedStringWithDefaultValue(@"sucessfully_client_inscription", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"sucessfully_client_inscription", GET_LOCALIZABLE ,"sucessfully_client_inscription"), "sucessfully_client_inscription")

//Tutorial texts
#pragma mark - Tutorial texts

#define TUTORIAL_HEADER NSLocalizedStringWithDefaultValue(@"tutorial_header", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"tutorial_header", GET_LOCALIZABLE ,"tutorial_header"), "tutorial_header")
#define INTRO_TEXT NSLocalizedStringWithDefaultValue(@"intro_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"intro_text", GET_LOCALIZABLE ,"intro_text"), "intro_text")

#define INTRO_STEP1_TEXT NSLocalizedStringWithDefaultValue(@"intro_step1_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"intro_step1_text", GET_LOCALIZABLE ,"intro_step1_text"), "intro_step1_text")
#define INTRO_STEP2_TEXT NSLocalizedStringWithDefaultValue(@"intro_step2_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"intro_step2_text", GET_LOCALIZABLE ,"intro_step2_text"), "intro_step2_text")
#define INTRO_STEP3_TEXT NSLocalizedStringWithDefaultValue(@"intro_step3_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"intro_step3_text", GET_LOCALIZABLE ,"intro_step3_text"), "intro_step3_text")
#define INTRO_STEP3_INFO_TEXT NSLocalizedStringWithDefaultValue(@"intro_step3_info_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"intro_step3_info_text", GET_LOCALIZABLE ,"intro_step3_info_text"), "intro_step3_info_text")
#define INTRO_STEP3_BUTTON_TEXT NSLocalizedStringWithDefaultValue(@"intro_step3_button_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"intro_step3_button_text", GET_LOCALIZABLE ,"intro_step3_button_text"), "intro_step3_button_text")
#define INTRO_STEP3_PART1 NSLocalizedStringWithDefaultValue(@"intro_step3_part1", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"intro_step3_part1", GET_LOCALIZABLE ,"intro_step3_part1"), "intro_step3_part1")

#define MARKER_POPUP_HEADER NSLocalizedStringWithDefaultValue(@"marker_popup_header", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"marker_popup_header", GET_LOCALIZABLE ,"marker_popup_header"), "marker_popup_header")
#define MARKER_POPUP_INDICATION1 NSLocalizedStringWithDefaultValue(@"marker_popup_indication1", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"marker_popup_indication1", GET_LOCALIZABLE ,"marker_popup_indication1"), "marker_popup_indication1")
#define MARKER_POPUP_INDICATION2 NSLocalizedStringWithDefaultValue(@"marker_popup_indication2", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"marker_popup_indication2", GET_LOCALIZABLE ,"marker_popup_indication2"), "marker_popup_indication2")
#define MARKER_POPUP_INDICATION3 NSLocalizedStringWithDefaultValue(@"marker_popup_indication3", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"marker_popup_indication3", GET_LOCALIZABLE ,"marker_popup_indication3"), "marker_popup_indication3")
#define MARKER_POPUP_DEMO NSLocalizedStringWithDefaultValue(@"marker_popup_demo", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"marker_popup_demo", GET_LOCALIZABLE ,"marker_popup_demo"), "marker_popup_demo")
#define MARKER_POPUP_MAIL_NOTE NSLocalizedStringWithDefaultValue(@"marker_popup_mail_note", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"marker_popup_mail_note", GET_LOCALIZABLE ,"marker_popup_mail_note"), "marker_popup_mail_note")
#define MARKER_POPUP_SEND_TEXT NSLocalizedStringWithDefaultValue(@"marker_popup_send_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"marker_popup_send_text", GET_LOCALIZABLE ,"marker_popup_send_text"), "marker_popup_send_text")

#define PDF_REQUEST_SUCESS NSLocalizedStringWithDefaultValue(@"pdf_request_sucess", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"pdf_request_sucess", GET_LOCALIZABLE ,"pdf_request_sucess"), "pdf_request_sucess")
#define PDF_REQUEST_FAIL NSLocalizedStringWithDefaultValue(@"pdf_request_FAIL", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"pdf_request_FAIL", GET_LOCALIZABLE ,"pdf_request_FAIL"), "pdf_request_FAIL")
#define MARKER_POPUP_INFO NSLocalizedStringWithDefaultValue(@"marker_popup_info", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"marker_popup_info", GET_LOCALIZABLE ,"marker_popup_info"), "marker_popup_info")
#define MARKER_TEXT NSLocalizedStringWithDefaultValue(@"marker_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"marker_text", GET_LOCALIZABLE ,"marker_text"), "marker_text")

//Products texts
#pragma mark - Products texts

#define PRODUCTS_HEADER NSLocalizedStringWithDefaultValue(@"products_header", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"products_header", GET_LOCALIZABLE ,"products_header"), "products_header")
#define PRODUCT_FILTER_1 NSLocalizedStringWithDefaultValue(@"product_filter_1", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"product_filter_1", GET_LOCALIZABLE ,"product_filter_1"), "product_filter_1")
#define PRODUCT_FILTER_2 NSLocalizedStringWithDefaultValue(@"product_filter_2", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"product_filter_2", GET_LOCALIZABLE ,"product_filter_2"), "product_filter_2")
#define PRODUCT_FILTER_3 NSLocalizedStringWithDefaultValue(@"product_filter_3", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"product_filter_3", GET_LOCALIZABLE ,"product_filter_3"), "product_filter_3")

//Product details texts
#pragma mark - Product details texts

#define PRODUCT_DETAILS_HEADER NSLocalizedStringWithDefaultValue(@"product_details_header", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"product_details_header", GET_LOCALIZABLE ,"product_details_header"), "product_details_header")
#define PRODUCT_STORES_INDICATOR NSLocalizedStringWithDefaultValue(@"product_stores_indicator", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"product_stores_indicator", GET_LOCALIZABLE ,"product_stores_indicator"), "product_stores_indicator")
#define PRODUCT_MARKER_DOWNLOAD_INDICATOR NSLocalizedStringWithDefaultValue(@"product_marker_download_indicator", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"product_marker_download_indicator", GET_LOCALIZABLE ,"product_marker_download_indicator"), "product_marker_download_indicator")

#define CURRENT_POSITION_TEXT NSLocalizedStringWithDefaultValue(@"current_position_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"current_position_text", GET_LOCALIZABLE ,"current_position_text"), "current_position_text")

//Events Texts
#pragma mark - Events Texts

#define EVENT_HEADER NSLocalizedStringWithDefaultValue(@"event_header", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"event_header", GET_LOCALIZABLE ,"event_header"), "event_header")
#define EVENT_DETAILS_HEADER NSLocalizedStringWithDefaultValue(@"event_details_header", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"event_details_header", GET_LOCALIZABLE ,"event_details_header"), "event_details_header")

//Stories texts
#pragma mark - Stories texts

#define STORIES_HEADER NSLocalizedStringWithDefaultValue(@"stories_header", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"stories_header", GET_LOCALIZABLE ,"stories_header"), "stories_header")
#define STORY_BTN_VIEW_ALL NSLocalizedStringWithDefaultValue(@"story_btn_view_all", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"story_btn_view_all", GET_LOCALIZABLE ,"story_btn_view_all"), "story_btn_view_all")

//Stories details texts
#pragma mark - Stories details texts

#define STORIES_DETAILS_HEADER NSLocalizedStringWithDefaultValue(@"stories_detail_header", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"stories_detail_header", GET_LOCALIZABLE ,"stories_detail_header"), "stories_detail_header")

//Biblio texts
#pragma mark - Biblio texts

#define BIBLIO_HEADER NSLocalizedStringWithDefaultValue(@"biblio_header", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"biblio_header", GET_LOCALIZABLE ,"biblio_header"), "biblio_header")

//Biblio detail texts
#pragma mark - Biblio detail texts

#define DELETE_CONFIRMATION_MSG NSLocalizedStringWithDefaultValue(@"delete_confirmation_msg", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"delete_confirmation_msg", GET_LOCALIZABLE ,"delete_confirmation_msg"), "delete_confirmation_msg")
#define MAIL_SUBJECT_TEXT NSLocalizedStringWithDefaultValue(@"mail_subject_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"mail_subject_text", GET_LOCALIZABLE ,"mail_subject_text"), "mail_subject_text")

//Distributors texts
#pragma mark - Distributors texts

#define DISTRIBUTOR_HEADER NSLocalizedStringWithDefaultValue(@"distributor_header", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"distributor_header", GET_LOCALIZABLE ,"distributor_header"), "distributor_header")
#define DISTRIBUTOR_DETAIL_TEXT NSLocalizedStringWithDefaultValue(@"distributor_detail_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"distributor_detail_text", GET_LOCALIZABLE ,"distributor_detail_text"), "distributor_detail_text")

//MesXperience texts
#pragma mark - MesXperience texts

#define MESXPERIENCE_HEADER NSLocalizedStringWithDefaultValue(@"mesxperience_header", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"mesxperience_header", GET_LOCALIZABLE ,"mesxperience_header"), "mesxperience_header")

//StoreDetail texts
#pragma mark - StoreDetail texts

#define STORE_DETAIL_HEADER NSLocalizedStringWithDefaultValue(@"store_detail_header", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"store_detail_header", GET_LOCALIZABLE ,"store_detail_header"), "store_detail_header")
#define STORE_PRODUCT_TEXT NSLocalizedStringWithDefaultValue(@"store_product_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"store_product_text", GET_LOCALIZABLE ,"store_product_text"), "store_product_text")
#define STORE_TEL_TEXT NSLocalizedStringWithDefaultValue(@"store_tel_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"store_tel_text", GET_LOCALIZABLE ,"store_tel_text"), "store_tel_text")
#define STORE_MAIL_TEXT NSLocalizedStringWithDefaultValue(@"store_mail_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"store_mail_text", GET_LOCALIZABLE ,"store_mail_text"), "store_mail_text")
#define STORE_SITE_TEXT NSLocalizedStringWithDefaultValue(@"store_site_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"store_site_text", GET_LOCALIZABLE ,"store_site_text"), "store_site_text")

//SHARE TEXTS
#pragma mark - SHARE TEXTS

#define TEXT_TO_SHARE NSLocalizedStringWithDefaultValue(@"text_to_share", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"text_to_share", GET_LOCALIZABLE ,"text_to_share"), "text_to_share")
#define SHARE_TEXT_LINK NSLocalizedStringWithDefaultValue(@"share_text_link", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"share_text_link", GET_LOCALIZABLE ,"share_text_link"), "share_text_link")

//SUGGEST STOP DOWNLOAD TEXTS
#pragma mark - SUGGEST STOP DOWNLOAD TEXTS

#define SUGGEST_STOP_DOWNLOAD_TEXT NSLocalizedStringWithDefaultValue(@"suggest_stop_download_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"suggest_stop_download_text", GET_LOCALIZABLE ,"suggest_stop_download_text"), "suggest_stop_download_text")
#define BTN_STOP_DOWNLOAD_TEXT NSLocalizedStringWithDefaultValue(@"btn_stop_download_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"btn_stop_download_text", GET_LOCALIZABLE ,"btn_stop_download_text"), "btn_stop_download_text")
#define INFO_POPUP_DOWNLOAD_TEXT NSLocalizedStringWithDefaultValue(@"info_popup_download_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"info_popup_download_text", GET_LOCALIZABLE ,"info_popup_download_text"), "info_popup_download_text")
#define INFO_POPUP_DOWNLOAD_TEXT_1 NSLocalizedStringWithDefaultValue(@"info_popup_download_text_1", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"info_popup_download_text_1", GET_LOCALIZABLE ,"info_popup_download_text_1"), "info_popup_download_text_1")
#define CLOSE_INFO_POPUP_DOWNLOAD_TEXT NSLocalizedStringWithDefaultValue(@"close_info_popup_download_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"close_info_popup_download_text", GET_LOCALIZABLE ,"close_info_popup_download_text"), "close_info_popup_download_text")

//NOTIFY APP UPDATING TEXT
#pragma mark - NOTIFY APP UPDATING TEXT

#define NOTIFY_APP_UPDATING_TEXT NSLocalizedStringWithDefaultValue(@"notify_app_updating_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"notify_app_updating_text", GET_LOCALIZABLE ,"notify_app_updating_text"), "notify_app_updating_text")

#define NOTIFY_AVAILABLE_UPDATE NSLocalizedStringWithDefaultValue(@"notify_available_update", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"notify_available_update", GET_LOCALIZABLE ,"notify_available_update"), "notify_available_update")
#define NOTIFY_UPDATE_NEEDED NSLocalizedStringWithDefaultValue(@"notify_update_needed", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"notify_update_needed", GET_LOCALIZABLE ,"notify_update_needed"), "notify_update_needed")

//NOTIFY APP OUT OF DATE TEXT
#pragma mark - NOTIFY APP OUT OF DATE TEXT

#define NOTIFY_APP_IS_OUT_OF_DATE_TEXT NSLocalizedStringWithDefaultValue(@"notify_app_is_out_of_date", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"notify_app_is_out_of_date", GET_LOCALIZABLE ,"notify_app_is_out_of_date"), "notify_app_is_out_of_date")
#define SUGGEST_DOWNLOAD_NEW_APP_VERSION NSLocalizedStringWithDefaultValue(@"suggest_download_new_app_version", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"suggest_download_new_app_version", GET_LOCALIZABLE ,"suggest_download_new_app_version"), "suggest_download_new_app_version")

//NOTIFICATION DETAIL VIEW
#pragma mark - NOTIFICATION DETAIL VIEW

#define BTN_DELETE_NOTIFICATION NSLocalizedStringWithDefaultValue(@"btn_delete_notification", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"btn_delete_notification", GET_LOCALIZABLE ,"btn_delete_notification"), "btn_delete_notification")
#define POPUP_CONFIRM_DELETE_NOTIFICATION NSLocalizedStringWithDefaultValue(@"popup_confirm_delete_notification", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"popup_confirm_delete_notification", GET_LOCALIZABLE ,"popup_confirm_delete_notification"), "popup_confirm_delete_notification")

//CONFIGURATION VIEW TEXT
#pragma mark - CONFIGURATION VIEW TEXT

#define CONFIG_VIDEO_TEXT NSLocalizedStringWithDefaultValue(@"config_video_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"config_video_text", GET_LOCALIZABLE ,"config_video_text"), "config_video_text")
#define VIDEO_TEXT NSLocalizedStringWithDefaultValue(@"video_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"video_text", GET_LOCALIZABLE ,"video_text"), "video_text")
#define HIGH_DEFINITION_TEXT NSLocalizedStringWithDefaultValue(@"high_definition_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"high_definition_text", GET_LOCALIZABLE ,"high_definition_text"), "high_definition_text")
#define MEDIUM_DEFINITION_TEXT NSLocalizedStringWithDefaultValue(@"medium_definition_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"medium_definition_text", GET_LOCALIZABLE ,"medium_definition_text"), "medium_definition_text")
#define LOW_DEFINITION_TEXT NSLocalizedStringWithDefaultValue(@"low_definition_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"low_definition_text", GET_LOCALIZABLE ,"low_definition_text"), "low_definition_text")
#define PIXEL_TEXT NSLocalizedStringWithDefaultValue(@"pixel_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"pixel_text", GET_LOCALIZABLE ,"pixel_text"), "pixel_text")

//LOCALIZATION TEXTS
#pragma mark - Localization TEXT

#define GPS_DISABLE_SHORT_INFO_TEXT NSLocalizedStringWithDefaultValue(@"gps_disable_short_info_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"gps_disable_short_info_text", GET_LOCALIZABLE ,"gps_disable_short_info_text"), "gps_disable_short_info_text")
#define GPS_DISABLE_INFO_TEXT NSLocalizedStringWithDefaultValue(@"gps_disable_info_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"gps_disable_info_text", GET_LOCALIZABLE ,"gps_disable_info_text"), "gps_disable_info_text")

//CLIENTS VIEW TEXTS
#pragma mark - CLIENTS VIEW TEXTS

#define CLIENTS_VIEW_HEADER NSLocalizedStringWithDefaultValue(@"clients_view_header", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"clients_view_header", GET_LOCALIZABLE ,"clients_view_header"), "clients_view_header")

//CLIENT DETAILS VIEW TEXTS
#pragma mark - CLIENT DETAILS VIEW TEXTS

#define CLIENT_DETAIL_VIEW_HEADER NSLocalizedStringWithDefaultValue(@"client_detail_view_header", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"client_detail_view_header", GET_LOCALIZABLE ,"client_detail_view_header"), "client_detail_view_header")

//DEMO VIEW TEXTS
#pragma mark - DEMO VIEW TEXTS

#define DEMO_SKIP_VIDEO NSLocalizedStringWithDefaultValue(@"demo_skip_video", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"demo_skip_video", GET_LOCALIZABLE ,"demo_skip_video"), "demo_skip_video")

//MSG FOR REDIRECT TO SETTING IOS < 8
#define SETTINGS_PATH_MSG_LOCALIZATION NSLocalizedStringWithDefaultValue(@"settings_path_msg_localization", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"settings_path_msg_localization", GET_LOCALIZABLE ,"settings_path_msg_localization"), "settings_path_msg_localization")

#define ACCEPT_PERMISSION_TEXT NSLocalizedStringWithDefaultValue(@"accept_permission_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"accept_permission_text", GET_LOCALIZABLE ,"accept_permission_text"), "accept_permission_text")
#define CANCEL_PERMISSION_TEXT NSLocalizedStringWithDefaultValue(@"cancel_permission_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"cancel_permission_text", GET_LOCALIZABLE ,"cancel_permission_text"), "cancel_permission_text")

//MSG FOR EMPTY CONTENT VIEW
#define ME_EMPTY_VIEW_BTN_TRY_AGAIN NSLocalizedStringWithDefaultValue(@"btn_try_again", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"btn_try_again", GET_LOCALIZABLE ,"btn_try_again"), "btn_try_again")

//PRODUCTS VIEW FILTER TEXTS
#define ME_FILTER_ALL_EVENTS_TEXT NSLocalizedStringWithDefaultValue(@"filter_all_events_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"filter_all_events_text", GET_LOCALIZABLE ,"filter_all_events_text"), "filter_all_events_text")
#define ME_FILTER_ALL_PRODUCTS_TYPES_TEXT NSLocalizedStringWithDefaultValue(@"filter_all_products_types_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"filter_all_products_types_text", GET_LOCALIZABLE ,"filter_all_products_types_text"), "filter_all_products_types_text")
#define ME_FILTER_ALL_STORIES_TEXT NSLocalizedStringWithDefaultValue(@"filter_all_stories_text", GET_CUSTOM_APP_LOCALIZABLE, NSBundle.mainBundle, NSLocalizedStringFromTable(@"filter_all_stories_text", GET_LOCALIZABLE ,"filter_all_stories_text"), "filter_all_stories_text")
