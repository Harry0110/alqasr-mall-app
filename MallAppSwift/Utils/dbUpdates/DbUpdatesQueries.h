//
//  DbUpdatesQueries.h
//  MagicXperience
//

#import <Foundation/Foundation.h>

//query to create in internaldb the new table animation_codes. Correspond to db version 1

#define QUERY_UPDATE_DB_1 @"CREATE TABLE animation_codes(id integer primary key, id_marker integer not null,id_animation integer not null, code text not null)"
#define QUERY_UPDATE_DB_2 @"ALTER TABLE notification_notification_status ADD COLUMN notification_received_date NUMERIC NOT NULL DEFAULT 0"
#define QUERY_UPDATE_DB_3 @"CREATE TABLE animation_variables (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE, marker_id INTEGER NOT NULL, animation_id INTEGER NOT NULL, variable_key TEXT, variable_value TEXT)"
#define QUERY_UPDATE_DB_4 @"CREATE TABLE local_users (id INTEGER PRIMARY KEY  AUTOINCREMENT  UNIQUE , name VARCHAR NOT NULL , last_name VARCHAR NOT NULL , email VARCHAR NOT NULL , country VARCHAR, receive_news_letter BOOL NOT NULL  DEFAULT 0, receive_news_letter_from_partners BOOL NOT NULL  DEFAULT 0, server_id VARCHAR)"

//query to create in internaldb the new table vuforia_db_data to storages. Correspond to db version 2

#define QUERY_UPDATE_DB_5 @"CREATE  TABLE vuforia_db_data (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE , name VARCHAR, modification_date VARCHAR)"
#define QUERY_UPDATE_DB_6 @"CREATE TABLE notification_generic_notification_status(id integer primary key, notification_id integer not null, notification_status_id integer not null, notification_received_date INTEGER NOT NULL  DEFAULT 0)"
