//
//  CommonsUtils.h
//

#import <UIKit/UIKit.h>
#import "FMDB.h"

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)


#define FMDBQuickCheck(SomeBool) { if (!(SomeBool)) { NSLog(@"Failure on line %d", __LINE__); abort(); } }

//////////////////////
// Screen
//////////////////////
#define IS_WIDESCREEN (([[UIScreen mainScreen]bounds].size.height - 568.0f) >= 0.0f)

#define IS_IPHONE ([[[UIDevice currentDevice]model] isEqualToString:@"iPhone"] || [[[UIDevice currentDevice]model] isEqualToString:@"iPhone Simulator"])
#define IS_IPAD ([[[UIDevice currentDevice]model] isEqualToString:@"iPad"] || [[[UIDevice currentDevice]model] isEqualToString:@"iPad Simulator"])

#define IS_IPOD   ([[[UIDevice currentDevice]model] isEqualToString:@"iPod touch"])
#define IS_IPHONE_5 (IS_IPHONE && IS_WIDESCREEN)
#define IS_IPHONE_6 (IS_IPHONE && (([[UIScreen mainScreen]bounds].size.height - 568.0f) > 0.0f))

#define IS_RETINA ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] && ([UIScreen mainScreen].scale == 2.0))

#define kCOLLECTION_CELL_HEIGHT_IPHONE 140
#define kCOLLECTION_CELL_HEIGHT_IPAD 160
#define kCOLLECTION_CELL_HEIGHT_IPHONE6 165

#pragma mark - UI Dimensions

//////////////////////
// Splash
//////////////////////
#define MINIM_SPLASH_TIME 5.0

/////////////////////////
///when pass this time the app will suggest stop download task
#define TIME_TO_SUGGEST_STOP_BD_DOWNLOAD 15.0

///////////////////////
//application downloaded reintent time
#define DB_DOWNLOAD_TIME 30.0

//////////////////////
// Menu
//////////////////////
#define MENU_ITEM_HEIGHT 35
//////////////////////
// Top bar
//////////////////////
#define TOP_BAR_HEIGHT 48
#define TOP_BAR_HEIGHT_LAND_SCAPE 35

//Video cropping
#define TOP_AND_BOTTOM_BAR_SPACE_HEIGHT 190


#define KLANG @"DEFAULT_LANG"                   //Key for safe default language in user defaults
//LANGUAGE SUPPORT
#define EN @"en"                                //English
#define FR @"fr"
#define ES @"es"
#define AR @"ar"
//#define ZH @"zh"                                //Chinese
#define DEFAULT_LANGUAGE_VALUE @"en"//Default language to use
#define DEFAULT_LANGUAGE_ARABIC @"ar"

//#define ZH_FOR_WS @"zh-hans"

#define APPLE_STORE_ID @"1052747627" // Magic Xperience
//#define APPLE_STORE_ID @"1438213439" // Pro a Pro
//#define APPLE_STORE_ID @"1303464415" // Artech
//#define APPLE_STORE_ID @"1442237535" // Bonduelle
//#define APPLE_STORE_ID @"1447096312" // Amsellem
#define APPLE_STORE_URL @"https://itunes.apple.com/LANG/app/idAPP_ID?ls=1&mt=8"

//#define GOOGLE_ANALYTICS_PROPERTY_ID @"UA-67616145-2"
#define GOOGLE_ANALYTICS_PROPERTY_ID @"XX-67616145-2"
//#define GOOGLE_ANALYTICS_TRACK_TEXT @"MAGIC_XPERIENCE_IOS_APP"
#define GOOGLE_ANALYTICS_PRODUCTS_SCREEN_TEXT @"PRODUCTS_IOS_APP"
#define GOOGLE_ANALYTICS_STORE_SCREEN_TEXT @"STORES_IOS_APP"
#define GOOGLE_ANALYTICS_PRODUCT_CATEGORY @"Product"
#define GOOGLE_ANALYTICS_STORE_CATEGORY @"Store"
#define GOOGLE_ANALYTICS_ACTION @"Consulted"
#define GOOGLE_ANALYTICS_NOTIFICATION_CATEGORY @"Notification"
#define GOOGLE_ANALYTICS_NOTIFICATION_READ_ACTION @"READ"
#define GOOGLE_ANALYTICS_NOTIFICATION_RECEIVED_ACTION @"RECEIVED"
#define GOOGLE_ANALYTICS_MARKER_CATEGORY @"Marker"
#define GOOGLE_ANALYTICS_MARKER_DOWNLOAD_ACTION @"Animation downloaded"
#define GOOGLE_ANALYTICS_MARKER_FOUND_ACTION @"Found"

#define APP_MAIL @"contact@magic-xperience.com"

#define MXPERIENCE_SHARE_URL @"magic-xperience.com/LANG"
#define MXPERIENCE_DEMO_SITE @"http://www.magic-xperience.com/LANG/demo"

#define LOCAL_DB_NAME @"localdatabase.sqlite"
#define DB_EXTENSION @"sqlite"

#define VUFORIA_DB_NAME @"Trackers"
#define INTERNAL_DB_NAME @"internaldb"

//#define MGX_GET_ARG_DATA_IN_FOLDER NSDocumentDirectory
#define MGX_GET_ARG_DATA_IN_FOLDER NSLibraryDirectory

#define MXPERIENCE_ALBUM_NAME @"Media"
#define MXPERIENCE_IMAGE_FOLDER_NAME @"ARGImages"
#define MXPERIENCE_DATABASE_FOLDER_NAME @"ARG_databases"
#define MXPERIENCE_PRODUCTS_IMG_FOLDER_NAME @"ARG_products"
#define MXPERIENCE_GROUP_IMG_FOLDER_NAME @"ARG_groups"
#define MXPERIENCE_STORE_FOLDER_NAME @"ARG_stores"
#define MEXPERIENCE_HISTORY_FOLDER_NAME @"ARG_history"
#define MEXPERIENCE_NOTIFICATIONS_FOLDER_NAME @"ARG_notifications"
#define MEXPERIENCE_CLIENTS_FOLDER_NAME @"ARG_clients"

#define kTUTORIAL_SHOW_FIRST_TIME @"TUTORIAL_SHOW_FIRST_TIME"
#define KHOMEVIEW_SHOW_FIRST_TIME @"KHOMEVIEW_SHOW_FIRST_TIME"

#define APP_OUT_OFF_DATE_MARK @"APP_OUT_OFF_DATE_MARK"
#define APP_OUT_OF_DATE_VERSION_MARK @"APP_OUT_OF_DATE_VERSION_MARK"

//this constants is used to save in user preference the modified date to app and vuforia databases
#define APP_DATABASE_NAME @"APPLICATION_DATABASE"
#define VUFORIA_DATABASE_NAME @"VUFORIA_DATABASE"
#define REQUESTED_APP_DB @"REQUESTED_APP_DB"
#define REQUESTED_APP_TRACKERS @"REQUESTED_APP_TRACKERS"

#define AUTH_USER_SESSION_ID @"AUTH_USER_SESSION_ID"
#define AUTH_USER_SESSION_NAME @"AUTH_USER_SESSION_NAME"
#define AUTH_USER_TOKEN @"AUTH_USER_TOKEN"

#define MAX_VALUE_FOR_HIGH_DEFINITION_MAX 1920.0f
#define MAX_VALUE_FOR_HIGH_DEFINITION_MIN 1080.0f

#define REDUCTION_PERCENT_FOR_MEDIUM_DEFINITION 60.0f
#define REDUCTION_PERCENT_FOR_LOW_DEFINITION 40.0f

#define APP_ID_FOR_TOKEN @"1111"
#define APP_SECRET_FOR_TOKEN @"123456789"

#define MIN_NECESSARY_FREE_DISK_SPACE 50 //The minimum free disk space necessary to allow video recording megas

#define DISTANCE_FOR_ZOOM_IN_MAP 20

#define INTERNAL_DB_CURRENT_VERSION 2

#define VUFORIA_LOCAL_DBS_DESCRIPTOR_FILE @"Trackers_data"

#define INIT_NO_CAMERA_ACCESS @"INIT_NO_CAMERA_ACCESS" //Vuforia error code when camera access denied

#define APP_TARGET [NSString stringWithString:[[[NSBundle mainBundle] infoDictionary]   objectForKey:@"CFBundleName"]]
#define APP_TARGET_PROAPRO @"proapro"

typedef void (^VideoExportToLibCompletionBlock)(BOOL success, BOOL auth);

@interface CommonsUtils : NSObject {
    
}

@property (nonatomic, copy) NSString *defaultLang;
@property (nonatomic, strong) FMDatabase *localDB;
@property (nonatomic, strong) FMDatabase *internalDB;
@property (nonatomic, retain) FMDatabaseQueue *localQueueDB;
@property (nonatomic, assign) BOOL appIsReachable;

@property (nonatomic, assign) BOOL needDownloadSQlite;
@property (nonatomic, assign) BOOL needDownloadVuforiaData;
@property (nonatomic, assign) BOOL neddDownloadVideoIntro;
@property (nonatomic, assign) BOOL needDownloadSubtitlefile;

@property (nonatomic, readonly) __block BOOL moveImagesToGalleryInProcess;
@property (nonatomic, readonly) __block long totalImageToMoveToGallery;

@property(nonatomic, assign) BOOL isAppUpdateInBgMode;
@property(nonatomic, assign) BOOL isUserAcceptUpdate;

@property(nonatomic, assign) int totalDbSucessfullyDownload; //number of database sucessfully download in a download process

@property(nonatomic, assign) int unreadedNotifications;
@property (nonatomic, assign) int totalNotifications;
@property(nonatomic, assign) UIDeviceOrientation homeScreenOrientationOnRecord;
@property(nonatomic, assign) UIInterfaceOrientation homeScreenViewOrientationOnRecord;

@property(nonatomic, assign) UIDeviceOrientation lastOrientation;

@property (nonatomic, strong) NSMutableArray *vuforiaDbDownloadList;

@property (nonatomic, assign) int lastDownloadPercentReported;

@property (nonatomic, assign) BOOL canResumeVideoDemo; //to fix problems with video demo resume when sharing action finish

+ (void) initializeGoogleAnalytics;

+ (CommonsUtils *) getCommonUtil;
+ (NSString*) getAppLanguage;

#pragma mark - Colors
#pragma mark Backgrounds & buttons colors

+ (UIColor*) getTopBarBgColor;
+ (UIColor*) getUnderlineTopBarColor;
+ (UIColor*) getTopBarTextColor;
+ (UIColor*) getPrimaryColor;
+ (UIColor*) getSecondaryColor;
+ (UIColor*) getComplementaryColor;
+ (UIColor*) getHighlightColor;
+ (NSString*) getStringColorForHtlmlText;
+ (NSString*) getStringColorForHtlmlLinkText;
+ (NSString*) getStringColorForFAQText;
+ (UIColor*) getClusterManager20;
+ (UIColor*) getClusterManager50;
+ (UIColor*) getClusterManager100;
+ (UIColor*) getClusterManager200;
+ (UIColor*) getClusterManager500;
+ (UIColor*) getClusterManager1000;
+ (UIColor*) getClusterManagerDefault;
+ (UIColor*) getMenuBgColor;
+ (UIColor*) getMenuItemTextColor;
+ (UIColor*) getMenuSeparatorColor;
+ (UIColor*) getStoreDetailsNameColor;
+ (UIColor*) getStoreDetailsAddressColor;
+ (UIColor*) getStoreDetailsPhoneMailSiteLabelColor;
+ (UIColor*) getStoreDetailsPhoneMailSiteValueColor;
+ (UIColor*) getStoreDetailsGpsButtonTextColor;
+ (UIColor*) getAppFAQDetailColor;


- (FMDatabase*) getInternalDB;
+ (NSString *) getDBPath;
- (FMDatabase *) getLocalDB;
- (FMDatabaseQueue*) getLocalQueueDB;
+ (NSString*) getLocalDatabaseName;
+ (NSString*) getRequestedVersion:(NSString*) aKeyForRequest;
- (BOOL) isLocalDbExits; // check if app db already exits and return TRUE, otherwise return FALSE
- (void) checkInternalDBAndUpdate;

+ (BOOL) isValidEmail:(NSString*)emailAddress;
+ (NSMutableArray*) loadPicturesInFolder:(NSString*)aImageFolderName;
+ (void) deletePicture:(NSString*)aImagePath;

+ (UIImage*) getImageInLocalFolder:(NSString *) aUrlImage folderName:(NSString*)aFolderName;
+ (void) saveImageInLocalFolder :(NSString *) aUrlImage imageFile:(UIImage*)aImage folderName:(NSString*)aFolderName;


+ (void) requestImageFromServer:(NSString *) aImagePath imageFolder:(NSURL*)aImageFolder imageIndex:(int)aImageIndex completionHandler:(void (^)(NSURLResponse *response, NSURL *filePath, NSError *error))completionHandler;

- (void) trackViewInGoogleAnalitics:(NSString *)screenName;
+ (void) reportEventToGoogleAnalytics:(NSString*)aCategory action:(NSString*)aAction label:(NSString*)aLabel;

+ (void)hideActivityViewer:(UIView*)aActivityView;
+ (UIView *) showActivityViewer:(NSString*)msg showInView:(UIView*)aParentView colorForView:(UIColor*)aColorView;
+ (UIView *) showActivityViewerWithCustomLoadingImage:(UIImage*)image message:(NSString*)msg showInView:(UIView*)aParentView colorForView:(UIColor*)aColorView;

+ (void) saveModifiedData:(NSString*)aDbName modifiedValue:(NSString*)aModifiedValue;
+ (BOOL) getIfUpdateNeeded:(NSString*)aDbName value:(NSString*)aValue;

+(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToSquare: (float) squareDimension maxScale: (float) aToScale;

-(void) closeOlderLocalDB;

+(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width;

-(void) updateUnreadedNotificationsCounter;

+ (BOOL) createDatabaseFolder;

+ (void) goToAppleStore;

+ (void) saveValueAsPreference: (NSString*)aValue key:(NSString*)aKey;
+ (NSString*) getPreferenceValueForKey: (NSString*)aKey;

+ (BOOL) isAppInAuthenticationMode;
+ (void) cleanSessionData;

+ (BOOL) createClientsFolder;

@end
