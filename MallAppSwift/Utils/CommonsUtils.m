//
//  CommonsUtils.m
//

#import "CommonsUtils.h"
#import "AFNetworkReachabilityManager.h"
#import "NotificationsName.h"
#import "ALAssetsLibrary+CustomPhotoAlbum.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import "UIView+ConstraintHelper.h"
//#import "UnityInterface.h" TODO:add sdk call
#import <Photos/Photos.h>
#import <AudioToolbox/AudioServices.h>
#import "VuforiaMExperience.h"
//#import "CustomAlbum.h"
#import "DbUpdatesQueries.h"
#import <ARGenieSDK/ARGenieManager.h>
#import "ConfigurationManager.h"
#import "AFURLSessionManager.h"
#import "Theme.h"

@implementation CommonsUtils

#pragma mark - Singleton Methods

static CommonsUtils *sharedPool = nil;

+ (CommonsUtils *) getCommonUtil {
    
    @synchronized(self)
    {
        if (sharedPool == nil)
        {
            sharedPool = [[CommonsUtils alloc] init];
            sharedPool.lastOrientation = UIDeviceOrientationPortrait;
        }
    }
    return sharedPool;
}

+ (id) allocWithZone:(NSZone *)zone  {
    @synchronized(self)
    {
        if (sharedPool == nil)
        {
            sharedPool = [super allocWithZone:zone];
            return sharedPool; // assignment and return on first allocation
        }
    }
    return nil; //on subsequent allocation attempts return nil
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

+ (BOOL) isValidEmail: (NSString*) emailAddress {
    if([emailAddress isEqualToString:@""]) return FALSE;
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
    return ([emailTest evaluateWithObject:emailAddress] == YES);
}

#pragma mark - Language


/*! Method for return the current application language
 * \returns (NSString*) current application language (EN, ES, FR). If not found, return DEFAULT_LANGUAGE_VALUE
 */
+ (NSString*) getAppLanguage {
    
    NSString* language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if (![language isEqualToString:EN] && ![language isEqualToString:FR] && ![language isEqualToString:ES])
    {
        NSArray *_languageComponent = [language componentsSeparatedByString:@"-"];
        if(_languageComponent != nil && [_languageComponent count] > 0)
        {
            NSString *_deviceLang = [_languageComponent objectAtIndex:0];
            if (![_deviceLang isEqualToString:EN] && ![_deviceLang isEqualToString:FR] && ![_deviceLang isEqualToString:ES] && ![_deviceLang isEqualToString:AR])
            language = DEFAULT_LANGUAGE_VALUE; //default lang
            else
            language = _deviceLang;
        }
        else
        language = DEFAULT_LANGUAGE_VALUE; //default lang
        
    }
    [sharedPool setDefaultLang:language];
    return language;
}
/*! Method for set application default language
 * \param (NSString*)defaultLang: language code to set
 * \returns (void)
 */
- (void) setDefaultLang:(NSString *)defaultLang {
    
    if ([_defaultLang isEqualToString:defaultLang])
    return;
    if (![defaultLang isEqualToString:EN] && ![defaultLang isEqualToString:FR] && ![defaultLang isEqualToString:ES])
    defaultLang = DEFAULT_LANGUAGE_VALUE; //default lang
    _defaultLang = defaultLang;
    [[NSUserDefaults standardUserDefaults] setObject:_defaultLang forKey:KLANG];
    [[NSUserDefaults standardUserDefaults] synchronize]; //to make the change immediate
}


#pragma mark - Colors
#pragma mark Backgrounds & buttons colors

+ (UIColor*) getTopBarBgColor
{
    UIColor *color = [ConfigurationManager getColorValue:APP_TOP_BAR_BG_COLOR];
    
    if (![color isEqual:[UIColor clearColor]])
    return color;
    else
    return [CommonsUtils getPrimaryColor];
}

+ (UIColor*) getUnderlineTopBarColor
{
    UIColor *color = [ConfigurationManager getColorValue:APP_UNDERLINE_TOP_BAR_COLOR];
    
    if (![color isEqual:[UIColor clearColor]])
    return color;
    else
    return [CommonsUtils getHighlightColor];
}

+ (UIColor*) getTopBarTextColor
{
    UIColor *color = [ConfigurationManager getColorValue:APP_TOP_BAR_TEXT_COLOR];
    
    if (![color isEqual:[UIColor clearColor]])
    return color;
    else
    return [CommonsUtils getSecondaryColor];
}

+ (UIColor*) getPrimaryColor
{
    return [ConfigurationManager getColorValue:APP_PRIMARY_COLOR];
}

+ (UIColor*) getSecondaryColor
{
    return [ConfigurationManager getColorValue:APP_SECONDARY_COLOR];
}

+ (UIColor*) getComplementaryColor
{
    return [ConfigurationManager getColorValue:APP_COMPLEMENTARY_COLOR];
}

+ (UIColor*) getHighlightColor
{
    return [ConfigurationManager getColorValue:APP_HIGTHLIGTH_COLOR];
}

+ (UIColor*) getClusterManager20
{
    return [ConfigurationManager getColorValue:APP_CLUSTERMANAGER_20];
}

+ (UIColor*) getClusterManager50
{
    return [ConfigurationManager getColorValue:APP_CLUSTERMANAGER_50];
}

+ (UIColor*) getClusterManager100
{
    return [ConfigurationManager getColorValue:APP_CLUSTERMANAGER_100];
}

+ (UIColor*) getClusterManager200
{
    return [ConfigurationManager getColorValue:APP_CLUSTERMANAGER_100];
}

+ (UIColor*) getClusterManager500
{
    return [ConfigurationManager getColorValue:APP_CLUSTERMANAGER_500];
}

+ (UIColor*) getClusterManager1000
{
    return [ConfigurationManager getColorValue:APP_CLUSTERMANAGER_100];
}

+ (UIColor*) getClusterManagerDefault
{
    return [ConfigurationManager getColorValue:APP_CLUSTERMANAGER_Default];
}

+ (NSString*) getStringColorForHtlmlText
{
    return [ConfigurationManager getColorValueForHtml:APP_HTML_TEXT_COLOR];
}

+ (NSString*) getStringColorForHtlmlLinkText
{
    return [ConfigurationManager getColorValueForHtml:APP_HTML_LINK_TEXT_COLOR];
}

+ (NSString*) getStringColorForFAQText
{
    return [ConfigurationManager getColorValueForHtml:APP_FAQ_TEXT_COLOR];
}

+ (UIColor*) getMenuBgColor
{
    UIColor *color = [ConfigurationManager getColorValue:APP_MENU_BG_COLOR];
    
    if (![color isEqual:[UIColor clearColor]])
    return color;
    else
    return [CommonsUtils getPrimaryColor];
}

+ (UIColor*) getMenuItemTextColor
{
    UIColor *color = [ConfigurationManager getColorValue:APP_MENU_ITEM_TEXT_COLOR];
    
    if (![color isEqual:[UIColor clearColor]])
    return color;
    else
    return [CommonsUtils getComplementaryColor];
}

+ (UIColor*) getMenuSeparatorColor
{
    UIColor *color = [ConfigurationManager getColorValue:APP_MENU_SEPARATOR_COLOR];
    
    if (![color isEqual:[UIColor clearColor]])
    return color;
    else
    return [CommonsUtils getComplementaryColor];
}

+ (UIColor*) getStoreDetailsNameColor
{
    UIColor *color = [ConfigurationManager getColorValue:APP_STORE_DETAILS_NAME_COLOR];
    
    if (![color isEqual:[UIColor clearColor]])
    return color;
    else
    return [CommonsUtils getSecondaryColor];
}

+ (UIColor*) getStoreDetailsAddressColor
{
    UIColor *color = [ConfigurationManager getColorValue:APP_STORE_DETAILS_ADDRESS_COLOR];
    
    if (![color isEqual:[UIColor clearColor]])
    return color;
    else
    return [CommonsUtils getComplementaryColor];
}

+ (UIColor*) getStoreDetailsPhoneMailSiteLabelColor
{
    UIColor *color = [ConfigurationManager getColorValue:APP_STORE_DETAILS_PHONE_MAIL_SITE_LABEL_COLOR];
    
    if (![color isEqual:[UIColor clearColor]])
    return color;
    else
    return [CommonsUtils getComplementaryColor];
}

+ (UIColor*) getStoreDetailsPhoneMailSiteValueColor
{
    UIColor *color = [ConfigurationManager getColorValue:APP_STORE_DETAILS_PHONE_MAIL_SITE_VALUE_COLOR];
    
    if (![color isEqual:[UIColor clearColor]])
    return color;
    else
    return [CommonsUtils getHighlightColor];
}

+ (UIColor*) getStoreDetailsGpsButtonTextColor
{
    UIColor *color = [ConfigurationManager getColorValue:APP_STORE_DETAILS_GPS_BUTTON_TEXT_COLOR];
    
    if (![color isEqual:[UIColor clearColor]])
    return color;
    else
    return [CommonsUtils getPrimaryColor];
}

+ (UIColor*) getAppFAQDetailColor {
    UIColor *color = [ConfigurationManager getColorValue:APP_FAQ_DETAIL_COLOR];
    
    if (![color isEqual:[UIColor clearColor]])
    return color;
    else
    return [CommonsUtils getPrimaryColor];
}

#pragma mark - DBs Methods
+ (NSString*) getDBPath {
    
    NSString *_documentsDir = nil;
    NSString *_databasesDir = nil;
    NSString *_databasePath = @"";
    // Get the path to the documents directory and append the databaseName
    //NSLibraryDirectory
    NSArray *_documentPaths = NSSearchPathForDirectoriesInDomains(MGX_GET_ARG_DATA_IN_FOLDER, NSUserDomainMask, YES);
    _documentsDir = [_documentPaths objectAtIndex:0];
    _databasesDir = [_documentsDir stringByAppendingPathComponent:MXPERIENCE_DATABASE_FOLDER_NAME];
    _databasePath = [_databasesDir stringByAppendingPathComponent:[self getLocalDatabaseName]];
    
    // Check if the SQL database has already been saved to the users phone, if not then copy it over
    BOOL _isSucceeded;
    
    // Create a FileManager object, we will use this to check the status
    // of the database and to copy it over if required
    NSFileManager *_fileManager = [NSFileManager defaultManager];
    
    // Check if the database has already been created in the users filesystem
    _isSucceeded = [_fileManager fileExistsAtPath:_databasePath];
    
    // If the database already exists then return without doing anything
    if(_isSucceeded) {
        return _databasePath;
    }
    
    return _databasePath;
}

+ (BOOL) createDatabaseFolder
{
    NSFileManager *_fileManager = [[NSFileManager alloc] init];
    NSError *_error = nil;
    [_fileManager createDirectoryAtPath:[[CommonsUtils getDBPath] stringByDeletingLastPathComponent] withIntermediateDirectories:FALSE attributes:Nil error:&_error];
    if(_error != nil)
    return FALSE;
    else
    return TRUE;
    
}

+ (BOOL) createClientsFolder
{
    NSFileManager *_fileManager = [[NSFileManager alloc] init];
    NSError *_error = nil;
    
    NSString *_documentsDir = nil;
    NSString *_clientPath = @"";
    // Get the path to the documents directory and append the databaseName
    NSArray *_documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    _documentsDir = [_documentPaths objectAtIndex:0];
    _clientPath = [_documentsDir stringByAppendingPathComponent:MEXPERIENCE_CLIENTS_FOLDER_NAME];
    
    [_fileManager createDirectoryAtPath:_clientPath withIntermediateDirectories:FALSE attributes:Nil error:&_error];
    if(_error != nil)
    return FALSE;
    else
    return TRUE;
    
}

+ (NSString*) getInternalDBPath {
    
    NSString *_documentsDir = nil;
    NSString *_databasePath = @"";
    // Get the path to the documents directory and append the databaseName
    NSArray *_documentPaths = NSSearchPathForDirectoriesInDomains(MGX_GET_ARG_DATA_IN_FOLDER, NSUserDomainMask, YES);
    _documentsDir = [_documentPaths objectAtIndex:0];
    _databasePath = [_documentsDir stringByAppendingPathComponent:MXPERIENCE_DATABASE_FOLDER_NAME];
    _databasePath = [_databasePath stringByAppendingPathComponent:INTERNAL_DB_NAME];
    
    // Check if the SQL database has already been saved to the users phone, if not then copy it over
    BOOL _isSucceeded;
    
    // Create a FileManager object, we will use this to check the status
    // of the database and to copy it over if required
    NSFileManager *_fileManager = [NSFileManager defaultManager];
    
    // Check if the database has already been created in the users filesystem
    _isSucceeded = [_fileManager fileExistsAtPath:_databasePath];
    
    // If the database already exists
    if(_isSucceeded)
    return _databasePath;
    
    
    // If not then proceed to copy the database from the application to the users filesystem
    
    // Get the path to the database in the application packaged
    NSString *databasePathFromApp = [[NSBundle mainBundle] pathForResource:INTERNAL_DB_NAME ofType:@""];
    
    NSError *_error = nil;
    //create a Application Support Directory
    [_fileManager createDirectoryAtPath:[_databasePath stringByDeletingLastPathComponent] withIntermediateDirectories:FALSE attributes:Nil error:&_error];
    if (_error != nil && _error.code == 516){
        NSLog(@"Folder exist, error: %@", _error);
    }
    
    _error = nil;
    // Copy the database from the package to the users filesystem
    [_fileManager copyItemAtPath:databasePathFromApp toPath:_databasePath error:&_error];
    
    if (_error != nil){
        return @"";
    }
    
    return _databasePath;
}

-(void) closeOlderLocalDB
{
    [_localDB close];
    _localDB = nil;
}

- (FMDatabase*) getInternalDB {
    if (_internalDB == nil)
    {
        _internalDB = [FMDatabase databaseWithPath:[CommonsUtils getInternalDBPath]];
    }
    return _internalDB;
}

- (FMDatabase*) getLocalDB {
    if (_localDB == nil)
    {
        _localDB = [FMDatabase databaseWithPath:[CommonsUtils getDBPath]];
    }
    return _localDB;
}

- (FMDatabaseQueue*) getLocalQueueDB {
    
    if (_localQueueDB == nil)
    {
        _localQueueDB = [FMDatabaseQueue databaseQueueWithPath:[CommonsUtils getDBPath]];
    }
    return _localQueueDB;
}

- (BOOL) isLocalDbExits
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(MGX_GET_ARG_DATA_IN_FOLDER, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:MXPERIENCE_DATABASE_FOLDER_NAME];
    dataPath = [dataPath stringByAppendingPathComponent: [NSString stringWithFormat:@"%@%@", [CommonsUtils getAppLanguage], LOCAL_DB_NAME]];
    NSDictionary* _attributes = [[NSFileManager defaultManager] attributesOfItemAtPath:dataPath error:nil];
    if([[NSFileManager defaultManager] fileExistsAtPath:dataPath] && [[_attributes valueForKey:NSFileSize] integerValue] > 10)
    return TRUE;
    else
    return FALSE;
}

+(NSString*) getLocalDatabaseName
{
    return [NSString stringWithFormat: @"%@%@", [self getAppLanguage], LOCAL_DB_NAME];
}

+ (void) saveModifiedData:(NSString*)aDbName modifiedValue:(NSString*)aModifiedValue
{
    NSString* lang = [CommonsUtils getAppLanguage];
    NSString *key = [NSString stringWithFormat:@"%@%@", lang, aDbName];
    [CommonsUtils saveValueAsPreference:aModifiedValue key:key];
}

+(BOOL) getIfUpdateNeeded:(NSString*)aDbName value:(NSString*)aValue
{
    NSString* lang = [CommonsUtils getAppLanguage];
    NSString *key = [NSString stringWithFormat:@"%@%@", lang, aDbName];
    
    NSString *modifiedValue = [CommonsUtils getPreferenceValueForKey:key];
    if(modifiedValue != nil && [modifiedValue isEqualToString:aValue])
    return FALSE;
    else
    return TRUE;
    
}
/*! @brief This method check the version of the internal db in user device. If the internal db version is lower than the INTERNAL_DB_CURRENT_VERSION,execute the corresponding scripts defined in DbUpdatesQueries.h for each old version.
 */
- (void) checkInternalDBAndUpdate
{
    [_internalDB open];
    int _userVersion = [_internalDB userVersion]; //user version of internaldb storage in device
    [_internalDB close];
    
    if (_userVersion < INTERNAL_DB_CURRENT_VERSION)
    {
        //for version 1
        NSNumber *_numberVersion1 = [[NSNumber alloc] initWithInt:1];
        NSArray *_queryDBUpdateListForVersion1 = [[NSArray alloc] initWithObjects:QUERY_UPDATE_DB_1, QUERY_UPDATE_DB_2, QUERY_UPDATE_DB_3, QUERY_UPDATE_DB_4,nil];
        
        //for version 2
        NSNumber *_numberVersion2 = [[NSNumber alloc] initWithInt:2];
        NSArray *_queryDBUpdateListForVersion2 = [[NSArray alloc] initWithObjects: QUERY_UPDATE_DB_5, QUERY_UPDATE_DB_6, nil];
        
        
        NSMutableDictionary *_queryDictionary = [[NSMutableDictionary alloc] init];
        [_queryDictionary setObject:_queryDBUpdateListForVersion1 forKey:_numberVersion1];
        [_queryDictionary setObject:_queryDBUpdateListForVersion2 forKey:_numberVersion2];
        
        for(int i = _userVersion; i < [[_queryDictionary allKeys] count]; i++)
        {
            NSNumber* _versionNumber = [[_queryDictionary allKeys] objectAtIndex:i];
            NSMutableArray *queryList = [_queryDictionary objectForKey:_versionNumber];
            
            for (NSString *query in queryList) {
                
                [self executeQueryToInternalDb:query numberOfVersion:_versionNumber.intValue];
                
            }
        }
        
    }
    else if (_userVersion == INTERNAL_DB_CURRENT_VERSION)
    {
        NSLog(@"User version of DB: %d, is updated", _userVersion);
    }
    else
    {
        NSLog(@"User version of DB: %d, is unknown", _userVersion);
    }
    
}

// Method to execute update db queries to internaldb and update the user db version

-(BOOL) executeQueryToInternalDb:(NSString*)aQuery numberOfVersion:(int)aVersionNumber
{
    [_internalDB open];
    BOOL _sucess = FALSE;
    
    if (![_internalDB open])
    {
        NSLog(@"Problems to open DB, please check");
        return _sucess;
    }
    
    FMDBQuickCheck(![_internalDB hasOpenResultSets])
    
    _sucess = [_internalDB executeUpdate:aQuery];
    
    if(_sucess)
    [_internalDB setUserVersion:aVersionNumber];
    
    [_internalDB close];
    
    return _sucess;
}

#pragma mark - Images Management Methods

/*!
 *  Creates an array of images paths on the given folder to load them later and use them in animation
 *
 *  @param aImageFolderName Folder that contains the images to make animation
 *
 *  @return The array with the paths to the images
 */
+ (NSMutableArray*) loadPicturesInFolder:(NSString*)aImageFolderName
{
    // NSString * resourcePath = [[NSBundle mainBundle] resourcePath];
    
    NSArray *resourcePath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *_documentsDir = [resourcePath objectAtIndex:0];
    _documentsDir = [_documentsDir stringByAppendingString:@"/"];
    
    NSString * imgFolderPath = [_documentsDir stringByAppendingPathComponent:aImageFolderName];
    NSError * error;
    NSMutableArray * directoryContents = [[NSMutableArray alloc] initWithArray:[[NSFileManager defaultManager] contentsOfDirectoryAtPath:imgFolderPath error:&error]];
    NSMutableArray * imagesPaths = nil;
    if (directoryContents.count > 0)
    {
        imagesPaths = [[NSMutableArray alloc] initWithCapacity:directoryContents.count];
        [directoryContents sortUsingSelector:@selector(compare:)];
        
        for (NSString* _imgName in directoryContents) {
            if([[[_imgName pathExtension] lowercaseString] isEqualToString:@"png"] || [[[_imgName pathExtension] lowercaseString] isEqualToString:@"jpg"] || [[[_imgName pathExtension] lowercaseString] isEqualToString:@"jpeg"])
            {
                NSString * imgPath = [imgFolderPath stringByAppendingPathComponent:_imgName];
                [imagesPaths addObject:imgPath];
            }
        }
    }
    
    return imagesPaths;
}

+ (void) deletePicture:(NSString*)aImagePath
{
    
    NSFileManager *_fileManager = [NSFileManager defaultManager];
    [_fileManager removeItemAtPath:aImagePath error:nil];
    
}

+ (UIImage*) getImageInLocalFolder:(NSString *) aUrlImage folderName:(NSString*)aFolderName
{
    // get image filename
    NSArray *_arrSplitUrlImg = [aUrlImage componentsSeparatedByString:@"/"];
    NSString *_fileName = (NSString*)[_arrSplitUrlImg objectAtIndex:[_arrSplitUrlImg count] -1 ];
    
    UIImage *_cachedImage = nil;
    
    //let's try to find image inside App Documents Directory
    NSArray *_paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *_documentsDirectory = [_paths objectAtIndex:0];
    NSString *_productsFolder = [_documentsDirectory stringByAppendingPathComponent: aFolderName];
    
    NSString *_imagePath =  [_productsFolder stringByAppendingPathComponent: _fileName];
    if ([[NSFileManager defaultManager] fileExistsAtPath:_imagePath])
    {
        _cachedImage = [UIImage imageWithContentsOfFile:_imagePath];
    }
    else
    _cachedImage = nil;
    return _cachedImage;
}

+ (void) saveImageInLocalFolder :(NSString *) aUrlImage imageFile:(UIImage*)aImage folderName:(NSString*)aFolderName
{
    [self createFolder:aFolderName];
    
    // get image filename
    NSArray *_arrSplitUrlImg = [aUrlImage componentsSeparatedByString:@"/"];
    NSString *_fileName = (NSString*)[_arrSplitUrlImg objectAtIndex:[_arrSplitUrlImg count] - 1];
    
    //let's try to find image inside App Documents Directory
    NSArray *_paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *_documentsDirectory = [_paths objectAtIndex:0];
    NSString *_productsFolder = [_documentsDirectory stringByAppendingPathComponent: aFolderName];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:[_productsFolder stringByAppendingPathComponent:_fileName]]) //file already exist
    return;
    
    NSString *extension = [_fileName pathExtension];
    if ([extension isEqualToString:@"png"])
    [UIImagePNGRepresentation(aImage) writeToFile: [_productsFolder stringByAppendingPathComponent:_fileName]
                                       atomically:YES];
    else if ([extension isEqualToString:@"jpg"])
    [UIImageJPEGRepresentation(aImage, 1.0) writeToFile: [_productsFolder stringByAppendingPathComponent:_fileName]
                                             atomically:YES];
}

+ (void) createFolder:(NSString*)aFolderName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:aFolderName];
    NSError *error;
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
    [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
}

+(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToSquare: (float) squareDimension maxScale: (float) aToScale
{
    float oldWidth = sourceImage.size.width;
    float oldHeight = sourceImage.size.height;
    float newWidth = 0;
    float newHeight  = 0;
    float scaleFactor = 0;
    float posX = 0;
    float posY = 0;
    
    if (oldWidth < oldHeight)
    {
        scaleFactor = squareDimension / oldWidth;
        
        newWidth = oldWidth * scaleFactor;
        newHeight = oldHeight * scaleFactor;
        
        posY = (newHeight - newWidth) / 2;
    }
    else
    {
        scaleFactor = squareDimension / oldHeight;
        
        newWidth = oldWidth * scaleFactor;
        newHeight = oldHeight * scaleFactor;
        
        posX = (newWidth - newHeight) / 2;
    }
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(squareDimension, squareDimension), NO, aToScale);
    [sourceImage drawInRect:CGRectMake(-posX, -posY, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+ (void) copyImgToImagesFolder: (UIImage*) aImgToCopy
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *imageFolderPath = [documentsDirectory stringByAppendingPathComponent:MXPERIENCE_IMAGE_FOLDER_NAME];
    
    NSFileManager *_fileManager = [NSFileManager defaultManager];
    if(![_fileManager fileExistsAtPath:imageFolderPath])
    {
        [_fileManager createDirectoryAtPath:imageFolderPath withIntermediateDirectories:FALSE attributes:Nil error:nil];
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm:ss.SSS"];
    
    NSString *imgFilePath = [imageFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"img_%@.png", [dateFormatter stringFromDate:[NSDate date]]]];
    
    BOOL _sucess = [UIImagePNGRepresentation(aImgToCopy) writeToFile: imgFilePath
                                                          atomically:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_MOVE_IMAGE_TO_ALBUM_FINISH object:nil];
    if(!_sucess)
    {
        // UnitySendMessage("UI", "NativeMessage", "picture failed"); TODO:add sdk call
        NSLog(@"error: cannot save photo in images folder");
    }
}

+ (void) saveImageToAlbum: (UIImage*) aImage
{
    NSLog(@"saveImageToAlbum called");
    
    if([PHPhotoLibrary authorizationStatus]==  PHAuthorizationStatusAuthorized )
    [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^
     {
         //Checks for App Photo Album and creates it if it doesn't exist
         PHFetchOptions *fetchOptions = [PHFetchOptions new];
         fetchOptions.predicate = [NSPredicate predicateWithFormat:@"title == %@", MXPERIENCE_ALBUM_NAME];
         PHFetchResult *fetchResult = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum subtype:PHAssetCollectionSubtypeAlbumRegular options:fetchOptions];
         
         if (fetchResult.count == 0)
         {
             //Create Album
             PHAssetCollectionChangeRequest *albumRequest = [PHAssetCollectionChangeRequest creationRequestForAssetCollectionWithTitle:MXPERIENCE_ALBUM_NAME];
             PHAssetChangeRequest *createImageRequest = [PHAssetChangeRequest creationRequestForAssetFromImage:aImage];
             [albumRequest addAssets:@[createImageRequest.placeholderForCreatedAsset]];
         }
         else
         {
             PHAssetCollection* exisitingCollection = fetchResult.firstObject;
             PHAssetCollectionChangeRequest *collectionRequest = [PHAssetCollectionChangeRequest changeRequestForAssetCollection:exisitingCollection];
             PHAssetChangeRequest* createAssetRequest = [PHAssetChangeRequest creationRequestForAssetFromImage:aImage];
             [collectionRequest addAssets:@[createAssetRequest.placeholderForCreatedAsset]];
         }
         
     }
     
                                      completionHandler:^(BOOL success, NSError *error)
     {
         if (!success) {
             
             [CommonsUtils copyImgToImagesFolder:aImage];
             
         }else{
             
             //UnitySendMessage("UI", "NativeMessage", "picture taken"); TODO:add sdk call
         }
     }];
    else
    [CommonsUtils copyImgToImagesFolder:aImage];
}


+ (void) requestImageFromServer:(NSString *) aImagePath imageFolder:(NSURL*)aImageFolder imageIndex:(int)aImageIndex completionHandler:(void (^)(NSURLResponse *response, NSURL *filePath, NSError *error))completionHandler
{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL = [NSURL URLWithString:aImagePath];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    NSURLSessionDownloadTask *_downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        
        return [aImageFolder URLByAppendingPathComponent:[[request URL] lastPathComponent]];
        
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        
        completionHandler(response,filePath, error);
        
    }];
    [_downloadTask resume];
}


#pragma mark - Google Analytics Report Methods


#pragma mark - ActivityViewer Methods

+ (void)hideActivityViewer:(UIView*)aActivityView {
    
    [[[aActivityView subviews] objectAtIndex:0] stopAnimating];
    [aActivityView removeFromSuperview];
    aActivityView = nil;
}

+ (UIView *) showActivityViewer:(NSString*)msg showInView:(UIView*)aParentView colorForView:(UIColor*)aColorView{
    
    UIView *_activityView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, aParentView.bounds.size.height)];
    _activityView.backgroundColor = aColorView;
    
    float _offset = 0;
    if (IS_IPHONE_5)
    {
        _offset = 44;
    }
    
    UIActivityIndicatorView *activityWheel = [[UIActivityIndicatorView alloc] initWithFrame: CGRectMake(_activityView.bounds.size.width / 2 - 12, _activityView.bounds.size.height / 2 - 12 - _offset, 50, 50)];
    activityWheel.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
    activityWheel.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin |
                                      UIViewAutoresizingFlexibleRightMargin |
                                      UIViewAutoresizingFlexibleTopMargin |
                                      UIViewAutoresizingFlexibleBottomMargin);
    activityWheel.center = _activityView.center;
    /*if([APP_TARGET isEqualToString:APP_TARGET_PROAPRO]){
     activityWheel.color = [CommonsUtils getSecondaryColor];
     }*/
    activityWheel.color = Theme.sharedInstance.activityIndicatorBackground.color;
    [_activityView addSubview:activityWheel];
    
    [_activityView constrainWithinSuperviewBounds];
    [_activityView constrainPosition:CGPointMake(0, 0)];
    
    UILabel    *messageLabel = [[UILabel alloc]initWithFrame:CGRectMake(30, 250, 260, 40)];
    messageLabel.text = msg;
    //messageLabel.font = [UIFont boldSystemFontOfSize:15];
    messageLabel.font = [Theme.sharedInstance fontBoldOfSize:15];
    messageLabel.backgroundColor = [UIColor clearColor];
    messageLabel.textColor = [UIColor whiteColor];
    messageLabel.textAlignment = NSTextAlignmentCenter;
    messageLabel.alpha = 0.5;
    messageLabel.adjustsFontSizeToFitWidth = YES;
    messageLabel.minimumScaleFactor = 0.5;
    [_activityView addSubview:messageLabel];
    [aParentView addSubview: _activityView];
    
    [[[_activityView subviews] objectAtIndex:0] startAnimating];
    return _activityView;
}

+ (UIView *) showActivityViewerWithCustomLoadingImage:(UIImage*)image message:(NSString*)msg showInView:(UIView*)aParentView colorForView:(UIColor*)aColorView{
    
    UIView *_activityView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, aParentView.bounds.size.height)];
    _activityView.backgroundColor = aColorView;
    
    float _offset = 0;
    if (IS_IPHONE_5)
    {
        _offset = 44;
    }
    
    UIImageView *activityWheel = [[UIImageView alloc] initWithFrame: CGRectMake(_activityView.bounds.size.width / 2 - image.size.width / 2.0, _activityView.bounds.size.height / 2 - image.size.height / 2, image.size.width, image.size.height)];
    [activityWheel setImage:image];
    activityWheel.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin |
                                      UIViewAutoresizingFlexibleRightMargin |
                                      UIViewAutoresizingFlexibleTopMargin |
                                      UIViewAutoresizingFlexibleBottomMargin);
    [_activityView addSubview:activityWheel];
    
    CABasicAnimation* anim = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    anim.fromValue = [NSNumber numberWithFloat:0];
    anim.toValue = [NSNumber numberWithFloat:2 * M_PI];
    anim.duration = .5;
    anim.repeatCount = HUGE_VALF;
    [activityWheel.layer removeAllAnimations];
    [activityWheel.layer addAnimation:anim forKey:@"rotating"];
    
    [_activityView constrainWithinSuperviewBounds];
    [_activityView constrainPosition:CGPointMake(0, 0)];
    
    UILabel    *messageLabel = [[UILabel alloc]initWithFrame:CGRectMake(30, 250, 260, 40)];
    messageLabel.text = msg;
    //messageLabel.font = [UIFont boldSystemFontOfSize:15];
    messageLabel.font = [Theme.sharedInstance fontBoldOfSize:15];
    messageLabel.backgroundColor = [UIColor clearColor];
    messageLabel.textColor = [UIColor whiteColor];
    messageLabel.textAlignment = NSTextAlignmentCenter;
    messageLabel.alpha = 0.5;
    messageLabel.adjustsFontSizeToFitWidth = YES;
    messageLabel.minimumScaleFactor = 0.5;
    [_activityView addSubview:messageLabel];
    [aParentView addSubview: _activityView];
    
    [[[_activityView subviews] objectAtIndex:0] startAnimating];
    return _activityView;
}



#pragma mark - App info Methods

+(NSString*) getRequestedVersion:(NSString*) aKeyForRequest
{
    NSString* lang = [CommonsUtils getAppLanguage];
    NSString *key = [NSString stringWithFormat:@"%@%@", lang, aKeyForRequest];
    return [CommonsUtils getPreferenceValueForKey:key];
}


#pragma mark - Apple store methods

+ (void) goToAppleStore {
    
    NSString *reviewURL = [ARGenieManager getAppleStoreUrl];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:reviewURL]];
}

#pragma mark - Access user preferences methods

+ (void) saveValueAsPreference: (NSString*)aValue key:(NSString*)aKey
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:aValue forKey:aKey];
    [defaults synchronize];
}

+ (NSString*) getPreferenceValueForKey: (NSString*)aKey
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults stringForKey:aKey];
}

#pragma mark - Authentication mode methods

+ (BOOL) isAppInAuthenticationMode
{
    if([CommonsUtils getPreferenceValueForKey:AUTH_USER_TOKEN] != nil && ![[CommonsUtils getPreferenceValueForKey:AUTH_USER_TOKEN] isEqualToString:@""] && [CommonsUtils getPreferenceValueForKey:AUTH_USER_SESSION_ID] != nil && ![[CommonsUtils getPreferenceValueForKey:AUTH_USER_SESSION_ID] isEqualToString:@""] && [CommonsUtils getPreferenceValueForKey:AUTH_USER_SESSION_NAME] != nil && ![[CommonsUtils getPreferenceValueForKey:AUTH_USER_SESSION_NAME] isEqualToString:@""] )
    return TRUE;
    else
    return FALSE;
}

+ (void) cleanSessionData
{
    [CommonsUtils saveValueAsPreference:@"" key:AUTH_USER_TOKEN];
    [CommonsUtils saveValueAsPreference:@"" key:AUTH_USER_SESSION_ID];
    [CommonsUtils saveValueAsPreference:@"" key:AUTH_USER_SESSION_NAME];
}

@end
