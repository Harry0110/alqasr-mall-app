//
//  PopupUtil.h
//  magicxperience
//
//  Created by erka on 03/10/2018.
//  Copyright © 2018 Artech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol PopupUtilDelegate
@optional
- (void) onTap;
@end

@interface PopupUtil : NSObject

@property (weak) id<PopupUtilDelegate> delegate;

+ (PopupUtil *) sharedInstance;
-(void) addBlurEffectViewToView:(UIView*) aView;
-(void) insertBlurEffectViewToView:(UIView*) aView belowSubView:(UIView*) aSubview;
-(void) removeBlurEffectViewFromView:(UIView*) aView;

@end
