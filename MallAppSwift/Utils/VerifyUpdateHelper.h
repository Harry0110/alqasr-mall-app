//
//  VerifyUpdateHelper.h
//  Unity-iPhone
//
//  Created by Magela Santana
//
//
#import <AVFoundation/AVFoundation.h>
#import "DBServiceClient.h"

@interface VerifyUpdateHelper : NSObject <DBServiceClientDelegate>

@property (nonatomic, strong) DBServiceClient * dbServiceClientInstance;

+ (VerifyUpdateHelper *) getHelper;
-(void) verifyAppUpdateState;

@end