//
//  VerifyUpdateHelper.m
//  Unity-iPhone
//
//  Created by Magela Santana
//
//


#import "VerifyUpdateHelper.h"
#import "CommonsUtils.h"

@implementation VerifyUpdateHelper

static VerifyUpdateHelper *helperInstance = nil;


+ (VerifyUpdateHelper *) getHelper {
    
    @synchronized(self)
    {
        if (helperInstance == nil)
        {
            helperInstance = [[VerifyUpdateHelper alloc] init];
        }
    }
    return helperInstance;
}

-(void) verifyAppUpdateState
{
   _dbServiceClientInstance = [DBServiceClient sharedDBServiceClient];
   _dbServiceClientInstance.delegate = self;
   [_dbServiceClientInstance getIfAppVersionIsOutOfDate];
}

-(void) dbServiceClient:(DBServiceClient *)client didRequestIfAppIsOutOfDateSucess:(WSNeedUpdateResult*)response withError:(NSError *)error
{
    if (response == nil || !response.updateAvailable)
    {
      
        if(_dbServiceClientInstance == nil)
        {
            _dbServiceClientInstance = [DBServiceClient sharedDBServiceClient];
            _dbServiceClientInstance.delegate = self;
        }
        [_dbServiceClientInstance getMXperienceDBUrl];
    }
    else if(response.updateAvailable)
    {
        //UnitySendMessage("UI", "NativeMessage", "Need App Update"); TODO:add sdk call
    }
}

-(void)dbServiceClient:(DBServiceClient *)client didDownloadDB:(id)database withError:(NSError *)error
{
//    if(error != nil) TODO:add sdk call
//         UnitySendMessage("UI", "NativeMessage", "App updated");
//    else if ([CommonsUtils getCommonUtil].needDownloadSQlite || [CommonsUtils getCommonUtil].needDownloadVuforiaData)
//        UnitySendMessage("UI", "NativeMessage", "Need Database update");
//    else
//        UnitySendMessage("UI", "NativeMessage", "App updated");

}

@end