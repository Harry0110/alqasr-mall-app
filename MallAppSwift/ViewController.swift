//
//  ViewController.swift
//  SampleSDKSwift
//
//  Created by Arianne Peiso on 12/06/2019.
//  Copyright © 2019 Arianne Peiso. All rights reserved.
//

import UIKit
import ARGenieSDK

class ViewController: UIViewController, ARGenieManagerDownloadCallback {
    
    func didARGenieManagerDownloadFinish(_ error: Error!) {
        
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func showAugmentedReality(_ sender: UIButton) {
        //DispatchQueue.main.async{
            ARGenieManager.shared()?.loadAugmentedRealityData(self)
        //}
    }
    
    
}

