//
//  CustomMenuButton.m
//  Created by Pedro Llanes on 8/10/14.
//

#import "CustomMenuButton.h"

@interface CustomMenuButton ()

@property (nonatomic, strong) id lastButtonTarget;

@end

@implementation CustomMenuButton

@synthesize btnSelected = _btnSelected;
@synthesize normalImage = _normalImage;
@synthesize selectedImage = _selectedImage;
@synthesize selectedIndex = _selectedIndex;

@synthesize lastButtonTarget;

- (id)init
{
    if (!(self = [super init]))
        return nil;
    
    _selectedIndex = -1;
    
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _selectedIndex = -1;
    }
    return self;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void) setNormalImage:(NSString *)normalImage
{
    if (_normalImage != nil)
    {
        _normalImage = nil;
    }
    
    if (normalImage != nil)
    {
        _normalImage = [NSString stringWithString:normalImage];
        [self setImage:[UIImage imageNamed:_normalImage] forState:UIControlStateNormal];
    }
}

- (void) setSelectedImage:(NSString *)selectedImage
{
    if (_selectedImage != nil)
    {
        _selectedImage = nil;
    }
    
    if (selectedImage != nil)
    {
        _selectedImage = [NSString stringWithString:selectedImage];
        [self setImage:[UIImage imageNamed:_selectedImage] forState:UIControlStateHighlighted];
    }
}

- (void) setSelected:(BOOL)selected
{
    _btnSelected = selected;
    
    UIImage *aImage = selected ?[UIImage imageNamed:_selectedImage]:[UIImage imageNamed:_normalImage];
    [self setImage:aImage forState:UIControlStateNormal];
}

- (void) setEnabled:(BOOL)enabled
{
    [super setEnabled:enabled];
}

#pragma mark - Public

- (void) addTarget:(id)target action:(SEL)action
{
    if (self.lastButtonTarget != nil)
    {
        [self removeTarget:self.lastButtonTarget action:NULL forControlEvents:UIControlEventTouchUpInside];
    }
    
    [self addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    self.lastButtonTarget = target;
}

@end
