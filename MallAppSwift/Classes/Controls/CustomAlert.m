/*
 
 Erica Sadun, http://ericasadun.com
 iOS 7 Cookbook
 Use at your own risk. Do no harm.
 
 */

#import "CustomAlert.h"
#import "Utility.h"
#import "CommonsUtils.h"
#import "Theme.h"

typedef void (^CustomAnimationBlock)(void);
typedef void (^CustomCompletionAnimationBlock)(BOOL finished);

@implementation CustomAlert
{
    
}

#pragma mark - Instance Creation and Initialization

- (void)internalCustomAlertInitializer
{
    // Add a content view for auto layout
    //[_contentView setBackgroundColor:[CommonsUtils getPrimaryColor]];
    //_contentView.layer.cornerRadius = 3;
    //_contentView.layer.borderColor = [[CommonsUtils getSecondaryColor] CGColor];
    /*if([APP_TARGET isEqualToString:APP_TARGET_PROAPRO]){
     _contentView.layer.borderColor = [[CommonsUtils getPrimaryColor] CGColor];
     }*/
    [_contentView setBackgroundColor:Theme.sharedInstance.appBodyBackground.color];
    _contentView.layer.borderWidth = 2;
    _contentView.layer.borderColor = Theme.sharedInstance.popupBorderBackground.color.CGColor;
    
    
    // Create label
    //[_label setTextColor:[CommonsUtils getComplementaryColor]];
    //[_label setFont:[UIFont systemFontOfSize:12.0]];
    _label.numberOfLines = 0;
    _label.textAlignment = NSTextAlignmentLeft;
    [_label setTextColor:Theme.sharedInstance.popupText.color];
    [_label setFont:Theme.sharedInstance.popupText.font];
    
    // Create button
    //_button.layer.cornerRadius = 3;
    /*if([APP_TARGET isEqualToString: APP_TARGET_PROAPRO]){
        [_button setTitleColor:[CommonsUtils getComplementaryColor] forState:UIControlStateNormal];
    } else {
        [_button setTitleColor:[CommonsUtils getPrimaryColor] forState:UIControlStateNormal];
    }*/
    //[_button setBackgroundColor:[CommonsUtils getSecondaryColor]];
    [_button setTitleColor:Theme.sharedInstance.popupButtonOKText.color forState:UIControlStateNormal];
    _button.titleLabel.font = Theme.sharedInstance.popupButtonOKText.font;
    [_button setBackgroundColor:Theme.sharedInstance.popupButtonOKBackground.color];
    
    [self layoutIfNeeded];
}

+ (instancetype) initCustomAlertView
{
    CustomAlert* _customAlert = [[[NSBundle mainBundle] loadNibNamed:@"CustomAlert" owner:nil options:nil] objectAtIndex:0];
    [_customAlert internalCustomAlertInitializer];
    return _customAlert;
}

+ (instancetype) initCustomInfoMessage
{
    CustomAlert* _customAlert = [[[NSBundle mainBundle] loadNibNamed:@"CustomInfoMessage" owner:nil options:nil] objectAtIndex:0];
    //[_customAlert.label setTextColor:[CommonsUtils getPrimaryColor]];
    //[_customAlert.contentView setBackgroundColor:[CommonsUtils getSecondaryColor]];
    
    [_customAlert.label setTextColor:Theme.sharedInstance.popupText.color];
    [_customAlert.label setFont:Theme.sharedInstance.popupText.font];
    [_customAlert.contentView setBackgroundColor:Theme.sharedInstance.appBodyBackground.color];
    
    return _customAlert;
}



#pragma mark - Presentation and Dismiss

- (void)centerInSuperview
{
    if (!self.superview)
    {
        return;
    }
    
    NSArray *constraintArray = [self.superview.constraints copy];
    for (NSLayoutConstraint *constraint in constraintArray)
    {
        if ((constraint.firstItem == self) || (constraint.secondItem == self))
            [self.superview removeConstraint:constraint];
    }
    [self.superview addConstraints:CONSTRAINTS_CENTERING(self)];
}

- (void)dismiss
{
    [self removeFromSuperview];
}

#pragma mark - actions

- (void) redirectToSettingView
{
    
}



@end
