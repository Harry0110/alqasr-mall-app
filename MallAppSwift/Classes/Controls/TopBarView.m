//
//  TopBarView.m
//  Ilyt
//
//  Created by Pedro Llanes on 8/10/14.
//  Copyright (c) 2013 DigitalHitec. All rights reserved.
//

#import "TopBarView.h"
#import "CommonsUtils.h"
#import "NotificationsName.h"
#import "Theme.h"

@interface TopBarView ()

@property (nonatomic, assign) SEL defaultLeftButtonSelector;            //Action method for left button
@property (nonatomic, assign) SEL defaultRightButtonSelector;           //Action method for right button

@end

@implementation TopBarView

@synthesize rightBtn = _rightBtn;
@synthesize leftBtn = _leftBtn;
@synthesize topViewTitle=_topViewTitle;

@synthesize targetController = _targetController;
@synthesize defaultLeftButtonSelector;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        //UI initialization
        //Set open/close menu action by default to left button
        self.defaultLeftButtonSelector = @selector(revealToggle:);
        self.defaultRightButtonSelector = @selector(openScannerView:);
    }
    
    return self;
}

/*!
 *  Method to initialize top bar instance with default values
 */
- (void) initializeTopBar
{
    self.defaultLeftButtonSelector = @selector(revealToggle:);
    self.defaultRightButtonSelector = @selector(openScannerView:);
    [self.bgView setHidden:YES];
    
    /*Theme Management Old
    self.backgroundColor = [CommonsUtils getTopBarBgColor];
    _underlineView.backgroundColor = [CommonsUtils getUnderlineTopBarColor];
    [self.topViewTitle setTextColor:[CommonsUtils getTopBarTextColor]];
    if([APP_TARGET isEqualToString:APP_TARGET_PROAPRO]){
        [_leftBtn setTintColor:[CommonsUtils getPrimaryColor]];
    }*/
    
    //Theme Management New Start
    self.backgroundColor = Theme.sharedInstance.toolbarBackground.color;
    _underlineView.backgroundColor = Theme.sharedInstance.toolbarUnderline.color;
    [self.topViewTitle setTextColor:Theme.sharedInstance.toolbarText.color];
    [self.topViewTitle setFont:Theme.sharedInstance.toolbarText.font];
    [_leftBtn setTintColor:Theme.sharedInstance.toolbarButtonBackground.color];
    //Theme Management New END
    
}

#pragma mark - Getters and Setters
/*!
 *  Method to set the target controller for left button
 *
 *  @param targetController View controller that implements the actions of left button (hide/show menu)
 */
- (void)setTargetController:(UIViewController *)targetController
{
    _targetController = targetController;
    
   // [self.leftBtn addTarget:targetController action:self.defaultLeftButtonSelector];
}

- (IBAction)openMenuFromTopView:(id)sender
{
    [[NSNotificationCenter defaultCenter]
     postNotificationName:NOTIFICATION_OPEN_HIDE_MENU
     object:self
     userInfo:nil];
}

- (IBAction)openScannerView:(id)sender
{
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:NOTIFICATION_OPEN_SCANNER
     object:self
     userInfo:nil];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:NOTIFICATION_OPEN_HIDE_MENU
     object:self
     userInfo:nil];
}

@end
