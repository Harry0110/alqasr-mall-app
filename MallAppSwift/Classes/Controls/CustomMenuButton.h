//
//  CustomMenuButton.h
//  Created by Pedro Llanes on 8/10/14.
//

#import <UIKit/UIKit.h>

@interface CustomMenuButton : UIButton

@property (nonatomic) BOOL btnSelected;                    //Selected status
@property (nonatomic, strong) NSString *normalImage;    //Image for normal status
@property (nonatomic, strong) NSString *selectedImage;  //Image for selected status
@property (nonatomic) NSInteger selectedIndex;

- (void) addTarget:(id)target action:(SEL)action;       //Action to do when the user taps the button

@end
