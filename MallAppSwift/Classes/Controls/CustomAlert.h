/*
 
 Erica Sadun, http://ericasadun.com
 iOS 7 Cookbook
 Use at your own risk. Do no harm.
 
 */

#import <UIKit/UIKit.h>

#define KCUSTOM_MIN_WITDH_TO_FIT 250

@interface CustomAlert : UIView

@property (nonatomic, weak) IBOutlet UILabel *label;
@property (nonatomic, weak) IBOutlet UIButton *button;
@property (nonatomic, weak) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nslcPopupWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nslcPopupHeigth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nslcLabelHeigth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nslcInfoBtnWidth;

- (void) dismiss;
+ (instancetype) initCustomAlertView;
+ (instancetype) initCustomInfoMessage;

@end
