//Custom top bar to include in every screen in the app
//  TopBarView.h
//  Ilyt
//
//  Created by Pedro Llanes on 8/10/14.
//  Copyright (c) 2013 DigitalHitec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomMenuButton.h"

@interface TopBarView : UIView

@property (weak, nonatomic) IBOutlet CustomMenuButton *rightBtn;  //Button for the right corner
@property (weak, nonatomic) IBOutlet CustomMenuButton *leftBtn;   //Button for the left corner
@property (nonatomic, strong) UIViewController *targetController;   //The view controller that contains the top bar instance
@property (weak, nonatomic) IBOutlet UILabel *topViewTitle;       //Header text label
@property (nonatomic, weak) IBOutlet UIView *bgView;
@property (nonatomic, weak) IBOutlet UIView *underlineView;

- (void) initializeTopBar;
- (IBAction)openMenuFromTopView:(id)sender;
- (IBAction)openScannerView:(id)sender;

@end
