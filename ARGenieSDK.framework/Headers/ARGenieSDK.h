//
//  ARGenieSDK.h
//  ARGenieSDK
//
//  Created by Magela Santana on 11/8/16.
//  Copyright © 2016 Magela Santana. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for ARGenieSDK.
FOUNDATION_EXPORT double ARGenieSDKVersionNumber;

//! Project version string for ARGenieSDK.
FOUNDATION_EXPORT const unsigned char ARGenieSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ARGenieSDK/PublicHeader.h>
