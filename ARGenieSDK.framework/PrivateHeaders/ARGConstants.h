//
//  ARGConstants.h
//  ARGenieSDK
//
//  Created by ARTECH.
//  Copyright © 2018 ARTECH. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark - Notifications

//fire this notification when ARGenie detect new marquer
FOUNDATION_EXPORT NSString *const ARG_NOTIFICATION_TRACKER_FOUND;

//KEY TO GET TRACKER FOUND OBJECT IN USERINFO
FOUNDATION_EXPORT NSString *const ARG_KEY_TRACKERFOUND;

//fire this notification when ARGenie download new data and applications its running
FOUNDATION_EXPORT NSString *const ARG_NOTIFICATION_TO_DELIVERY_NEW_APP_DATA_UPDATE;

//fire this notification when animation start (the client app should hide top bar)
FOUNDATION_EXPORT NSString *const ARG_NOTIFICATION_ANIMATION_START;

//fire this notification when animation finish (the client app should show top bar if the bar was hide before)
FOUNDATION_EXPORT NSString *const ARG_NOTIFICATION_ANIMATION_FINISH;

FOUNDATION_EXPORT NSString *const ARG_NOTIFICATION_GALLERY_INDICATOR_SELECTED;

//fire this notification when animation event is received
FOUNDATION_EXPORT NSString *const ARG_NOTIFICATION_VIRTUAL_BUTTON_TOUCHED;

//fire this notification when stand alone animation ended
FOUNDATION_EXPORT NSString *const ARG_NOTIFICATION_STANDALONE_ANIMATION_ENDED;

//fire this notification when augmented reality view will show
FOUNDATION_EXPORT NSString *const ARG_NOTIFICATION_AUGMENTED_REALITY_WILL_SHOW;

//fire this notification when augmented reality view will hide
FOUNDATION_EXPORT NSString *const ARG_NOTIFICATION_AUGMENTED_REALITY_WILL_HIDE;

//fire this notification when ARG_USE_DEFAULT_GALLERY_FEATURE in plist has value FALSE et user select close ARGalleryView
FOUNDATION_EXPORT NSString *const ARG_NOTIFICATION_SELECT_CLOSE_GALLERY_VIEW;

//fire this notification when gallery detail view will show to user
FOUNDATION_EXPORT NSString *const ARG_NOTIFICATION_GALLERY_DETAIL_WILL_SHOW;

//fire this notification when gallery detail view will hide
FOUNDATION_EXPORT NSString *const ARG_NOTIFICATION_GALLERY_DETAIL_WILL_HIDE;

#pragma mark - Contants

//Use video constants to set video quality options
FOUNDATION_EXPORT NSString *const ARG_VIDEO_HIGH_DEFINITION_MODE;
FOUNDATION_EXPORT NSString *const ARG_VIDEO_MEDIUM_DEFINITION_MODE;
FOUNDATION_EXPORT NSString *const ARG_VIDEO_LOW_DEFINITION_MODE;

//SplashView TAG values and RESOURCES names to customize view. Use this tag if you provide a new xib for splasw view.

//resources SPLASH
FOUNDATION_EXPORT NSString *const ARG_SPLASH_XIB_NAME;
FOUNDATION_EXPORT NSString *const ARG_SPLASH_IMAGE_LOADING_NAME;
FOUNDATION_EXPORT NSString *const ARG_SPLASH_IMAGE_GENIUS_NAME;

//tags SPLASH
FOUNDATION_EXPORT int const ARGTAG_IMAGE_LOADING;
FOUNDATION_EXPORT int const ARGTAG_PERCENT_TEXT_LOADING;
FOUNDATION_EXPORT int const ARGTAG_DETAIL_TEXT_LOADING;
FOUNDATION_EXPORT int const ARGTAG_SKIP_BUTTON_TEXT;
FOUNDATION_EXPORT int const ARGTAG_IMAGE_GENIE;
FOUNDATION_EXPORT int const ARGTAG_SPLASH_VIEW;
FOUNDATION_EXPORT int const ARGTAG_CONTAINER_VIEW;

//tags SPLASH TIPS UI COMPONENTS
FOUNDATION_EXPORT int const ARGTAG_TIPS_CONTAINER_VIEW;
FOUNDATION_EXPORT int const ARGTAG_TIPS_IMG;
FOUNDATION_EXPORT int const ARGTAG_TIPS_LBTITLE;
FOUNDATION_EXPORT int const ARGTAG_TIPS_WEBVIEW_DESCRIPTION;
FOUNDATION_EXPORT int const ARGTAG_TIPS_IMG_LOADING;
FOUNDATION_EXPORT int const ARGTAG_TIPS_LBLOADINGMS;

//resources AR ERROR VIEW
FOUNDATION_EXPORT NSString *const ARG_ERROR_XIB_NAME;
FOUNDATION_EXPORT NSString *const ARG_ERROR_FULLIMAGE_NAME;
FOUNDATION_EXPORT NSString *const ARG_ERROR_ACTION_BUTTON_IMAGE;

//tags AR ERROR VIEW
FOUNDATION_EXPORT int const ARGTAG_ERROR_VIEW;
FOUNDATION_EXPORT int const ARGTAG_IMAGE_FULLIMAGE;
FOUNDATION_EXPORT int const ARGTAG_BUTTON_PERFORMACTION;
FOUNDATION_EXPORT int const ARGTAG_LABEL_ERROR_DESCRIPTION;
