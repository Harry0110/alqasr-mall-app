//
//  ARGenieExternalInitializer.h
//  ARGenieSDK
//
//  Created by ARTECH.
//  Copyright © 2018 ARTECH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ARGenieExternalInitializer : NSObject

+(void) initializeForXamarin:(int)argc argv:(NSString*)argv;

+(void) initialize:(int)argc argv:(char**)argv;



@end
