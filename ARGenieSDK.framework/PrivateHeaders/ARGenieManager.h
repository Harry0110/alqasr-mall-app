//
//  ARGenieManager.h
//  ARGenieSDK
//
//  Created by ARTECH on 11/9/16.
//  Copyright © 2016 ARTECH. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ARGenieManagerDownloadCallback <NSObject>

- (void) didARGenieManagerDownloadFinish:(NSError*) error;

@end

@protocol ARGenieManagerUpdateCallback <NSObject>

- (void) didARGenieManagerUpdateFinish:(NSError*) error;

@end

@protocol ARGenieManagerSendPDFCallback <NSObject>

- (void) didARGenieManagerSendPDFFinish:(NSNumber*) aResult error:(NSError*) error;

@end

@protocol ARGenieManagerRegisterDeviceForNotificationCallback <NSObject>

- (void) didARGenieManagerRegisterDeviceForNotificationFinish:(NSNumber*) aResult error:(NSError*) error;

@end

@interface ARGenieManager : NSObject

+ (ARGenieManager*) sharedManager;

+ (void) notifyAppStarted:(UIApplication *)application options:(NSDictionary *)launchOptions;

- (void) loadAugmentedRealityDataAndStart;
- (void) loadAugmentedRealityData:(id<ARGenieManagerDownloadCallback>) callback;


- (void) notifyDownloadAnimationError: (NSString*) aErrorMsg;

- (void) sendPDFWithMarkerToEmail:(NSString*) aEmail specificProduct:(int) aProductId callback:(id<ARGenieManagerSendPDFCallback>) aCallback;

- (UIViewController*) getARGenieSDKViewController;
- (UIViewController*) createAugmentedRealityViewController;

- (void) updateSDKData:(id<ARGenieManagerUpdateCallback>) callback;
- (BOOL) isARGenieVCShow;

- (void) requestAugmentedRealityStart;
- (void) requestAugmentedRealityStop;
- (void) stopAugmentedRealityViewController;

+ (NSString*) getVideoDemoPath;
+ (NSString *) getVideoDemoSubtitlePath;

+ (BOOL) isVideoRecordingInProgress;
+ (NSMutableArray*) getSupportedVideoModeList;
+ (NSString*) getSelectedVideoConfigurationMode;
+ (void) setSelectedVideoConfigurationMode:(NSString*)aVideoConfigMode;

+ (BOOL) isErrorViewIsShow;

- (void) initializeUserData: (NSString*) aUserData;
+ (void) playStandAloneAnimation: (NSString*) aAnimationTitle;

+ (UIViewController*) getARGGalleryControllerWithTopBar:(BOOL) aShowGalleryTopBar;

+ (NSString*) getAppleStoreUrl;

- (void) registerDeviceForNotifications:(NSString*) aDeviceIdentifier callback:(id<ARGenieManagerRegisterDeviceForNotificationCallback>) aCallback;

#pragma mark - method for get is ARGenieViewController can rotate

+ (BOOL) canARGenieViewRotate;

#pragma mark - method for get active orientation for ARGenieviewcontroller when recording video start

- (UIInterfaceOrientationMask) getActiveOrientationForVideoRecording;

- (void) downloadVuforiaDbFromMetaMarker:(NSMutableArray*) aVuforiaDbList completion:(void (^)(void))completionResult;

@end
