//
//  ARGTrackerFoundData.h
//  ARGenieSDK
//
//  Created by ARTECH on 3/24/17.
//  Copyright © 2017 ARTECH. All rights reserved.
//

#import <Foundation/Foundation.h>

/*! @brief This class contains information about a marker detected inside the Augmented Reality view
 */
@interface ARGTrackerFoundData : NSObject

// Marker name
@property (nonatomic, strong) NSString *trackerName;
// Number of times the marker has been detected in the device
@property (nonatomic, assign) int trackerTimesUsed;

- (id) initWithName:(NSString*) aName;

@end
